#ifndef MUTA_CHARACTER_H
#define MUTA_CHARACTER_H

#include "../../shared/types.h"
#include "../../shared/common_defs.h"

typedef struct character_props_t character_props_t;

struct character_props_t
{
    player_guid_t   id;
    uint64          account_id;
    int32           race;
    int32           sex;
    uint32          instance_id;
    uint32          map_id;
    world_pos_t     position;
    uint32          name_len;
    char            name[MAX_CHARACTER_NAME_LEN + 1];
};

int
charcache_init();

character_props_t *
charcache_claim_by_name(const char *name);

void
charcache_unclaim(character_props_t *cp);

character_props_t *
charcache_store(character_props_t *cp);

#endif /* MUTA_CHARACTER_H */

