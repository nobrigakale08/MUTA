#ifndef MUTA_PLAYER_MSGS_H
#define MUTA_PLAYER_MSGS_H

/* world_player_msgs: message handlers for players currently logged on to a
 * character. */

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct client_t     client_t;
typedef struct byte_buf_t   bbuf_t;

enum wpm_actions
{
    WPM_ACTION_OK = 0,
    WPM_ACTION_INCOMPLETE,
    WPM_ACTION_DISCONNECT,
    WPM_ACTION_UNDEFINED_MSG
};

int
wpm_handle_player_msg(client_t *cl, int msg_type, bbuf_t *bb);
/* Return value is one of wpm_actions. */

#endif /* MUTA_PLAYER_MSGS_H */
