#ifndef MUTA_SERVER_H
#define MUTA_SERVER_H

#include "../../shared/common_utils.h"
#include "../../shared/net.h"
#include "../../shared/common_defs.h"

/* Forward declaration(s) */
typedef struct w_event_spawn_player_t       w_event_spawn_player_t;
typedef struct w_event_despawn_player_t     w_event_despawn_player_t;
typedef struct w_event_spawn_player_fail_t  w_event_spawn_player_fail_t;

/* Types defined here */
typedef struct sv_config_t sv_config_t;

enum conn_status_t
{
    CONN_DISCONNECTED,
    CONN_CONNECTING,
    CONN_WAIT_FOR_INIT_DATA,
    CONN_CONNECTED
};

struct sv_config_t
{
    uint64  authed_client_timeout;   /* Milliseconds */
    uint64  unauthed_client_timeout;
    uint64  client_timeout_check_freq;
    uint32  max_unah_clients;
    uint32  max_players;
    uint32  max_pf_req_freq;     /* For pathfinding request timers */
    uint32  tickrate;
    uint16  world_port;
    addr_t  db_addr;
    addr_t  login_addr;
    bool32  use_proxy;          /* Do client connections go through a proxy? */
    uint16  proxy_port;
    char    shard_name[MAX_SHARD_NAME_LEN + 1];
};

extern sv_config_t  sv_config;
extern mutex_t      txt_log_mutex;
extern txt_log_t    txt_log;
extern bool32       txt_log_need_refresh;
extern int          w_conn_status;
extern uint64       sv_program_ticks_ms;

int
sv_init(const char *cfg_file);
/* If cfg_file is 0, the default path is used */

int
sv_run();

bool32
sv_update();
/* Returns non-zero if application should exit */

void
sv_stop();

/* World event handlers. A return value of 0 indicates messages should be sent
 * forward. */
int
sv_handle_w_event_spawn_player(w_event_spawn_player_t *ev);

int
sv_handle_w_event_spawn_player_fail(w_event_spawn_player_fail_t *ev);

int
sv_handle_w_event_despawn_player(w_event_despawn_player_t *ev);

int
sv_new_client(uint32 proxy_socket_index, void **ret_ptr);
/* Called from proxy.c when a new client successfully auths. ret_ptr is a
 * pointer to the client structure. */

void
sv_read_client_packet(void *client, const void *msg, int num_bytes);

#endif /* MUTA_SERVER_H */
