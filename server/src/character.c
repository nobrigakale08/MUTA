#include <inttypes.h>
#include "character.h"
#include "../../shared/common_utils.h"

static struct
{
    obj_pool_t          char_prop_pool;
    character_props_t   **cached_props;
} _cache;

int
charcache_init()
{
    const uint32 num = 2048;
    obj_pool_init(&_cache.char_prop_pool, num, sizeof(character_props_t));
    darr_reserve(_cache.cached_props, num);
    return 0;
}

character_props_t *
charcache_claim_by_name(const char *name)
{
    uint32 num_props = darr_num(&_cache.cached_props);
    for (uint32 i = 0; i < num_props; ++i)
    {
        if (!streq(_cache.cached_props[i]->name, name))
            continue;
        return _cache.cached_props[i];
    }
    return 0;
}

void
charcache_unclaim(character_props_t *cp)
{
}

character_props_t *
charcache_store(character_props_t *cp)
{
    character_props_t *rcp = obj_pool_reserve(&_cache.char_prop_pool);
    DEBUG_PRINTFF("stored %p\n", rcp);
    *rcp = *cp;
    darr_push(_cache.cached_props, rcp);
    return rcp;
}
