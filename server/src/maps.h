#ifndef MUTA_SERVER_MAP_H
#define MUTA_SERVER_MAP_H

#include "../../shared/types.h"

typedef struct map_data_t map_data_t;

struct map_data_t
{
    uint32  id;
    dchar   *name;
    dchar   *spawn_file_path;
    uint32  tiled_width;
    uint32  tiled_height;
};

int
maps_init();

void
maps_destroy();

map_data_t *
maps_get_by_name(const char *map_name);

map_data_t *
maps_get_by_id(uint32 map_id);

#endif /* MUTA_SERVER_MAP_H */
