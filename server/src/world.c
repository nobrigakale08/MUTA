#include <inttypes.h>
#include "world.h"
#include "server.h"
#include "db.h"
#include "instance.h"
#include "character.h"
#include "client.h"
#include "maps.h"
#include "../../shared/netqueue.h"
#include "../../shared/crypt.h"
#include "../../shared/sv_time.h"
#include "../../shared/rwbits.inl"
#include "../../shared/world_packets.h"
#include "../../shared/world_common.inl"
#include "../../shared/packets.h"
#include "../../shared/containers.h"
#include "../../shared/emote.h"

#define W_MAX_INSTANCES             32
#define W_CLIENT_BUF_SZ             256
#define W_DAEMON_NI_BUF_SZ          16384
#define W_DAEMON_OUT_BUF_SZ         MUTA_MTU * 2
#define W_MAIN_MAP_INDEX            0
#define W_GLOBAL_TO_LOCAL_X(ip, x)  (x - (int)((ip)->cx * MAP_CHUNK_W))
#define W_GLOBAL_TO_LOCAL_Y(ip, y)  (y - (int)((ip)->cy * MAP_CHUNK_W))
#define W_LOCAL_TO_GLOBAL_X(ip, x)  (x + (int)((ip)->cx * MAP_CHUNK_W))
#define W_LOCAL_TO_GLOBAL_Y(ip, y)  (y + (int)((ip)->cy * MAP_CHUNK_W))
#define W_PANIC()                   muta_panic(-1, __func__)

enum players_flags
{
    PLAYER_SPAWN_CONFIRMED  = (1 << 0), /* Did a worldd spawn the entity yet? */
    PLAYER_SEX_FEMALE       = (1 << 1)
};

enum w_timer_types
{
    W_SPAWN_REQ_DYNAMIC_OBJ /* Asked a daemon to spawn */
};

typedef struct w_timer_t        w_timer_t;
typedef w_timer_t               w_timer_darr_t;
typedef struct w_client_t       w_client_t;
typedef w_client_t *            w_client_ptr_darr_t;
typedef struct accept_cmd_t     accept_cmd_t;
typedef struct daemon_t         daemon_t;
typedef struct player_t         player_t;
typedef player_t*               player_ptr_darr_t;
typedef struct dynamic_obj_t    dynamic_obj_t;
typedef struct creature_t       creature_t;

struct w_timer_t
{
    int     type;
    uint32  time_left;
};

struct w_client_t
{
    net_handle_t    nh;
    cryptchan_t     cc;
    net_queue_t     *nq;
    daemon_t        *daemon;
    uint64          recv_time;
    bbuf_t          msgs_in;
    uint8           msgs_in_mem[W_CLIENT_BUF_SZ];
};

struct accept_cmd_t
{
    socket_t    fd;
    net_queue_t *nq;
};

struct daemon_t
{
    daemon_t            *next;
    w_client_t          *c; /* null if not connected */
    instance_part_t     *inst_parts;
    dynamic_bbuf_t      msgs_in;
    dynamic_bbuf_t      msgs_out;
    w_timer_darr_t      *timers;
};
/* Represents a single worldd process' state */

struct player_t
{
    instance_part_t *inst_part;
    world_pos_t     pos;
    uint8           dir;
    player_t        *next;
    player_guid_t   id;
    w_client_data_t cd;
    interest_area_t *ia;
    uint8           flags;

    /* Name may be different from the real character name due
     * to effects etc. */
    char            name[MAX_CHARACTER_NAME_LEN + 1];
    uint8           name_len;
};

struct dynamic_obj_t
{
    dobj_guid_t     id;
    dobj_type_id_t  type_id;
    world_pos_t     pos;
    interest_area_t *ia;
    instance_part_t *inst_part;
    dynamic_obj_t   *next;
    uint8           dir;
    bool8           confirmed_by_daemon;
};

struct creature_t
{
    creature_guid_t id;
    world_pos_t     pos;
    uint32          type_id;
    interest_area_t *ia;
    instance_part_t *inst_part;
    creature_t      *next;
    uint8           dir;
    uint8           creature_flags;
    struct
    {
        uint confirmed_by_daemon:1;
    } flags;

};

STATIC_OBJ_POOL_DEFINITION(daemon_t, daemon_pool);
STATIC_OBJ_POOL_DEFINITION(player_t, player_pool);
DYNAMIC_OBJ_POOL_DEFINITION(dynamic_obj_t, dynamic_obj_pool, 32);
DYNAMIC_OBJ_POOL_DEFINITION(creature_t, creature_pool, 32);
DYNAMIC_HASH_TABLE_DEFINITION(guid_player_table, player_t *, player_guid_t,
    player_guid_t, HASH_FROM_NUM, 4);
DYNAMIC_HASH_TABLE_DEFINITION(guid_dynamic_obj_table, dynamic_obj_t *,
    dobj_guid_t, dobj_guid_t, HASH_FROM_NUM, 4);
DYNAMIC_HASH_TABLE_DEFINITION(guid_creature_table, creature_t *,
    creature_guid_t, creature_guid_t, HASH_FROM_NUM, 4);

static struct
{
    obj_pool_t          pool;
    w_client_ptr_darr_t *res;
    double_cmd_buf_t    read_cmds;      /* Contains w_client_t pointers */
} _w_clients;

static struct
{
    daemon_pool_t   pool;
    daemon_t        *items;
    daemon_t        *res;
} _w_daemons;

static struct
{
    player_pool_t       pool;
    guid_player_table_t guid_table;
} _w_players;

static struct
{
    dobj_guid_t                 running_id;
    dynamic_obj_pool_t          pool;
    guid_dynamic_obj_table_t    guid_table;
} _w_dynamic_objs;

static struct
{
    creature_guid_t         running_id;
    creature_pool_t         pool;
    guid_creature_table_t   guid_table;
} _w_creatures;

static bool32               _w_initialized;
static uint                 _w_timeout_check_freq   = 5000;
static uint                 _w_timeout              = 5000;
static uint64               _w_last_timeout_cleanup;
static double_cmd_buf_t     _w_accept_cmds; /* Contains accept_cmd_ts */
static instance_t           _w_instances[W_MAX_INSTANCES];
static uint32               _w_num_instances;
static const char           *_shared_instances_path = "shared_instances.txt";

static int
_w_daemon_add_inst_part(daemon_t *d, instance_part_t *ip);

static int
_w_daemon_remove_map_part(daemon_t *d, instance_part_t *ip);

static bbuf_t *
_w_daemon_send_msg(daemon_t *d, int sz);

static inline bbuf_t *
_w_daemon_send_msg_const_encrypted(daemon_t *d, int sz);

static inline bbuf_t *
_w_daemon_send_msg_var_encrypted(daemon_t *d, int sz);

static int
_w_daemon_send_despawn_player_msg(daemon_t *d, player_guid_t id);

static int
_w_daemon_send_despawn_dynamic_obj_msg(daemon_t *d, dobj_guid_t id);

static int
_w_set_ia_obj_pos(void *obj, uint8 type, instance_t *inst,
    instance_part_t **ret_ip, interest_area_t **ret_ia, int32 x, int32 y,
    int32 z);

static void
_w_remove_ia_obj_from_interest_area(interest_area_t **ret_ia, void *ptr);

static inline uint8
_w_player_get_sex(player_t *p);

static inline bbuf_t
_w_player_send(player_t *p, int size);

static inline bbuf_t
_w_player_send_const_encrypted(player_t *p, int size);

static inline bbuf_t
_w_player_send_var_encrypted(player_t *p, int size);

static inline player_t *
_w_get_player_by_id(player_guid_t id);

static inline dynamic_obj_t *
_w_get_dynamic_obj_by_id(dobj_guid_t id);

static inline creature_t *
_w_get_creature_by_id(creature_guid_t id);

static instance_t *
_w_get_instance_by_id(instance_guid_t id);

static instance_t *
_w_get_shared_instance_by_map_id(uint32 map_id);

static int
_w_init_shared_instances();

static int
_w_init_clients(uint32 num);

static void
_w_destroy_clients();

static w_client_t *
_w_reserve_client();

static void
_w_free_client(w_client_t *c);

static void
_w_disconnect_and_free_client(w_client_t *c);

static daemon_t *
_w_create_new_daemon(w_client_t *c);

static void
_w_daemon_destroy_allocs(daemon_t *w);

static int
_w_player_set_pos(player_t *p, instance_t *inst, int32 x, int32 y, int8 z);

static inline int
_w_player_set_pos_from_local(player_t *p, instance_t *m, int32 x, int32 y,
    int8 z);

static bool32
_w_player_set_dir(player_t *p, int dir);
/* Returns true if direction changed */

static bool32
_w_creature_set_dir(creature_t *c, int dir);
/* Returns true if direction changed */

static int
_w_player_send_initial_world_data(player_t *p);

static int
_w_dynamic_obj_set_pos(dynamic_obj_t *obj, instance_t *inst, int32 x, int32 y,
    int8 z);

static int
_w_creature_set_pos(creature_t *cr, instance_t *inst, int32 x, int32 y, int8 z);

static int
_w_broadcast_player_walk(instance_t *m, player_t *p, wpos_t old_pos);

static int
_w_broadcast_creature_walk(instance_t *inst, creature_t *c, wpos_t old_pos);

static void
_w_remove_player_from_interest_area(player_t *p, interest_area_t *ia);

static void
_w_remove_player_inner(player_t *p);

static void
_w_process_accepts();

static void
_w_check_timeouts();

static void
_w_read_clients();

static void
_w_send_msgs();

static int
_w_read_daemon_client(w_client_t *c);

static int
_w_read_new_client(w_client_t *c);

static int
_w_read_new_packet(w_client_t *c, uint8 *pkt, int pkt_len);

static int
_w_read_daemon_packet(w_client_t *c, uint8 *pkt, int pkt_len);

static int
_w_broadcast_player_dir(player_t *p, uint8 dir);

static int
_w_spawn_dynamic_obj(instance_guid_t inst, dobj_type_id_t type_id,
    world_pos_t pos, int dir);

static void
_w_despawn_dynamic_obj(dobj_guid_t id);

static int
_w_handle_new_wmsg_pub_key_read(w_client_t *c, wmsg_pub_key_t *s);

static int
_w_handle_new_wmsg_stream_header_read(w_client_t *c, wmsg_stream_header_t *s);

static int
_w_handle_wsmsg_confirm_spawn_player(w_client_t *c,
    wsmsg_confirm_spawn_player_t *s);

static int
_w_handle_wsmsg_walk_creature(w_client_t *c, wsmsg_walk_creature_t *s);

static int
_w_handle_wsmsg_walk_player(w_client_t *c, wsmsg_walk_player_t *s);

static int
_w_handle_wsmsg_request_spawn_creature(w_client_t *c,
    wsmsg_request_spawn_creature_t *s);

static int
_w_handle_wsmsg_spawn_player_fail(w_client_t *c, wsmsg_spawn_player_fail_t *s);

static int
_w_handle_wsmsg_load_instance_part_result(w_client_t *c,
    wsmsg_load_instance_part_result_t *s);

static int
_w_handle_wsmsg_player_dir(w_client_t *c, wsmsg_player_dir_t *s);

static int
_w_handle_wsmsg_confirm_spawn_dynamic_obj(w_client_t *c,
    wsmsg_confirm_spawn_dynamic_obj_t *s);

static int
_w_handle_wsmsg_teleport_player(w_client_t *c,
    wsmsg_teleport_player_t *s);

static int
_w_handle_wsmsg_confirm_spawn_creature(w_client_t *c,
    wsmsg_confirm_spawn_creature_t *s);

static int
_w_handle_wsmsg_creature_dir(w_client_t *c, wsmsg_creature_dir_t *s);

static void
_w_fill_svmsg_new_player_character(svmsg_new_player_character_t *s,
    player_t *p);

static void
_w_fill_svmsg_new_dynamic_obj(svmsg_new_dynamic_obj_t *s, dynamic_obj_t *obj);

static void
_w_fill_svmsg_new_creature(svmsg_new_creature_t *s, creature_t *cr);

/* Returns the remaining number of interest areas in ias_a */
static uint
_w_purge_non_unique_interest_areas(interest_area_t **ias_a, uint num_ias_a,
    interest_area_t **ias_b, uint num_ias_b);

static uint
_w_get_common_interest_areas_in_arrays(interest_area_t **ias_a, uint num_ias_a,
    interest_area_t **ias_b, uint num_ias_b, interest_area_t **ret_ias);

static int
_w_send_all_msgs_on_player_entered_new_interest_areas(player_t *p,
    interest_area_t **ias, uint num_ias);

#define W_BROADCAST_TO_NEARBY_PLAYERS(inst_part, excl_player, px, py, pz, \
    send_func, write_func, write_arg, sz) \
interest_area_t *ias[27]; \
uint num_ias = instance_get_relevant_interest_areas((inst_part)->inst, \
    px, py, pz, ias); \
uint    i, j, num_ps; \
bbuf_t  bb; \
for (i = 0; i < num_ias; ++i) \
{ \
    player_t **ps = ias[i]->players; \
    num_ps = darr_num(ias[i]->players); \
    for (j = 0; j < num_ps; ++j) \
    { \
        if (ps[j] == excl_player) \
            continue; \
        bb = send_func(ps[j], (sz)); \
        if (!bb.max_bytes) \
            {DEBUG_PRINTFF("need a handler!\n"); continue;} \
        write_func(&bb, (write_arg)); \
    } \
}

#define W_SEND_MSG_TO_PLAYERS_IN_IAS(ias, num_ias, send_func, msg_sz, \
    msg_struct, msg_func, excl_player) \
for (uint32 i = 0; i < (num_ias); ++i) \
{ \
    player_t    **ps    = (ias)[i]->players; \
    uint32      num_ps  = darr_num((ias)[i]->players); \
    bbuf_t      bb; \
    player_t    *pl; \
    for (uint j = 0; j < num_ps; ++j) \
    { \
        pl = ps[j]; \
        if (pl == (excl_player)) \
            continue; \
        bb = send_func(pl, (msg_sz)); \
        if (!bb.max_bytes) \
            continue; \
        msg_func(&bb, (msg_struct)); \
    } \
}

int
w_init(w_config_t *cfg)
{
    int ret = 0;

    if (cfg->num_h_daemons < 1 || cfg->num_v_daemons < 1)
    {
        puts("World: not enough daemons specified. Please specify num v "
            "daemons and num h daemons in config.");
        ret = 1;
        goto out;
    }

    if (wc_load_dynamic_obj_defs("muta-data/common/dynamic_objs.def"))
    {
        puts("Error: failed to load dynamic object definitions.");
        ret = 2;
        goto out;
    }

    if (wc_load_creature_defs("muta-data/common/creatures.def"))
    {
        puts("Error: failed to load dynamic object definitions.");
        ret = 2;
        goto out;
    }

    if (_w_init_clients(64))
        {ret = 3; goto out;}

    /* Initialize accept command buffer */
    if (double_cmd_buf_init(&_w_accept_cmds, 64, sizeof(accept_cmd_t)))
        {ret = 4; goto out;}

    /* Initialize daemons */
    uint num_daemons = cfg->num_v_daemons + cfg->num_h_daemons + \
        cfg->num_extra_daemons;

    _w_daemons.items = calloc(num_daemons, sizeof(daemon_t));
    if (!_w_daemons.items)
        {ret = 5; goto out;}
    daemon_pool_init(&_w_daemons.pool, _w_daemons.items, num_daemons);

    for (int i = 0; i < _w_daemons.pool.max; ++i)
    {
        daemon_t *d = &_w_daemons.pool.items[i];
        if (dynamic_bbuf_init(&d->msgs_in, W_DAEMON_NI_BUF_SZ)
        ||  dynamic_bbuf_init(&d->msgs_out, W_DAEMON_OUT_BUF_SZ))
            {ret = 6; goto out;}
        darr_reserve(d->timers, 256);
    }

    /* Initialize players */
    player_t *players = calloc(cfg->max_players, sizeof(player_t));
    if (!players)
        {ret = 10; goto out;}
    player_pool_init(&_w_players.pool, players, cfg->max_players);
    guid_player_table_einit(&_w_players.guid_table, cfg->max_players);

    /* Initialize dynamic objects. TODO: change hardcoded init value */
    dynamic_obj_pool_einit(&_w_dynamic_objs.pool, 2048, 16);
    guid_dynamic_obj_table_einit(&_w_dynamic_objs.guid_table, 2048);

    /* Initialize creatures */
    creature_pool_einit(&_w_creatures.pool, 4096, 16);
    guid_creature_table_einit(&_w_creatures.guid_table, 4096);

    /* Initialize maps and shared instances */
    if (maps_init())
        {ret = 7; goto out;}
    if (_w_init_shared_instances())
        {ret = 8; goto out;}

    out:
        if (ret)
        {
            printf("%s failed with code %d.\n", __func__, ret);
            w_destroy();
        } else
            _w_initialized = 1;
        return ret;
}

void
w_destroy()
{
    _w_destroy_clients();

    /* Destroy accept command buffer */
    double_cmd_buf_destroy(&_w_accept_cmds);

    /* Destroy daemons */
    for (int i = 0; i < _w_daemons.pool.max; ++i)
        _w_daemon_destroy_allocs(&_w_daemons.items[i]);
    free(_w_daemons.items);
    memset(&_w_daemons, 0, sizeof(_w_daemons));

    /* Destroy maps */
    for (uint32 i = 0; i < _w_num_instances; ++i)
        instance_destroy(&_w_instances[i]);

    /* Destroy players */
    free(_w_players.pool.items);
    guid_player_table_destroy(&_w_players.guid_table);
    memset(&_w_players, 0, sizeof(_w_players));

    /* Destroy dynamic objects */
    dynamic_obj_pool_destroy(&_w_dynamic_objs.pool);
    guid_dynamic_obj_table_destroy(&_w_dynamic_objs.guid_table);

    /* Destroy creatures */
    creature_pool_destroy(&_w_creatures.pool);
    guid_creature_table_destroy(&_w_creatures.guid_table);

    maps_destroy();

    wc_destroy_dynamic_obj_defs();

    _w_initialized = 0;
}

void
w_on_accept(net_queue_t *nq, socket_t fd)
{
    DEBUG_PUTS("World: accepting new connection");
    if (make_socket_non_block(fd) || net_disable_nagle(fd))
        {net_shutdown_sock(fd, SOCKSD_BOTH); return;}

    accept_cmd_t cmd;
    cmd.fd = fd;
    cmd.nq = nq;
    if (double_cmd_buf_push_static(&_w_accept_cmds, &cmd))
    {
        printf("%s: accept cmd queue full!", __func__);
        net_shutdown_sock(fd, SOCKSD_BOTH);
    }
}

void
w_on_read(net_handle_t *h, int num_bytes)
{
    w_client_t *c = h->user_data.ptr;

    if (num_bytes <= 0)
    {
        printf("%s: num bytes %d! IMPLEMENTME\n", __func__, num_bytes);
        return;
    }

    if (c->daemon)
        c->daemon->msgs_in.num_bytes += num_bytes;
    else
        c->msgs_in.num_bytes += num_bytes;

    int r = double_cmd_buf_push_static(&_w_clients.read_cmds, &c);
    muta_assert(!r);
}

void
w_update()
{
    _w_process_accepts();
    _w_check_timeouts();
    _w_read_clients();
    _w_send_msgs();
}

#if 0
int
w_spawn_player(player_guid_t id, inst_guid_t inst_id, pc_props_t *pp,
    w_client_data_t cd)
{
    instance_t *inst = _w_get_instance_by_id(inst_id);
    if (!inst)
        return W_ERR_INSTANCE_NOT_FOUND;

    instance_part_t *ip = instance_get_part_by_pos(inst , pp->x, pp->y);
    if (!instance_part_ok(ip))
        return W_ERR_DAEMON_NOT_CONNECTED;

    if (guid_player_table_exists(&_w_players.guid_table, id))
        return W_ERR_PLAYER_ALREADY_LOGGED;

    player_t *p = player_pool_reserve(&_w_players.pool);
    if (!p) return W_ERR_TOO_MANY_PLAYERS;

    if (guid_player_table_insert(&_w_players.guid_table, id, p))
    {
        player_pool_free(&_w_players.pool, p);
        return W_ERR_TOO_MANY_PLAYERS;
    }

    p->id           = id;
    p->ia           = 0;
    p->flags        = 0;
    p->inst_part    = ip;
    p->cd           = cd;
    p->name_len     = (uint8)strlen(pp->name);
    memcpy(p->name, pp->name, p->name_len + 1);

    /* Send message to daemon */
    swmsg_spawn_player_t s;
    s.id            = id;
    s.inst_part_id  = ip->runtime_id;
    s.x             = W_GLOBAL_TO_LOCAL_X(ip, pp->x);
    s.y             = W_GLOBAL_TO_LOCAL_Y(ip, pp->y);
    s.z             = pp->z;
    s.dir           = 0;

    bbuf_t *bb = _w_daemon_send_msg(ip->daemon, SWMSG_SPAWN_PLAYER_SZ);

    if (!bb)
    {
        player_pool_free(&_w_players.pool, p);
        guid_player_table_erase(&_w_players.guid_table, id);
        return W_ERR_DAEMON_NOT_CONNECTED;
    }

    swmsg_spawn_player_write(bb, &s);
    return 0;
}
#endif

int
w_spawn_player(uint64 account_id, w_client_data_t cd, character_props_t *cp)
{
    if (cp->account_id != account_id)
    {
        DEBUG_PRINTFF("%" PRIu64 ", %" PRIu64 "\n", cp->account_id, account_id);
        return W_ERR_WRONG_ACCOUNT_ID;
    }

    if (guid_player_table_exists(&_w_players.guid_table, cp->id))
        return W_ERR_PLAYER_ALREADY_LOGGED;

    /* Attempt to find the instance of the character */
    instance_t *inst = _w_get_instance_by_id(cp->instance_id);

    /* Check if the map id of the instance is the same. If not, this is a
     * different map using the same runtime id (the user logged in after a
     * server restart. In this case, if its a shared instance, attempt to spawn
     * the player in the current version of said map. */
    if (!inst || inst->map_data->id != cp->map_id)
        inst = _w_get_shared_instance_by_map_id(cp->map_id);

    if (!inst)
    {
        DEBUG_PRINTFF("no instance found for map id %u.\n", cp->map_id);
        return W_ERR_INSTANCE_NOT_FOUND;
    }

    instance_part_t *ip = instance_get_part_by_pos(inst, cp->position.x,
        cp->position.y);
    if (!instance_part_ok(ip))
        return W_ERR_DAEMON_NOT_CONNECTED;

    /* Create the character and send a message to a worldd to do the same.
     * Message of the spawn is not sent to any players until a confirmation from
     * the worldd arrives. */

    player_t *p = player_pool_reserve(&_w_players.pool);
    if (!p)
        return W_ERR_TOO_MANY_PLAYERS;

    if (guid_player_table_insert(&_w_players.guid_table, cp->id, p))
    {
        player_pool_free(&_w_players.pool, p);
        return W_ERR_TOO_MANY_PLAYERS;
    }

    p->id           = cp->id;
    p->ia           = 0;
    p->flags        = 0;
    p->inst_part    = ip;
    p->cd           = cd;
    p->name_len     = (uint8)strlen(cp->name);
    memcpy(p->name, cp->name, p->name_len + 1);

    /* Send message to daemon */
    swmsg_spawn_player_t s;
    s.id            = cp->id;
    s.inst_part_id  = ip->runtime_id;
    s.x             = W_GLOBAL_TO_LOCAL_X(ip, cp->position.x);
    s.y             = W_GLOBAL_TO_LOCAL_Y(ip, cp->position.y);
    s.z             = cp->position.z;
    s.dir           = 0;

    bbuf_t *bb = _w_daemon_send_msg(ip->daemon, SWMSG_SPAWN_PLAYER_SZ);

    if (!bb)
    {
        player_pool_free(&_w_players.pool, p);
        guid_player_table_erase(&_w_players.guid_table, cp->id);
        return W_ERR_DAEMON_NOT_CONNECTED;
    }

    swmsg_spawn_player_write(bb, &s);
    return 0;
}

int
w_despawn_player(player_guid_t id)
{
    player_t *p = _w_get_player_by_id(id);
    if (!p)
    {
        printf("World: attempted to despawn unexistent player %llu!\n",
            (lluint)id);
        return 0;
    }

    world_pos_t pos = p->pos;

    /* Send despawn message to others in the area */
    interest_area_t *ias[27];
    uint num_ias = instance_get_relevant_interest_areas(p->inst_part->inst,
        p->pos.x, p->pos.y, p->pos.z, ias);

    svmsg_remove_player_character_t s;
    s.id = id;

    uint    i, j;
    bbuf_t  bb;

    for (i = 0; i < num_ias; ++i)
    {
        player_t    **ps    = ias[i]->players;
        uint32      num_ps  = darr_num(ias[i]->players);
        for (j = 0; j < num_ps; ++j)
        {
            bb = _w_player_send(ps[j], SVMSG_REMOVE_PLAYER_CHARACTER_SZ);
            if (!bb.max_bytes)
            {
                DEBUG_PRINTFF("need a handler!\n");
                continue;
            }
            svmsg_remove_player_character_write(&bb, &s);
        }
    }

    daemon_t *d = p->inst_part->daemon;

    if (d)
    {
        swmsg_despawn_player_t s;
        s.id = id;

        bbuf_t *bb = _w_daemon_send_msg(d, SWMSG_DESPAWN_PLAYER_SZ);
        if (bb)
            swmsg_despawn_player_write(bb, &s);
        else
            printf("%s: error sending message.", __func__);

        w_event_despawn_player_t ev;
        ev.char_id = p->id;
        sv_handle_w_event_despawn_player(&ev);
    }

    _w_remove_player_inner(p);
    db_update_char_pos(id, pos.x, pos.y, pos.z);
    return 0;
}

int
w_walk_player(player_guid_t id, int dir)
{
    player_t *p = _w_get_player_by_id(id);
    if (!p) return 1;

    swmsg_move_player_t s;
    s.id    = p->id;
    s.dir   = CLAMP(dir, 0, NUM_ISODIRS - 1);

    daemon_t *d = p->inst_part->daemon;
    muta_assert(d);
    bbuf_t *bb = _w_daemon_send_msg(d, SWMSG_MOVE_PLAYER_SZ);
    if (!bb) return 2;
    swmsg_move_player_write(bb, &s);
    return 0;
}

int
w_find_path_player(player_guid_t id, int32 x, int32 y, int8 z)
{
    player_t *p = _w_get_player_by_id(id);
    if (!p) return 1;
    if (p->pos.x == x && p->pos.y == y && p->pos.z == z) return 0;

    swmsg_find_path_player_t s;
    s.id = id;
    s.x = x;
    s.y = y;
    s.z = z;

    bbuf_t *bb = _w_daemon_send_msg(p->inst_part->daemon,
        SWMSG_FIND_PATH_PLAYER_SZ);
    if (!bb) W_PANIC();
    swmsg_find_path_player_write(bb, &s);
    puts("World: requested pathfinding from a daemon.");
    return 0;
}

const char *
w_err_str(int err)
{
    const char *r;
    switch (err)
    {
        case W_ERR_OK:                      r = "no error";             break;
        case W_ERR_DAEMON_NOT_CONNECTED:    r = "daemon not connected"; break;
        case W_ERR_PLAYER_ALREADY_LOGGED:   r = "player already in";    break;
        case W_ERR_TOO_MANY_PLAYERS:        r = "too many players";     break;
        default: r = "unknown error";
    }
    return r;
}

int
w_player_player_emote(player_guid_t id, bool32 have_target,
    player_guid_t target_id, emote_id_t emote_id)
{
    if (!emote_is_id_valid(emote_id))   return 1;
    player_t *p = _w_get_player_by_id(id);
    if (!p) return 2;

    svmsg_player_player_emote_t s;
    s.player_id     = id;
    s.emote_id      = emote_id;

    if (have_target)
    {
        player_t *tar = _w_get_player_by_id(target_id);
        if (!tar) return 0;
        s.target_id     = target_id;
        s.have_target   = 1;
    } else
        s.have_target = 0;

    {
    W_BROADCAST_TO_NEARBY_PLAYERS(p->inst_part, 0, p->pos.x, p->pos.y,
        p->pos.z, _w_player_send, svmsg_player_player_emote_write, &s,
        SVMSG_PLAYER_PLAYER_EMOTE_SZ);
    }
    return 0;
}

int
w_teleport_player(player_guid_t id, int32 x, int32 y, int8 z)
{
    player_t *p = _w_get_player_by_id(id);
    if (!p) return 1;
    swmsg_teleport_player_t s;
    s.id    = id;
    s.x     = x;
    s.y     = y;
    s.z     = z;
    bbuf_t *bb = _w_daemon_send_msg(p->inst_part->daemon,
        SWMSG_TELEPORT_PLAYER_SZ);
    if (!bb) return 1;
    swmsg_teleport_player_write(bb, &s);
    return 0;
}

int
w_spawn_creature(instance_guid_t inst_id, creature_type_id_t type_id,
    world_pos_t pos, int dir, uint8 flags)
{
    instance_t *inst = _w_get_instance_by_id(inst_id);
    if (!inst)
        return W_ERR_INSTANCE_NOT_FOUND;

    instance_part_t *ip = instance_get_part_by_pos(inst , pos.x, pos.y);
    if (!ip->daemon)
        return W_ERR_DAEMON_NOT_CONNECTED;

    creature_t *c = creature_pool_reserve(&_w_creatures.pool);
    if (!c) return W_ERR_NO_MEM;

    /* Generate a guid */
    creature_guid_t id = _w_dynamic_objs.running_id++;

    if (guid_creature_table_insert(&_w_creatures.guid_table, id, c))
    {
        creature_pool_free(&_w_creatures.pool, c);
        return W_ERR_NO_MEM;
    }

    c->id                           = id;
    c->type_id                      = type_id;
    c->ia                           = 0;
    c->inst_part                    = ip;
    c->flags.confirmed_by_daemon    = 0;
    c->creature_flags               = flags;

    /* Send message to daemon */
    swmsg_spawn_creature_t s;
    s.id            = id;
    s.type_id       = type_id;
    s.inst_part_id  = ip->runtime_id;
    s.x             = W_GLOBAL_TO_LOCAL_X(ip, pos.x);
    s.y             = W_GLOBAL_TO_LOCAL_Y(ip, pos.y);
    s.z             = pos.z;
    s.dir           = (uint8)dir;
    s.flags         = flags;

    bbuf_t *bb = _w_daemon_send_msg(ip->daemon, SWMSG_SPAWN_CREATURE_SZ);
    if (!bb)
    {
        creature_pool_free(&_w_creatures.pool, c);
        guid_creature_table_erase(&_w_creatures.guid_table, id);
        return W_ERR_DAEMON_NOT_CONNECTED;
    }
    swmsg_spawn_creature_write(bb, &s);
    DEBUG_PRINTFF("successful.\n");
    return 0;
}

uint32
w_get_default_shared_instance_id()
    {return 0;}

void
w_tmp_broadcast_chat_msg(player_guid_t talker, const char *msg, int msg_len)
{
    player_t *p = _w_get_player_by_id(talker);
    if (!p)
        return;

    svmsg_global_chat_broadcast_t s;
    s.name     = p->name;
    s.name_len = p->name_len;
    s.msg      = msg;
    s.msg_len  = msg_len;

    {
    W_BROADCAST_TO_NEARBY_PLAYERS(p->inst_part, 0, p->pos.x, p->pos.y,
        p->pos.z, _w_player_send, svmsg_global_chat_broadcast_write, &s,
        SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(s.name_len, s.msg_len));
    }
}

void
w_reload_entity_script(const char *script_name)
{
    swmsg_reload_entity_script_t s;
    s.script_name       = script_name;
    s.script_name_len   = (uint8)strlen(script_name);
    for (daemon_t *d = _w_daemons.res; d; d = d->next)
    {
        bbuf_t *bb = _w_daemon_send_msg(d, SWMSG_RELOAD_ENTITY_SCRIPT_SZ);
        swmsg_reload_entity_script_write(bb, &s);
    }
}

static int
_w_daemon_add_inst_part(daemon_t *d, instance_part_t *ip)
{
    puts("World: adding an instance part to daemon.");

    muta_assert(!ip->daemon);
    ip->confirmed   = 0;
    ip->daemon      = d;
    ip->next        = d->inst_parts;
    d->inst_parts   = ip;

    swmsg_load_instance_part_t s;
    s.map_id        = ip->inst->map_data->id;
    s.part_id       = ip->runtime_id;
    s.cx            = ip->cx;
    s.cy            = ip->cy;
    s.cw            = ip->cw;
    s.ch            = ip->ch;
    s.num_entities  = 1024; /* tmp */

    bbuf_t *bb = _w_daemon_send_msg_const_encrypted(d,
        SWMSG_LOAD_INSTANCE_PART_SZ);
    if (!bb)
        W_PANIC();
    muta_assert(cryptchan_is_encrypted(&d->c->cc));
    swmsg_load_instance_part_write_const_encrypted(bb, &d->c->cc, &s);
    return 0;
}

static int
_w_daemon_remove_map_part(daemon_t *d, instance_part_t *ip)
{
    for (instance_part_t **ipp = &d->inst_parts;
         *ipp;
         ipp = &(*ipp)->next)
    {
        if (*ipp != ip)
            continue;
        swmsg_remove_instance_part_t s;
        s.part_id = ip->runtime_id;
        bbuf_t *bb = _w_daemon_send_msg_const_encrypted(d,
            SWMSG_REMOVE_INSTANCE_PART_SZ);
        if (!bb)
            return 1;
        swmsg_remove_instance_part_write_const_encrypted(bb, &d->c->cc, &s);
    }
    return 2;
}

static bbuf_t *
_w_daemon_send_msg(daemon_t *d, int sz)
{
    int tot_sz = WMSGTSZ + sz;
    if (BBUF_FREE_SPACE(&d->msgs_out) < tot_sz
    && send_all_from_bbuf(d->c->nh.fd, &d->msgs_out, MUTA_MTU))
        return 0;
    return &d->msgs_out;
}

static inline bbuf_t *
_w_daemon_send_msg_const_encrypted(daemon_t *d, int sz)
    {return _w_daemon_send_msg(d, CRYPT_MSG_ADDITIONAL_BYTES + sz);}

static inline bbuf_t *
_w_daemon_send_msg_var_encrypted(daemon_t *d, int sz)
{
    int tot_sz = sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + sz;
    return _w_daemon_send_msg(d, tot_sz);
}

static int
_w_daemon_send_despawn_player_msg(daemon_t *d, player_guid_t id)
{
    /* Temporarily use the normal despawn msg for this */
    bbuf_t *bb = _w_daemon_send_msg(d, SWMSG_DESPAWN_PLAYER_SZ);
    if (!bb) return 1;
    swmsg_despawn_player_t s;
    s.id = id;
    swmsg_despawn_player_write(bb, &s);
    return 0;
}

static int
_w_daemon_send_despawn_dynamic_obj_msg(daemon_t *d, dobj_guid_t id)
{
    bbuf_t *bb = _w_daemon_send_msg(d, SWMSG_DESPAWN_DYNAMIC_OBJ_SZ);
    if (!bb) return 1;
    swmsg_despawn_dynamic_obj_t s;
    s.id = id;
    swmsg_despawn_dynamic_obj_write(bb, &s);
    return 0;
}

static int
_w_set_ia_obj_pos(void *obj, uint8 type, instance_t *inst,
    instance_part_t **ret_ip, interest_area_t **ret_ia, int32 x, int32 y,
    int32 z)
{
    interest_area_t *new_ia = instance_get_interest_area_by_tile_pos(
        inst, x, y, z);
    if (*ret_ia == new_ia)
        return 0;
    if (*ret_ia)
        _w_remove_ia_obj_from_interest_area(obj, ret_ia);

#ifdef _MUTA_DEBUG /* Check if the item is already contained here */
    uint32 num = darr_num(new_ia->objs);
    for (uint32 i = 0; i < num; ++i)
        muta_assert(new_ia->objs[i].ptr != obj);
#endif

    ia_obj_t item;
    item.ptr     = obj;
    item.type    = type;
    darr_push(new_ia->objs, item);
    muta_assert(item.ptr);
    muta_assert(new_ia->objs[darr_num(new_ia->objs) - 1].ptr);
    *ret_ia = new_ia;

    instance_part_t *ip = instance_get_part_by_pos(inst, x, y);
    if (ip != *ret_ip)
    {
        *ret_ip = ip;
        DEBUG_PRINTFF("instance part changed for object of type %u, but not "
            "implemented!\n", (uint)type);
    }

    /* TODO: handle instance parts changing here */
    return 0;
}

static void
_w_remove_ia_obj_from_interest_area(interest_area_t **ret_ia, void *obj)
{
    uint32 num = darr_num((*ret_ia)->objs);
    for (uint32 i = 0; i < num; ++i)
    {
        if ((*ret_ia)->objs[i].ptr != obj)
            continue;
        darr_erase((*ret_ia)->objs, i);
        *ret_ia = 0;
        return;
    }
    muta_assert(0);
}

static inline uint8
_w_player_get_sex(player_t *p)
    {return p->flags & PLAYER_SEX_FEMALE ? SEX_FEMALE : SEX_MALE;}

#define SEND_FUNC_BODY(func_name) \
    bbuf_t bb = {0}; \
    if (!p->cd.c->flags.reserved || p->cd.c->id != p->cd.id) \
        return bb; \
    bb =func_name(p->cd.c, (size)); \
    return bb;
static inline bbuf_t
_w_player_send(player_t *p, int size)
    {SEND_FUNC_BODY(cl_send);}
static inline bbuf_t
_w_player_send_const_encrypted(player_t *p, int size)
    {SEND_FUNC_BODY(cl_send_const_encrypted);}
static inline bbuf_t
_w_player_send_var_encrypted(player_t *p, int size)
    {SEND_FUNC_BODY(cl_send_var_encrypted);}
#undef SEND_FUNC_BODY

static inline player_t *
_w_get_player_by_id(player_guid_t id)
{
    player_t **pp = guid_player_table_get_ptr(&_w_players.guid_table, id);
    return pp ? *pp : 0;
}

static inline dynamic_obj_t *
_w_get_dynamic_obj_by_id(dobj_guid_t id)
{
    dynamic_obj_t **op = guid_dynamic_obj_table_get_ptr(
        &_w_dynamic_objs.guid_table, id);
    return op ? *op : 0;
}

static inline creature_t *
_w_get_creature_by_id(creature_guid_t id)
{
    creature_t **cp = guid_creature_table_get_ptr(&_w_creatures.guid_table, id);
    return cp ? *cp : 0;
}

static instance_t *
_w_get_instance_by_id(instance_guid_t id)
{
    uint32 num = _w_num_instances;
    for (uint32 i = 0; i < num; ++i)
        if (_w_instances[i].id == id)
            return &_w_instances[i];
        else
            printf("id: %u, wanted: %u\n", (uint32)_w_instances[i].id, (uint32)id);
    return 0;
}

static instance_t *
_w_get_shared_instance_by_map_id(uint32 map_id)
{
    uint32 num = _w_num_instances;
    for (uint32 i = 0; i < num; ++i)
        if (_w_instances[i].shared && _w_instances[i].map_data->id == map_id)
            return &_w_instances[i];
    return 0;
}

static int
_w_init_shared_instances()
{
    int         ret = 0;
    FILE        *f  = fopen(_shared_instances_path, "r");
    char        map_name[128];
    inst_guid_t running_id = 0;

    if (!f)
    {
        printf("Error: could not open %s.\n", _shared_instances_path);
        return 1;
    }

    while (fgets(map_name, sizeof(map_name) - 1, f))
    {
        str_strip_trailing_spaces(map_name);
        map_data_t *map_data = maps_get_by_name(map_name);

        if (!map_data)
        {
            printf("Error in shared_instance.txt: map %s not found.\n",
                map_name);
            ret = 2;
            goto out;
        }

        if (instance_init(&_w_instances[running_id], 1, running_id, map_data,
            1, 1))
        {
            printf("Failed to instance shared instance number %u.\n",
                (uint32)running_id - 1);
            ret = 3;
            goto out;
        }

        running_id++;
        _w_num_instances++;
    }


    out:
        fclose(f);
        return ret;
}

static int
_w_init_clients(uint32 num)
{
    obj_pool_init(&_w_clients.pool, num, sizeof(w_client_t));
    darr_reserve(_w_clients.res, num);
    if (double_cmd_buf_init(&_w_clients.read_cmds, num, sizeof(accept_cmd_t)))
        return 3;
    return 0;
}

static void
_w_destroy_clients()
{
    obj_pool_destroy(&_w_clients.pool);
    double_cmd_buf_destroy(&_w_clients.read_cmds);
    memset(&_w_clients, 0, sizeof(_w_clients));
}

static w_client_t *
_w_reserve_client()
{
    w_client_t *ret = obj_pool_reserve(&_w_clients.pool);
    if (ret)
        darr_push(_w_clients.res, ret);
    return ret;
}

static void
_w_free_client(w_client_t *c)
{
    uint32 num_res = darr_num(_w_clients.res);
    for (uint32 i = 0; i < num_res; ++i)
    {
        if (_w_clients.res[i] != c)
            continue;
        uint32 last_index = _darr_head(_w_clients.res)->num - 1;
        _w_clients.res[i] = _w_clients.res[last_index];
        _darr_head(_w_clients.res)->num = last_index;
        obj_pool_free(&_w_clients.pool, c);
        return;
    }
    muta_assert(0);
}

static void
_w_disconnect_and_free_client(w_client_t *c)
{
    daemon_t *d = c->daemon;
    if (d)
    {
        if (c->nh.fd != KSYS_INVALID_SOCKET)
            send_all_from_bbuf(c->nh.fd, &d->msgs_out, MUTA_MTU);
        for (instance_part_t *ip = d->inst_parts; ip; ip = ip->next)
        {
            ip->daemon = 0;
            /* Disconnect players or create event? */
        }
        daemon_t **rd = &_w_daemons.res;
        for (; *rd != d; rd = &(*rd)->next);
        muta_assert(*rd == d);
        *rd = (d->next);
        daemon_pool_free(&_w_daemons.pool, d);
    }
    net_handle_close(&c->nh);
    _w_free_client(c);
    puts("World: disconnected daemon client.");
}

static daemon_t *
_w_create_new_daemon(w_client_t *c)
{
    daemon_t *d = daemon_pool_reserve(&_w_daemons.pool);
    if (!d) return 0;
    c->daemon       = d;
    BBUF_CLEAR(&d->msgs_in);
    BBUF_CLEAR(&d->msgs_out);
    darr_clear(d->timers);
    d->c            = c;
    d->inst_parts    = 0;
    d->next         = _w_daemons.res;
    _w_daemons.res  = d;
    return d;
}

static void
_w_daemon_destroy_allocs(daemon_t *w)
{
    dynamic_bbuf_destroy(&w->msgs_in);
    dynamic_bbuf_destroy(&w->msgs_out);
    darr_free(w->timers);
}

static int
_w_player_set_pos(player_t *p, instance_t *inst, int32 x, int32 y, int8 z)
{
    interest_area_t *old_ia = p->ia;
    interest_area_t *new_ia = instance_get_interest_area_by_tile_pos(inst,
        x, y, z);
    p->pos.x = x;
    p->pos.y = y;
    p->pos.z = z;
    if (old_ia == new_ia)
        return 0;
    if (old_ia)
        _w_remove_player_from_interest_area(p, old_ia);
#ifdef _MUTA_DEBUG /* Check if the player is already contained here */
    uint32 num_players = darr_num(new_ia->players);
    for (uint32 i = 0; i < num_players; ++i)
        muta_assert(new_ia->players[i]->id != p->id);
#endif
    darr_push(new_ia->players, p);
    p->ia = new_ia;
    instance_part_t *ip = instance_get_part_by_pos(inst, x, y);
    if (ip != p->inst_part)
    {
        p->inst_part = ip; /* Move from part to another? Unimplemented. */
        DEBUG_PRINTFF("instance part changed, but not implemented!\n");
    }
    return 0;
}

static int
_w_dynamic_obj_set_pos(dynamic_obj_t *obj, instance_t *inst, int32 x, int32 y,
    int8 z)
{
    obj->pos.x = x;
    obj->pos.y = y;
    obj->pos.z = z;
    _w_set_ia_obj_pos(obj, IA_OBJ_DYNAMIC_OBJ, inst, &obj->inst_part, &obj->ia,
        x, y, z);
    return 0;
}

static int
_w_creature_set_pos(creature_t *cr, instance_t *inst, int32 x, int32 y, int8 z)
{
    cr->pos.x = x;
    cr->pos.y = y;
    cr->pos.z = z;
    _w_set_ia_obj_pos(cr, IA_OBJ_CREATURE, inst, &cr->inst_part, &cr->ia,
        x, y, z);
    return 0;
}

static inline int
_w_player_set_pos_from_local(player_t *p, instance_t *inst, int32 x, int32 y,
    int8 z)
{
    instance_part_t *ip = p->inst_part;
    return _w_player_set_pos(p, inst, W_LOCAL_TO_GLOBAL_X(ip, x),
        W_LOCAL_TO_GLOBAL_Y(ip, y), z);
}

static bool32
_w_player_set_dir(player_t *p, int dir)
{
    if (dir < 0 || dir >= NUM_ISODIRS)
        return 0;
    p->dir = dir;
    return 1;
}

static bool32
_w_creature_set_dir(creature_t *c, int dir)
{
    if (dir < 0 || dir >= NUM_ISODIRS)
        return 0;
    c->dir = dir;
    return 1;
}

static int
_w_player_send_initial_world_data(player_t *p)
{
    svmsg_player_init_data_t f;
    f.char_id   = p->id;
    f.name      = p->name;
    f.name_len  = p->name_len;
    f.x         = p->pos.x;
    f.y         = p->pos.y;
    f.z         = p->pos.z;
    f.sex       = _w_player_get_sex(p);
    f.dir       = p->dir;
    f.map_id    = p->inst_part->inst->map_data->id;

    bbuf_t bb = _w_player_send_var_encrypted(p,
        SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(f.name_len));
    if (!bb.max_bytes)
        return 1;
    int r = svmsg_player_init_data_write_var_encrypted(&bb, &p->cd.c->cryptchan,
        &f);
    muta_assert(!r);

    interest_area_t *ias[27];
    uint32 num_ias = instance_get_relevant_interest_areas(p->inst_part->inst,
        p->pos.x, p->pos.y, p->pos.z, ias);
    _w_send_all_msgs_on_player_entered_new_interest_areas(p, ias, num_ias);
    return 0;
}

static int
_w_broadcast_player_walk(instance_t *m, player_t *p, world_pos_t old_pos)
{
    /* Send to the player of this character */
    bbuf_t bb = _w_player_send(p, SVMSG_WALK_PLAYER_CHARACTER_SZ);
    if (!bb.max_bytes)
        return 0;

    svmsg_walk_player_character_t s;
    s.id    = p->id;
    s.x     = p->pos.x;
    s.y     = p->pos.y;
    s.z     = p->pos.z;

    svmsg_walk_player_character_write(&bb, &s);

    /* Send to other players */
    bool32 ia_changed = !instance_is_pos_same_interest_area(m, &old_pos,
        &p->pos);

    interest_area_t *ias[27];
    int num_ias = (int)instance_get_relevant_interest_areas(m, old_pos.x,
        old_pos.y, old_pos.z, ias);

    int         i, j, num_players;
    player_t    **players;
    player_t    *op;

    for (i = 0; i < num_ias; ++i)
    {
        players     = ias[i]->players;
        num_players = (int)darr_num(players);

        for (j = 0; j < num_players; ++j)
        {
            op = players[j];
            if (op == p)
                continue;
            bb = _w_player_send(op, SVMSG_WALK_PLAYER_CHARACTER_SZ);
            svmsg_walk_player_character_write(&bb, &s);
        }
    }

    /* Check if player entered a new interest area. If yes, inform the newly
     * relevant interest area's of the movement, too. */
    if (!ia_changed)
        return 0;

    DEBUG_PRINTFF("walking player's interest area changed.\n");

    interest_area_t *new_ias[27];
    int num_new_ias = (int)instance_get_relevant_interest_areas(m,
        p->pos.x, p->pos.y, p->pos.z, new_ias);

    for (i = 0; i < num_new_ias; ++i)
        for (j = 0; j < num_ias; ++j)
        {
            /* Cut out any ias that already knew of the object */
            if (new_ias[i] != ias[j])
                continue;
            new_ias[i--] = new_ias[--num_new_ias];
            break;
        }

    svmsg_new_player_character_t sb, sc;
    sb.id       = p->id;
    sb.name     = p->name;
    sb.name_len = p->name_len;
    sb.type_id  = 0;
    sb.x        = p->pos.x; /* Was: old_pos.x, y, z (?) */
    sb.y        = p->pos.y;
    sb.z        = p->pos.z;
    sb.dir      = p->dir;
    sb.sex      = _w_player_get_sex(p);

    for (i = 0; i < num_new_ias; ++i)
    {
        players     = new_ias[i]->players;
        num_players = darr_num(new_ias[i]->players);
        for (j = 0; j < num_players; ++j)
        {
            op = players[j];
            if (op == p)
                continue;
            /* Send the other player to the player that moved */
            bb = _w_player_send(p,
                SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(op->name_len));
            if (bb.max_bytes)
            {
                sc.id       = op->id;
                sc.name     = op->name;
                sc.name_len = op->name_len;
                sc.type_id  = 0;
                sc.x        = op->pos.x;
                sc.y        = op->pos.y;
                sc.z        = op->pos.z;
                sc.dir      = op->dir;
                sc.sex      = _w_player_get_sex(op);
                svmsg_new_player_character_write(&bb, &sc);
            }
            /* Send the moved player to the op */
            bb = _w_player_send(op,
                SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(sb.name_len));
            if (!bb.max_bytes)
                continue;
            svmsg_new_player_character_write(&bb, &sb);
            bb = _w_player_send(op, SVMSG_WALK_PLAYER_CHARACTER_SZ);
            if (!bb.max_bytes)
                continue;
            svmsg_walk_player_character_write(&bb, &s);
        }
    }
    return 0;
}

static int
_w_broadcast_creature_walk(instance_t *inst, creature_t *c, wpos_t old_pos)
{
    bbuf_t bb;
    svmsg_walk_creature_t sa;
    sa.id = c->id;
    sa.x = c->pos.x;
    sa.y = c->pos.y;
    sa.z = c->pos.z;

    bool32 ia_changed = !instance_is_pos_same_interest_area(inst, &old_pos,
        &c->pos);

    /* Send to old players in the old interest areas */
    interest_area_t *ias[27];
    int num_ias = (int)instance_get_relevant_interest_areas(inst, old_pos.x,
        old_pos.y, old_pos.z, ias);

    for (int i = 0; i < num_ias; ++i)
    {
        player_t    **players   = ias[i]->players;
        uint32      num_players = darr_num(players);
        for (uint32 j = 0; j < num_players; ++j)
        {
            bb = _w_player_send(players[j], SVMSG_WALK_CREATURE_SZ);
            svmsg_walk_creature_write(&bb, &sa);
        }
    }

    /* If the creature's interest area didn't change, we don't need to send
     * any more messages */
    if (!ia_changed)
        return 0;

    interest_area_t *new_ias[27];
    int num_new_ias = (int)instance_get_relevant_interest_areas(inst, c->pos.x,
        c->pos.y, c->pos.z, ias);

    for (int i = 0; i < num_new_ias; ++i)
        for (int j = 0; j < num_ias; ++j)
        {
            /* Cut out any ias that already knew of the object */
            if (new_ias[i] != ias[j])
                continue;
            new_ias[i--] = new_ias[--num_new_ias];
            break;
        }

    svmsg_new_creature_t sb;
    _w_fill_svmsg_new_creature(&sb, c);

    for (int i = 0; i < num_new_ias; ++i)
    {
        player_t    **players   = new_ias[i]->players;
        uint32      num_players = darr_num(players);
        for (uint32 j = 0; j < num_players; ++j)
        {
            bb = _w_player_send(players[j], SVMSG_NEW_CREATURE_SZ);
            svmsg_new_creature_write(&bb, &sb);
        }
    }

    return 0;
}

static int
_w_broadcast_player_teleport(player_t *p, wpos_t old_pos)
{
    instance_t *inst = p->inst_part->inst;

    DEBUG_PRINTF("%s: %d.%d.%d to %d.%d.%d.\n", __func__, old_pos.x, old_pos.y,
        old_pos.z, p->pos.x, p->pos.y, p->pos.z);

    /* Send to the player of this character */
    bbuf_t bb = _w_player_send(p, SVMSG_UPDATE_PLAYER_POSITION_SZ);
    if (!bb.max_bytes)
        return 0;
    svmsg_update_player_position_t s;
    s.id    = p->id;
    s.x     = p->pos.x;
    s.y     = p->pos.y;
    s.z     = p->pos.z;
    svmsg_update_player_position_write(&bb, &s);

    /* Send to other players */

    interest_area_t *old_ias[27];
    interest_area_t *new_ias[27];
    interest_area_t *com_ias[27];

    uint32 num_old_ias = instance_get_relevant_interest_areas(inst, old_pos.x,
        old_pos.y, old_pos.z, old_ias);
    uint32 num_new_ias = instance_get_relevant_interest_areas(inst, p->pos.x,
        p->pos.y, p->pos.z, new_ias);
    uint32 num_com_ias = _w_get_common_interest_areas_in_arrays(old_ias,
        num_old_ias, new_ias, num_new_ias, com_ias);
    num_old_ias = _w_purge_non_unique_interest_areas(old_ias, num_old_ias,
        com_ias, num_com_ias);
    num_new_ias = _w_purge_non_unique_interest_areas(new_ias, num_new_ias,
        com_ias, num_com_ias);

    /* Inform the player of their new surroundings */
    _w_send_all_msgs_on_player_entered_new_interest_areas(p, new_ias,
        num_new_ias);

    /* Send update messages to common interest areas */
    {
        svmsg_update_player_position_t s;
        s.id    = p->id;
        s.x     = p->pos.x;
        s.y     = p->pos.y;
        s.z     = p->pos.z;
        W_SEND_MSG_TO_PLAYERS_IN_IAS(com_ias, num_com_ias, _w_player_send,
            SVMSG_UPDATE_PLAYER_POSITION_SZ, &s,
            svmsg_update_player_position_write, p);
    }

    /* Send remove messages to now non-relevant interest areas */
    {
        svmsg_remove_player_character_t s;
        s.id = p->id;
        W_SEND_MSG_TO_PLAYERS_IN_IAS(old_ias, num_old_ias, _w_player_send,
            SVMSG_REMOVE_PLAYER_CHARACTER_SZ, &s,
            svmsg_remove_player_character_write, p);
    }

    /* Send new character messages to new interest areas */
    {
        svmsg_new_player_character_t s;
        _w_fill_svmsg_new_player_character(&s, p);
        W_SEND_MSG_TO_PLAYERS_IN_IAS(new_ias, num_new_ias, _w_player_send,
            SVMSG_NEW_PLAYER_CHARACTER_SZ, &s, svmsg_new_player_character_write,
            p);
    }

    return 0;
}

static void
_w_remove_player_from_interest_area(player_t *p, interest_area_t *ia)
{
    player_t    **players   = ia->players;
    uint        num         = darr_num(ia->players);
    for (uint i = 0; i < num; ++i)
    {
        if (players[i] != p)
            continue;
        darr_erase(ia->players, i);
        p->ia = 0;
        return;
    }
    muta_assert(0);
}

static void
_w_remove_player_inner(player_t *p)
{
    player_guid_t id    = p->id;
    player_pool_free(&_w_players.pool, p);
    guid_player_table_erase(&_w_players.guid_table, id);
    if (p->ia) _w_remove_player_from_interest_area(p, p->ia);
    printf("Removed player %llu.\n", (lluint)p->id);
}

static void
_w_process_accepts()
{
    double_cmd_buf_arr_t    *arr    = double_cmd_buf_swap(&_w_accept_cmds);
    accept_cmd_t            *cmds   = arr->mem;
    int                     num     = arr->num;

    for (int i = 0; i < num; ++i)
    {
        socket_t    fd  = cmds[i].fd;
        net_queue_t *nq = cmds[i].nq;

        w_client_t *c = _w_reserve_client();
        if (!c) {DEBUG_PRINTFF("%s: no free clients.", __func__); goto fail;}

        int err = 0;
        if (net_handle_init(&c->nh, fd, NETQOP_READ, W_DAEMON_CLIENT_TYPE_ID,
            c))
            {err = 1; goto fail;}
        c->recv_time    = get_program_ticks_ms();
        c->daemon       = 0;
        c->nq           = nq;
        BBUF_INIT(&c->msgs_in, c->msgs_in_mem, W_CLIENT_BUF_SZ);
        if (cryptchan_init(&c->cc, 0)) {err = 2; goto fail;}
        if (net_queue_add(nq, &c->nh, BBUF_CUR_PTR(&c->msgs_in),
            BBUF_FREE_SPACE(&c->msgs_in)))
            {err = 3; goto fail;}

        puts("World: accepted new potential daemon client.");
        continue;

        fail:
            printf("%s failed with error %d\n", __func__, err);
            _w_free_client(c);
            net_shutdown_sock(fd, SOCKSD_BOTH);
    }
    double_cmd_buf_arr_clear(arr);
}

static void
_w_check_timeouts()
{
    uint64 ct = get_program_ticks_ms();
    if (ct - _w_last_timeout_cleanup < _w_timeout_check_freq) return;
    int num_res = (int)darr_num(_w_clients.res);
    for (int i = 0; i < num_res; ++i)
    {
        if (ct - _w_clients.res[i]->recv_time < _w_timeout)
            continue;
        puts("World: a daemon timed out!");
        _w_disconnect_and_free_client(_w_clients.res[i]);
        num_res--;
        i--;
    }
    _w_last_timeout_cleanup = ct;
}

static void
_w_read_clients()
{
    double_cmd_buf_arr_t *arr = double_cmd_buf_swap(&_w_clients.read_cmds);
    w_client_t  **cs    = arr->mem;
    int         num     = arr->num;

    for (int i = 0; i < num; ++i)
    {
        int r;
        if (cs[i]->daemon)
            r = _w_read_daemon_client(cs[i]);
        else
            r = _w_read_new_client(cs[i]);

        if (r)
        {
            _w_disconnect_and_free_client(cs[i]);
            continue;
        }

        bbuf_t *bb;
        if (cs[i]->daemon)
            bb = &cs[i]->daemon->msgs_in;
        else
            bb = &cs[i]->msgs_in;

        if (net_queue_add(cs[i]->nq, &cs[i]->nh, BBUF_CUR_PTR(bb),
            BBUF_FREE_SPACE(bb)))
            _w_disconnect_and_free_client(cs[i--]);
    }
    double_cmd_buf_arr_clear(arr);
}

static void
_w_send_msgs()
{
    w_client_t  *c;
    daemon_t    *d;
    int         num_res = (int)darr_num(_w_clients.res);
    for (int i = 0; i < num_res; ++i)
    {
        c = _w_clients.res[i];
        d = c->daemon;
        if (!d)
            continue;
        if (d->msgs_out.num_bytes > 0
        && send_all_from_bbuf(c->nh.fd, &d->msgs_out, MUTA_MTU))
        {
            _w_disconnect_and_free_client(c);
            i--;
        }
    }
}

static int
_w_read_daemon_client(w_client_t *c)
{
    daemon_t *d = c->daemon;
    int left = _w_read_daemon_packet(c, d->msgs_in.mem, d->msgs_in.num_bytes);
    if (left < 0)
        return 1;
    bbuf_cut_portion(&d->msgs_in, 0, d->msgs_in.num_bytes - left);
    return 0;
}

static int
_w_read_new_client(w_client_t *c)
{
    int left = _w_read_new_packet(c, c->msgs_in.mem, c->msgs_in.num_bytes);
    if (left < 0)
        return 1;
    bbuf_cut_portion(&c->msgs_in, 0, c->msgs_in.num_bytes - left);
    return 0;
}

static int
_w_read_new_packet(w_client_t *c, uint8 *pkt, int pkt_len)
{
    bbuf_t      bb = BBUF_INITIALIZER(pkt, pkt_len);
    int         incomplete = 0;
    int         dc;
    wmsg_type_t t;

    while (BBUF_FREE_SPACE(&bb) >= WMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, wmsg_type_t, &t);

        switch (t)
        {
        case WMSG_KEEP_ALIVE:
            dc = 0;
            break;
        case WMSG_PUB_KEY:
        {
            DEBUG_PUTS("WMSG_PUB_KEY");
            wmsg_pub_key_t s;
            incomplete = wmsg_pub_key_read(&bb, &s);
            if (!incomplete) dc = _w_handle_new_wmsg_pub_key_read(c, &s);
        }
            break;
        case WMSG_STREAM_HEADER:
        {
            DEBUG_PUTS("WMSG_STREAM_HEADER");
            wmsg_stream_header_t s;
            incomplete = wmsg_stream_header_read(&bb, &s);
            if (!incomplete) dc = _w_handle_new_wmsg_stream_header_read(c,
                &s);
        }
            break;
        default:
            dc = 1;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: err, type %d, dc %d, inc. %d\n", __func__, (int)t, dc,
                incomplete);
            return -1;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static int
_w_read_daemon_packet(w_client_t *c, uint8 *pkt, int pkt_len)
{
    uint64      ct = get_program_ticks_ms();
    bbuf_t      bb = BBUF_INITIALIZER(pkt, pkt_len);
    int         incomplete = 0;
    int         dc;
    wmsg_type_t t;

    while (BBUF_FREE_SPACE(&bb) >= WMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, wmsg_type_t, &t);

        switch (t)
        {
        case WMSG_KEEP_ALIVE:
            dc = 0;
            break;
        case WSMSG_LOAD_INSTANCE_PART_RESULT:
        {
            puts("WSMSG_LOAD_INSTANCE_PART_RESULT");
            wsmsg_load_instance_part_result_t s;
            incomplete =
                wsmsg_load_instance_part_result_read_const_encrypted(
                    &bb, &c->cc, &s);
            if (!incomplete) dc =
                _w_handle_wsmsg_load_instance_part_result(c, &s);
        }
            break;
        case WSMSG_CONFIRM_SPAWN_PLAYER:
        {
            puts("WSMSG_CONFIRM_SPAWN_PLAYER");
            wsmsg_confirm_spawn_player_t s;
            incomplete = wsmsg_confirm_spawn_player_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_confirm_spawn_player(c,
                &s);
        }
            break;
        case WSMSG_DESPAWN_PLAYER:
            puts("WSMSG_DESPAWN_PLAYER (IMPLEMENTME)");
            break;
        case WSMSG_REMOVE_CREATURE:
            puts("WSMSG_REMOVE_CREATURE (IMPLEMENTME)");
            break;
        case WSMSG_WALK_CREATURE:
        {
            wsmsg_walk_creature_t s;
            incomplete = wsmsg_walk_creature_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_walk_creature(c, &s);
        }
            break;
        case WSMSG_WALK_PLAYER:
        {
            wsmsg_walk_player_t s;
            incomplete = wsmsg_walk_player_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_walk_player(c, &s);
        }
            break;
        case WSMSG_REQUEST_SPAWN_CREATURE:
        {
            wsmsg_request_spawn_creature_t s;
            incomplete = wsmsg_request_spawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _w_handle_wsmsg_request_spawn_creature(c, &s);
        }
            break;
        case WSMSG_SPAWN_PLAYER_FAIL:
        {
            wsmsg_spawn_player_fail_t s;
            incomplete = wsmsg_spawn_player_fail_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_spawn_player_fail(c, &s);
        }
            break;
        case WSMSG_PLAYER_DIR:
        {
            wsmsg_player_dir_t s;
            incomplete = wsmsg_player_dir_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_player_dir(c, &s);
        }
            break;
        case WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ:
        {
            wsmsg_confirm_spawn_dynamic_obj_t s;
            incomplete = wsmsg_confirm_spawn_dynamic_obj_read(&bb, &s);
            if (!incomplete)
                dc = _w_handle_wsmsg_confirm_spawn_dynamic_obj(c, &s);
        }
            break;
        case WSMSG_TELEPORT_PLAYER:
        {
            wsmsg_teleport_player_t s;
            incomplete = wsmsg_teleport_player_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_teleport_player(c, &s);
        }
            break;
        case WSMSG_CONFIRM_SPAWN_CREATURE:
        {
            wsmsg_confirm_spawn_creature_t s;
            incomplete = wsmsg_confirm_spawn_creature_read(&bb, &s);
            if (!incomplete)
                dc = _w_handle_wsmsg_confirm_spawn_creature(c, &s);
        }
            break;
        case WSMSG_CREATURE_DIR:
        {
            wsmsg_creature_dir_t s;
            incomplete = wsmsg_creature_dir_read(&bb, &s);
            if (!incomplete) dc = _w_handle_wsmsg_creature_dir(c, &s);
        }
            break;
        default:
            dc = 1;
            break;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: err, type %d, dc %d, inc. %d\n", __func__, (int)t, dc,
                incomplete);
            return -1;
        } else
            c->recv_time = ct;
    }
    return BBUF_FREE_SPACE(&bb);
}

static int
_w_broadcast_player_dir(player_t *p, uint8 dir)
{
    interest_area_t *ias[27];
    uint num_ias = instance_get_relevant_interest_areas(p->inst_part->inst,
        p->pos.x, p->pos.y, p->pos.z, ias);

    svmsg_player_character_dir_t s;
    s.id    = p->id;
    s.dir   = dir;

    uint32      i, j, num_ps;
    player_t    **ps;
    bbuf_t      bb;

    for (i = 0; i < num_ias; ++i)
    {
        ps      = ias[i]->players;
        num_ps  = darr_num(ias[i]->players);

        for (j = 0; j < num_ps; ++j)
        {
            if (ps[j] == p)
                continue;
            bb = _w_player_send(ps[j], SVMSG_PLAYER_CHARACTER_DIR_SZ);
            svmsg_player_character_dir_write(&bb, &s);
        }
    }

    return 0;
}

static int
_w_spawn_dynamic_obj(instance_guid_t inst_id, dynamic_obj_type_id_t type_id,
    wpos_t pos, int dir)
{
    instance_t *inst = _w_get_instance_by_id(inst_id);
    if (!inst )
        return 1;

    instance_part_t *ip = instance_get_part_by_pos(inst , pos.x, pos.y);
    if (!ip->daemon)
        return W_ERR_DAEMON_NOT_CONNECTED;

    dynamic_obj_t *d = dynamic_obj_pool_reserve(&_w_dynamic_objs.pool);
    if (!d) return W_ERR_NO_MEM;

    /* Generate a guid */
    dobj_guid_t id = _w_dynamic_objs.running_id++;
    muta_assert(
        !guid_dynamic_obj_table_exists(&_w_dynamic_objs.guid_table, id));

    if (guid_dynamic_obj_table_insert(&_w_dynamic_objs.guid_table, id, d))
    {
        dynamic_obj_pool_free(&_w_dynamic_objs.pool, d);
        return W_ERR_NO_MEM;
    }

    d->id                   = id;
    d->ia                   = 0;
    d->inst_part            = ip;
    d->confirmed_by_daemon  = 0;

    /* Send message to daemon */
    swmsg_spawn_dynamic_obj_t s;
    s.id            = id;
    s.inst_part_id  = ip->runtime_id;
    s.x             = W_GLOBAL_TO_LOCAL_X(ip, pos.x);
    s.y             = W_GLOBAL_TO_LOCAL_Y(ip, pos.y);
    s.z             = pos.z;
    s.dir           = (uint8)dir;

    bbuf_t *bb = _w_daemon_send_msg(ip->daemon, SWMSG_SPAWN_DYNAMIC_OBJ_SZ);
    if (!bb)
    {
        dynamic_obj_pool_free(&_w_dynamic_objs.pool, d);
        guid_dynamic_obj_table_erase(&_w_dynamic_objs.guid_table, id);
        return W_ERR_DAEMON_NOT_CONNECTED;
    }
    swmsg_spawn_dynamic_obj_write(bb, &s);
    DEBUG_PRINTFF("successful\n");
    return 0;
}

static void
_w_despawn_dynamic_obj(dobj_guid_t id)
{
    dynamic_obj_t *obj = _w_get_dynamic_obj_by_id(id);
    if (!obj) return;
    _w_daemon_send_despawn_dynamic_obj_msg(obj->inst_part->daemon, id);

    if (obj->confirmed_by_daemon)
    {
        svmsg_remove_dynamic_obj_t s;
        s.id = id;
        W_BROADCAST_TO_NEARBY_PLAYERS(obj->inst_part, 0, obj->pos.x, obj->pos.y,
            obj->pos.z, _w_player_send, svmsg_remove_dynamic_obj_write, &s,
            SVMSG_REMOVE_DYNAMIC_OBJ_SZ);
    }
    muta_assert(obj->ia);
    _w_remove_ia_obj_from_interest_area(&obj->ia, obj);
    DEBUG_PRINTFF("completed.\n");
}

static int
_w_handle_new_wmsg_pub_key_read(w_client_t *c, wmsg_pub_key_t *s)
{
    wmsg_stream_header_t fa;
    int r = cryptchan_sv_store_pub_key(&c->cc, s->key, fa.header);
    if (r) return 1;

    wmsg_pub_key_t fb;
    memcpy(fb.key, c->cc.pk, CRYPTCHAN_PUB_KEY_SZ);

    #define BUF_SZ (WMSG_PUB_KEY_SZ + WMSG_STREAM_HEADER_SZ + 2 * WMSGTSZ)
    uint8 out_mem[BUF_SZ];
    bbuf_t bb = BBUF_INITIALIZER(out_mem, BUF_SZ);
    #undef BUF_SZ

    r = wmsg_pub_key_write(&bb, &fb); muta_assert(!r);
    r = wmsg_stream_header_write(&bb, &fa); muta_assert(!r);
    if (send_all_from_bbuf(c->nh.fd, &bb, MUTA_MTU)) return 2;
    return 0;
}

static int
_w_handle_new_wmsg_stream_header_read(w_client_t *c, wmsg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&c->cc, s->header)) return 1;
    if (cryptchan_is_encrypted(&c->cc))
        puts("World: connection with daemon client is now encrypted.");

    /* Create a new daemon (should copy remaining msgs here btw */
    daemon_t *d = _w_create_new_daemon(c);
    if (!d) {printf("%s: too many daemons.\n", __func__); return 1;}

    /* Find a map for this daemon to simulate */
    instance_t *inst = &_w_instances[W_MAIN_MAP_INDEX];

    uint num_parts = inst->pw * inst->ph;

    for (uint i = 0; i < num_parts; ++i)
    {
        if (inst->parts[i].daemon)
            continue;
        _w_daemon_add_inst_part(d, &inst->parts[i]);
        break;
    }
    return 0;
}

static int
_w_handle_wsmsg_confirm_spawn_player(w_client_t *c,
    wsmsg_confirm_spawn_player_t *s)
{
    player_t *p = _w_get_player_by_id(s->id);

    if (!p)
    {
        printf("World: daemon-confirmed player %llu not found! Telling daemon "
            "to despawn.\n", (lluint)s->id);
        /* Tell the world to despawn the player if it doesn't exist here */
        swmsg_despawn_player_t f;
        f.id = s->id;
        bbuf_t *bb = _w_daemon_send_msg(c->daemon, SWMSG_DESPAWN_PLAYER_SZ);
        if (!bb) return 1;
        swmsg_despawn_player_write(bb, &f);
        return 0;
    }

    int32   gx = W_LOCAL_TO_GLOBAL_X(p->inst_part, s->x);
    int32   gy = W_LOCAL_TO_GLOBAL_Y(p->inst_part, s->y);
    int8    gz = s->z;

    p->flags |= PLAYER_SPAWN_CONFIRMED;
    if (_w_player_set_pos(p, p->inst_part->inst, gx, gy, s->z))
        W_PANIC();

    w_event_spawn_player_t ev;
    ev.id   = s->id;
    ev.z    = gx;
    ev.y    = gy;
    ev.x    = gz;
    ev.dir  = s->dir;
    ev.sex  = _w_player_get_sex(p);
    ev.cd   = p->cd;

    if (sv_handle_w_event_spawn_player(&ev)) /* Don't send messages forward */
    {
        printf("%s: returning early due to sv request.\n", __func__);
        return 0;
    }

    /* Inform player */
    if (_w_player_send_initial_world_data(p))
    {
        DEBUG_PRINTFF("Failed to send initial player data.");
        cl_disconnect(p->cd.c);
    } else
        DEBUG_PRINTFF("sent player character init data for character %" PRIu64
            ".\n", s->id);

    /* Broadcast spawn */
    DEBUG_PRINTF("World: broadcasting player spawn.\n");
    svmsg_new_player_character_t f;
    _w_fill_svmsg_new_player_character(&f, p);

    interest_area_t *ias[27];
    uint32 num_ias = instance_get_relevant_interest_areas(p->inst_part->inst,
        p->pos.x, p->pos.y, p->pos.z, ias);

    uint32      i, j, num_players;
    player_t    **players, *op;
    bbuf_t      bb;

    for (i = 0; i < num_ias; ++i)
    {
        players     = ias[i]->players;
        num_players = darr_num(ias[i]->players);
        for (j = 0; j < num_players; ++j)
        {
            op = players[j];
            if (op == p)
                continue;
            bb = _w_player_send(op,
                SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(f.name_len));
            if (!bb.max_bytes)
                continue;
            svmsg_new_player_character_write(&bb, &f);
            printf("World: sent player %s spawn to one %s!\n",
                p->name, op->name);
        }
    }
    return 0;
}

static int
_w_handle_wsmsg_walk_creature(w_client_t *wc, wsmsg_walk_creature_t *s)
{
    creature_t *c = _w_get_creature_by_id(s->id);
    if (!c)
        muta_assert(0);
    instance_t *inst    = c->inst_part->inst;
    wpos_t      old_pos = c->pos;

    int dx = s->x - old_pos.x;
    int dy = s->y - old_pos.y;
    if (dx || dy)
        _w_creature_set_dir(c, vec2_to_iso_dir(dx, dy));
    _w_creature_set_pos(c, inst, s->x, s->y, s->z);
    _w_broadcast_creature_walk(inst, c, old_pos);
    return 0;
}

static int
_w_handle_wsmsg_walk_player(w_client_t *c, wsmsg_walk_player_t *s)
{
    player_t *p = _w_get_player_by_id(s->id);
    if (!p)
    {
        printf("World: daemon walked an inexistent player's spawn (id %llu)!\n",
            (lluint)s->id);
        _w_daemon_send_despawn_player_msg(c->daemon, s->id);
        return 0;
    }

    instance_t  *inst   = p->inst_part->inst;
    world_pos_t old_pos = p->pos;
    int dx = s->x - old_pos.x;
    int dy = s->y - old_pos.y;
    if (dx || dy)
        _w_player_set_dir(p, vec2_to_iso_dir(dx, dy));
    _w_player_set_pos_from_local(p, inst, s->x, s->y, s->z);
    _w_broadcast_player_walk(inst, p, old_pos);
    return 0;
}

static int
_w_handle_wsmsg_request_spawn_creature(w_client_t *c,
    wsmsg_request_spawn_creature_t *s)
{
    DEBUG_PUTS("WSMSG_REQUEST_SPAWN_CREATURE :D");
    IMPLEMENTME();
}

static int
_w_handle_wsmsg_spawn_player_fail(w_client_t *c, wsmsg_spawn_player_fail_t *s)
{
    player_t *p = _w_get_player_by_id(s->id);
    if (!p)
    {
        printf("Daemon suggested failure of inexistent player's spawn "
            "(id %llu)!", (lluint)s->id);
        return 0;
    }

    if (p->flags & PLAYER_SPAWN_CONFIRMED)
        return 0;

    w_event_spawn_player_fail_t ev;
    ev.char_id  = s->id;
    ev.cd       = p->cd;
    sv_handle_w_event_spawn_player_fail(&ev);
    _w_remove_player_inner(p);
    return 0;
}

static int
_w_handle_wsmsg_load_instance_part_result(w_client_t *c,
    wsmsg_load_instance_part_result_t *s)
{
    daemon_t        *d  = c->daemon;
    instance_part_t *ip = d->inst_parts;

    for (; ip; ip = ip->next)
        if (ip->runtime_id == s->part_id)
            break;

    if (!ip || ip->runtime_id != s->part_id)
        return 1;

    if (!s->result)
    {
        ip->confirmed = 1;
        DEBUG_PRINTFF("daemon confirmed load of map part id %u.\n", s->part_id);
        DEBUG_PRINTFF("tmp: spawning creatures and dynamic objects\n");
        for (uint32 i = 0; i < darr_num(ip->inst->creature_spawns); ++i)
        {
            creature_def_t *def = wc_get_creature_def(ip->inst->creature_spawns[i].type_id);
            muta_assert(def);
            w_spawn_creature(ip->inst->id, ip->inst->creature_spawns[i].type_id,
                ip->inst->creature_spawns[i].position, 0, 0);
            DEBUG_PRINTFF("spawn creature type %u.\n",
                ip->inst->creature_spawns[i].type_id);
        }
        for (uint32 i = 0; i < darr_num(ip->inst->dynamic_obj_spawns); ++i)
            _w_spawn_dynamic_obj(ip->inst->id,
                ip->inst->dynamic_obj_spawns[i].type_id,
                ip->inst->dynamic_obj_spawns[i].position, 0);
    } else
    {
        _w_daemon_remove_map_part(d, ip);
        printf("World: daemon failed to load map id %u!", s->part_id);
    }
    return 0;
}

static int
_w_handle_wsmsg_player_dir(w_client_t *c, wsmsg_player_dir_t *s)
{
    player_t *p = _w_get_player_by_id(s->id);
    if (!p) {DEBUG_PRINTF("%s: player not found", __func__); return 0;}
    if (_w_player_set_dir(p, s->dir))
        _w_broadcast_player_dir(p, s->dir);
    return 0;
}

static int
_w_handle_wsmsg_confirm_spawn_dynamic_obj(w_client_t *c,
    wsmsg_confirm_spawn_dynamic_obj_t *s)
{
    dynamic_obj_t *obj = _w_get_dynamic_obj_by_id(s->id);
    if (!obj)
    {
        int r = _w_daemon_send_despawn_dynamic_obj_msg(c->daemon, s->id);
        DEBUG_PRINTF("%s: dynamic obj not found\n", __func__);
        return r ? 1 : 0;
    }
    if (obj->inst_part->daemon != c->daemon)
        return 2;
    if (obj->confirmed_by_daemon)
        {DEBUG_PRINTF("%s: double confirm!\n", __func__); return 0;}
    obj->dir    = s->dir;
    obj->confirmed_by_daemon = 1;
    if (_w_dynamic_obj_set_pos(obj, obj->inst_part->inst, s->x, s->y, s->z))
        W_PANIC();

    {
    svmsg_new_dynamic_obj_t f;
    f.id        = obj->id;
    f.type_id   = obj->type_id;
    f.x         = obj->pos.x;
    f.y         = obj->pos.y;
    f.z         = obj->pos.z;
    f.dir       = obj->dir;
    W_BROADCAST_TO_NEARBY_PLAYERS(obj->inst_part, 0, obj->pos.x, obj->pos.y,
        obj->pos.z, _w_player_send, svmsg_new_dynamic_obj_write, &f,
        SVMSG_NEW_DYNAMIC_OBJ_SZ);
    }
    return 0;
}

static int
_w_handle_wsmsg_teleport_player(w_client_t *c, wsmsg_teleport_player_t *s)
{
    player_t *p = _w_get_player_by_id(s->id);
    if (!p)
    {
        _w_daemon_send_despawn_player_msg(c->daemon, s->id);
        return 0;
    }
    wpos_t old_pos = p->pos;
    _w_player_set_pos_from_local(p, p->inst_part->inst, s->x, s->y, s->z);
    _w_broadcast_player_teleport(p, old_pos);
    return 0;
}

static int
_w_handle_wsmsg_confirm_spawn_creature(w_client_t *c,
    wsmsg_confirm_spawn_creature_t *s)
{
    DEBUG_PRINTFF("called.\n");
    creature_t *cr = _w_get_creature_by_id(s->id);
    if (cr->flags.confirmed_by_daemon)
        return 1;
    if (cr->inst_part->daemon != c->daemon)
        return 2;
    cr->flags.confirmed_by_daemon = 1;
    _w_creature_set_pos(cr, cr->inst_part->inst, s->x, s->y, s->z);

    svmsg_new_creature_t f;
    _w_fill_svmsg_new_creature(&f, cr);
    W_BROADCAST_TO_NEARBY_PLAYERS(cr->inst_part, 0, cr->pos.x, cr->pos.y,
        cr->pos.z, _w_player_send, svmsg_new_creature_write, &f,
        SVMSG_NEW_CREATURE_SZ);
    DEBUG_PRINTFF("broadcast to %u players\n", num_ps);

    return 0;
}

static int
_w_handle_wsmsg_creature_dir(w_client_t *c, wsmsg_creature_dir_t *s)
{
    creature_t *cr = _w_get_creature_by_id(s->id);
    if (!cr)
    {
        DEBUG_PRINTFF("warning 1.\n");
        return 0;
    }
    if (cr->inst_part->daemon != c->daemon)
    {
        DEBUG_PRINTFF("warning 2.\n");
        return 2;
    }
    if (!_w_creature_set_dir(cr, s->dir))
        return 0;
    interest_area_t *ias[27];
    uint32 num_ias = instance_get_relevant_interest_areas(cr->inst_part->inst,
        cr->pos.x, cr->pos.y, cr->pos.z, ias);

    svmsg_creature_dir_t f;
    f.id    = cr->id;
    f.dir   = cr->dir;
    bbuf_t bb;
    for (uint32 i = 0; i < num_ias; ++i)
    {
        player_t    **players   = ias[i]->players;
        uint32      num_players = darr_num(players);
        for (uint32 j = 0; j < num_players; ++j)
        {
            bb = _w_player_send(players[j], SVMSG_CREATURE_DIR_SZ);
            svmsg_creature_dir_write(&bb, &f);
        }
    }
    return 0;
}

static void
_w_fill_svmsg_new_player_character(svmsg_new_player_character_t *s, player_t *p)
{
    s->id       = p->id;
    s->name     = p->name;
    s->name_len = p->name_len;
    s->type_id  = 0;
    s->x        = p->pos.x;
    s->y        = p->pos.y;
    s->z        = p->pos.z;
    s->dir      = p->dir;
    s->sex      = _w_player_get_sex(p);
}

static void
_w_fill_svmsg_new_dynamic_obj(svmsg_new_dynamic_obj_t *s, dynamic_obj_t *obj)
{
    s->id       = obj->id;
    s->type_id  = obj->type_id;
    s->x        = obj->pos.x;
    s->y        = obj->pos.y;
    s->z        = obj->pos.z;
}

static void
_w_fill_svmsg_new_creature(svmsg_new_creature_t *s, creature_t *cr)
{
    s->id       = cr->id;
    s->type_id  = cr->type_id;
    s->x        = cr->pos.x;
    s->y        = cr->pos.y;
    s->z        = cr->pos.z;
    s->dir      = cr->dir;
    s->flags    = cr->creature_flags;
}

static uint
_w_purge_non_unique_interest_areas(interest_area_t **ias_a, uint num_ias_a,
    interest_area_t **ias_b, uint num_ias_b)
{
    if (!num_ias_a || !num_ias_b) return num_ias_a;
    uint i, j;
    for (i = 0; i < num_ias_a; ++i)
        for (j = 0; j < num_ias_b; ++j)
        {
            /* Cut out any ias that already knew of the object */
            if (ias_a[i] != ias_b[j]) continue;
            ias_a[i--] = ias_a[--num_ias_a];
            break;
        }
    return num_ias_a;
}

static uint
_w_get_common_interest_areas_in_arrays(interest_area_t **ias_a, uint num_ias_a,
    interest_area_t **ias_b, uint num_ias_b, interest_area_t **ret_ias)
{
    uint num = 0;
    uint i, j;
    for (i = 0; i < num_ias_a; ++i)
        for (j = 0; j < num_ias_a; ++j)
        {
            if (ias_a[i] != ias_b[j]) continue;
            ret_ias[num++] = ias_b[j];
            break;
        }
    return num;
}

static int
_w_send_all_msgs_on_player_entered_new_interest_areas(player_t *p,
    interest_area_t **ias, uint num_ias)
{
    uint    i, j;
    bbuf_t  bb;
    for (i = 0; i < num_ias; ++i)
    {
        player_t        **players   = ias[i]->players;
        uint32          num_players = darr_num(ias[i]->players);
        player_t        *op;

        for (j = 0; j < num_players; ++j)
        {
            op = players[j];
            if (op == p) continue;

            svmsg_new_player_character_t f;
            _w_fill_svmsg_new_player_character(&f, op);
            bb = _w_player_send(p,
                SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(op->name_len));
            if (!bb.max_bytes)
                return 2;
            svmsg_new_player_character_write(&bb, &f);
        }

        ia_obj_t        *objs       = ias[i]->objs;
        uint32          num_objs   = darr_num(ias[i]->objs);
        dynamic_obj_t   *dobj;
        creature_t      *cr;

        for (j = 0; j < num_objs; ++j)
        {
            switch (objs[j].type)
            {
            case IA_OBJ_DYNAMIC_OBJ:
            {
                dobj = objs[j].ptr;
                svmsg_new_dynamic_obj_t f;
                _w_fill_svmsg_new_dynamic_obj(&f, dobj);
                bb = _w_player_send(p, SVMSG_NEW_DYNAMIC_OBJ_SZ);
                if (!bb.max_bytes)
                    return 3;
                svmsg_new_dynamic_obj_write(&bb, &f);
            }
                break;
            case IA_OBJ_CREATURE:
            {
                cr = objs[j].ptr;
                svmsg_new_creature_t f;
                _w_fill_svmsg_new_creature(&f, cr);
                bb = _w_player_send(p, SVMSG_NEW_CREATURE_SZ);
                if (!bb.max_bytes)
                    return 4;
                svmsg_new_creature_write(&bb, &f);
            }
                break;
            default:
                muta_assert(0);
            }
        }
    }
    return 0;
}
