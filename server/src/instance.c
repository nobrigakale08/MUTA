#include "instance.h"
#include "maps.h"
#include "../../shared/common_utils.h"

static uint32 _running_inst_part_id;

int
instance_init(instance_t *inst, int shared, inst_guid_t id,
    map_data_t *map_data, uint32 part_w, uint32 part_h)
{
    memset(inst, 0, sizeof(instance_t));
    int ret = 0;

    inst->map_data  = map_data;
    inst->shared    = shared;

    inst->tw = map_data->tiled_width;
    inst->th = map_data->tiled_height;

    /* Initialize map parts */
    inst->parts = calloc(part_w * part_h, sizeof(instance_part_t));
    if (!inst->parts)
        {ret = 3; goto fail;}

    inst->pw = part_w;
    inst->ph = part_h;

    int cw = inst->tw / MAP_CHUNK_W;
    int ch = inst->th / MAP_CHUNK_W;

    int num_h = cw / part_w + cw % part_w;
    int num_v = ch / part_h + ch % part_h;

    uint32 x, y, i;
    for (x = 0; x < part_w; ++x)
        for (y = 0; y < part_h; ++y)
        {
            i                           = y * part_w + x;
            inst->parts[i].cx           = x * num_h;
            inst->parts[i].cy           = y * num_v;
            inst->parts[i].cw           = cw / part_w;
            inst->parts[i].ch           = ch / part_h;
            inst->parts[i].runtime_id   = _running_inst_part_id++;
            inst->parts[i].inst         = inst;
            printf("World: created instance part with dims %u, %u, %u, %u.\n",
                inst->parts[i].cx, inst->parts[i].cy, inst->parts[i].cw,
                inst->parts[i].ch);
        }

    /* Initialize interest areas */
    inst->num_x_ias = inst->tw / IA_W + inst->tw % IA_W;
    inst->num_y_ias = inst->th / IA_W + inst->th % IA_W;
    inst->num_z_ias = MAP_CHUNK_T / IA_T + MAP_CHUNK_T % IA_T;
    uint num_ias = inst->num_x_ias * inst->num_y_ias * inst->num_z_ias;

    inst->int_areas = calloc(num_ias, sizeof(interest_area_t));
    if (!inst->int_areas)
        {ret = 4; goto fail;}

    for (uint i = 0; i < num_ias; ++i)
    {
        interest_area_t *ia = &inst->int_areas[i];
        darr_reserve(ia->players, 16);
        darr_reserve(ia->objs, 32);
    }

    inst->id = id;


    /* Load spawns */
    FILE *f = fopen(map_data->spawn_file_path, "r");
    if (!f && map_data->spawn_file_path)
    {
        printf("Error: failed to open spawn file %s for map %s.\n",
            map_data->spawn_file_path, map_data->name);
        ret = 4;
        goto fail;
    }

    char *line = 0;
    darr_reserve(line, 64);

    for (;;)
    {
        darr_clear(line);

        int c;
        while ((c = fgetc(f)) != '\n' && c != EOF)
            darr_push(line, (char)c);
        darr_push(line, 0);

        if (c == EOF && darr_num(line) == 1)
            break;

        char    *line_ptr   = line;
        char    *spawn_type = 0;
        char    *type_id    = 0;
        char    *position   = 0;
        wpos_t  wpos;

        while (!spawn_type || !type_id || !position)
        {
            char    *begin = line_ptr;
            bool32  do_break;

            /* Find the comma - each line is a CSV list */
            for (; *line_ptr != ',' && *line_ptr; ++line_ptr);
            do_break = *line_ptr ? 0 : 1;
            *line_ptr = 0;
            str_strip_trailing_spaces(begin);

            if (!strncmp(begin, "type_id = ", 10))
                type_id = begin + 10;
            else if (!strncmp(begin, "position = ", 11))
                position = begin + 11;
            else if (!strncmp(begin, "spawn_type = ", 13))
                spawn_type = begin + 13;
            if (do_break)
                break;
            line_ptr++;
        }

        if (spawn_type && type_id && position &&
            sscanf(position, "%d %d %hhd", &wpos.x, &wpos.y, &wpos.z) == 3)
        {
            char *c = type_id;
            for (; *c == ' '; ++c);
            char *end = c;
            for (; *end && *end != ' '; ++end);
            if (*end)
                *(end - 1) = 0;
            if (streq(spawn_type, "creature"))
            {
                creature_def_t *cd = wc_get_creature_def_by_str_id(c);
                if (cd)
                {
                    creature_spawn_t spawn;
                    spawn.type_id   = cd->id_num;
                    spawn.position  = wpos;
                    darr_push(inst->creature_spawns, spawn);
                    DEBUG_PRINTFF("Spawn: creature, type_id %s, "
                        "position %s\n", type_id, position);
                } else
                    printf("Spawns: no creature definition for '%s' found.\n",
                        c);
            } else
            if (streq(spawn_type, "dynamic object"))
            {
#if 0
                dynamic_obj_def_t *dd = wc_get_dynamic_obj_def(utype_id);
                if (dd)
                {
                    dynamic_obj_spawn_t spawn;
                    spawn.type_id   = utype_id;
                    spawn.position  = wpos;
                    darr_push(inst->dynamic_obj_spawns, spawn);
                    DEBUG_PRINTFF("Spawn: dynamic object, type_id %s, "
                        "position %s\n", type_id, position);
                } else
                    printf("Spawns: no dynamic object definition for %u "
                            "found.\n", utype_id);
#endif
            }
        } else
            printf("Invalid line in %s: spawn_type: %s, type_id: %s, "
                "position: %s.\n", map_data->spawn_file_path, spawn_type,
                    type_id, position);

        if (c == EOF)
            break;
    }

    darr_free(line);
    fclose(f);

    return 0;

    fail:
        printf("%s failed with code %d\n", __func__, ret);
        instance_destroy(inst);
        return ret;
}

void
instance_destroy(instance_t *inst)
{
    free_dynamic_str(inst->path);

    /* Destroy map parts */
    free(inst->parts);

    /* Destroy interest areas */
    uint num_ias = inst->num_y_ias * inst->num_x_ias * inst->num_z_ias;
    for (uint i = 0; i < num_ias; ++i)
    {
        darr_free(inst->int_areas[i].players);
        darr_free(inst->int_areas[i].objs);
    }

    free(inst->int_areas);
    darr_free(inst->creature_spawns);
    darr_free(inst->dynamic_obj_spawns);
    memset(inst, 0, sizeof(instance_t));
}

uint32
instance_get_relevant_interest_areas(instance_t *inst, int32 x, int32 y, int8 z,
    interest_area_t *ret_arr[27])
{
    int iax = x / IA_W;
    int iay = y / IA_W;
    int iaz = z / IA_T;

    int i, j, k;
    uint num_ias = 0;

    int fi = CLAMP(iax - 1, 0, (int)inst->num_x_ias - 1);
    int ei = CLAMP(iax + 1, 0, (int)inst->num_x_ias - 1);
    int fj = CLAMP(iay - 1, 0, (int)inst->num_y_ias - 1);
    int ej = CLAMP(iay + 1, 0, (int)inst->num_y_ias - 1);
    int fk = CLAMP(iaz - 1, 0, (int)inst->num_z_ias - 1);
    int ek = CLAMP(iaz + 1, 0, (int)inst->num_z_ias - 1);

    for (i = fi; i <= ei; ++i)
        for (j = fj; j <= ej; ++j)
            for (k = fk; k <= ek; ++k)
                ret_arr[num_ias++] = instance_get_interest_area(inst, i, j, k);
    return num_ias;
}

