#ifndef MUTA_SERVER_COMMON_H
#define MUTA_SERVER_COMMON_H
#include <stdio.h>
#define LOG(fmt_, ...) printf(fmt_ "\n", ##__VA_ARGS__)
#define LOGF(fmt_, ...) printf("%s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#ifdef _MUTA_DEBUG
    #define DEBUG_LOG(fmt_, ...) printf("[DEBUG] " fmt_ "\n", ##__VA_ARGS__)
    #define DEBUG_LOGF(fmt_, ...) \
        printf("[DEBUG] %s: " fmt_ "\n", __func__, ##__VA_ARGS__)
#else
    #define DEBUG_LOG(fmt_, ...) ((void)0)
    #define DEBUG_LOGF(fmt_, ...) ((void)0)
#endif
#endif /* MUTA_SERVER_COMMON_H */
