
typedef struct daemon_t daemon_t;

struct daemon_t
{
    daemon_t            *next;
    w_client_t          *c; /* null if not connected */
    instance_part_t     *inst_parts;
    dynamic_bbuf_t      msgs_in;
    dynamic_bbuf_t      msgs_out;
    w_timer_darr_t      *timers;
};
/* Represents a single worldd process' state */
