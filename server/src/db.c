#include "db.h"
#include "character.h"
#include "../../shared/containers.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_time.h"
#include "../../shared/crypt.h"
#include "../../shared/net.h"
#include "../../shared/sv_common_defs.h"
#include "../../shared/acc_utils.h"
#include "../../shared/rwbits.inl"
#include "../../shared/db_packets.h"
#include <inttypes.h>

#define NUM_INITIAL_DB_QUERIES  512
#define DB_ID_MAX               0xFFFFFFFF
typedef struct db_qcontainer_t  db_qcontainer_t;
typedef struct db_timer_t       db_timer_t;
typedef db_timer_t              db_timer_darr_t;
typedef struct db_cmd_t         db_cmd_t;

enum db_cmd_codes
{
    DB_CMD_LOG,
    DB_CMD_CANCEL,
    DB_CMD_CONFIRM
};

struct db_qcontainer_t
{
    uint32      timer_index;
    db_query_t  query;
};

struct db_timer_t
{
    uint32 id;
    uint64 start_time;
};

struct db_cmd_t
{
    int opcode;
    union
    {
        struct {uint32 id; db_query_t q;}                           log;
        struct {uint32 id;}                                         cancel;
        struct {uint32 id; int num_bytes; uint8 sv_claimed_type;}   confirm;
    } d;
    int     offset, num_bytes;
};

DYNAMIC_HASH_TABLE_DEFINITION(db_qcontainer_table, db_qcontainer_t, uint32,
    uint32, HASH_FROM_NUM, 2);
DOUBLE_VAR_CMD_BUF_DEFINITION(db_cmd_buf, db_cmd_t);
DOUBLE_VAR_CMD_BUF_DEFINITION(db_event_buf, db_query_event_t);

static spinlock_t               _db_id_spinlock;
static db_qcontainer_table_t    _db_table;
static db_timer_darr_t          *_db_timers;
static db_event_buf_t           _db_events;
static uint32                   _db_running_id;
static uint64                   _db_timeout;
static db_cmd_buf_t             _db_cmds;
static addr_t                   _db_sv_addr;
static bool32                   _db_running;
static int32                    _db_need_dc;

static struct
{
    int                 status;
    socket_t            fd;
    thread_t            thread;
    cryptchan_t         cryptchan;
    double_msg_buf_t    msgs_out;
    double_msg_buf_t    msgs_in;
    mutex_t             msgs_in_mtx;
    bool32              authed;
    bool32              printed_connecting;
    fd_set              fds;
} _db_conn;

static struct
{
    db_query_result_darr_t  *results;
    segfit_stack_t          stack;
} _db_query_results;

static int
_db_init_msg_buffers();

static int
_db_init_query_results();

static int
_db_log_query(db_query_t *q, uint32 *ret_id);
/* ret_id can be null */

static int
_db_cancel_query(uint32 id);
/* Cancel a query. No events will be generated */

static int
_db_confirm_query(uint32 id, uint8 sv_claimed_type, db_query_t *ret_q);
/* Confirm a query has been received, removing the timer for it. */

static int
_db_read_msgs();

static int
_db_execute_cmds();

static int
_db_update_timers();

static int
_db_send_msgs();

static void
_db_remove_timer(uint32 timer_index);

static int
_db_handle_cmd_log(db_cmd_t *cmd);

static int
_db_handle_cmd_cancel(db_cmd_t *cmd);

static int
_db_handle_cmd_confirm(db_cmd_t *cmd);

static void
_db_disconnect_deferred();

static thread_ret_t
_db_thread_callback(void *args);

static void
_db_recv();

static int
_db_read_packet(uint8 *pkt, int pkt_sz);

static inline byte_buf_t *
_db_begin_send_msg(int size);

static inline byte_buf_t *
_db_begin_send_var_encrypted_msg(int sz);

static inline byte_buf_t *
_db_begin_send_const_encrypted_msg(int sz);

#define _db_finalize_send_msg() \
    double_msg_buf_finalize_write(&_db_conn.msgs_out)

static inline int
_handle_dbmsg_pub_key(dbmsg_pub_key_t *s);

static inline int
_handle_dbmsg_stream_header(dbmsg_stream_header_t *s);

static inline int
_handle_dbmsg_login_result(dbmsg_login_result_t *s);

static inline int
_handle_fdbmsg_reply_login_account(fdbmsg_reply_login_account_t *s);

static inline int
_handle_fdbmsg_reply_create_character(fdbmsg_reply_create_character_t *s);

static inline int
_handle_fdbmsg_confirm_set_character_position(
    fdbmsg_confirm_set_character_position_t *s);

static inline int
_handle_fdbmsg_reply_account_characters(
    fdbmsg_reply_account_characters_t *s);

int
db_init(uint64 timeout, addr_t *db_server_addr)
{
    darr_reserve(_db_timers, NUM_INITIAL_DB_QUERIES);
    if (db_event_buf_init(&_db_events, 128 * sizeof(db_query_event_t),
        128 * sizeof(uint32)))
        return 2;
    if (db_qcontainer_table_init(&_db_table, NUM_INITIAL_DB_QUERIES))
        return 3;
    if (db_cmd_buf_init(&_db_cmds, 256, 256 * sizeof(uint32)))
        return 4;
    if (_db_init_msg_buffers())
        return 5;
    if (_db_init_query_results())
        return 6;

    spinlock_init(&_db_id_spinlock);
    _db_timeout   = timeout;

    if (db_server_addr)
        _db_sv_addr = *db_server_addr;
    else
        _db_sv_addr = create_addr(127, 0, 0, 1, DEFAULT_DB_PORT);

    return 0;
}

static int
_db_log_query(db_query_t *q, uint32 *ret_id)
{
    db_cmd_t cmd;
    cmd.opcode  = DB_CMD_LOG;
    cmd.d.log.q = *q;

    spinlock_lock(&_db_id_spinlock);
    uint32 id           = _db_running_id;
    cmd.d.log.id        = id;
    _db_running_id      = id != DB_ID_MAX ? id + 1 : 0;
    spinlock_unlock(&_db_id_spinlock);

    *ret_id = id;
    return !db_cmd_buf_push(&_db_cmds, &cmd) ? 0 : DB_ERR_NOMEM;
}

int
db_update()
{
    if (interlocked_compare_exchange_int32(&_db_need_dc, 0, 1))
        db_disconnect();
    int ret = 0;
    ret |= _db_read_msgs()      ? (1 << 0) : 0;
    ret |= _db_execute_cmds()   ? (1 << 1) : 0;
    ret |= _db_update_timers()  ? (1 << 2) : 0;
    ret |= _db_send_msgs()      ? (1 << 3) : 0;
    return ret;
}

db_query_event_t *
db_events(uint32 *num)
    {return db_event_buf_swap(&_db_events, num);}

db_query_result_t *
db_get_query_results(uint32 *num)
{
    *num = darr_num(_db_query_results.results);
    darr_clear(_db_query_results.results);
    if (*num)
        segfit_stack_clear(&_db_query_results.stack);
    return _db_query_results.results;
}

void
db_clear_events()
    {db_event_buf_clear_read(&_db_events);}

int
db_conn_status()
    {return _db_conn.status;}

bool32
db_ok()
    {return _db_conn.status == DB_CONN_CONNECTED && _db_conn.authed == 1;}

int
db_connect()
{
    if (_db_conn.status != DB_CONN_DISCONNECTED)
        return 1;

    _db_conn.authed     = 0;
    _db_conn.fd         = net_tcp_ipv4_sock();

    if (_db_conn.fd == KSYS_INVALID_SOCKET)
        return 2;

    if (net_make_sock_reusable(_db_conn.fd) ||  net_disable_nagle(_db_conn.fd))
        {net_shutdown_sock(_db_conn.fd, SOCKSD_BOTH); return 3;}

    _db_running = 1;
    _db_conn.status = DB_CONN_CONNECTING;
    interlocked_exchange_int32(&_db_need_dc, 0);

    if (!_db_conn.printed_connecting)
    {
        puts("db: attempting to connect...");
        _db_conn.printed_connecting = 1;
    }

    if (thread_create(&_db_conn.thread, _db_thread_callback, 0))
        {_db_conn.status = DB_CONN_DISCONNECTED; return 4;}

    return 0;
}

void
db_disconnect()
{
    if (_db_conn.status == DB_CONN_DISCONNECTED) return;
    _db_running = 0;
    net_shutdown_sock(_db_conn.fd, SOCKSD_BOTH);
    thread_join(&_db_conn.thread);
    _db_conn.status             = DB_CONN_DISCONNECTED;
    _db_conn.printed_connecting = 0;
}

void *
db_get_query_event_data(db_query_event_t *ev)
{
    if (ev->num_bytes > 0)
        return db_event_buf_cmd_data(&_db_events, ev);
    return 0;
}

int
db_query_account_exists(const char *name, int name_len)
{
    tdbmsg_query_account_exists_t s;
    s.name_len  = (uint8)strlen(name);
    s.name      = name;

    db_query_t q;
    q.code = DB_QUERY_ACCOUNT_EXISTS;
    if (_db_log_query(&q, &s.query_id))
        return 1;

    byte_buf_t *bb = _db_begin_send_var_encrypted_msg(
        TDBMSG_QUERY_ACCOUNT_EXISTS_COMPUTE_SZ(s.name_len));

    if (!bb)
    {
        _db_cancel_query(s.query_id);
        return 2;
    }

    tdbmsg_query_account_exists_write_var_encrypted(bb, &_db_conn.cryptchan,
        &s);
    _db_finalize_send_msg();
    return 0;
}

int
db_query_login_account(uint32 client_id, const char *name, uint8 name_len,
    const char *pw, uint8 pw_len)
{
    db_query_t q;
    q.code          = DB_QUERY_LOGIN_ACCOUNT;
    q.d.client_id   = client_id;

    tdbmsg_query_login_account_t s;
    s.name_len              = name_len;
    s.pw_len                = pw_len;
    s.create_if_not_exists  = 1;
    s.name                  = name;
    s.pw                    = pw;

    if (_db_log_query(&q, &s.query_id)) return 1;
    bbuf_t *bb = _db_begin_send_var_encrypted_msg(
        TDBMSG_QUERY_LOGIN_ACCOUNT_COMPUTE_SZ(name_len, pw_len));
    if (!bb) {_db_cancel_query(s.query_id); return 2;}
    tdbmsg_query_login_account_write_var_encrypted(bb, &_db_conn.cryptchan, &s);
    _db_finalize_send_msg();
    return 0;
}

int
db_query_create_character(character_props_t *cp)
{
    db_query_t q;
    q.code          = DB_QUERY_CREATE_CHARACTER;
    q.d.acc_id      = cp->account_id;

    tdbmsg_insert_create_character_t s;
    s.name_len      = (uint8)strlen(cp->name);
    s.acc_id        = cp->account_id;
    s.x             = cp->position.x;
    s.y             = cp->position.y;
    s.z             = cp->position.z;
    s.map_id        = cp->map_id;
    s.instance_id   = cp->instance_id;
    s.race          = cp->race;
    s.sex           = cp->sex;
    s.name          = cp->name;

    if (_db_log_query(&q, &s.query_id)) return 1;
    bbuf_t *bb = _db_begin_send_var_encrypted_msg(
        TDBMSG_INSERT_CREATE_CHARACTER_COMPUTE_SZ(s.name_len));
    if (!bb)
    {
        _db_cancel_query(s.query_id);
        return 2;
    }
    tdbmsg_insert_create_character_write_var_encrypted(bb,
        &_db_conn.cryptchan, &s);
    _db_finalize_send_msg();
    return 0;
}

int
db_update_char_pos(uint64 char_id, int32 x, int32 y, int8 z)
{
    db_query_t q;
    q.code = DB_UPDATE_CHARACTER_POSITION;

    tdbmsg_update_character_position_t s;
    s.character_id = char_id;
    s.x = x;
    s.y = y;
    s.z = z;

    if (_db_log_query(&q, &s.query_id))
        return DB_ERR_NOMEM;
    bbuf_t *bb = _db_begin_send_const_encrypted_msg(
        TDBMSG_UPDATE_CHARACTER_POSITION_SZ);
    if (!bb) {_db_cancel_query(s.query_id); return DB_ERR_NOMEM;}
    tdbmsg_update_character_position_write_const_encrypted(bb,
        &_db_conn.cryptchan, &s);
    _db_finalize_send_msg();
    return 0;
}

int
db_query_account_characters(uint64 acc_id, uint8 max)
{
    db_query_t q;
    q.code      = DB_QUERY_ACCOUNT_CHARACTERS;
    q.d.acc_id  = acc_id;

    tdbmsg_query_account_characters_t s;
    s.account_id    = acc_id;
    s.max           = max;

    if (_db_log_query(&q, &s.query_id))
        return DB_ERR_NOMEM;

    bbuf_t *bb = _db_begin_send_const_encrypted_msg(
        TDBMSG_QUERY_ACCOUNT_CHARACTERS_SZ);
    if (!bb)
    {
        _db_cancel_query(s.query_id);
        return DB_ERR_NOMEM;
    }
    tdbmsg_query_account_characters_write_const_encrypted(bb,
        &_db_conn.cryptchan, &s);
    _db_finalize_send_msg();
    return 0;
}

static int
_db_init_msg_buffers()
{
    if (double_msg_buf_init(&_db_conn.msgs_out, MUTA_MTU * 2))
        return 1;
    if (double_msg_buf_init(&_db_conn.msgs_in,  MUTA_MTU * 2))
        return 2;
    mutex_init(&_db_conn.msgs_in_mtx);
    return 0;
}

static int
_db_init_query_results()
{
    segfit_stack_init(&_db_query_results.stack, 256);
    darr_reserve(_db_query_results.results, 256);
    return 0;
}

static int
_db_cancel_query(uint32 id)
{
    db_cmd_t cmd;
    cmd.opcode      = DB_CMD_CANCEL;
    cmd.d.cancel.id = id;
    return db_cmd_buf_push(&_db_cmds, &cmd) != 0;
}

static int
_db_confirm_query(uint32 id, uint8 sv_claimed_type, db_query_t *ret_q)
{
    db_qcontainer_t qc;
    if (db_qcontainer_table_try_pop(&_db_table, id, &qc))
        return DB_ERR_MISUSE;
    if (qc.query.code != sv_claimed_type)
        return DB_ERR_MISUSE;
    _db_remove_timer(qc.timer_index);
    if (ret_q)
        *ret_q = qc.query;
    return 0;
}

static int
_db_read_msgs()
{
    bbuf_t *bb = double_msg_buf_swap(&_db_conn.msgs_in);
    if (!bb->num_bytes)
        return 0;
    int left = _db_read_packet(bb->mem, bb->num_bytes);
    if (left >= 0)
        bbuf_cut_portion(bb, 0, bb->num_bytes - left);
    else if (left < 0)
        _db_disconnect_deferred();
    return 0;
}

static int
_db_execute_cmds()
{
    uint32      num;
    db_cmd_t    *cmds   = db_cmd_buf_swap(&_db_cmds, &num);
    int         ret     = 0;
    db_cmd_t    *cmd;

    for (uint32 i = 0; i < num; ++i)
    {
        cmd = &cmds[i];
        switch (cmd->opcode)
        {
        case DB_CMD_LOG:
            ret |= _db_handle_cmd_log(cmd);
            break;
        case DB_CMD_CANCEL:
            ret |= _db_handle_cmd_cancel(cmd);
            break;
        case DB_CMD_CONFIRM:
            ret |= _db_handle_cmd_confirm(cmd);
            break;
        default:
            muta_assert(0);
        }
    }

    db_cmd_buf_clear_read(&_db_cmds);
    return ret;
}

static int
_db_update_timers()
{
    int             ret         = 0;
    uint64          cur_time    = get_program_ticks_ms();
    uint64          time_passed;
    db_timer_t    *t;

    for (uint32 i = 0; i < darr_num(_db_timers); ++i)
    {
        t = &_db_timers[i];
        time_passed = cur_time - t->start_time;
        if (time_passed < _db_timeout)
            continue;
        db_query_event_t ev;
        ev.type     = DB_EVENT_QUERY_TIMEOUT;
        ev.query    = db_qcontainer_table_pop(&_db_table, t->id).query;

        if (!db_event_buf_push(&_db_events, &ev))
            darr_erase(_db_timers, i);
        else
            ret |= DB_ERR_NOMEM;
    }
    return ret;
}

static int
_db_send_msgs()
{
    if (_db_conn.status != DB_CONN_CONNECTED) return 0;
    byte_buf_t *bb = double_msg_buf_swap(&_db_conn.msgs_out);
    if (send_all_from_bbuf(_db_conn.fd, bb, MUTA_MTU))
        return DB_ERR_MISUSE;
    return 0;
}

static void
_db_remove_timer(uint32 index)
{
    db_timer_t *ta = &_db_timers[index];
    db_timer_t *tb = &_db_timers[darr_num(_db_timers) - 1];
    if (ta != tb)
    {
        db_qcontainer_t *qb = db_qcontainer_table_get_ptr(&_db_table, tb->id);
        qb->timer_index = index;
        *ta             = *tb;
    }
    darr_head(_db_timers)->num--;
}

static int
_db_handle_cmd_log(db_cmd_t *cmd)
{
    uint32 id       = cmd->d.log.id;
    db_query_t *q   = &cmd->d.log.q;

    db_qcontainer_t *c = db_qcontainer_table_insert_empty(&_db_table, id);
    if (!c) return DB_ERR_NOMEM;

    c->query        = *q;
    c->timer_index  = darr_num(_db_timers);

    db_timer_t *t = darr_push_empty(_db_timers);
    if (!t)
    {
        db_qcontainer_table_erase(&_db_table, id);
        return DB_ERR_NOMEM;
    }

    t->start_time   = get_program_ticks_ms();
    t->id           = id;
    return 0;
}

static int
_db_handle_cmd_cancel(db_cmd_t *cmd)
{
    uint32 id = cmd->d.cancel.id;
    db_qcontainer_t qc;
    if (db_qcontainer_table_try_pop(&_db_table, id, &qc))
        return DB_ERR_MISUSE;
    _db_remove_timer(qc.timer_index);
    return 0;
}

static int
_db_handle_cmd_confirm(db_cmd_t *cmd)
{
    uint32 id = cmd->d.confirm.id;
    db_qcontainer_t qc;
    if (db_qcontainer_table_try_pop(&_db_table, id, &qc))
        return DB_ERR_MISUSE;
    _db_remove_timer(qc.timer_index);

    db_query_event_t ev;
    ev.type     = DB_EVENT_QUERY_COMPLETED;
    ev.query    = qc.query;
    /* Add query here */
    int r = db_event_buf_push_data(&_db_events, &ev,
        db_cmd_buf_cmd_data(&_db_cmds, cmd), cmd->num_bytes);
    return !r ? 0 : DB_ERR_NOMEM;
}

static void
_db_disconnect_deferred()
{
    _db_running = 0;
    interlocked_exchange_int32(&_db_need_dc, 1);
}

static thread_ret_t
_db_thread_callback(void *args)
{
    (void)args;

    int code;
    int r = net_connect(_db_conn.fd, _db_sv_addr);

    if (!r)
    {
        puts("db: connected. Beginning handshake...");
        if (cryptchan_init(&_db_conn.cryptchan, 0))
            {code = 2; goto err;}

        /* Send our public key to the db server. */
        byte_buf_t *bb = _db_begin_send_msg(DBMSG_PUB_KEY_SZ);

        if (bb)
        {
            dbmsg_pub_key_t s;
            memcpy(s.key, _db_conn.cryptchan.pk, CRYPTCHAN_PUB_KEY_SZ);
            dbmsg_pub_key_write(bb, &s);
            _db_finalize_send_msg();
        } else
            {code = 3; goto err;}

        _db_conn.status = DB_CONN_CONNECTED;
    } else
        {code = 1; goto err;}

    while (_db_running && _db_conn.status == DB_CONN_CONNECTED)
        _db_recv();

    printf("%s: returning successfully.\n", __func__);
    return 0;

    err:
        (void)code;
        net_shutdown_sock(_db_conn.fd, SOCKSD_BOTH);
        _db_conn.status = DB_CONN_DISCONNECTED;
        return 0;
}

static void
_db_recv()
{
    static uint8 buf[4096];
    int num_bytes = net_recv(_db_conn.fd, buf, 4096);
    if (num_bytes > 0)
    {
        double_msg_buf_write(&_db_conn.msgs_in, buf, num_bytes);
#if 0
        _db_conn.msgs_in.num_bytes += num_bytes;

        int left = _db_read_packet(_db_conn.msgs_in.mem,
            _db_conn.msgs_in.num_bytes);

        if (left >= 0)
        {
            bbuf_cut_portion(&_db_conn.msgs_in, 0,
                _db_conn.msgs_in.num_bytes - left);
        } else if (left < 0)
            _db_disconnect_deferred();
#endif
    } else
        _db_disconnect_deferred();
}

static int
_db_read_packet(uint8 *pkt, int pkt_sz)
{
    byte_buf_t bb = BBUF_INITIALIZER(pkt, pkt_sz);

    dbmsg_type_t   type;
    int             incomplete  = 0;
    int             dc          = 0;

    while (BBUF_FREE_SPACE(&bb) >= DBMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, dbmsg_type_t, &type);

        switch (type)
        {
        case DBMSG_PUB_KEY:
        {
            DEBUG_PUTS("DBMSG_PUB_KEY");
            dbmsg_pub_key_t s;
            incomplete = dbmsg_pub_key_read(&bb, &s);
            if (!incomplete) dc = _handle_dbmsg_pub_key(&s);
        }
            break;
        case DBMSG_STREAM_HEADER:
        {
            DEBUG_PUTS("DBMSG_STREAM_HEADER");
            dbmsg_stream_header_t s;
            incomplete = dbmsg_stream_header_read(&bb, &s);
            if (!incomplete) dc = _handle_dbmsg_stream_header(&s);
        }
            break;
        case DBMSG_LOGIN_RESULT:
        {
            DEBUG_PUTS("DBMSG_LOGIN_RESULT");
            dbmsg_login_result_t s;
            incomplete = dbmsg_login_result_read_const_encrypted(&bb,
                &_db_conn.cryptchan, &s);
            if (!incomplete) dc = _handle_dbmsg_login_result(&s);
        }
            break;
        case FDBMSG_REPLY_LOGIN_ACCOUNT:
        {
            DEBUG_PUTS("FDBMSG_REPLY_LOGIN_ACCOUNT");
            fdbmsg_reply_login_account_t s;
            incomplete = fdbmsg_reply_login_account_read_const_encrypted(
                &bb, &_db_conn.cryptchan, &s);
            if (!incomplete) dc = _handle_fdbmsg_reply_login_account(&s);
        }
            break;
        case FDBMSG_REPLY_CREATE_CHARACTER:
        {
            DEBUG_PUTS("FDBMSG_REPLY_CREATE_CHARACTER");
            fdbmsg_reply_create_character_t s;
            incomplete = fdbmsg_reply_create_character_read(&bb, &s);
            if (!incomplete) dc = _handle_fdbmsg_reply_create_character(&s);
        }
            break;
        case FDBMSG_CONFIRM_SET_CHARACTER_POSITION:
        {
            DEBUG_PUTS("FDBMSG_CONFIRM_SET_CHARACTER_POSITION");
            fdbmsg_confirm_set_character_position_t s;
            incomplete = fdbmsg_confirm_set_character_position_read(&bb,
                &s);
            if (!incomplete)
                dc = _handle_fdbmsg_confirm_set_character_position(&s);
        }
            break;
        case FDBMSG_REPLY_ACCOUNT_CHARACTERS:
        {
            DEBUG_PUTS("FDBMSG_REPLY_ACCOUNT_CHARACTERS");
            fdbmsg_reply_account_characters_t s;
            incomplete = fdbmsg_reply_account_characters_read(&bb, &s);
            if (!incomplete) dc = _handle_fdbmsg_reply_account_characters(&s);
        }
            break;
        default:
            dc = 1;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: invalid packet, type %d, dc %d, "
                "incomplete %d.\n", __func__, (int)type, dc, incomplete);
            return -1;
        }
    }

    return BBUF_FREE_SPACE(&bb);
}

static byte_buf_t *
_db_begin_send_msg(int sz)
{
    int tot_sz = DBMSGTSZ + sz;
    return double_msg_buf_begin_write(&_db_conn.msgs_out, tot_sz);
}

static inline byte_buf_t *
_db_begin_send_var_encrypted_msg(int sz)
{
    int tot_sz = DBMSGTSZ + sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + sz;
    return double_msg_buf_begin_write(&_db_conn.msgs_out, tot_sz);
}

static inline byte_buf_t *
_db_begin_send_const_encrypted_msg(int sz)
{
    int tot_sz = DBMSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES + sz;
    return double_msg_buf_begin_write(&_db_conn.msgs_out, tot_sz);
}

static inline int
_handle_dbmsg_pub_key(dbmsg_pub_key_t *s)
{
    puts("Received db public key.");
    dbmsg_stream_header_t fwd;

    if (cryptchan_cl_store_pub_key(&_db_conn.cryptchan, s->key, fwd.header))
        {puts("Db public key was invalid!"); return 1;}

    bbuf_t *bb = _db_begin_send_msg(DBMSG_STREAM_HEADER_SZ);
    if (!bb) return 1;
    dbmsg_stream_header_write(bb, &fwd);
    _db_finalize_send_msg();
    puts("Sent stream header to db.");
    return 0;
}

static inline int
_handle_dbmsg_stream_header(dbmsg_stream_header_t *s)
{
    puts("Received db stream header.");

    if (cryptchan_store_stream_header(&_db_conn.cryptchan, s->header))
        {puts("Db stream header was invalid!"); return 1;}
    puts("Database connection is now encrypted.");

    dbmsg_login_t fwd;
    fwd.client_type = 0;
    fwd.name        = "muta";
    fwd.pw          = "muta";
    fwd.name_len    = 4;
    fwd.pw_len      = 4;

    bbuf_t *bb = _db_begin_send_var_encrypted_msg(
        DBMSG_LOGIN_COMPUTE_SZ(4, 4));

    if (bb)
    {
        dbmsg_login_write_var_encrypted(bb, &_db_conn.cryptchan, &fwd);
        _db_finalize_send_msg();
    } else
        return 2;

    printf("Sent system login message to db-server as user %s.\n", fwd.name);
    return 0;
}

static inline int
_handle_dbmsg_login_result(dbmsg_login_result_t *s)
{
    int ret;
    switch (s->code)
    {
    case SYS_LOGIN_OK:
        puts("Successfully logged in to database.");
        _db_conn.authed = 1;
        ret = 0;
        break;
    case SYS_LOGIN_REJECTED:
        puts("Database rejected login.");
        ret = 1;
        break;
    default:
        puts("Database login result: invalid value.");
        ret = 2;
    }
    return ret;
}

static inline int
_handle_fdbmsg_reply_login_account(fdbmsg_reply_login_account_t *s)
{
    db_query_result_t res;
    if (_db_confirm_query(s->query_id, DB_QUERY_LOGIN_ACCOUNT, &res.query))
        return 0;
    res.data.login_account.result       = s->result;
    res.data.login_account.account_id   = s->account_id;
    darr_push(_db_query_results.results, res);
    return 0;
}

static inline int
_handle_fdbmsg_reply_create_character(fdbmsg_reply_create_character_t *s)
{
    db_query_result_t res;
    if (_db_confirm_query(s->query_id, DB_QUERY_CREATE_CHARACTER, &res.query))
        return 0;

    int error = (int)s->error;
    res.data.create_character.error = (uint8)error;
    character_props_t *cp =  segfit_stack_malloc(&_db_query_results.stack,
        sizeof(character_props_t));
    res.data.create_character.character_props = cp;

    if (!error)
    {
        memcpy(cp->name, s->name, s->name_len);
        cp->account_id  = res.query.d.acc_id;
        cp->name[s->name_len] = 0;
        cp->name_len    = s->name_len;
        cp->id          = s->id;
        cp->race        = s->race;
        cp->sex         = s->sex;
        cp->map_id      = s->map_id;
        cp->instance_id = s->map_id;
        cp->position.x  = s->x;
        cp->position.y  = s->y;
        cp->position.z  = s->z;
    } else
    {
        memset(cp, 0, sizeof(*cp));
    }

    darr_push(_db_query_results.results, res);
    return 0;
}

static inline int
_handle_fdbmsg_confirm_set_character_position(
    fdbmsg_confirm_set_character_position_t *s)
    {return _db_confirm_query(s->query_id, DB_UPDATE_CHARACTER_POSITION, 0);}

static inline int
_handle_fdbmsg_reply_account_characters(fdbmsg_reply_account_characters_t *s)
{
    db_query_result_t res;
    if (_db_confirm_query(s->query_id, DB_QUERY_ACCOUNT_CHARACTERS,
        &res.query))
        return 0;

    uint32 num_chars = (uint32)s->ids_len;

    /* Sanity check */
    if (s->races_len != num_chars || s->sexes_len != num_chars
    ||  s->map_ids_len != num_chars || s->instance_ids_len != num_chars
    ||  s->name_indices_len != num_chars || s->xs_len != num_chars
    || s->ys_len != num_chars || s->zs_len != num_chars)
        return 1;


    uint32 num_names = 0;

    for (uint32 i = 0; i < s->names_len; ++i)
        if (!s->names[i])
            num_names++;

    if (num_names != num_chars)
        return 3;

    for (uint32 i = 0; i < s->name_indices_len; ++i)
        if (s->name_indices[i] >= s->names_len)
            return 2;

    character_props_t *all_props = segfit_stack_malloc(&_db_query_results.stack,
        num_chars * sizeof(character_props_t));
    character_props_t   *cp;
    const char          *name;
    uint32              name_len;

    for (uint32 i = 0; i < num_chars; ++i)
    {
        cp          = &all_props[i];
        name        = s->names + s->name_indices[i];
        name_len    = (uint32)strlen(name);

        if (name_len < MIN_CHARACTER_NAME_LEN
        ||  name_len > MAX_CHARACTER_NAME_LEN)
            return 4;

        memcpy(cp->name, name, name_len);
        cp->name[name_len]  = 0;
        cp->name_len        = name_len;

        cp->id          = s->ids[i];
        cp->account_id  = res.query.d.acc_id;
        cp->race        = s->races[i];
        cp->sex         = s->sexes[i];
        cp->instance_id = s->instance_ids[i];
        cp->map_id      = s->map_ids[i];
        cp->position.x  = s->xs[i];
        cp->position.y  = s->ys[i];
        cp->position.z  = s->zs[i];

        DEBUG_PRINTFF("Character: %s, id %" PRIu64 ".\n", cp->name, cp->id);
    }

    res.data.account_characters.character_props     = all_props;
    res.data.account_characters.num_character_props = num_chars;

    darr_push(_db_query_results.results, res);
    return 0;
}
