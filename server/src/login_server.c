#include "login_server.h"
#include "server.h"
#include "common.h"
#include "proxy.h"
#include "../../shared/netpoll.h"
#include "../../shared/net.h"
#include "../../shared/login_packets.h"
#include "../../shared/common_defs.h"
#include "../../shared/acc_utils.h"

#define IN_BUF_SIZE     (MUTA_MTU * 4)
#define OUT_BUF_SIZE    (MUTA_MTU * 4)

typedef struct client_data_t client_data_t;

struct client_data_t
{
    uint64  account_id;
    uint32  ip;
    uint8   token[AUTH_TOKEN_SZ];
};

static thread_t         _thread;
static socket_t         _socket;
static cryptchan_t      _cryptchan;
static bool32           _running;
static bool32           _should_disconnect;
static blocking_queue_t _msg_queue;
static bool32           _authed;
static char             _password[MAX_LOGIN_SERVER_PW_LEN];

static struct
{
    uint8   memory[MUTA_MTU * 4];
    int     num_bytes;
} _out_buf;

static struct
{
    uint8   mem[IN_BUF_SIZE];
    int     num_bytes;
} _in_buf;

static thread_ret_t
_main(void *args);

static socket_t
_connect();

static void
_read_in_buf_unauthed();

static void
_read_in_buf_authed();

static int
_flush_out_buf();

static bbuf_t
_send_msg_to_login_server(int num_bytes);

static inline bbuf_t
_send_const_crypt_msg_to_login_server(int num_bytes);

static inline bbuf_t
_send_var_crypt_msg_to_login_server(int num_bytes);

static int
_handle_lmsg_stream_header(lmsg_stream_header_t *s);

static int
_handle_lmsg_pub_key(lmsg_pub_key_t *s);

static int
_handle_flmsg_login_result(flmsg_login_result_t *s);

static int
_handle_flmsg_player_select_shard(flmsg_player_select_shard_t *s);

static int
_handle_flmsg_player_cancel_select_shard(flmsg_player_cancel_select_shard_t *s);

int
ls_init()
{
    int ret = 0;
    if (thread_init(&_thread))
        {ret = 1; goto fail;}
    strcpy(_password, "password");
    blocking_queue_init(&_msg_queue, IN_BUF_SIZE, 1);
    return ret;
    fail:
        DEBUG_PRINTFF("Failed with code %d.\n", ret);
        return ret;
}

void
ls_destroy()
{
    thread_destroy(&_thread);
    blocking_queue_destroy(&_msg_queue);
}

int
ls_start()
{
    int ret = 0;
    _socket             = KSYS_INVALID_SOCKET;
    _running            = 1;
    _should_disconnect  = 0;
    _authed             = 0;
    if (thread_create(&_thread, _main, 0))
        {ret = 1; goto fail;}
    return ret;
    fail:
        DEBUG_PRINTFF("Failed with code %d.\n", ret);
        return ret;
}

void
ls_stop()
{
    _running = 0;
    thread_join(&_thread);
}

bool32
ls_is_connected()
    {return _socket != KSYS_INVALID_SOCKET;}

void
ls_update()
{
    if (_socket == KSYS_INVALID_SOCKET)
        return;
    if (_should_disconnect)
    {
        net_close_blocking_sock(_socket);
        _socket             = KSYS_INVALID_SOCKET;
        _should_disconnect  = 0;
        _authed             = 0;
        _out_buf.num_bytes  = 0;
        LOGF("Disconnected from login server.");
        return;
    }
    uint32 max_bytes = IN_BUF_SIZE - _in_buf.num_bytes;
    uint32 num_bytes = blocking_queue_pop_num(&_msg_queue, max_bytes,
        _in_buf.mem + _in_buf.num_bytes);
    if (num_bytes)
    {
        _in_buf.num_bytes += num_bytes;
        if (!_authed)
            _read_in_buf_unauthed();
        else
            _read_in_buf_authed();
    }
    if (_flush_out_buf())
        _should_disconnect = 1;
}

int
ls_send_player_select_shard_result(uint64 account_id, uint32 login_session_id,
    int result)
{
    if (_socket == KSYS_INVALID_SOCKET)
        return 1;
    tlmsg_player_select_shard_result_t r_msg;
    r_msg.account_id        = account_id;
    r_msg.login_session_id  = login_session_id;
    r_msg.result            = (uint8)result;
    bbuf_t bb = _send_const_crypt_msg_to_login_server(
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
    if (!bb.max_bytes)
    {
        _should_disconnect = 1;
        return 2;
    }
    int r = tlmsg_player_select_shard_result_write_const_encrypted(&bb,
        &_cryptchan, &r_msg);
    return r ? 3 : 0;
}

static thread_ret_t
_main(void *args)
{
    uint8   buf[MUTA_MTU];
    int     num_bytes;
    while (_running)
    {
        if (_socket == KSYS_INVALID_SOCKET &&
            (_socket = _connect()) == KSYS_INVALID_SOCKET)
        {
            DEBUG_PRINTF("Failed to connect to login server! Reconnecting in 2 "
                "sec...\n");
            sleep_ms(2000);
            continue;
        }
        if (_should_disconnect)
        {
            sleep_ms(32);
            continue;
        }
        if ((num_bytes = net_recv(_socket, buf, sizeof(buf))) <= 0)
        {
            DEBUG_PRINTFF("net_recv() returned %d, disconnecting.\n",
                num_bytes);
            _should_disconnect = 1;
            continue;
        }
        DEBUG_PRINTFF("Received %d bytes from login server.\n", num_bytes);
        blocking_queue_push_num(&_msg_queue, buf, num_bytes);
    }
    return 0;
}

static socket_t
_connect()
{
    DEBUG_PRINTFF("Connecting to login server on port %u...\n",
        sv_config.login_addr.port);
    int         err = 0;
    socket_t    s   = KSYS_INVALID_SOCKET;
    blocking_queue_clear(&_msg_queue);
    _in_buf.num_bytes = 0;
    if (cryptchan_init(&_cryptchan, 0))
        {err = 1; goto fail;}
    s = net_tcp_ipv4_sock();
    if (s == KSYS_INVALID_SOCKET)
        {err = 2; goto fail;}
    if (net_connect(s, sv_config.login_addr))
        {err = 3; goto fail;}
    if (net_disable_nagle(s))
        {err = 4; goto fail;}
    uint8   buf[LMSGTSZ + LMSG_PUB_KEY_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    lmsg_pub_key_t k_msg;
    memcpy(k_msg.key, _cryptchan.pk, CRYPTCHAN_PUB_KEY_SZ);
    lmsg_pub_key_write(&bb, &k_msg);
    if (net_send_all(s, bb.mem, bb.num_bytes) <= 0)
        {err = 6; goto fail;}
    DEBUG_PRINTF("Successfully connected to login server.\n");
    return s;
    fail:
        DEBUG_PRINTFF("Failed with code %d.\n", err);
        close_socket(s);
        return KSYS_INVALID_SOCKET;
}

static void
_read_in_buf_unauthed()
{
    bbuf_t      bb          = BBUF_INITIALIZER(_in_buf.mem, _in_buf.num_bytes);
    int         dc          = 0;
    int         incomplete  = 0;
    lmsg_type_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= LMSGTSZ && !incomplete && !dc)
    {
        BBUF_READ(&bb, lmsg_type_t, &msg_type);
        switch (msg_type)
        {
        case LMSG_STREAM_HEADER:
        {
            DEBUG_LOGF("LMSG_STREAM_HEADER");
            lmsg_stream_header_t s;
            incomplete = lmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_lmsg_stream_header(&s);
        }
            break;
        case LMSG_PUB_KEY:
        {
            DEBUG_LOGF("LMSG_PUB_KEY");
            lmsg_pub_key_t s;
            incomplete = lmsg_pub_key_read(&bb, &s);
            if (!incomplete)
                dc = _handle_lmsg_pub_key(&s);
        }
            break;
        case FLMSG_LOGIN_RESULT:
        {
            DEBUG_LOGF("FLMSG_LOGIN_RESULT");
            flmsg_login_result_t s;
            incomplete = flmsg_login_result_read(&bb, &s);
            if (!incomplete)
                dc = _handle_flmsg_login_result(&s);
        }
            break;
        default:
            dc = 1;
        }
    }
    if (dc || incomplete < 0)
    {
        LOGF("msg_type: %u, dc: %d, incomplete: %d.", msg_type, dc, incomplete);
        _should_disconnect = 1;
    } else
        _in_buf.num_bytes = BBUF_FREE_SPACE(&bb);
}

static void
_read_in_buf_authed()
{
    bbuf_t      bb          = BBUF_INITIALIZER(_in_buf.mem, _in_buf.num_bytes);
    int         dc          = 0;
    int         incomplete  = 0;
    lmsg_type_t msg_type;
    while (BBUF_FREE_SPACE(&bb) >= LMSGTSZ && !incomplete && !dc)
    {
        BBUF_READ(&bb, lmsg_type_t, &msg_type);
        switch (msg_type)
        {
        case FLMSG_PLAYER_SELECT_SHARD:
        {
            DEBUG_LOG("FLMSG_PLAYER_SELECT_SHARD");
            flmsg_player_select_shard_t s;
            incomplete = flmsg_player_select_shard_read_var_encrypted(&bb,
                &_cryptchan, &s);
            if (!incomplete)
                dc = _handle_flmsg_player_select_shard(&s);
        }
            break;
        case FLMSG_PLAYER_CANCEL_SELECT_SHARD:
        {
            DEBUG_LOG("FLMSG_PLAYER_CANCEL_SELECT_SHARD");
            flmsg_player_cancel_select_shard_t s;
            incomplete = flmsg_player_cancel_select_shard_read(&bb, &s);
            if (!incomplete)
                dc = _handle_flmsg_player_cancel_select_shard(&s);
        }
            break;
        default:
            dc = 1;
        }
    }
    if (dc || incomplete < 0)
    {
        LOGF("msg_type: %u, dc: %d, incomplete: %d", msg_type, dc, incomplete);
        _should_disconnect = 1;
    } else
        _in_buf.num_bytes = BBUF_FREE_SPACE(&bb);;
}

static int
_flush_out_buf()
{
    int num_left = _out_buf.num_bytes;
    while (num_left)
    {
        int num_to_send = MIN(MUTA_MTU, num_left);
        if (net_send_all(_socket,
            _out_buf.memory + _out_buf.num_bytes - num_left, num_to_send) <= 0)
            return 1;
        num_left -= num_to_send;
    }
    _out_buf.num_bytes = num_left;
    return 0;
}

static bbuf_t
_send_msg_to_login_server(int num_bytes)
{
    bbuf_t  ret         = {0};
    int     req_bytes   = LMSGTSZ + num_bytes;
    if (req_bytes > OUT_BUF_SIZE - _out_buf.num_bytes &&
        _flush_out_buf())
        return ret;
    muta_assert(OUT_BUF_SIZE - _out_buf.num_bytes >= req_bytes);
    BBUF_INIT(&ret, _out_buf.memory + _out_buf.num_bytes, req_bytes);
    _out_buf.num_bytes += req_bytes;
    return ret;
}

static inline bbuf_t
_send_const_crypt_msg_to_login_server(int num_bytes)
{
    return _send_msg_to_login_server(CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static inline bbuf_t
_send_var_crypt_msg_to_login_server(int num_bytes)
{
    return _send_msg_to_login_server(sizeof(msg_sz_t) +
        CRYPT_MSG_ADDITIONAL_BYTES + num_bytes);
}

static int
_handle_lmsg_stream_header(lmsg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&_cryptchan, s->header))
        return 1;
    if (!cryptchan_is_encrypted(&_cryptchan))
        return 2;
    tlmsg_login_request_t l_msg;
    l_msg.shard_name        = sv_config.shard_name;
    l_msg.shard_name_len    = (uint8)strlen(sv_config.shard_name);
    l_msg.password          = _password;
    l_msg.password_len      = (uint8)strlen(_password);
    uint8 buf[LMSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES +
        TLMSG_LOGIN_REQUEST_MAX_SZ];
    bbuf_t bb = BBUF_INITIALIZER(buf, sizeof(buf));
    if (tlmsg_login_request_write_var_encrypted(&bb, &_cryptchan, &l_msg))
        muta_assert(0);
    if (net_send_all(_socket, bb.mem, bb.num_bytes) <= 0)
        return 4;
    return 0;
}

static int
_handle_lmsg_pub_key(lmsg_pub_key_t *s)
{
    lmsg_stream_header_t h_msg;
    if (cryptchan_cl_store_pub_key(&_cryptchan, s->key, h_msg.header))
        return 1;
    uint8   buf[LMSGTSZ + LMSG_STREAM_HEADER_SZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    lmsg_stream_header_write(&bb, &h_msg);
    if (net_send_all(_socket, bb.mem, bb.num_bytes) <= 0)
        return 2;
    return 0;
}

static int
_handle_flmsg_login_result(flmsg_login_result_t *s)
{
    if (!cryptchan_is_encrypted(&_cryptchan))
        return 1;
    if (s->result)
    {
        DEBUG_PRINTFF("Logging in to login server failed!\n");
        return 2;
    }
    _authed = 1;
    DEBUG_PRINTFF("Successfully logged in to login server.\n");
    uint8   buf[LMSGTSZ];
    bbuf_t  bb = BBUF_INITIALIZER(buf, sizeof(buf));
    tlmsg_online_for_players_write(&bb);
    if (net_send_all(_socket, bb.mem, bb.num_bytes) <= 0)
        return 3;
    return 0;
}

static int
_handle_flmsg_player_select_shard(flmsg_player_select_shard_t *s)
{
    char account_name[MAX_ACC_NAME_LEN + 1];
    muta_assert(s->account_name_len <= MAX_ACC_NAME_LEN);
    memcpy(account_name, s->account_name, s->account_name_len);
    account_name[s->account_name_len] = 0;
    if (!accut_is_name_legaln(account_name, s->account_name_len))
        return 1;
    uint32 socket = proxy_new_socket(account_name, s->account_id,
        s->login_session_id, s->ip, s->token);
    if (socket != INVALID_PROXY_SOCKET)
        return 0;
    tlmsg_player_select_shard_result_t r_msg;
    r_msg.result            = 1;
    r_msg.account_id        = s->account_id;
    r_msg.login_session_id  = s->login_session_id;
    uint8 buf[LMSGTSZ + CRYPT_MSG_ADDITIONAL_BYTES +
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ];
    bbuf_t bb = BBUF_INITIALIZER(buf, sizeof(buf));
    tlmsg_player_select_shard_result_write_const_encrypted(&bb, &_cryptchan,
        &r_msg);
    if (net_send_all(_socket, bb.mem, bb.num_bytes) <= 0)
        return 3;
    return 0;
}

static int
_handle_flmsg_player_cancel_select_shard(flmsg_player_cancel_select_shard_t *s)
{
    proxy_cancel_socket(s->account_id, s->login_session_id);
    return 0;
}
