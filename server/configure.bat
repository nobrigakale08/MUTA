:: This script assumes ..\configure.bat has already been called

@echo off

set ERRORLEVEL=0
set base_dir=%cd%

::Print help
if "%~1"=="-h" goto print_help
if "%~1"=="--help" goto print_help

::Check architecture
set arch=%1
if %arch%==x86 goto do_configure
if %arch%==x64 goto do_configure
if not %arch%=="" goto bad_arch
set arch=x64

:do_configure
    cd ..
    call replace_arch.bat %base_dir%\Makefile %arch%
    cd %base_dir%

    if %ERRORLEVEL% neq 0 goto out

    nmake clean
    rmdir /S /Q rundir\muta-data
    if exist rundir\config.cfg      del rundir\config.cfg
    if exist rundir\libsodium.dll   del rundir\libsodium.dll

    cd rundir
    if exist ..\..\MUTA-Data   mklink /D /J muta-data   ..\..\MUTA-Data 
    cd ..

    if not exist build mkdir build

    copy ..\libs\windows\lib\%arch%\libsodium.dll rundir\libsodium.dll
    if ERRORLEVEL 1 goto bad_dll_path

    copy default_config.cfg rundir\config.cfg
    copy shared_instances.txt rundir\shared_instances.txt

    goto out

:bad_arch
    echo %~dp0%0: error, architecture must be x86 or x64
    goto out

:bad_dll_path
    echo %~dp0%0: a dll file was not found!
    goto out

:print_help
    echo Usage: %0 [architecture]
    echo Valid architectures: x86, x64
    echo Example: %0 x64
    goto out

:out
