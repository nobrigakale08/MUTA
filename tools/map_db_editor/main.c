#include "../../shared/muta_map_format.c"
#define _MUTA_COMMON_UTILS_NO_NET
#include "../../shared/common_utils.c"

static int
_create_new_db(char *name);

static int
_dump(char *fp);

static int
_insert(char *db_fp, char *name, char *map_fp);

static int
_delete(char *db_fp, char *id_opt, char *id_val);

static int
_increment_version_and_save(muta_map_db_t *db, char *fp);

static int
_find(char *db_fp, char *id_opt, char *id_val);

static int
_help();

int main(int argc, char **argv)
{
    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "--new") || streq(argv[i], "-n"))
        {
            if (argc == i + 2) return _create_new_db(argv[i + 1]);
        } else
        if (streq(argv[i], "--dump") || streq(argv[i], "-d"))
        {
            if (argc == i + 2) return _dump(argv[i + 1]);
        } else
        if (streq(argv[i], "--insert") || streq(argv[i], "-i"))
        {
            if (argc == i + 4)
                return _insert(argv[i + 1], argv[i + 2], argv[i + 3]);
        } else
        if (streq(argv[i], "--delete") || streq(argv[i], "-l"))
        {
            if (argc == i + 4)
                return _delete(argv[i + 1], argv[i + 2], argv[i + 3]);
        } else
        if (streq(argv[i], "--find") || streq(argv[i], "-f"))
        {
            if (argc == i + 4)
                return _find(argv[i + 1], argv[i + 2], argv[i + 3]);
        } else
        if (streq(argv[i], "--help") || streq(argv[i], "-h"))
            return _help();
        else
            {printf("Invalid argument: %s.\n", argv[i]); return 1;}

        printf("Not enough parameters for option %s.\n", argv[i]);
        return 2;
    }

    puts("No parameters given.");
    return 1;
}

static int
_create_new_db(char *name)
{
    if (!str_is_valid_file_name(name))
    {
        printf("Error creating new map database: %s is not a valid file "
            "name.\n", name);
        return 1;
    }

    char *save_name = name;
    int len = strlen(save_name);

    if (len < 6 || !streq(name + len - 6, ".mapdb"))
    {
        save_name = create_empty_dynamic_str(len + 6);
        if (!save_name)
        {
            printf("Error creating new map database %s: out of memory.\n",
                name);
            return 2;
        }
        memcpy(save_name, name, len);
        memcpy(save_name + len, ".mapdb", 6);
        save_name[len + 6] = 0;
    }

    muta_map_db_t db = {0};
    int r = muta_map_db_save(&db, save_name);

    if (r)
    {
        printf("Error creating new map database: muta_map_db_save() failed "
            "with code %d.\n", r);
        return 2;
    }

    printf("Created map database %s.\n", save_name);
    return 0;
}

static int
_dump(char *fp)
{
    int     ret = 0;
    char    *err_str;

    muta_map_db_t f;
    if (muta_map_db_load(&f, fp))
        {err_str = "file not found or corrupted"; ret = 1; goto out;}

    printf("Dumping map database from %s:\n", fp);
    printf("Version:            %u\n", f.header.version);
    printf("Running ID:         %u\n", f.header.running_id);
    printf("Number of entries:  %u\n", f.header.num_entries);

    for (uint32 i = 0; i < f.header.num_entries; ++i)
        printf("Entry %u:\n\tid:   %u\n\tname: %s\n\tpath: %s\n", i,
            f.entries[i].id, f.entries[i].name, f.entries[i].path);

    puts("Dump finished.");

    out:
        if (ret) printf("Error dumping map database: %s.\n", err_str);
        return ret;
}

static int
_insert(char *db_fp, char *name, char *map_fp)
{
    int     ret = 0;
    char    *err_str;

    if (!str_is_valid_file_path(map_fp))
        {err_str = "invalid file path"; ret = 1; goto out;}

    muta_map_db_t db;
    if (muta_map_db_load(&db, db_fp))
        {err_str = "database file not found or corrupted"; ret = 2; goto out;}

    if (muta_map_db_get_entry_by_name(&db, name))
        {err_str = "entry exists"; ret = 3; goto out;}

    db.entries = realloc(db.entries,
        (db.header.num_entries + 1) * sizeof(muta_map_db_entry_t));
    if (!db.entries)
        {err_str = "out of memory"; ret = 4; goto out;}

    muta_map_db_entry_t *entry = &db.entries[db.header.num_entries++];
    entry->id   = db.header.running_id++;
    entry->name = create_dynamic_str(name);
    entry->path = create_dynamic_str(map_fp);

    if (!entry->name || !entry->path)
        {err_str = "out of memory"; ret = 5; goto out;}

    if (_increment_version_and_save(&db, db_fp))
        {err_str = "muta_map_db_save() failed."; ret = 6; goto out;}

    out:
        if (ret) printf("Error inserting map to database: %s.\n", err_str);
        else printf("Inserted map %s to database as id %u.\n", name, entry->id);
        return ret;
}

static int
_delete(char *db_fp, char *id_opt, char *id_val)
{
    int     ret = 0;
    char    *err_str;

    muta_map_db_t db;
    if (muta_map_db_load(&db, db_fp))
        {err_str = "database file not found or corrupted"; ret = 1; goto out;}

    muta_map_db_entry_t *entry;

    if (streq(id_opt, "--name") || streq(id_opt, "-n"))
    {
        entry = muta_map_db_get_entry_by_name(&db, id_val);
    } else
    if (streq(id_opt, "--id") || streq(id_opt, "-i"))
    {
        entry = muta_map_db_get_entry_by_id(&db, str_to_uint32(id_val));
    } else
    {
        err_str = "second option did not specify identifier type";
        ret = 3;
        goto out;
    }

    if (!entry)
        {err_str = "entry not found"; ret = 4; goto out;}

    uint32 index = (uint32)(size_t)(entry - db.entries);
    memmove(entry, entry + 1,
        (db.header.num_entries - index - 1) * sizeof(muta_map_db_entry_t));
    db.header.num_entries--;

    if (_increment_version_and_save(&db, db_fp))
        {err_str = "failed to save database."; ret = 5; goto out;}

    printf("Deleted entry %s.\n", id_val);

    out:
        if (ret) printf("Error deleting entry: %s.\n", err_str);
        return ret;
}

static int
_increment_version_and_save(muta_map_db_t *db, char *fp)
{
    db->header.version++;
    int ret = muta_map_db_save(db, fp);
    return ret;
}

static int
_find(char *db_fp, char *id_opt, char *id_val)
{
    int     ret = 0;
    char    *err_str;

    muta_map_db_t db;
    if (muta_map_db_load(&db, db_fp))
        {err_str = "database file not found or corrupted."; ret = 1; goto out;}

    muta_map_db_entry_t *entry;

    if (streq(id_opt, "-i") || streq(id_opt, "--id"))
        entry = muta_map_db_get_entry_by_id(&db, str_to_uint32(id_val));
    else if (streq(id_opt, "-n") || streq(id_opt, "--name"))
        entry = muta_map_db_get_entry_by_name(&db, id_val);
    else
        {err_str = "invalid identified specifier"; ret = 2; goto out;}

    if (!entry)
        {err_str = "entry does not exist"; ret = 3; goto out;}

    printf("Found entry:\n\tid:   %u\n\tname: %s\n\tpath: %s\n",
        entry->id, entry->name, entry->path);

    out:
    if (ret) printf("Error finding entry: %s.\n", err_str);
    return ret;
}

static int
_help()
{
    puts("MUTA Map Database Editor\n"
    "Usage: ./muta_mapdb [OPTION] [ARGUMENTS] ...\n"
    "-h, --help:            show this dialogue.\n"
    "-n, --new [NAME]:      create new map database file.\n"
    "-d, --dump [NAME]:     print contents of a .mapdb file.\n"
    "-i, --insert [DB_PATH] [MAP_NAME] [MAP_PATH]:\n"
    "                       insert a map into existing database.\n"
    "-l, --delete [DB_NAME] [-i, --id OR -n, --name] [MAP_IDENTIFIER]:\n"
    "                       Delete a map from a map database. -i specifies\n"
    "                       removal by id, -n specifies removal by name.\n");
    return 0;
}
