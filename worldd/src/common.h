#ifndef MUTA_WORLDD_COMMON_H
#define MUTA_WORLDD_COMMON_H
#include <stdlib.h>
#include "../../shared/ksys.h"

#define panic(msg_, ...) \
    ((void)fprintf(stderr, "Panic! " msg_, ##__VA_ARGS__), \
    (void)fflush(stderr), sleep_ms(5000), (void)exit(666))

#define panic_if(cond_, msg_, ...) \
    ((cond_) ? panic(msg_, ##__VA_ARGS__) : (void)0)

#endif /* MUTA_WORLDD_COMMON_H */
