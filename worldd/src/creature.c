#include "creature.h"
#include "world.h"

int
creature_set_dir(entity_t *cr, int dir)
{
    muta_assert(cr->type == ENT_TYPE_CREATURE);
    int old_dir = cr->dir;
    entity_set_dir(cr, dir);
    int new_dir = cr->dir;
    if (new_dir == old_dir)
        return 0;
    w_event_t ev;
    ev.type = W_EVENT_CREATURE_DIR;
    ev.d.creature_dir.id    = cr->type_data.creature.id;
    ev.d.creature_dir.dir   = new_dir;
    w_post_event(&ev);
    return 0;
}

int
creature_find_path(entity_t *cr, int32 x, int32 y, int8 z)
{
    muta_assert(cr->type == ENT_TYPE_CREATURE);
    return entity_find_path(cr, x, y, z);
}

void
creature_emote(entity_t *cr, entity_t *target, uint16 emote_id)
{
    muta_assert(cr->type == ENT_TYPE_CREATURE);

    if (!target)
    {
        

        return;
    }

    w_event_t ev;

    switch (target->type)
    {
    case ENT_TYPE_PLAYER:
        ev.type                                 = W_EVENT_CREATURE_PLAYER_EMOTE;
        ev.d.creature_player_emote.creature_id  = cr->type_data.creature.id;
        ev.d.creature_player_emote.player_id    = target->type_data.player.id;
        w_post_event(&ev);
        break;
    default:
        muta_assert(0);
    }
}
