#include "script.h"
#include "instance_part.h"
#include "common.h"
#include "../../shared/lua/lauxlib.h"
#include "../../shared/lua/lualib.h"
#include "../../shared/lua/lapi.h"
#include "../../shared/world_common.h"
#include "../../shared/emote.h"
#include "world.h"
#include "creature.h"

DYNAMIC_HASH_TABLE_DEFINITION(lua_script_table, lua_script_t*, const char *,
    uint64, fnv_hash64_from_str, 2);

static const char *_script_list_path =
    "muta-data/server/entity_scripts.cfg";
static obj_pool_t           _sbnk_pool;
static lua_script_table_t   _sbnk_table;
static lua_script_t         **_sbnk_all_scripts; /* For freeing the scripts */

/*-- Lua component --*/

static void
_cmp_lua_on_remove(entity_t *e, cmp_lua_pool_item_t *cmp_a,
    cmp_lua_pool_item_t *cmp_b);

COMPONENT_DEFINITION(cmp_lua, cmp_lua_pool, ENT_CMP_LUA, _cmp_lua_on_remove);

static void
_parse_script_list_callback(void *ctx, const char *name, const char *path);
/* Save a script of the given name and path listed in the script list file. */

static inline void
_load_lua_script(lua_State *lua, lua_script_t *script);

static inline entity_t *
_get_creature_arg(lua_State *lua, int index);

static void
_register_script_funcs(void *statep, lua_script_t *script);

/* MUTA Lua API C functions */

static int
_lapi_entity_get_pos(lua_State *lua);

static int
_lapi_entity_emote(lua_State *lua);

static int
_lapi_creature_walk(lua_State *lua);

static int
_lapi_creature_get_dir(lua_State *lua);

static int
_lapi_creature_set_dir(lua_State *lua);

static int
_lapi_get_creature_def(lua_State *lua);

static int
_lapi_creature_find_path(lua_State *lua);

int
lua_init(lua_t *l)
{
    memset(l, 0, sizeof(lua_t));
    l->state = luaL_newstate();
    if (!l->state)
        return 1;
    lua_State *lua = l->state;
    luaL_openlibs(lua);

    /* Register MUTA API functions */
    lua_register(lua, "entity_get_pos", _lapi_entity_get_pos);
    lua_register(lua, "entity_emote", _lapi_entity_emote);
    lua_register(lua, "creature_walk", _lapi_creature_walk);
    lua_register(lua, "creature_get_dir", _lapi_creature_get_dir);
    lua_register(lua, "creature_set_dir", _lapi_creature_set_dir);
    lua_register(lua, "creature_get_creature_def", _lapi_get_creature_def);
    lua_register(lua, "creature_find_path", _lapi_creature_find_path);

    /* Register all the scripts' callback functions with the lua instance */
    uint32 num_scripts = darr_num(_sbnk_all_scripts);
    for (uint32 i = 0; i < num_scripts; ++i)
    {
        lua_script_t *script = _sbnk_all_scripts[i];
        _register_script_funcs(lua, script);
    }

    return 0;
}

void
lua_destroy(lua_t *lua)
{
    if (lua->state)
        lua_close((lua_State*)lua->state);
}

void
update_cmp_lua_scripts(instance_part_t *inst_part, double dt)
{
    uint32              num_cmps;
    cmp_lua_pool_item_t *items;
    cmp_lua_t           *cmp;
    lua_State           *lua = inst_part->lua.state;
    lua_script_t        *script;
    items = cmp_lua_pool_get_items(&inst_part->cmp_lua_pool, &num_cmps);

    for (uint32 i = 0; i < num_cmps; ++i)
    {
        cmp     = &items[i].cmp;
        script  = cmp->script;
        if (!script)
            continue;

        lua_pushstring(lua, cmp->tick_key);
        lua_gettable(lua, LUA_REGISTRYINDEX);

        /* Set call parameters (entity pointer) */
        lua_pushlightuserdata(lua, cmp_lua_get_entity(cmp));
        lua_pushnumber(lua, dt);

        lua_call(lua, 2, 0);
    }
}

void
cmp_lua_set_script(cmp_lua_t *cmp, const char *script_name)
{
    entity_t    *e      = cmp_lua_get_entity(cmp);
    lua_State   *lua    = e->inst->lua.state;

    if (cmp->script) /* Unload current script if one exists */
    {
        lua_pushlightuserdata(lua, e);
        lua_pushnil(lua);
        lua_settable(lua, LUA_REGISTRYINDEX);
        cmp->script = 0;
    }

    if (!script_name)
        return;

    cmp->script = sbnk_get(script_name);
    panic_if(!cmp->script, "Attempted to use lua script '%s', but not found.\n",
        script_name);

    DEBUG_PRINTFF("Script set to %p\n", cmp->script);

    /* Push the whole script to the registry */
    lua_pushlightuserdata(lua, e);
    _load_lua_script(lua, cmp->script);
    lua_settable(lua, LUA_REGISTRYINDEX);

    /* Call the script's Init() function */
    lua_pushstring(lua, cmp->script->init_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    if (!lua_isnil(lua, 1))
    {
        lua_pushlightuserdata(lua, e);
        lua_call(lua, 1, 0);
    }

    cmp->tick_key = cmp->script->tick_key;
}

/* Load all lua scripts from disc as described in data files */
int
sbnk_init()
{
    int ret         = 0;
    int num_scripts = 128;
    obj_pool_init(&_sbnk_pool, num_scripts, sizeof(lua_script_t));
    lua_script_table_init(&_sbnk_table, num_scripts);
    if (parse_cfg_file(_script_list_path, _parse_script_list_callback, 0))
    {
        printf("Error: failed to read %s.\n", _script_list_path);
        ret = 1;
        goto out;
    }

    out:
        if (ret)
            obj_pool_destroy(&_sbnk_pool);
        return ret;
}

void
sbnk_destroy()
{
    uint32 num_scripts = darr_num(_sbnk_all_scripts);
    for (uint32 i = 0; i < num_scripts; ++i)
    {
        lua_script_t *script = _sbnk_all_scripts[i];
        dstr_free(&script->name);
        dstr_free(&script->path);
        dstr_free(&script->code);
        dstr_free(&script->init_key);
        dstr_free(&script->tick_key);
    }
    darr_free(_sbnk_all_scripts);
    lua_script_table_destroy(&_sbnk_table);
    obj_pool_destroy(&_sbnk_pool);
}

lua_script_t *
sbnk_get(const char *name)
{
    lua_script_t **s = lua_script_table_get_ptr(&_sbnk_table, name);
    return s ? *s : 0;
}

int
sbnk_reload(const char *script_name)
{
    lua_script_t *ls = sbnk_get(script_name);
    if (!ls)
    {
        printf("Attempted to reload script '%s', but not found.\n",
            script_name);
        return 1;
    }
    dstr_free(&ls->code);
    ls->code = load_text_file_to_dstr(ls->path);

    w_on_reload_lua_script(ls, _register_script_funcs);
    printf("Reloaded entity lua script '%s'.\n", script_name);
    return 0;
}

static void
_cmp_lua_on_remove(entity_t *e, cmp_lua_pool_item_t *cmp_a,
    cmp_lua_pool_item_t *cmp_b)
{
    cmp_lua_t *cmp = &cmp_a->cmp;
    if (!cmp->script)
        return;
    lua_State *lua = e->inst->lua.state;
    lua_pushlightuserdata(lua, e);
    lua_pushnil(lua);
    lua_settable(lua, LUA_REGISTRYINDEX);
}

static void
_parse_script_list_callback(void *ctx, const char *name, const char *path)
{
    lua_script_t **existing_script = lua_script_table_get_ptr(&_sbnk_table, name);

    if (existing_script)
    {
        printf("Warning: script %s defined more than once in %s. Using the "
            "earliest definition...\n", name, _script_list_path);
        return;
    }

    lua_script_t *script = obj_pool_reserve(&_sbnk_pool);
    script->name = dstr_create(name);
    script->path = dstr_create(path);

    /* Panic because an uninitialized script might lead to unintended behavior,
     * like monsters not attacking, etc. */
    script->code = load_text_file_to_dstr(path);
    panic_if(!script->code, "Failed to to load script %s from %s.\n", name,
        path);

    script->init_key = 0;
    dstr_setf(&script->init_key, "%s_Init", name);

    script->tick_key = 0;
    dstr_setf(&script->tick_key, "%s_Tick", name);

    lua_script_table_einsert(&_sbnk_table, name, script);
    darr_push(_sbnk_all_scripts, script);
}

static inline void
_load_lua_script(lua_State *lua, lua_script_t *script)
{
    int r = luaL_loadbuffer(lua, script->code, dstr_len(script->code),
        script->name);
    panic_if(r, "Failed to load lua script %s\n", script->name);
}

static inline entity_t *
_get_creature_arg(lua_State *lua, int index)
{
    entity_t *e =  lua_touserdata(lua, 1);
    muta_assert(e->type == ENT_TYPE_CREATURE);
    return e;
}

static void
_register_script_funcs(void *statep, lua_script_t *script)
{
    lua_State *lua = statep;

    _load_lua_script(lua, script);
    lua_call(lua, 0, 0);

    /* Register the Init function */
    lua_pushstring(lua, script->init_key);
    lua_getglobal(lua, "Init");
    lua_settable(lua, LUA_REGISTRYINDEX);

    /* Register the Tick function */
    lua_pushstring(lua, script->tick_key);
    lua_getglobal(lua, "Tick");
    lua_settable(lua, LUA_REGISTRYINDEX);
}

static int
_lapi_entity_get_pos(lua_State *lua)
{
    entity_t    *e  = lua_touserdata(lua, 1);
    wpos_t      pos = entity_get_pos(e);

    lua_newtable(lua);
    lua_pushstring(lua, "x");
    lua_pushnumber(lua, (lua_Number)pos.x);
    lua_settable(lua, -3);
    lua_pushstring(lua, "y");
    lua_pushnumber(lua, (lua_Number)pos.y);
    lua_settable(lua, -3);
    lua_pushstring(lua, "z");
    lua_pushnumber(lua, (lua_Number)pos.z);
    lua_settable(lua, -3);

    return 1;
}

static int
_lapi_entity_emote(lua_State *lua)
{
    entity_t *e1 = lua_touserdata(lua, 1);
    entity_t *e2 = lua_touserdata(lua, 2);
    const char *emote_name = lua_tostring(lua, 3);
    if (!e1)
        return 0;
    emote_id_t emote_id = emote_get_id_by_name(emote_name);
    if (!emote_is_id_valid(emote_id))
    {
        printf("Invalid emote '%s' provided in script!\n", emote_name);
        return 0;
    }
    switch (e1->type)
    {
    case ENT_TYPE_CREATURE:
        creature_emote(e1, e2, emote_id);
        break;
    default:
        muta_assert(0);
    }
    return 0;
}

static int
_lapi_creature_walk(lua_State *lua)
{
    entity_t    *e  = _get_creature_arg(lua, 1);
    int         dir = (int)lua_tonumber(lua, 2);
    w_walk_creature(e->type_data.creature.id, dir);
    return 0;
}

static int
_lapi_creature_get_dir(lua_State *lua)
{
    entity_t    *e  = _get_creature_arg(lua, 1);
    lua_Number  dir = (lua_Number)e->dir;
    lua_pushnumber(lua, dir);
    return 1;
}

static int
_lapi_creature_set_dir(lua_State *lua)
{
    entity_t    *e  = _get_creature_arg(lua, 1);
    lua_Number  dir = lua_tonumber(lua, 2);
    creature_set_dir(e, (int)dir);
    return 0;
}

static int
_lapi_get_creature_def(lua_State *lua)
{
    entity_t *e = _get_creature_arg(lua, 1);
    lua_pushlightuserdata(lua, wc_get_creature_def(
        e->type_data.creature.type_id));
    return 1;
}

static int
_lapi_creature_find_path(lua_State *lua)
{
    entity_t *e = _get_creature_arg(lua, 1);
    int32   x = (int32)lua_tonumber(lua, 2);
    int32   y = (int32)lua_tonumber(lua, 3);
    int8    z = (int8)lua_tonumber(lua, 4);
    creature_find_path(e, x, y, z);
    return 0;
}
