#ifndef MUTA_PATHFIND_H
#define MUTA_PATHFIND_H

#include "../../shared/types.h"
#include "../../shared/sv_common_defs.h"

#define MAX_PF_GRIDS 64

/* Forward declaration(s) */
typedef struct world_pos_t  world_pos_t;
typedef struct entity_t     entity_t;

/* Types defined here */
typedef struct pf_path_t    pf_path_t;
typedef struct pf_result_t  pf_result_t;
typedef pf_result_t         pf_result_darr_t;
typedef struct pf_config_t  pf_config_t;

enum pf_errors
{
    PFERR_OK      = 0,
    PFERR_NO_MEM  = 1,
    PFERR_THREADS = 2
};

struct pf_path_t
{
    world_pos_t *nodes;
    int         num_nodes;
    int         max_nodes;
    pf_path_t   *prev, *next;
#ifdef _MUTA_DEBUG
    uint8       is_reserved;
#endif
};

struct pf_result_t
{
    int             error;
    pf_path_t       *path;
    entity_t        *entity;
};

struct pf_config_t
{
    int grid_w;
    int grid_t;
    int num_grids;
};

int
pf_init(pf_config_t *cfg, int num_initial_requests);

int
pf_trace(entity_t *e, int32 sx, int32 sy, int8 sz, int32 dx, int32 dy, int8 dz);

void
pf_free_path(pf_path_t *pp);

void
pf_wait_for_jobs();
/* This should be per instance! */

#endif /* MUTA_PATHFIND_H */
