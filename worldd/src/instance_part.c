#include "instance_part.h"
#include "maps.h"
#include "../../shared/world_common.h"

uint32
instance_part_tiled_w(instance_part_t *inst_part)
    {return inst_part->data->cw * MAP_CHUNK_W;}

uint32
instance_part_tiled_h(instance_part_t *inst_part)
    {return inst_part->data->ch * MAP_CHUNK_W;}

int
instance_part_init(instance_part_t *inst, uint32 id, map_data_t *data,
    uint num_ents)
{
    if (num_ents == 0)
        return 1;
    int ret = 0;
    memset(inst, 0, sizeof(instance_part_t));
    obj_pool_init(&inst->ents, num_ents, sizeof(ent_cont_t));
    darr_reserve(inst->ents_darr, num_ents);
    darr_reserve(inst->despawn_ent_cmds, num_ents);
    darr_reserve(inst->pf_results, 256);
    if (cmp_mobility_pool_init(&inst->cmp_mobility_pool, num_ents))
        {ret = 4; goto out;}
    if (cmp_pathfind_pool_init(&inst->cmp_pathfind_pool, num_ents))
        {ret = 5; goto out;}
    if (cmp_lua_pool_init(&inst->cmp_lua_pool, num_ents))
        {ret = 6; goto out;}
    if (lua_init(&inst->lua))
        {ret = 7; goto out;}
    mutex_init(&inst->pf_res_mtx);
    inst->runtime_id    = id;
    inst->data          = data;
    inst->target_dt     = 1.f / 60.f;
    inst->accum_dt      = 0.f;
    out:
        if (ret)
            instance_part_destroy(inst);
        return ret;
}

void
instance_part_destroy(instance_part_t *inst)
{
    obj_pool_destroy(&inst->ents);
    cmp_mobility_pool_destroy(&inst->cmp_mobility_pool);
    cmp_pathfind_pool_destroy(&inst->cmp_pathfind_pool);
    darr_free(inst->despawn_ent_cmds);
    darr_free(inst->pf_results);
    mutex_destroy(&inst->pf_res_mtx);
    lua_destroy(&inst->lua);
    memset(inst, 0, sizeof(instance_part_t));
}

map_chunk_t *
instance_part_get_chunk(instance_part_t *inst, uint x, uint y)
    {return map_data_get_chunk(inst->data, x, y);}

tile_t
instance_part_get_tile_type(instance_part_t *inst, int32 x, int32 y, int8 z)
{
    if (x >= 0 && y >= 0 && z >= 0
    && x < (int)instance_part_tiled_w(inst)
    && y < (int)instance_part_tiled_h(inst)
    && z < MAP_CHUNK_T)
        return GET_TILE_FROM_INST_PART(inst, x, y, z);
    return 0; /* Invalid */
}

tile_def_t *
instance_part_get_tile_def(instance_part_t *inst, int32 x, int32 y, int8 z)
{
    tile_t t = instance_part_get_tile_type(inst, x, y, z);
    return t < tile_defs_num ? &tile_defs[t] : 0;
}

world_pos_t
instance_part_global_to_local_pos(instance_part_t *inst, int32 x, int32 y,
    int8 z)
{
    world_pos_t pos;
    pos.x = x - inst->data->cx * MAP_CHUNK_W;
    pos.y = y - inst->data->cy * MAP_CHUNK_T;
    pos.z = z;
    return pos;
}

world_pos_t
instance_part_local_to_global_pos(instance_part_t *inst, int32 x, int32 y,
    int8 z)
{
    world_pos_t pos;
    pos.x = x + inst->data->cx * MAP_CHUNK_W;
    pos.y = y + inst->data->cy * MAP_CHUNK_T;
    pos.z = z;
    return pos;
}

int
instance_part_check_diagonals(instance_part_t *inst, int dir, int nx, int ny,
    int nz)
{
    tile_def_t *tdef = 0;

    switch (dir)
    {
        case ISODIR_NORTH_EAST:
        {
            tdef = instance_part_get_tile_def(inst, nx + 1, ny, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny - 1, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx + 1, ny, nz - 1);
            if (!tdef || tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny - 1, nz - 1);
            if (!tdef || tdef->passthrough)
                return 1;
        }
            break;
        case ISODIR_NORTH_WEST:
        {
            tdef = instance_part_get_tile_def(inst, nx - 1, ny, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny - 1, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx - 1, ny, nz - 1);
            if (tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny - 1, nz - 1);
            if (tdef->passthrough) return 1;
        }
            break;
        case ISODIR_SOUTH_EAST:
        {
            tdef = instance_part_get_tile_def(inst, nx + 1, ny, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny + 1, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx + 1, ny, nz - 1);
            if (tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny + 1, nz - 1);
            if (tdef->passthrough) return 1;
        }
            break;
        case ISODIR_SOUTH_WEST:
        {
            tdef = instance_part_get_tile_def(inst, nx - 1, ny, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny + 1, nz);
            if (!tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx - 1, ny, nz - 1);
            if (tdef->passthrough) return 1;
            tdef = instance_part_get_tile_def(inst, nx, ny + 1, nz - 1);
            if (tdef->passthrough) return 1;
        }
            break;
        default:
            break;
    }

    return 0;
}

int
instance_part_push_pf_result(instance_part_t *inst, pf_result_t res)
{
    mutex_lock(&inst->pf_res_mtx);
    darr_push(inst->pf_results, res);
    mutex_unlock(&inst->pf_res_mtx);
    return 0;
}

int
instance_part_find_nearest_spawnable_pos(instance_part_t *inst, int32 ix,
    int32 iy, int8 iz, int obj_height, world_pos_t *ret_pos)
{
    int         z, zb;
    tile_def_t  *t;

    #define CHECK_TOP() \
        bool32 top_ok = 1; \
        for (zb = z + 1; zb <  z + obj_height; ++zb) \
        { \
            t = instance_part_get_tile_def(inst, ix, iy, (int8)zb); \
            if (t->passthrough) continue; \
            top_ok = 0; \
            break; \
        } \
        if (top_ok) \
        { \
            ret_pos->x = ix; \
            ret_pos->y = iy; \
            ret_pos->z = z + 1; \
            return 0; \
        }

    for (z = iz; z >= -1; --z)
    {
        t = instance_part_get_tile_def(inst, ix, iy, (int8)z);
        if (!t->passthrough || z < 0) {CHECK_TOP();}
    }

    for (z = iz; z <= MAX_TILE_Z; ++z)
    {
        t = instance_part_get_tile_def(inst, ix, iy, (int8)z);
        if (!t->passthrough || z == 0) {CHECK_TOP();}
    }

    #undef CHECK_TOP
    ret_pos->x = ix;
    ret_pos->y = iy;
    ret_pos->z = MAX_TILE_Z + 1; /* Spawn on top of the world */
    return 0;
}
