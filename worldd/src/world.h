#ifndef MUTA_WORLDD_WORLD_H
#define MUTA_WORLDD_WORLD_H

#include "../../shared/types.h"
#include "../../shared/sv_common_defs.h"

/* Forward declaration(s) */
typedef struct pf_config_t  pf_config_t;
typedef struct lua_script_t lua_script_t;

/* Types defined here */
typedef struct w_event_t w_event_t;

enum w_event_type_t
{
    W_EVENT_SPAWN_PLAYER,
    W_EVENT_WALK_PLAYER,
    W_EVENT_PLAYER_DIR,
    W_EVENT_SPAWN_DYNAMIC_OBJ,
    W_EVENT_TELEPORT_PLAYER,
    W_EVENT_SPAWN_CREATURE,
    W_EVENT_WALK_CREATURE,
    W_EVENT_CREATURE_DIR,
    W_EVENT_CREATURE_PLAYER_EMOTE
};

struct w_event_t
{
    int type;
    union
    {
        struct
        {
            player_guid_t   id;
            uint32          map_id;
            int32           x;
            int32           y;
            int8            z;
            uint8           dir;
        } spawn_player;
        struct {player_guid_t id;}                  despawn_player;
        struct {player_guid_t id; world_pos_t pos;} walk_player;
        struct {player_guid_t id; uint8 dir;}       player_dir;
        struct
        {
            dobj_guid_t id;
            uint32      map_id;
            int32       x;
            int32       y;
            int8        z;
            uint8       dir;
        } spawn_dynamic_obj;
        struct
        {
            player_guid_t   id;
            wpos_t          pos;
        } teleport_player;
        struct
        {
            creature_guid_t id;
            uint32          map_id;
            int32           x;
            int32           y;
            int8            z;
            uint8           dir;
            uint8           flags;
        } spawn_creature;
        struct {creature_guid_t id; wpos_t pos;} walk_creature;
        struct {creature_guid_t id; uint8 dir;} creature_dir;
        struct
        {
            creature_guid_t creature_id;
            player_guid_t   player_id;
            uint16          emote_id;
        } creature_player_emote;
    } d;
};

/* All tile coordinates are global coordinates (coordinates on the whole
 * map, not just local to the instance part) unless otherwise specified. */

int
w_init(uint num_players, uint num_creatures, pf_config_t *pf_cfg,
    const char *map_db_path, const char *tile_path, const char *sobj_path,
    const char *dobj_path, const char *prace_path);

void
w_destroy();

int
w_load_map_data(uint32 map_id, uint cx, uint cy, uint cw, uint ch);

int
w_load_instance_part(uint32 map_id, uint32 part_id, uint32 num_ents,
    uint32 cx, uint32 cy, uint32 cw, uint32 ch);
/* cx, cy, cw, ch are map chunk coordinates. */

int
w_spawn_player(player_guid_t id, player_race_id_t race, uint32 map_id, int32 x,
    int32 y, int8 z, int dir);

void
w_despawn_player(player_guid_t id);

int
w_spawn_dynamic_obj(dobj_guid_t id, dobj_type_id_t type_id, uint32 map_id,
    int32 x, int32 y, int8 z, int dir);

void
w_despawn_dynamic_obj(dobj_guid_t id);

int
w_spawn_creature(creature_guid_t id, creature_type_id_t type_id, uint32 map_id,
    int32 x, int32 y, int8 z, int dir, uint8 flags);

void
w_despawn_creature(creature_guid_t id);

int
w_find_path_player(player_guid_t id, int32 x, int32 y, int8 z);

int
w_walk_player(player_guid_t id, int dir);

int
w_walk_creature(creature_guid_t id, int dir);

int
w_teleport_player(player_guid_t id, int32 x, int32 y, int8 z);

void
w_post_event(w_event_t *ev);

w_event_t *
w_get_events(uint *ret_num);

void
w_clear_events();

void
w_update(double dt);

void
w_on_reload_lua_script(lua_script_t *script, void (*callback)(void*,
    lua_script_t *script));

#endif /* MUTA_WORLDD_WORLD_H */
