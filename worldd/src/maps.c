#include <stddef.h>
#include "maps.h"
#include "../../shared/world_common.h"

muta_map_db_t   map_db;
muta_map_db_t   _map_db;
const char *    _map_db_path = "muta-data/common/maps.mapdb";
obj_pool_t      _map_pool;
map_data_t      **_all_maps;

int
map_data_init(map_data_t *d, muta_map_db_entry_t *me, uint32 cx,
    uint32 cy, uint32 cw, uint32 ch);
/* cx and cy are the xy coordinates, in chunks, of where this map will start
 * loading from. */

void
map_data_destroy(map_data_t *d);

int
map_data_init(map_data_t *d, muta_map_db_entry_t *me, uint32 cx, uint32 cy,
    uint32 cw, uint32 ch)
{
    memset(d, 0, sizeof(map_data_t));
    d->map_db_entry = me;

    /* Allocate chunks */
    d->chunks = calloc(cw * ch, sizeof(map_chunk_t));
    if (!d->chunks)
        return 1;

    int ret = 0;

    uint32 num_chunks = cw * ch;
    for (uint32 i = 0; i < num_chunks; ++i)
        if (muta_chunk_file_init(&d->chunks[i].file))
            {ret = 2; goto out;}

    d->cx = cx;
    d->cy = cy;
    d->cw = cw;
    d->ch = ch;

    /* Read the map files */
    muta_map_file_t f = {0};
    if (muta_map_file_load(&f, me->path))
        return 3;

    /* Check if our given params have been valid */
    if (cx + d->cw > f.header.w) {ret = 5; goto out;}
    if (cy + d->ch > f.header.h) {ret = 6; goto out;}

    /* Read each chunk's tile data */
    map_chunk_t  *chunk;
    uint                i, j, k;

    for (i = cx; i < cx  + cw; ++i)
        for (j = cy; j < cy + ch; ++j)
        {
            chunk = map_data_get_chunk(d, i, j);
            if (muta_chunk_file_load(&chunk->file,
                muta_map_file_get_chunk_path(&f, i, j)))
                {ret = 7; goto out;}
            if (!wc_chunk_file_tiles_ok(&chunk->file))
                {ret = 8; goto out;}

            /* Create invisible colliders for static objects */

            uint32 num = chunk->file.header.num_static_objs;
            stored_static_obj_t *objs = chunk->file.static_objs;
            stored_static_obj_t *o;
            static_obj_def_t    *d;
            for (k = 0; k < num; ++k)
            {
                o = &objs[k];
                if (o->x >= MUTA_CHUNK_W || o->y >= MUTA_CHUNK_W
                || o->z < 0 || o->z >= MUTA_CHUNK_T)
                    continue;

                d = wc_get_static_obj_def(o->type_id);
                if (!d) continue;

                if (d->passthrough) continue;

                GET_TILE_FROM_CHUNK(chunk, o->x, o->y, o->z) =
                    INVISIBLE_COLLIDER;
            }
        }

    out:
        if (ret)
        {
            map_data_destroy(d);
            DEBUG_PRINTF("%s failed with code %d.\n", __func__, ret);
        }
        muta_map_file_destroy(&f);
        return ret;
}

void
map_data_destroy(map_data_t *d)
{
    free(d->chunks);
    memset(d, 0, sizeof(map_data_t));
}

int
maps_init()
{
    int ret = 0;
    if (muta_map_db_load(&_map_db, _map_db_path))
    {
        printf("Error: %s not found.\n", _map_db_path);
        return 1;
    }
    obj_pool_init(&_map_pool, _map_db.header.num_entries, sizeof(map_data_t));
    darr_reserve(_all_maps, _map_db.header.num_entries);
    return ret;
}

void
maps_destroy()
{
    muta_map_db_destroy(&_map_db);
    obj_pool_destroy(&_map_pool);
    darr_free(_all_maps);
}

map_data_t *
maps_claim(uint32 map_id, uint32 chunk_x, uint32 chunk_y, uint32 chunk_w,
    uint32 chunk_h)
{
    map_data_t *ret = 0;
    uint32 num_maps = darr_num(_all_maps);
    for (uint32 i = 0; i < num_maps; ++i)
    {
        map_data_t *md = _all_maps[i];
        if (md->map_db_entry->id != map_id || md->cx != chunk_x ||
            md->cy != chunk_y || md->cw != chunk_w || md->ch != chunk_h)
            continue;
        ret = _all_maps[i];
        ret->user_count++;
        return ret;
    }

    ret = obj_pool_reserve(&_map_pool);
    muta_map_db_entry_t *mdbe = muta_map_db_get_entry_by_id(&_map_db, map_id);

    if (map_data_init(ret, mdbe, chunk_x, chunk_y, chunk_w, chunk_h))
    {
        printf("Error loading map %s.\n", mdbe->name);
        obj_pool_free(&_map_pool, ret);
        return 0;
    }

    return ret;
}

void
maps_unclaim(map_data_t *md)
{
    muta_assert(md->user_count > 0);
    if (--md->user_count)
        return;
    map_data_destroy(md);
    uint32 num_maps = darr_num(_all_maps);
    for (uint32 i = 0; num_maps; ++i)
    {
        if (_all_maps[i] != md)
            continue;
        darr_erase(_all_maps, i);
        break;
    }
}
