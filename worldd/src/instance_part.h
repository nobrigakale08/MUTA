#ifndef MUTA_INSTANCE_PART_H
#define MUTA_INSTANCE_PART_H

#include "entity.h"
#include "script.h"

/* Forward declaration(s) */
typedef struct entity_t     entity_t;
typedef struct pf_result_t  pf_result_t;
typedef pf_result_t         pf_result_darr_t;
typedef struct map_chunk_t  map_chunk_t;
typedef struct map_data_t   map_data_t;
typedef struct tile_def_t   tile_def_t;

/* Types defiend here */
typedef struct despawn_ent_cmd_t    despawn_ent_cmd_t;
typedef despawn_ent_cmd_t           despawn_ent_cmd_darr_t;
typedef struct instance_part_t      instance_part_t;

struct despawn_ent_cmd_t
{
    int         ent_type; /* Why is this here? Its already part of the ent. */
    entity_t    *e;
    union
    {
        player_guid_t   player;
        dobj_guid_t     dynamic_obj;
    } type_data;
};

struct instance_part_t
{
    uint32                  runtime_id; /* Temporary id given by the master */
    map_data_t              *data;
    obj_pool_t              ents;
    entity_t                **ents_darr; /* For iteration */
    cmp_mobility_pool_t     cmp_mobility_pool;
    cmp_pathfind_pool_t     cmp_pathfind_pool;
    cmp_lua_pool_t          cmp_lua_pool;
    despawn_ent_cmd_darr_t  *despawn_ent_cmds;
    pf_result_darr_t        *pf_results;
    mutex_t                 pf_res_mtx;
    double                  target_dt;
    double                  accum_dt;
    lua_t                   lua;
};

#define GET_TILE_FROM_INST_PART(inst, x, y, z) \
    GET_TILE_FROM_CHUNK(&GET_CHUNK_OF_TILE((inst), (x), (y)), \
        (x) % MAP_CHUNK_W, (y) % MAP_CHUNK_W, (z))

uint32
instance_part_tiled_w(instance_part_t *inst_part);

uint32
instance_part_tiled_h(instance_part_t *inst_part);

int
instance_part_init(instance_part_t *inst, uint32 id, map_data_t *data,
    uint num_ents);

void
instance_part_destroy(instance_part_t *inst);

entity_t *
instance_part_reserve_entity(instance_part_t *inst);

map_chunk_t *
instance_part_get_chunk(instance_part_t *inst, uint x, uint y);

tile_t
instance_part_get_tile_type(instance_part_t *inst, int32 x, int32 y, int8 z);

tile_def_t *
instance_part_get_tile_def(instance_part_t *inst, int32 x, int32 y, int8 z);

world_pos_t
instance_part_global_to_local_pos(instance_part_t *inst, int32 x, int32 y,
    int8 z);

world_pos_t
instance_part_local_to_global_pos(instance_part_t *inst, int32 x, int32 y,
    int8 z);

bool32
instance_part_check_diagonals(instance_part_t *inst, int dir, int nx, int ny,
    int nz);
/* Returns true if can pass through diagonal tiles */

int
instance_part_push_pf_result(instance_part_t *inst, pf_result_t res);


int
instance_part_find_nearest_spawnable_pos(instance_part_t *ip, int32 ix,
    int32 iy, int8 iz, int obj_height, world_pos_t *ret_pos);

#endif /* MUTA_INSTANCE_PART_H */
