#include "core.h"
#include "world.h"
#include "pathfind.h"
#include "script.h"
#include "../../shared/net.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"
#include "../../shared/sv_time.h"
#include "../../shared/rwbits.inl"
#include "../../shared/world_packets.h"
#include "../../shared/containers.h"
#include "../../shared/sv_common.inl"
#include "../../shared/kthp.h"

enum conn_status_t
{
    CONN_DISCONNECTED,
    CONN_CONNECTING,
    CONN_CONNECTED
};

static struct
{
    addr_t      master_ip;
    int         incoming_buf_size;
    uint        tickrate;
    uint        num_threads;
    uint        keep_alive;
    pf_config_t pathfind;
    dchar       *map_db_path;
    dchar       *tile_path;
    dchar       *static_obj_path;
    dchar       *dynamic_obj_path;
    dchar       *player_race_path;
} _config;

static struct
{
    socket_t            s;
    cryptchan_t         cc;
    int                 status;
    thread_t            th;
    bool32              th_created;
    int                 last_err;
    double_msg_buf_t    msgs_in;
    uint8               tmp_msgs_in[MUTA_MTU];
    dynamic_bbuf_t      msgs_out;
    uint64              last_sent;
    int32               need_dc; /* Atomic */
} _conn;

static perf_clock_t     _mt_clock;
static kth_pool_t       _th_pool;

static void
_read_config(const char *fp);

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val);

static int
_connect_to_master();

static int
_disconnect_from_master();

static thread_ret_t
_conn_callback(void *args);

static void
_conn_update_connecting();

static void
_conn_update_connected();

static void
_read_msg_buffers();

static void
_update_world(double dt);

static int
_read_packet(uint8 *pkt, int len);

static bbuf_t *
_send_msg(int sz);

static inline bbuf_t *
_send_const_encrypted_msg(int sz);

static inline bbuf_t *
_send_var_encrypted_msg(int sz);

static int
_handle_swmsg_load_instance_part(swmsg_load_instance_part_t *s);

static int
_handle_swmsg_spawn_player(swmsg_spawn_player_t *s);

static int
_handle_swmsg_move_player(swmsg_move_player_t *s);

static int
_handle_swmsg_despawn_player(swmsg_despawn_player_t *s);

static int
_handle_swmsg_find_path_player(swmsg_find_path_player_t *s);

static int
_handle_swmsg_spawn_dynamic_obj(swmsg_spawn_dynamic_obj_t *s);

static int
_handle_swmsg_despawn_dynamic_obj(swmsg_despawn_dynamic_obj_t *s);

static int
_handle_swmsg_teleport_player(swmsg_teleport_player_t *s);

static int
_handle_swmsg_spawn_creature(swmsg_spawn_creature_t *s);

static int
_handle_swmsg_despawn_creature(swmsg_despawn_creature_t *s);

static int
_handle_swmsg_reload_entity_script(swmsg_reload_entity_script_t *s);

static void
_handle_wev_spawn_player(w_event_t *ev);

static void
_handle_wev_walk_player(w_event_t *ev);

static void
_handle_wev_player_dir(w_event_t *ev);

static void
_handle_wev_spawn_dynamic_obj(w_event_t *ev);

static void
_handle_wev_teleport_player(w_event_t *ev);

static void
_handle_wev_spawn_creature(w_event_t *ev);

static void
_handle_wev_walk_creature(w_event_t *ev);

static void
_handle_wev_creature_dir(w_event_t *ev);

static void
_handle_wev_creature_player_emote(w_event_t *ev);

int
core_init(const char *cfg_fp)
{
    _read_config(cfg_fp);
    if (init_time())                                        return 1;
    if (init_socket_api())                                  return 2;
    if (crypt_init())                                       return 3;
    if (thread_init(&_conn.th))                             return 4;
    if (w_init(4096, 4096, &_config.pathfind, _config.map_db_path,
        _config.tile_path, _config.static_obj_path, _config.dynamic_obj_path,
        _config.player_race_path))
        return 5;
    if (dynamic_bbuf_init(&_conn.msgs_out, 8128))           return 6;
    if (kth_pool_init(&_th_pool, _config.num_threads, 64))  return 7;
    if (double_msg_buf_init(&_conn.msgs_in, _config.incoming_buf_size))
        return 8;
    perf_clock_init(&_mt_clock, _config.tickrate);
    return 0;
}

int
core_shutdown()
{
    int r = _disconnect_from_master();
    return !r ? 0 : 1;
}

int
core_update()
{
    perf_clock_tick(&_mt_clock);
    int r = _connect_to_master();
    if (r) return 1;
    _read_msg_buffers();
    _update_world(_mt_clock.delta_s);

    /* Send keep alive message if necessary */
    uint64 ct = get_program_ticks_ms();
    if (ct - _conn.last_sent >= _config.keep_alive)
    {
        bbuf_t *bb = _send_msg(0);
        if (!bb) _disconnect_from_master();
        int r = wmsg_keep_alive_write(bb);
        muta_assert(!r);
    }
    if (_conn.status == CONN_CONNECTED)
        send_all_from_bbuf(_conn.s, &_conn.msgs_out, MUTA_MTU);
    return 0;
}

int
core_add_th_job(void (*func)(void*), void *args)
    {return kth_pool_add_job(&_th_pool, func, args);}

bool32
core_do_th_work()
    {return kth_pool_do_work(&_th_pool);}

static void
_read_config(const char *fp)
{
    _config.master_ip = create_addr(127, 0, 0, 1, DEFAULT_WORLD_PORT);
    _config.incoming_buf_size       = 32768 * 2;
    _config.tickrate                = 60;
    _config.num_threads             = 2;
    _config.keep_alive              = 1000;
    _config.pathfind.grid_w         = 128;
    _config.pathfind.grid_t         = 8;
    _config.pathfind.num_grids      = 4;
    _config.map_db_path = set_dynamic_str(_config.map_db_path,
        "muta-data/common/maps.mapdb");
    _config.tile_path = set_dynamic_str(_config.tile_path,
        "muta-data/common/tiles.dat");
    _config.static_obj_path = set_dynamic_str(_config.static_obj_path,
        "muta-data/common/static_objs.dat");
    _config.dynamic_obj_path = set_dynamic_str(_config.dynamic_obj_path,
        "muta-data/common/dynamic_objs.dat");
    _config.player_race_path = set_dynamic_str(_config.player_race_path,
        "muta-data/common/player_races.dat");
    parse_cfg_file(fp ? fp : "config.cfg", _read_cfg_callback, 0);
}

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val)
{
    (void)ctx;
    if (streq(opt, "master ip"))
    {
        int ip[4];
        if (streq(val, "localhost"))
            {ip[0] = 127; ip[1] = 0; ip[2] = 0; ip[3] = 1;} else
        if (sscanf(val, "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]) != 4)
            return;
        _config.master_ip.ip = uint32_ip_from_uint8s(ip[0], ip[1], ip[2], ip[3]);
        printf("cfg - %s: %d.%d.%d.%d\n", opt,
            ADDR_IP_TO_PRINTF_ARGS(&_config.master_ip));
    } else
    if (streq(opt, "port"))
    {
        int v = atoi(val);
        if (v < 0 || v > 0xFFFF) return;
        _config.master_ip.port = (uint16)v;
        printf("cfg - %s: %u\n", opt, (uint)v);
    } else
    if (streq(opt, "incoming buf size"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.incoming_buf_size = v;
        printf("cfg - %s: %i\n", opt, v);
    } else
    if (streq(opt, "tickrate"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.tickrate = (uint)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "num threads"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.num_threads = (uint)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "keep alive frequency"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.keep_alive = (uint64)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "pathfind grid width"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.pathfind.grid_w = (uint)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "pathfind grid height"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.pathfind.grid_t = (uint)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "num pathfind grids"))
    {
        int v = atoi(val);
        if (v < 0) return;
        _config.pathfind.num_grids = (uint)v;
        printf("cfg - %s: %d\n", opt, v);
    } else
    if (streq(opt, "map db path"))
    {
        if (!str_is_valid_file_path(opt)) return;
        _config.tile_path = set_dynamic_str(_config.map_db_path, val);
        printf("cfg - %s: %s\n", opt, val);
    } else
    if (streq(opt, "tile path"))
    {
        if (!str_is_valid_file_path(opt)) return;
        _config.tile_path = set_dynamic_str(_config.tile_path, val);
        printf("cfg - %s: %s\n", opt, val);
    } else
    if (streq(opt, "static obj path"))
    {
        if (!str_is_valid_file_name(opt)) return;
        _config.static_obj_path = set_dynamic_str(_config.static_obj_path, val);
        printf("cfg - %s: %s\n", opt, val);
    } else
    if (streq(opt, "dynamic obj path"))
    {
        if (!str_is_valid_file_name(opt)) return;
        _config.dynamic_obj_path = set_dynamic_str(_config.dynamic_obj_path,
            val);
        printf("cfg - %s: %s\n", opt, val);
    } else
    if (streq(opt, "player race path"))
    {
        if (!str_is_valid_file_name(opt)) return;
        _config.player_race_path = set_dynamic_str(_config.player_race_path,
            val);
        printf("cfg - %s: %s\n", opt, val);
    }
}

static int
_connect_to_master()
{
    if (_conn.status != CONN_DISCONNECTED) return 0;
    if (_conn.th_created)
    {
        thread_join(&_conn.th);
        _conn.th_created = 0;
    }
    double_msg_buf_clear(&_conn.msgs_in);
    BBUF_CLEAR(&_conn.msgs_out);
    _conn.status = CONN_CONNECTING;
    if (thread_create(&_conn.th, _conn_callback, 0))
        {_conn.status = CONN_DISCONNECTED; return 2;}
    _conn.th_created = 1;
    return 0;
}

static int
_disconnect_from_master()
{
    if (_conn.status == CONN_DISCONNECTED) return 0;
    puts("Disconnecting from master.");
    send_all_from_bbuf(_conn.s, &_conn.msgs_out, MUTA_MTU);
    _conn.status = CONN_DISCONNECTED;
    int r = net_shutdown_sock(_conn.s, SOCKSD_BOTH);
    thread_join(&_conn.th);
    return r == 0 ? 0 : 1;
}

static thread_ret_t
_conn_callback(void *args)
{
    (void)args;
    while (_conn.status != CONN_DISCONNECTED)
    {
        if (_conn.status == CONN_CONNECTING)
            _conn_update_connecting();
        else
            _conn_update_connected();
    }
    return 0;
}

static void
_conn_update_connecting()
{
    _conn.s = net_tcp_ipv4_sock();
    int err = 0;

    if (_conn.s == KSYS_INVALID_SOCKET)             {err = 1; goto fail;}
    if (net_connect(_conn.s, _config.master_ip))    {err = 2; goto fail;}

    /* Handshake */
    wmsg_pub_key_t pks;
    if (cryptchan_init(&_conn.cc, pks.key)) {err = 3; goto fail;}
    uint8 send_buf[256];
    bbuf_t sb = BBUF_INITIALIZER(send_buf, 256);
    wmsg_pub_key_write(&sb, &pks);
    if (send_all_from_bbuf(_conn.s, &sb, MUTA_MTU))
        {err = 3; goto fail;}
    puts("Sent public key to master.");

    bool32          handshook   = 0;
    int             dc          = 0;
    int             incomplete;
    int             num_bytes;
    wmsg_type_t     type;
    uint8           recv_buf[512];
    bbuf_t          in_buf = BBUF_INITIALIZER(recv_buf, 512);
    bbuf_t          rb;

    while (!handshook)
    {
        num_bytes = net_recv(_conn.s, BBUF_CUR_PTR(&in_buf),
            BBUF_FREE_SPACE(&in_buf));
        if (num_bytes <= 0)
            {err = 4; goto fail;}
        in_buf.num_bytes += num_bytes;

        BBUF_INIT(&rb, in_buf.mem, in_buf.num_bytes);
        incomplete = 0;

        while (BBUF_FREE_SPACE(&rb) && !incomplete && !handshook)
        {
            BBUF_READ(&rb, wmsg_type_t, &type);

            switch (type)
            {
                case WMSG_PUB_KEY:
                {
                    puts("WMSG_PUB_KEY");
                    wmsg_pub_key_t s;
                    incomplete = wmsg_pub_key_read(&rb, &s);
                    if (incomplete) break;
                    wmsg_stream_header_t f;
                    if (cryptchan_cl_store_pub_key(&_conn.cc, s.key, f.header))
                        {dc = 1; break;}
                    puts("Received public key from master.");
                    wmsg_stream_header_write(&sb, &f);
                    if (send_all_from_bbuf(_conn.s, &sb, MUTA_MTU)) dc = 1;
                }
                    break;
                case WMSG_STREAM_HEADER:
                {
                    puts("WMSG_STREAM_HEADER");
                    wmsg_stream_header_t s;
                    incomplete = wmsg_stream_header_read(&rb, &s);
                    if (incomplete) break;
                    if (cryptchan_store_stream_header(&_conn.cc, s.header))
                        {dc = 1; break;}
                    puts("Received stream header from master.");
                    if (!cryptchan_is_encrypted(&_conn.cc))
                        {dc = 2; break;}
                    puts("Connection with master is now encrypted.");
                    /* Log in? */
                    handshook = 1;
                }
                    break;
                default:
                    dc = 1;
            }

            if (dc || incomplete < 0)
            {
                printf("%s: disconnect, type %d, incomp. %d, dc %d\n", __func__,
                    (int)type, dc, incomplete);
                err = 5; goto fail;
            }
        }

        bbuf_cut_portion(&in_buf, 0, in_buf.num_bytes - BBUF_FREE_SPACE(&rb));
        double_msg_buf_write(&_conn.msgs_in, in_buf.mem, in_buf.num_bytes);
    }

    puts("Master handshake successful.");
    _conn.status = CONN_CONNECTED;
    return;

    fail:
    {
        printf("%s: fail with code %d.\n", __func__, err);
        char b[256];
        sock_err_str(b, 256); puts(b);
        net_shutdown_sock(_conn.s, SOCKSD_BOTH);
        _conn.last_err  = err;
        _conn.status    = CONN_DISCONNECTED;
    }
}

static void
_conn_update_connected()
{
    int num_bytes = net_recv(_conn.s, _conn.tmp_msgs_in, MUTA_MTU);
    if (num_bytes <= 0)
    {
        printf("%s: master disconnected.\n", __func__);
        interlocked_exchange_int32(&_conn.need_dc, 1);
        return;
    }
    if (double_msg_buf_write(&_conn.msgs_in, _conn.tmp_msgs_in, num_bytes))
    {
        printf("%s: need dc (2).\n", __func__);
        interlocked_exchange_int32(&_conn.need_dc, 1);
    }
}

static void
_read_msg_buffers()
{
    bbuf_t *bb = double_msg_buf_swap(&_conn.msgs_in);
    int left = _read_packet(bb->mem, bb->num_bytes);
    if (left < 0)
        _disconnect_from_master();
    else
    {
        bbuf_cut_portion(bb, 0, bb->num_bytes - left);
        if (interlocked_compare_exchange_int32(&_conn.need_dc, 0, 1))
            _disconnect_from_master();
    }

}

static int
_read_packet(uint8 *pkt, int len)
{
    wmsg_type_t type;
    bbuf_t      bb          = BBUF_INITIALIZER(pkt, len);
    int         dc          = 0;
    int         incomplete  = 0;

    while (BBUF_FREE_SPACE(&bb) >= WMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, wmsg_type_t, &type);

        DEBUG_PRINTF("type %d\n", (int)type);

        switch (type)
        {
        case SWMSG_LOAD_INSTANCE_PART:
        {
            DEBUG_PUTS("SWMSG_LOAD_INSTANCE_PART");
            swmsg_load_instance_part_t s;
            muta_assert(cryptchan_is_encrypted(&_conn.cc));
            incomplete = swmsg_load_instance_part_read_const_encrypted(&bb,
                &_conn.cc, &s);
            if (!incomplete) dc = _handle_swmsg_load_instance_part(&s);
        }
            break;
        case SWMSG_SPAWN_PLAYER:
        {
            DEBUG_PUTS("SWMSG_SPAWN_PLAYER");
            swmsg_spawn_player_t s;
            incomplete = swmsg_spawn_player_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_spawn_player(&s);
        }
            break;
        case SWMSG_MOVE_PLAYER:
        {
            DEBUG_PUTS("SWMSG_MOVE_PLAYER");
            swmsg_move_player_t s;
            incomplete = swmsg_move_player_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_move_player(&s);
        }
            break;
        case SWMSG_DESPAWN_PLAYER:
        {
            DEBUG_PUTS("SWMSG_DESPAWN_PLAYER");
            swmsg_despawn_player_t s;
            incomplete = swmsg_despawn_player_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_despawn_player(&s);
        }
            break;
        case SWMSG_FIND_PATH_PLAYER:
        {
            DEBUG_PUTS("SWMSG_FIND_PATH_PLAYER");
            swmsg_find_path_player_t s;
            incomplete = swmsg_find_path_player_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_find_path_player(&s);
        }
            break;
        case SWMSG_SPAWN_DYNAMIC_OBJ:
        {
            DEBUG_PUTS("SWMSG_SPAWN_DYNAMIC_OBJ");
            swmsg_spawn_dynamic_obj_t s;
            incomplete = swmsg_spawn_dynamic_obj_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_spawn_dynamic_obj(&s);
        }
            break;
        case SWMSG_DESPAWN_DYNAMIC_OBJ:
        {
            DEBUG_PUTS("SWMSG_DESPAWN_DYNAMIC_OBJ");
            swmsg_despawn_dynamic_obj_t s;
            incomplete = swmsg_despawn_dynamic_obj_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_despawn_dynamic_obj(&s);
        }
            break;
        case SWMSG_TELEPORT_PLAYER:
        {
            DEBUG_PUTS("SWMSG_TELEPORT_PLAYER");
            swmsg_teleport_player_t s;
            incomplete = swmsg_teleport_player_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_teleport_player(&s);
        }
            break;
        case SWMSG_SPAWN_CREATURE:
        {
            DEBUG_PUTS("SWMSG_SPAWN_CREATURE");
            swmsg_spawn_creature_t s;
            incomplete = swmsg_spawn_creature_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_spawn_creature(&s);
        }
            break;
        case SWMSG_DESPAWN_CREATURE:
        {
            DEBUG_PUTS("SWMSG_DESPAWN_CREATURE");
            swmsg_despawn_creature_t s;
            incomplete = swmsg_despawn_creature_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_despawn_creature(&s);
        }
            break;
        case SWMSG_RELOAD_ENTITY_SCRIPT:
        {
            DEBUG_PUTS("SWMSG_RELOAD_ENTITY_SCRIPT");
            swmsg_reload_entity_script_t s;
            incomplete = swmsg_reload_entity_script_read(&bb, &s);
            if (!incomplete) dc = _handle_swmsg_reload_entity_script(&s);
        }
            break;
        default:
            dc = 1;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: bad packet type %d, dc %d, incomplete %d.\n", __func__,
                (int)type, dc, incomplete);
            return -1;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static bbuf_t *
_send_msg(int sz)
{
    int tot_sz = WMSGTSZ + sz;
    if (BBUF_FREE_SPACE(&_conn.msgs_out) < tot_sz
    && send_all_from_bbuf(_conn.s, &_conn.msgs_out, MUTA_MTU))
        return 0;
    _conn.last_sent = get_program_ticks_ms();
    return &_conn.msgs_out;
}

static inline bbuf_t *
_send_const_encrypted_msg(int sz)
    {return _send_msg(CRYPT_MSG_ADDITIONAL_BYTES + sz);}

static inline bbuf_t *
_send_var_encrypted_msg(int sz)
{
    int tot_sz = sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES + sz;
    return _send_msg(tot_sz);
}

static void
_update_world(double dt)
{
    w_update(dt);

    uint num_evs, i;
    w_event_t *evs = w_get_events(&num_evs);

    for (i = 0; i < num_evs; ++i)
    {
        switch ((enum w_event_type_t)evs[i].type)
        {
            case W_EVENT_SPAWN_PLAYER:
                _handle_wev_spawn_player(&evs[i]);
                break;
            case W_EVENT_WALK_PLAYER:
                _handle_wev_walk_player(&evs[i]);
                break;
            case W_EVENT_PLAYER_DIR:
                _handle_wev_player_dir(&evs[i]);
                break;
            case W_EVENT_SPAWN_DYNAMIC_OBJ:
                _handle_wev_spawn_dynamic_obj(&evs[i]);
                break;
            case W_EVENT_TELEPORT_PLAYER:
                _handle_wev_teleport_player(&evs[i]);
                break;
            case W_EVENT_SPAWN_CREATURE:
                _handle_wev_spawn_creature(&evs[i]);
                break;
            case W_EVENT_WALK_CREATURE:
                _handle_wev_walk_creature(&evs[i]);
                break;
            case W_EVENT_CREATURE_DIR:
                _handle_wev_creature_dir(&evs[i]);
                break;
            case W_EVENT_CREATURE_PLAYER_EMOTE:
                _handle_wev_creature_player_emote(&evs[i]);
                break;
        }
    }
    w_clear_events();
}

static int
_handle_swmsg_load_instance_part(swmsg_load_instance_part_t *s)
{
    DEBUG_PRINTFF("called for map id %u\n", s->map_id);

    int r = w_load_instance_part(s->map_id, s->part_id, s->num_entities,
        s->cx, s->cy, s->cw, s->ch);
    wsmsg_load_instance_part_result_t f;
    f.part_id   = s->part_id;
    f.result    = !r ? 0 : WORLDD_ERR_MAP_NOT_FOUND;

    if (r)
        DEBUG_PRINTF("%s: failed to add instance part %u (code %d).\n",
            __func__, s->part_id, r);

    bbuf_t *bb = _send_msg(WSMSG_LOAD_INSTANCE_PART_RESULT_SZ);
    if (!bb) muta_panic(MUTA_ERR_OUT_OF_MEMORY, __func__);
    wsmsg_load_instance_part_result_write_const_encrypted(bb,
        &_conn.cc, &f);
    return 0;
}

static int
_handle_swmsg_spawn_player(swmsg_spawn_player_t *s)
{
    int r = w_spawn_player(s->id, s->race, s->inst_part_id, s->x, s->y, s->z,
        s->dir);
    if (!r) return 0;
    wsmsg_spawn_player_fail_t f;
    f.id        = s->id;
    bbuf_t *bb = _send_msg(WSMSG_SPAWN_PLAYER_FAIL_SZ);
    if (!bb) muta_panic(MUTA_ERR_OUT_OF_MEMORY, __func__);
    wsmsg_spawn_player_fail_write(bb, &f);
    return 0;
}

static int
_handle_swmsg_move_player(swmsg_move_player_t *s)
{
    int r = w_walk_player(s->id, s->dir);
    return !r ? 0 : 1;
}

static int
_handle_swmsg_despawn_player(swmsg_despawn_player_t *s)
    {w_despawn_player(s->id); return 0;}

static int
_handle_swmsg_find_path_player(swmsg_find_path_player_t *s)
    {w_find_path_player(s->id, s->x, s->y, s->z); return 0;}

static int
_handle_swmsg_spawn_dynamic_obj(swmsg_spawn_dynamic_obj_t *s)
{
    w_spawn_dynamic_obj(s->id, s->type_id, s->inst_part_id, s->x, s->y, s->z,
        s->dir);
    return 0;
}

static int
_handle_swmsg_despawn_dynamic_obj(swmsg_despawn_dynamic_obj_t *s)
    {w_despawn_dynamic_obj(s->id); return 0;}

static int
_handle_swmsg_teleport_player(swmsg_teleport_player_t *s)
    {w_teleport_player(s->id, s->x, s->y, s->z); return 0;}

static int
_handle_swmsg_spawn_creature(swmsg_spawn_creature_t *s)
{
    w_spawn_creature(s->id, s->type_id, s->inst_part_id, s->x, s->y, s->z,
        s->dir, s->flags);
    return 0;
}

static int
_handle_swmsg_despawn_creature(swmsg_despawn_creature_t *s)
{
    IMPLEMENTME();
}

static int
_handle_swmsg_reload_entity_script(swmsg_reload_entity_script_t *s)
{
    char *name = stack_alloc(s->script_name_len + 1);
    memcpy(name, s->script_name, s->script_name_len);
    name[s->script_name_len] = 0;
    sbnk_reload(name);
    return 0;
}

static void
_handle_wev_spawn_player(w_event_t *ev)
{
    wsmsg_confirm_spawn_player_t s;
    s.id        = ev->d.spawn_player.id;
    s.map_id    = ev->d.spawn_player.map_id;
    s.x         = ev->d.spawn_player.x;
    s.y         = ev->d.spawn_player.y;
    s.z         = ev->d.spawn_player.z;
    s.dir       = ev->d.spawn_player.dir;

    bbuf_t *bb = _send_msg(WSMSG_CONFIRM_SPAWN_PLAYER_SZ);
    wsmsg_confirm_spawn_player_write(bb, &s);

    printf("Confirmed spawn of player %llu to master.\n", (lluint)s.id);
}

static void
_handle_wev_walk_player(w_event_t *ev)
{
    wsmsg_walk_player_t s;
    s.id    = ev->d.walk_player.id;
    s.x     = ev->d.walk_player.pos.x;
    s.y     = ev->d.walk_player.pos.y;
    s.z     = ev->d.walk_player.pos.z;
    bbuf_t *bb = _send_msg(WSMSG_WALK_PLAYER_SZ);
    wsmsg_walk_player_write(bb, &s);
}

static void
_handle_wev_player_dir(w_event_t *ev)
{
    wsmsg_player_dir_t s;
    s.id    = ev->d.player_dir.id;
    s.dir   = ev->d.player_dir.dir;
    bbuf_t *bb = _send_msg(WSMSG_PLAYER_DIR_SZ);
    wsmsg_player_dir_write(bb, &s);
}

static void
_handle_wev_spawn_dynamic_obj(w_event_t *ev)
{
    puts(__func__);
    wsmsg_confirm_spawn_dynamic_obj_t s;
    s.id            = ev->d.spawn_dynamic_obj.id;
    s.inst_part_id  = ev->d.spawn_dynamic_obj.map_id;
    s.x             = ev->d.spawn_dynamic_obj.x;
    s.y             = ev->d.spawn_dynamic_obj.y;
    s.z             = ev->d.spawn_dynamic_obj.z;
    s.dir           = ev->d.spawn_dynamic_obj.dir;

    bbuf_t *bb = _send_msg(WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ_SZ);
    wsmsg_confirm_spawn_dynamic_obj_write(bb, &s);

    printf("Confirmed spawn of dynamic object %llu to master.\n", (lluint)s.id);
}

static void
_handle_wev_teleport_player(w_event_t *ev)
{
    wsmsg_teleport_player_t s;
    s.id    = ev->d.teleport_player.id;
    s.x     = ev->d.teleport_player.pos.x;
    s.y     = ev->d.teleport_player.pos.y;
    s.z     = ev->d.teleport_player.pos.z;
    bbuf_t *bb = _send_msg(WSMSG_TELEPORT_PLAYER_SZ);
    wsmsg_teleport_player_write(bb, &s);
}

static void
_handle_wev_spawn_creature(w_event_t *ev)
{
    DEBUG_PRINTFF("called.\n");
    wsmsg_confirm_spawn_creature_t s;
    s.id            = ev->d.spawn_creature.id;
    s.inst_part_id  = ev->d.spawn_creature.map_id;
    s.x             = ev->d.spawn_creature.x;
    s.y             = ev->d.spawn_creature.y;
    s.z             = ev->d.spawn_creature.z;
    s.dir           = ev->d.spawn_creature.dir;
    bbuf_t *bb = _send_msg(WSMSG_CONFIRM_SPAWN_CREATURE_SZ);
    wsmsg_confirm_spawn_creature_write(bb, &s);
}

static void
_handle_wev_walk_creature(w_event_t *ev)
{
    wsmsg_walk_creature_t s;
    s.id    = ev->d.walk_creature.id;
    s.x     = ev->d.walk_creature.pos.x;
    s.y     = ev->d.walk_creature.pos.y;
    s.z     = ev->d.walk_creature.pos.z;
    bbuf_t *bb = _send_msg(WSMSG_WALK_CREATURE_SZ);
    wsmsg_walk_creature_write(bb, &s);
}

static void
_handle_wev_creature_dir(w_event_t *ev)
{
    wsmsg_creature_dir_t s;
    s.id    = ev->d.creature_dir.id;
    s.dir   = ev->d.creature_dir.dir;
    bbuf_t *bb = _send_msg(WSMSG_CREATURE_DIR_SZ);
    wsmsg_creature_dir_write(bb, &s);
}

static void
_handle_wev_creature_player_emote(w_event_t *ev)
{
    wsmsg_creature_player_emote_t s;
    s.creature_id   = ev->d.creature_player_emote.creature_id;
    s.player_id     = ev->d.creature_player_emote.player_id;
    s.emote_id      = ev->d.creature_player_emote.emote_id;
    bbuf_t *bb = _send_msg(WSMSG_CREATURE_PLAYER_EMOTE_SZ);
    wsmsg_creature_player_emote_write(bb, &s);
}
