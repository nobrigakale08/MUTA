#include <stddef.h>
#include "world.h"
#include "maps.h"
#include "instance_part.h"
#include "entity.h"
#include "../../shared/world_common.h"
#include "../../shared/world_common.inl"
#include "../../shared/sv_common.inl"

#define MAX_MAPS 16

typedef w_event_t w_event_darr_t;

enum entity_walk_result_t
{
    ENT_WALK_TIMER = 1,
    ENT_WALK_BLOCKED,
    ENT_WALK_ERR,
};

enum entity_flag_t
{
    ENT_FLAG_REMOVED
};

DYNAMIC_HASH_TABLE_DEFINITION(pguid_ent_cont_table, ent_cont_t*, player_guid_t,
    player_guid_t, HASH_FROM_NUM, 4);
DYNAMIC_HASH_TABLE_DEFINITION(dguid_ent_cont_table, ent_cont_t*, dobj_guid_t,
    dobj_guid_t, HASH_FROM_NUM, 4);
DYNAMIC_HASH_TABLE_DEFINITION(cguid_ent_cont_table, ent_cont_t*,
    creature_guid_t, creature_guid_t, HASH_FROM_NUM, 4);

static muta_map_db_t            _w_map_db;
static instance_part_t          _w_insts[MAX_MAPS];
static uint32                   _w_num_inst_parts;
static w_event_darr_t           *_w_events;
static pguid_ent_cont_table_t   _w_guid_player_table;
static dguid_ent_cont_table_t   _w_guid_dynamic_obj_table;
static cguid_ent_cont_table_t   _w_guid_creature_table;

/* Each iso_dir_t mapped to a 2D vector */
static int _w_dir_move_table[NUM_ISODIRS][2] =
{
    { 0, -1}, /* ISODIR_NORTH */
    { 1, -1}, /* ISODIR_NORTH_EAST */
    { 1,  0}, /* ISODIR_EAST */
    { 1,  1}, /* ISODIR_SOUTH_EAST */
    { 0,  1}, /* ISODIR_SOUTH */
    {-1,  1}, /* ISODIR_SOUTH_WEST */
    {-1,  0}, /* ISODIR_WEST */
    {-1, -1}  /* ISODIR_NORTH_WEST */
};

#define _w_post_event(ev) darr_push(_w_events, ev)

static entity_t *
_w_create_empty_entity(uint32 map_id);

static instance_part_t *
_w_get_instance_part_by_id(uint32 id);

static inline entity_t *
_w_get_player(player_guid_t id);

static inline entity_t *
_w_get_creature(creature_guid_t id);

static inline entity_t *
_w_get_dynamic_obj(dobj_guid_t id);

static void
_w_update_instance_part_cmp_mobility_set(instance_part_t *inst_part, double dt);

static void
_w_update_instance_part_cmp_pathfind_set(instance_part_t *inst_part);

static void
_w_purge_removed_ents(instance_part_t *map);

static int
_w_walk_entity(entity_t *e, int dir);

static void
_w_destroy_entity(entity_t *e);

int
w_init(uint max_players, uint max_creatures, pf_config_t *pf_cfg,
    const char *map_db_path, const char *tile_path, const char *sobj_path,
    const char *dobj_path, const char *prace_path)
{
    if (sbnk_init())
        return 1;

    pguid_ent_cont_table_einit(&_w_guid_player_table, max_players);
    dguid_ent_cont_table_einit(&_w_guid_dynamic_obj_table, 512);
    cguid_ent_cont_table_einit(&_w_guid_creature_table, 512);
    if (pf_init(pf_cfg, 256))
        return 1;
    int r = 0;
    if (wc_load_tile_defs(tile_path, 0))        r |= (0 << 1);
    if (wc_load_static_obj_defs(sobj_path))     r |= (0 << 2);
    if (wc_load_dynamic_obj_defs(dobj_path))    r |= (0 << 3);
    if (wc_load_player_race_defs(prace_path))   r |= (0 << 4);
    if (wc_load_creature_defs("muta-data/common/creatures.def"))   r |= (0 << 5);
    if (r)
        printf("Failed to load some world definition files, mask %d.\n", r);

    if (maps_init())
        return 2;

    if (muta_map_db_load(&_w_map_db, map_db_path))
        printf("%s not found. No maps will be loaded.\n", map_db_path);

    return 0;
}

void
w_destroy()
{
    for (uint32 i = 0; i < _w_num_inst_parts; ++i)
        instance_part_destroy(&_w_insts[i]);
    _w_num_inst_parts = 0;
    pguid_ent_cont_table_destroy(&_w_guid_player_table);
    sbnk_destroy();
    maps_destroy();
}

int
w_load_instance_part(uint32 map_id, uint32 part_id, uint32 num_ents,
    uint32 cx, uint32 cy, uint32 cw, uint32 ch)
{
    map_data_t      *md     = maps_claim(map_id, cx, cy, cw, ch);
    instance_part_t *map    = &_w_insts[_w_num_inst_parts];
    if (instance_part_init(map, part_id, md, num_ents))
        return 3;
    _w_num_inst_parts++;
    return 0;
}

int
w_spawn_player(player_guid_t id, player_race_id_t race, uint32 map_id, int32 x,
    int32 y, int8 z, int dir)
{
    DEBUG_PRINTF("Attempting to spawn player to instance id %u.\n", map_id);

    entity_t *e = _w_create_empty_entity(map_id);
    if (!e) return 1;

    w_despawn_player(id); /* Despawn if already exists */

    world_pos_t pos;
    if (instance_part_find_nearest_spawnable_pos(e->inst, x, y, z, 3, &pos))
        return 2;

    entity_set_pos(e, pos.x, pos.y, pos.z);
    entity_set_dir(e, dir);
    e->type                 = ENT_TYPE_PLAYER;
    e->type_data.player.id  = id;

    pguid_ent_cont_table_einsert(&_w_guid_player_table, id,
        ENTITY_CONTAINER(e));

    {
        cmp_mobility_t cmp = {0};
        cmp.walk_timer = 0;
        cmp.walk_speed = WC_PLAYER_BASE_WALK_SPEED;
        entity_attach_cmp_mobility(e, &cmp);
    }

    {
        cmp_pathfind_t cmp = {0};
        entity_attach_cmp_pathfind(e, &cmp);
    }

    wpos_t lpos = entity_get_local_pos(e);

    w_event_t ev;
    ev.type                     = W_EVENT_SPAWN_PLAYER;
    ev.d.spawn_player.id        = id;
    ev.d.spawn_player.map_id    = map_id;
    ev.d.spawn_player.x         = lpos.x;
    ev.d.spawn_player.y         = lpos.y;
    ev.d.spawn_player.z         = lpos.z;
    ev.d.spawn_player.dir       = entity_get_dir(e);
    _w_post_event(ev);
    return 0;
}

void
w_despawn_player(player_guid_t id)
{
    DEBUG_PRINTF("%s called.\n", __func__);

    entity_t *e = _w_get_player(id);
    if (!e)
    {
        DEBUG_PRINTF("%s: entity not found.\n", __func__);
        return;
    }

    if (e->flags & ENT_FLAG_REMOVED)
        {printf("%s: entity already removed.\n", __func__); return;}

    despawn_ent_cmd_t cmd;
    cmd.ent_type            = ENT_TYPE_PLAYER;
    cmd.e                   = e;
    cmd.type_data.player    = id;

    e->flags |= ENT_FLAG_REMOVED;
    darr_push(e->inst->despawn_ent_cmds, cmd);
    DEBUG_PRINTFF("successful.\n");
}

int
w_spawn_dynamic_obj(dobj_guid_t id, dobj_type_id_t type_id, uint32 map_id,
    int32 x, int32 y, int8 z, int dir)
{
    w_despawn_dynamic_obj(id); /* Despawn if already exists */

    entity_t *e = _w_create_empty_entity(map_id);
    if (!e) return 1;

    world_pos_t pos;
    if (instance_part_find_nearest_spawnable_pos(e->inst, x, y, z, 3, &pos))
        {_w_destroy_entity(e); return 1;}

    entity_set_pos(e, pos.x, pos.y, pos.z);
    entity_set_dir(e, dir);
    e->type                             = ENT_TYPE_DYNAMIC_OBJ;
    e->type_data.dynamic_obj.id         = id;
    e->type_data.dynamic_obj.type_id    = type_id;

    if (dguid_ent_cont_table_insert(&_w_guid_dynamic_obj_table, id,
        ENTITY_CONTAINER(e)))
        {_w_destroy_entity(e); return 3;}

    {
        cmp_mobility_t cmp = {0};
        cmp.walk_timer = 0;
        cmp.walk_speed = WC_PLAYER_BASE_WALK_SPEED;
        entity_attach_cmp_mobility(e, &cmp);
    }

    {
        cmp_pathfind_t cmp = {0};
        entity_attach_cmp_pathfind(e, &cmp);
    }

    wpos_t lpos = entity_get_local_pos(e);

    w_event_t ev;
    ev.type                         = W_EVENT_SPAWN_DYNAMIC_OBJ;
    ev.d.spawn_dynamic_obj.id       = id;
    ev.d.spawn_dynamic_obj.map_id   = map_id;
    ev.d.spawn_dynamic_obj.x        = lpos.x;
    ev.d.spawn_dynamic_obj.y        = lpos.y;
    ev.d.spawn_dynamic_obj.z        = lpos.z;
    ev.d.spawn_dynamic_obj.dir      = entity_get_dir(e);
    _w_post_event(ev);
    return 0;
}

void
w_despawn_dynamic_obj(dobj_guid_t id)
{
    DEBUG_PRINTFF("called.\n");

    entity_t *e = _w_get_dynamic_obj(id);
    if (!e) {printf("%s: entity not found.\n", __func__); return;}

    if (e->flags & ENT_FLAG_REMOVED)
        {printf("%s: entity already removed.\n", __func__); return;}

    despawn_ent_cmd_t cmd;
    cmd.ent_type                = ENT_TYPE_DYNAMIC_OBJ;
    cmd.e                       = e;
    cmd.type_data.dynamic_obj   = id;

    e->flags |= ENT_FLAG_REMOVED;
    darr_push(e->inst->despawn_ent_cmds, cmd);
}

int
w_spawn_creature(creature_guid_t id, creature_type_id_t type_id, uint32 map_id,
    int32 x, int32 y, int8 z, int dir, uint8 flags)
{
    w_despawn_creature(id);

    creature_def_t *def = wc_get_creature_def(type_id);
    if (!def)
    {
        printf("Spawn error: no creature definition for id '%u'\n", type_id);
        return 1;
    }

    entity_t *e = _w_create_empty_entity(map_id);
    if (!e)
        return 1;

    world_pos_t pos;
    if (instance_part_find_nearest_spawnable_pos(e->inst, x, y, z, 3, &pos))
        {_w_destroy_entity(e); return 1;}

    entity_set_pos(e, pos.x, pos.y, pos.z);
    entity_set_dir(e, dir);
    e->type                             = ENT_TYPE_CREATURE;
    e->type_data.dynamic_obj.id         = id;
    e->type_data.dynamic_obj.type_id    = type_id;
    e->type_data.creature.flags         = flags;

    if (cguid_ent_cont_table_insert(&_w_guid_creature_table, id,
        ENTITY_CONTAINER(e)))
    {
        _w_destroy_entity(e);
        return 3;
    }

    {
        cmp_mobility_t cmp  = {0};
        cmp.walk_timer      = 0;
        cmp.walk_speed      = WC_CREATURE_BASE_WALK_SPEED;
        entity_attach_cmp_mobility(e, &cmp);
    }

    {
        cmp_pathfind_t cmp = {0};
        entity_attach_cmp_pathfind(e, &cmp);
    }

    {
        cmp_lua_t cmp = {0};
        entity_attach_cmp_lua(e, &cmp);
        cmp_lua_t *cmp_ptr = entity_get_cmp_lua(e);
        printf("script name: %s\n", def->script_name);
        if (def->script_name)
            cmp_lua_set_script(cmp_ptr, def->script_name);
    }

    wpos_t lpos = entity_get_local_pos(e);

    w_event_t ev;
    ev.type                     = W_EVENT_SPAWN_CREATURE;
    ev.d.spawn_creature.id      = id;
    ev.d.spawn_creature.map_id  = map_id;
    ev.d.spawn_creature.x       = lpos.x;
    ev.d.spawn_creature.y       = lpos.y;
    ev.d.spawn_creature.z       = lpos.z;
    ev.d.spawn_creature.dir     = entity_get_dir(e);
    ev.d.spawn_creature.flags   = e->type_data.creature.flags;
    _w_post_event(ev);
    return 0;
}

void
w_despawn_creature(creature_guid_t id)
{
    IMPLEMENTME();
}

int
w_find_path_player(uint64 id, int32 x, int32 y, int8 z)
{
    entity_t *e = _w_get_player(id);
    if (!e) return 1;
    return entity_find_path(e, x, y, z) ? 2 : 0;
}

int
w_walk_player(player_guid_t id, int dir)
{
    entity_t *e = _w_get_player(id);
    if (!e)
        return 1;
    cmp_pathfind_t *pfc = entity_get_cmp_pathfind(e);
    if (pfc)
        cmp_pathfind_stop(pfc);
    return _w_walk_entity(e, dir) != ENT_WALK_ERR ? 0 : 2;
}

int
w_walk_creature(creature_guid_t id, int dir)
{
    entity_t *e = _w_get_creature(id);
    if (!e)
        return 1;
    cmp_pathfind_t *pfc = entity_get_cmp_pathfind(e);
    if (pfc)
        cmp_pathfind_stop(pfc);
    return _w_walk_entity(e, dir) != ENT_WALK_ERR ? 0 : 2;
}

int
w_teleport_player(player_guid_t id, int32 x, int32 y, int8 z)
{
    entity_t *e = _w_get_player(id);
    if (!e)
        return 1;
    entity_set_pos(e, x, y, z);
    w_event_t ev;
    ev.type                     = W_EVENT_TELEPORT_PLAYER;
    ev.d.teleport_player.id     = id;
    ev.d.teleport_player.pos    = entity_get_pos(e);
    _w_post_event(ev);
    return 0;
}

void
w_post_event(w_event_t *ev)
    {_w_post_event(*ev);}

w_event_t *
w_get_events(uint *ret_num)
{
    *ret_num = darr_num(_w_events);
    return _w_events;
}

void
w_clear_events()
    {darr_clear(_w_events);}

void
w_update(double dt)
{
    instance_part_t *inst;
    for (uint32 i = 0; i < _w_num_inst_parts; ++i)
    {
        inst = &_w_insts[i];

        /* Fixed timestep */
        double target   = inst->target_dt ? inst->target_dt : dt;
        double accum    = inst->accum_dt + dt;

        while (accum >= target)
        {
            _w_update_instance_part_cmp_mobility_set(inst, target);
            _w_update_instance_part_cmp_pathfind_set(inst);
            update_cmp_lua_scripts(inst, target);
            _w_purge_removed_ents(inst);

            pf_wait_for_jobs(); /* pf jobs should be per instance part... */

            uint32 num = darr_num(inst->pf_results);
            for (uint32 j = 0; j < num; ++j)
            {
                entity_t        *e      = inst->pf_results[j].entity;
                cmp_pathfind_t  *cmp    = entity_get_cmp_pathfind(e);
                cmp->path               = inst->pf_results[j].path;
                cmp->path_node_index    = cmp->path->num_nodes -1;
            }
            darr_clear(inst->pf_results);
            accum -= target;
        }

        inst->accum_dt = accum;
    }
}

void
w_on_reload_lua_script(lua_script_t *script, void (*callback)(void*,
    lua_script_t *script))
{
    uint32 num_ips = _w_num_inst_parts;

    for (uint32 i = 0; i < num_ips; ++i)
    {
        callback(_w_insts[i].lua.state, script);

        uint32 num_cmps;
        cmp_lua_pool_item_t *items = cmp_lua_pool_get_items(
            &_w_insts[i].cmp_lua_pool, &num_cmps);
        for (uint32 j = 0; j < num_cmps; ++j)
        {
            if (items[j].cmp.script != script)
                continue;
            cmp_lua_reload(&items[j].cmp);
        }
    }
}

static entity_t *
_w_create_empty_entity(uint32 inst_part_id)
{
    instance_part_t *ip = _w_get_instance_part_by_id(inst_part_id);
    if (!ip)
    {
        DEBUG_PRINTFF("no instance part %u (num parts: %u).\n", inst_part_id,
            _w_num_inst_parts);
        return 0;
    }
    ent_cont_t *ec = obj_pool_reserve(&ip->ents);
    if (!ec)
    {
        DEBUG_PRINTF("%s: could not reserve entity.\n", __func__);
        return 0;
    }
    entity_t *e = &ec->e;
    e->flags        = 0;
    e->inst         = ip;
    e->type         = ENT_TYPE_UNKNOWN;
    e->cmp_flags    = 0;
    e->dir          = 0;
    e->pos.x        = 0;
    e->pos.y        = 0;
    e->pos.z        = 0;
    e->darr_index   = darr_num(ip->ents_darr);
    darr_push(ip->ents_darr, e);
    return e;
}

static instance_part_t *
_w_get_instance_part_by_id(uint32 id)
{
    DEBUG_PRINTF("%s: attempting id %u\n", __func__, id);
    for (uint32 i = 0; i < _w_num_inst_parts; ++i)
    {
        if (_w_insts[i].runtime_id == id)
            return &_w_insts[i];
        DEBUG_PRINTF("%s: checked id %u\n", __func__, _w_insts[i].runtime_id);
    }
    return 0;
}

static inline entity_t *
_w_get_player(player_guid_t id)
{
    ent_cont_t **ec = pguid_ent_cont_table_get_ptr(&_w_guid_player_table, id);
    return ec ? &(*ec)->e : 0;
}

static inline entity_t *
_w_get_creature(creature_guid_t id)
{
    ent_cont_t **ec = cguid_ent_cont_table_get_ptr(&_w_guid_creature_table, id);
    return ec ? &(*ec)->e : 0;
}

static inline entity_t *
_w_get_dynamic_obj(dobj_guid_t id)
{
    ent_cont_t **ec = dguid_ent_cont_table_get_ptr(&_w_guid_dynamic_obj_table,
        id);
    return ec ? &(*ec)->e : 0;
}

static void
_w_update_instance_part_cmp_mobility_set(instance_part_t *inst_part, double dt)
{
    uint32                      num, i;
    cmp_mobility_pool_item_t    *items;
    items = cmp_mobility_pool_get_items(&inst_part->cmp_mobility_pool, &num);
    cmp_mobility_t *cmp;

    for (i = 0; i < num; ++i)
    {
        cmp = &items[i].cmp;
        cmp->walk_timer -= (float)MIN(dt, (double)cmp->walk_timer);
    }
}

static void
_w_update_instance_part_cmp_pathfind_set(instance_part_t *inst_part)
{
    uint32                      num, i;
    cmp_pathfind_pool_item_t    *items;
    items = cmp_pathfind_pool_get_items(&inst_part->cmp_pathfind_pool, &num);

    int32           dx, dy;
    cmp_pathfind_t  *cmp;
    pf_path_t       *path;
    entity_t        *e;
    uint16          node_index;
    world_pos_t     pos;
    int             walk_res;

    for (i = 0; i < num; ++i)
    {
        cmp = &items[i].cmp;
        if (!cmp->path) continue;

        e   = cmp_pathfind_get_entity(cmp);
        pos = entity_get_pos(e);

        path        = cmp->path;
        node_index  = cmp->path_node_index;

        dx = path->nodes[node_index].x - pos.x;
        dy = path->nodes[node_index].y - pos.y;

        walk_res = _w_walk_entity(e, vec2_to_iso_dir(dx, dy));
        if (walk_res == ENT_WALK_TIMER || walk_res != 0)
            continue;

        if (node_index == 0)
        {
            pf_free_path(path);
            cmp->path = 0;
        } else
            cmp->path_node_index = node_index - 1;
    }
}

static void
_w_purge_removed_ents(instance_part_t *map)
{
    uint32              num     = darr_num(map->despawn_ent_cmds);
    despawn_ent_cmd_t   *items  = map->despawn_ent_cmds;
    for (uint32 i = 0; i < num; ++i)
    {
        switch (items[i].ent_type)
        {
        case ENT_TYPE_PLAYER:
        {
            if (items[i].e->type_data.player.id
            != items[i].type_data.player)
            {
                DEBUG_PRINTF("%s: wrong player id (ids: %llu, %llu)!\n",
                    __func__, (lluint)items[i].e->type_data.player.id,
                    (lluint)items[i].type_data.player);
                continue;
            }
            pguid_ent_cont_table_erase(&_w_guid_player_table,
                items[i].type_data.player);
            printf("%s: despawned player. IMPLEMENT: confirmation msg.\n",
                __func__);
        }
            break;
        case ENT_TYPE_DYNAMIC_OBJ:
            dguid_ent_cont_table_erase(&_w_guid_dynamic_obj_table,
                items[i].type_data.dynamic_obj);
            break;
        case ENT_TYPE_CREATURE:
            IMPLEMENTME();
            break;
        }
        _w_destroy_entity(items[i].e);
    }
    darr_clear(map->despawn_ent_cmds);
}

static int
_w_walk_entity(entity_t *e, int dir)
{
    if (dir < 0 || dir >= NUM_ISODIRS) return ENT_WALK_ERR;

    cmp_mobility_t *mcmp = entity_get_cmp_mobility(e);
    if (mcmp && mcmp->walk_timer > 0) return ENT_WALK_TIMER;

    instance_part_t  *inst  = e->inst;
    world_pos_t pos         = entity_get_local_pos(e);

    /* Destination */
    int32   nx = pos.x;
    int32   ny = pos.y;
    int8    nz = pos.z;

    /* Get the tile the entity is standing on */
    tile_def_t *tb = instance_part_get_tile_def(inst, nx, ny, nz - 1);

    /* Is the destination on a lower level? */
    if (check_ramp_down(dir, tb->ramp) == 0)
        nz = CLAMP(nz - 1, 0, MAP_CHUNK_T - 1);

    int tmp_nx = nx + _w_dir_move_table[dir][0];
    int tmp_ny = ny + _w_dir_move_table[dir][1];

    nx = CLAMP(tmp_nx, 0, (int)(instance_part_tiled_w(inst) - 1));
    ny = CLAMP(tmp_ny, 0, (int)(instance_part_tiled_h(inst) - 1));

    /* Destination */
    tile_def_t *t = instance_part_get_tile_def(inst, nx, ny, nz);

    int let_move = 0;
    int err_code = 0;

    if (t->passthrough)
    {
        if (nz > 1 && instance_part_get_tile_def(inst, nx, ny, nz - 1)->passthrough)
            let_move = 0;
        else
            let_move = 1;
    } else
    if (check_ramp_up(dir, t->ramp) == 0)
    {
        /* The entity is moving up a ramp  */
        nz = CLAMP(nz + 1, 0, MAP_CHUNK_T - 1);

        /* Check if there's something on top of the ramp */
        if (instance_part_get_tile_def(inst, nx, ny, nz + 1)->passthrough)
            {err_code = 0; let_move = 1;}
    } else
        err_code = ENT_WALK_BLOCKED;

    int odir = e->dir;
    int rdir = vec2_to_iso_dir(nx - pos.x, ny - pos.y);
    entity_set_dir(e, rdir);

    w_event_t ev;

    if (let_move && (pos.x != nx || pos.y != ny || pos.z != nz))
    {
        entity_set_pos(e, nx, ny, nz);
        if (mcmp)
            mcmp->walk_timer = WC_WALK_SPEED_TO_SEC(mcmp->walk_speed);

        /* Generate a world event */
        switch (e->type)
        {
        case ENT_TYPE_PLAYER:
        {
            ev.type                 = W_EVENT_WALK_PLAYER;
            ev.d.walk_player.id     = e->type_data.player.id;
            ev.d.walk_player.pos.x  = nx;
            ev.d.walk_player.pos.y  = ny;
            ev.d.walk_player.pos.z  = nz;
        }
            break;
        case ENT_TYPE_CREATURE:
        {
            ev.type = W_EVENT_WALK_CREATURE;
            ev.d.walk_creature.id       = e->type_data.creature.id;
            ev.d.walk_creature.pos.x    = nx;
            ev.d.walk_creature.pos.y    = ny;
            ev.d.walk_creature.pos.z    = nz;
        }
            break;
        default:
            muta_assert(0);
        }
        _w_post_event(ev);
    } else
    if (odir != rdir) /* Direction change? */
    {
        switch (e->type)
        {
        case ENT_TYPE_PLAYER:
            ev.type             = W_EVENT_PLAYER_DIR;
            ev.d.player_dir.dir = (uint8)rdir;
            break;
        case ENT_TYPE_CREATURE:
            ev.type                 = W_EVENT_CREATURE_DIR;
            ev.d.creature_dir.dir   = (uint8)rdir;
            break;
        default:
            muta_assert(0);
        }
        _w_post_event(ev);
    }
    return err_code;
}

static void
_w_destroy_entity(entity_t *e)
{
    {
        cmp_pathfind_t *cmp = entity_get_cmp_pathfind(e);
        if (cmp) cmp_pathfind_stop(cmp);
    }
    entity_remove_cmp_mobility(e);
    entity_remove_cmp_pathfind(e);
    entity_remove_cmp_lua(e);

    uint32 last_index = darr_num(e->inst->ents_darr) - 1;
    entity_t *last                      = e->inst->ents_darr[last_index];
    last->darr_index                    = e->darr_index;
    e->inst->ents_darr[e->darr_index]   = last;
    _darr_head(e->inst->ents_darr)->num = last_index;

    obj_pool_free(&e->inst->ents, ENTITY_CONTAINER(e));
}
