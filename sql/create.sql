CREATE USER muta@localhost IDENTIFIED BY 'muta';
CREATE DATABASE muta_accounts DEFAULT CHARACTER SET utf8 COLLATE
    utf8_general_ci;
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER, LOCK TABLES,
    CREATE TEMPORARY TABLES ON muta_accounts.* TO muta@localhost;

USE muta_accounts;
DROP TABLE IF EXISTS accounts;
DROP TABLE IF EXISTS characters;

CREATE TABLE accounts
(
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name        VARCHAR(32)     NOT NULL,
    pw          CHAR(128)       NOT NULL,
    email       TEXT,
    last_login  TIMESTAMP       NOT NULL DEFAULT '0000-00-00 00:00:00',

    CONSTRAINT unique_name UNIQUE (name),
    CONSTRAINT unique_id   UNIQUE(id),
    PRIMARY KEY(id)
);

CREATE TABLE characters
(
    id          BIGINT UNSIGNED AUTO_INCREMENT,
    account_id  BIGINT UNSIGNED,
    name        VARCHAR(32) NOT NULL,
    race        TINYINT UNSIGNED,
    sex         TINYINT UNSIGNED,
    instance_id INT UNSIGNED NOT NULL DEFAULT 0,
    map_id      INT UNSIGNED NOT NULL DEFAULT 0,
    pos_x       INT,
    pos_y       INT,
    pos_z       TINYINT,

    CONSTRAINT unique_name UNIQUE (name),
    PRIMARY KEY(id)
);
