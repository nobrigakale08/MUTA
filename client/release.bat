:: MUTA client release packing script

call configure.bat
nmake all-opt
    
if exist "release" rmdir /S /Q "release"

mkdir release

echo hostname = 127.0.0.1 > "release/server_info.cfg"

COPY "default_config.cfg"       "release\config.cfg"
COPY "rundir\muta_win32.exe"    "release\muta.exe"
COPY "rundir\SDL2.dll"          "release\SDL2.dll"
COPY "rundir\libsodium.dll"     "release\libsodium.dll"
COPY "rundir\OpenAL32.dll"      "release\OpenAL32.dll"

if exist "..\MUTA-Data"   XCOPY /i /e "..\MUTA-Data"    "release\muta-data"
if exist "..\MUTA-Assets" XCOPY /i /e "..\MUTA-Assets"  "release\muta-assets"

XCOPY /i /e "..\assets" "release\assets"
XCOPY /i /e "..\data"   "release\data"

mkdir release\paperwork
XCOPY /i /e "paperwork" "release\paperwork"

COPY "..\COPYING" "release\COPYING"

:: Delete unnecessary files
del /s release\*~
del /s release\*.swp
del /s release\*.swpo

