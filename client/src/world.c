#include <stddef.h>
#include "world_internal.h"
#include "core.h"
#include "assets.h"
#include "../../shared/world_common.inl"
#include "render.h"

#define ENTITY_WALK_SNAP_TRESHOLD   3 /* (In tiles, for animation snapping) */
#define WORLD_TIMESTEP              0.016667f
#define IDLE_ANIM_BEGIN_TIME        .250f

typedef struct threaded_work_div_t  threaded_work_div_t;
typedef struct mobility_up_arg_t    mobility_up_arg_t;

struct threaded_work_div_t
{
    uint32 num_jobs;
    uint32 num_per_job;
    uint32 num_last_job;
};

struct mobility_up_arg_t
{
    world_t                     *w;
    mobility_data_pool_item_t   *items;
    uint32                      first, num;
    double                      dt;
};

static muta_map_db_t _map_db;

static void
_entity_set_pos(entity_t *e, int x, int y, int z);

static void
_chunk_cache_purge_static_objs(chunk_cache_t *ch);

static int
_world_post_event(world_t *w, world_event_t ev);

static inline int
_world_post_event_unsafe(world_t *w, world_event_t ev);

static entity_t *
_world_create_empty_entity(world_t *w);

static entity_t *
_world_spawn_static_object_internal(world_t *w, static_obj_type_id_t type_id,
    uint32 file_index, int32 x, int32 y, int8 z, int dir);

static void
_world_update_mobility_components_callback(void *args);

static void
_world_update_mobility_components(world_t *w, double dt);

static void
_world_handle_events(world_t *w);

static void
_world_handle_wev_walk_finished(world_t *w, world_event_t *ev);

static void
_world_update_cmp_renderables(world_t *w);

static void
_world_update_cmp_iso_animators(world_t *w);

static void
_world_update_cmp_ae_animators(world_t *w);

static void
_world_purge_removed_entities(world_t *w);

static void
_world_defer_remove_entity(world_t *w, entity_t *e);

static void
_remove_entity_internal(entity_t *epi);

static inline void *
_world_stack_alloc(world_t *w, size_t sz);

static int
_world_load_static_objects(world_t *w);

static threaded_work_div_t
_divide_threaded_work(uint32 num_items);

static void
_cmp_spritesheet_clear(cmp_spritesheet_t *cmp);

static int
_cmp_spritesheet_set_asset(cmp_spritesheet_t *cmp, const char *fp,
    uint32 index, int16 ox, int16 oy);

static void
_cmp_renderable_set_gfx(cmp_renderable_t *cmp, tex_t *tex, float *clip);

static void
_cmp_renderable_get_rect(cmp_renderable_t *cmp, int ret_rect[4]);

static void
_cmp_iso_animator_update_dir(cmp_iso_animator_t *cmp, uint8 dir);

#if 0 /* Kept in case it'll be used for creatures or similar. */
static void
_cmp_iso_animator_play(cmp_iso_animator_t *cmp, const char *fp,
    bool32 loop);
#endif

static void
_cmp_iso_animator_stop(cmp_iso_animator_t *cmp);

static void
_cmp_ae_animator_play(cmp_ae_animator_t *cmp, ae_asset_t *asset, int dir,
    bool32 loop);

static void
_cmp_ae_animator_play_no_restart(cmp_ae_animator_t *cmp, ae_asset_t *asset,
    int dir, bool32 loop);

static void
_cmp_ae_animator_stop(cmp_ae_animator_t *cmp);

static inline bool32
_cmp_ae_animator_is_stopped(cmp_ae_animator_t *cmp);

static void
_cmp_ae_animator_set_dir(cmp_ae_animator_t *cmp, int dir);

#define NULL_CMP_REM_FUNC(e, item, last)
#define NULL_HANDLED_CMP_REM_FUNC(e, h, u, last_h, last_u)
#define NULL_STATIC_CMP_REM_FUNC(e, cmp)
#define NULL_CMP_ATTACH_CHECK_FUNC(e) 0
#define NULL_CMP_ATTACH_INIT_FUNC(cmp, e)
#define NULL_STATIC_CMP_REM_FUNC(e, cmp)

/*-- mobility_data --*/
ENT_CMP_DEF(mobility_data, mobility_data_pool, ENTCMP_MOBILITY_DATA,
    NULL_CMP_REM_FUNC, NULL_CMP_ATTACH_CHECK_FUNC, NULL_CMP_ATTACH_INIT_FUNC);

/*-- cmp_renderable --*/
static inline void _cmp_renderable_on_attach(cmp_renderable_t *cmp, entity_t *e)
{
    cmp->tx         = e->x;
    cmp->ty         = e->y;
    cmp->tz         = e->z;
    cmp->scale[0]   = 1.f;
    cmp->scale[1]   = 1.f;
    cmp->rot        = 0.f;
    cmp->travel_dir = INVALID_ISODIR;
    cmp->tex        = 0;
    cmp->ox         = 0;
    cmp->oy         = 0;
    cmp->flip       = 0;
}

ENT_CMP_DEF(cmp_renderable, cmp_renderable_pool, ENTCMP_RENDERABLE,
    NULL_CMP_REM_FUNC, NULL_CMP_ATTACH_CHECK_FUNC, _cmp_renderable_on_attach);

/* cmp_iso_animator */
ENT_CMP_DEF(cmp_iso_animator, cmp_iso_animator_pool, ENTCMP_ISO_ANIMATOR,
    NULL_CMP_REM_FUNC, NULL_CMP_ATTACH_CHECK_FUNC, NULL_CMP_ATTACH_INIT_FUNC);

/* cmp_ae_animator */
#define CMP_AE_ANIMATOR_ATTACH_CHECK_FUNC(e) \
    (!(entity_has_cmp_renderable(e)))

#define CMP_AE_ANIMATOR_REM_FUNC(e, item, last) \
    if ((item)->cmp.asset) (item)->cmp.asset = 0;

ENT_CMP_DEF(cmp_ae_animator, cmp_ae_animator_pool, ENTCMP_AE_ANIMATOR,
    CMP_AE_ANIMATOR_REM_FUNC, CMP_AE_ANIMATOR_ATTACH_CHECK_FUNC,
    NULL_CMP_ATTACH_INIT_FUNC);

/*-- cmp_spritesheet --*/
#define CMP_SPRITESHEET_ATTACH_CHECK_FUNC(e) (!entity_has_cmp_renderable(e))
#define CMP_SPRITESHEET_REM_FUNC(e, cmp) _cmp_spritesheet_clear(cmp)
ENT_STATIC_CMP_DEF(cmp_spritesheet, cmp_spritesheet_pool, ENTHCMP_SPRITESHEET,
    CMP_SPRITESHEET_REM_FUNC, CMP_SPRITESHEET_ATTACH_CHECK_FUNC,
    NULL_CMP_ATTACH_INIT_FUNC);

/*-- cmp_player_ae_set --*/
#define CMP_PLAYER_AE_SET_ATTACH_CHECK_FUNC(e) \
    (!entity_has_cmp_ae_animator(e) && (e)->type == ENT_TYPE_PLAYER)
#define CMP_PLAYER_AE_SET_REM_FUNC(e, cmp) \
    if ((cmp)->idle_asset) as_unclaim_ae((cmp)->idle_asset); \
    if ((cmp)->walk_asset) as_unclaim_ae((cmp)->walk_asset);

static void
_cmp_player_ae_set_init(cmp_player_ae_set_t *cmp, entity_t *e);

ENT_STATIC_CMP_DEF(cmp_player_ae_set, cmp_player_ae_set_pool,
    ENTHCMP_PLAYER_AE_SET, CMP_PLAYER_AE_SET_REM_FUNC,
    CMP_PLAYER_AE_SET_ATTACH_CHECK_FUNC, _cmp_player_ae_set_init);

/*-- cmp_creature_ae_set --*/

#define CMP_CREATURE_AE_SET_ATTACH_CHECK_FUNC(e) \
    (!entity_has_cmp_ae_animator(e) && (e)->type == ENT_TYPE_CREATURE)
#define CMP_CREATURE_AE_SET_REM_FUNC(e, cmp) \
    if ((cmp)->idle_asset) as_unclaim_ae((cmp)->idle_asset); \
    if ((cmp)->walk_asset) as_unclaim_ae((cmp)->walk_asset);

static void
_cmp_creature_ae_set_init(cmp_creature_ae_set_t *cmp, entity_t *e);

ENT_STATIC_CMP_DEF(cmp_creature_ae_set, cmp_creature_ae_set_pool,
    ENTHCMP_CREATURE_AE_SET, CMP_CREATURE_AE_SET_REM_FUNC,
    CMP_CREATURE_AE_SET_ATTACH_CHECK_FUNC, _cmp_creature_ae_set_init);

static int
_load_chunk_cache_from_disk(chunk_cache_t *cache)
{
    map_chunk_t *chunk = cache->chunk;
    int ret = 0;
    mutex_lock(&chunk->mtx);
    if (muta_chunk_file_load(&cache->file, chunk->path))
        ret = 1;
    else if (!wc_chunk_file_tiles_ok(&cache->file))
    {
        printf("Warning: Chunk file %s contains invalid tiles.\n", chunk->path);
        memset(cache->file.tiles, 0,
            MAP_CHUNK_W * MAP_CHUNK_W * MAP_CHUNK_T * sizeof(tile_t));
        ret = 2;
    }
    mutex_unlock(&chunk->mtx);
    return ret;
}

int
init_world_data()
{
    DEBUG_PUTS("\n### Loading world data ###");
    int ret = 0;
    int r;
    if ((r = wc_load_tile_defs(game_cfg.game_data.tile_path, 1)))
    {
        DEBUG_PRINTF("\tFailed to load tile defs, code %d.\n", r);
        ret |= (1 << 0);
    }
    if ((r = wc_load_static_obj_defs(game_cfg.game_data.static_obj_path)))
    {
        DEBUG_PRINTF("\tFailed to load static obj defs, code %d.\n", r);
        ret |= (1 << 2);
    }
    if ((r = wc_load_dynamic_obj_defs(game_cfg.game_data.dynamic_obj_path)))
    {
        DEBUG_PRINTF("\tFailed to load dynamic obj defs, code %d.\n", r);
        ret |= (1 << 3);
    }
    if ((r = wc_load_player_race_defs(game_cfg.game_data.player_race_path)))
    {
        DEBUG_PRINTF("\tFailed to load player race defs, code %d.\n", r);
        ret |= (1 << 4);
    }
    if ((r = wc_load_creature_defs(game_cfg.game_data.creature_path)))
    {
        DEBUG_PRINTF("\tFailed to load player race defs, code %d.\n", r);
        ret |= (1 << 5);
    }
    if ((r = muta_map_db_load(&_map_db, game_cfg.game_data.map_db_path)))
        printf("\tError: failed to load %s, code %d.\n",
            game_cfg.game_data.map_db_path, r);
    return ret;
}

void
destroy_world_data()
{
    wc_destroy_tile_defs();
    wc_destroy_static_obj_defs();
    wc_destroy_dynamic_obj_defs();
    wc_destroy_player_race_defs();
}

int
reload_world_data()
    {destroy_world_data(); return init_world_data();}

int
map_init(map_t *map, int w, int h)
{
    memset(map, 0, sizeof(map_t));

    int     ret         = 0;
    uint32  num_chunks  = w * h;

    if (num_chunks)
    {
        map->chunks = emalloc(num_chunks * sizeof(map_chunk_t));

        map->w                  = w;
        map->h                  = h;
        map->tw                 = w * MAP_CHUNK_W;
        map->th                 = h * MAP_CHUNK_W;
        map->num_alloc_chunks   = num_chunks;

        for (int i = 0; i < w * h; ++i)
        {
            map->chunks[i].path = 0;
            mutex_init(&map->chunks[i].mtx);
        }
    }

    for (int i = 0; i < 9; ++i)
    {
        mutex_init(&map->caches[i].mtx);
        if (muta_chunk_file_init(&map->caches[i].file))
            {ret = 2; goto out;}
        darr_reserve(map->caches[i].static_objs, 256);
    }

    out:
        if (ret)
            map_destroy(map);
        return ret;
}

void
map_resize(map_t *map, int w, int h)
{
    int num_chunks = w * h;
    if (map->num_alloc_chunks < num_chunks)
    {
        for (int i = 0; i < map->num_alloc_chunks; ++i)
            mutex_destroy(&map->chunks[i].mtx);
        map->chunks = erealloc(map->chunks, num_chunks * sizeof(map_chunk_t));
        map->num_alloc_chunks = num_chunks;
        for (int i = 0; i < num_chunks; ++i)
        {
            map->chunks[i].path = 0;
            mutex_init(&map->chunks[i].mtx);
        }
    }
    map->w  = w;
    map->h  = h;
    map->tw = w * MAP_CHUNK_W;
    map->th = h * MAP_CHUNK_W;
}

void
map_destroy(map_t *map)
{
    for (int i = 0; i < map->w * map->h; ++i)
        mutex_destroy(&map->chunks[i].mtx);
    for (int i = 0; i < 9; ++i)
    {
        muta_chunk_file_destroy(&map->caches[i].file);
        darr_free(map->caches[i].static_objs);
        mutex_destroy(&map->caches[i].mtx);
    }
    free(map->chunks);
    muta_map_file_destroy(&map->file);
    memset(map, 0, sizeof(map_t));
}

entity_handle_t
entity_get_handle(entity_t *e)
{
    entity_handle_t ret;
    ret.ptr = e;
    switch (e->type)
    {
        case ENT_TYPE_PLAYER:
            ret.id = e->type_data.player.id;
            break;
        default:
            IMPLEMENTME();
    }
    return ret;
}

int
entity_get_sex(entity_t *e)
    {return GET_BITFLAG(e->flags, ENT_SEX) == 0 ? SEX_MALE : SEX_FEMALE;}

static void
_entity_set_pos(entity_t *e, int x, int y, int z)
{
    e->x = x; e->y = y; e->z = z;
    cmp_renderable_t *cmp = entity_get_cmp_renderable(e);
    if (!cmp) return;
    cmp->tx = x; cmp->ty = y; cmp->tz = z;
}

static void
_chunk_cache_purge_static_objs(chunk_cache_t *ch)
{
    entity_t    **es    = ch->static_objs;
    uint32      num     = darr_num(es);
    for (uint32 i = 0; i < num; ++i)
        _remove_entity_internal(es[i]);
    darr_clear(ch->static_objs);
}

static int
_world_post_event(world_t *w, world_event_t ev)
{
    mutex_lock(&w->event_mtx);
    darr_push(w->events, ev);
    mutex_unlock(&w->event_mtx);
    return 0;
}

static inline int
_world_post_event_unsafe(world_t *w, world_event_t ev)
    {darr_push(w->events, ev); return 0;}

void
entity_set_dir(entity_t *e, int dir)
{
    if (dir < 0 || dir >= NUM_ISODIRS) return;
    e->dir = (uint8)dir;

    {
        cmp_iso_animator_t *cmp = entity_get_cmp_iso_animator(e);
        if (cmp)
            _cmp_iso_animator_update_dir(cmp, dir);
    }

    {
        cmp_ae_animator_t *cmp = entity_get_cmp_ae_animator(e);
        if (cmp)
            _cmp_ae_animator_set_dir(cmp, dir);
    }
}


float
world_render_props_scale_x(world_render_props_t *rp)
    {return (float)rp->screen_area[2] / (float)rp->coord_space[2];}

float
world_render_props_scale_y(world_render_props_t *rp)
    {return (float)rp->screen_area[3] / (float)rp->coord_space[3];}

int
world_render_props_px_pos_from_window(world_render_props_t *rp, int *ret_in_x,
    int *ret_in_y)
{
    if (*ret_in_x <  rp->scissor[0]
    ||  *ret_in_x >= rp->scissor[0] + rp->scissor[2]
    ||  *ret_in_y <  rp->scissor[1]
    ||  *ret_in_y >= rp->scissor[1] + rp->scissor[3])
        return 1;
    int         rx  = *ret_in_x - rp->screen_area[0];
    int         ry  = *ret_in_y - rp->screen_area[1];
    float       sx  = world_render_props_scale_x(rp);
    float       sy  = world_render_props_scale_y(rp);
    if (sx <= 0.f || sy <= 0.f) return 2;
    *ret_in_x = (int)((float)rx / sx);
    *ret_in_y = (int)((float)ry / sy);
    return 0;
}

int
world_set_player_dir_by_id(world_t *w, player_guid_t id, int dir)
{
    entity_t *e = world_get_player_by_id(w, id);
    if (!e) return 1;
    entity_set_dir(e, dir);
    return 0;
}

entity_t *
world_get_static_obj_by_window_coords(world_t *w, int x, int y)
{
    world_render_props_t    *rp = &w->render_props;
    uint                    i, j, num_objs;
    entity_t                **objs, *e, *ret = 0;
    int                     tx, ty;
    chunk_cache_t           *ch;
    cmp_renderable_t        *cmp;

    int px = x;
    int py = y;
    if (world_render_props_px_pos_from_window(rp, &px, &py))
        return 0;

    for (i = 0; i < 9; ++i)
    {
        ch = w->map.cache_ptrs[i];
        if (world_compute_chunk_cache_coords(w, ch, &tx, &ty)) continue;

        tx *= MAP_CHUNK_W;
        ty *= MAP_CHUNK_W;

        if (tx + MAP_CHUNK_W < rp->tx)  continue;
        if (tx >= rp->tx + rp->tw)      continue;
        if (ty + MAP_CHUNK_W < rp->ty)  continue;
        if (ty >= rp->ty + rp->th)      continue;

        objs        = ch->static_objs;
        num_objs    = darr_num(objs);

        for (j = 0; j < num_objs; ++j)
        {
            e   = objs[j];
            cmp = entity_get_cmp_renderable(e);
            if (!cmp) continue;

            int rect[4];
            _cmp_renderable_get_rect(cmp, rect);
            if (px < rect[0] || px > rect[2] || py < rect[1] || py > rect[3])
                continue;
            ret = e;
        }
    }
    return ret;
}

entity_t *
world_get_entity_by_window_coords(world_t *w, int x, int y)
{
    entity_t *ret = 0;
    world_get_entities_by_window_coords(w, x, y, &ret, 1);
    return ret;
}

int
world_get_entities_by_window_coords(world_t *w, int x, int y,
    entity_t **ret_arr, int arr_sz)
{
    if (!ret_arr) return 0;

    int num = 0;
    int px  = x;
    int py  = y;
    if (world_render_props_px_pos_from_window(&w->render_props, &px, &py))
        return 0;

    uint32                      num_cmps    = darr_num(w->cmp_renderable_pool);
    cmp_renderable_pool_item_t  *cmps       = w->cmp_renderable_pool;
    cmp_renderable_t            *cmp;

    for (uint32 i = 0; i < num_cmps && num < arr_sz; ++i)
    {
        cmp = &cmps[i].cmp;

        int rect[4];
        _cmp_renderable_get_rect(cmp, rect);
        if (px < rect[0] || px > rect[2] || py < rect[1] || py > rect[3])
            continue;
        ret_arr[num++] = cmp_renderable_get_entity(cmp);
    }
    return num;
}

int
world_get_visible_entities_inside_box(world_t *wld, int x, int y, int z, int w,
    int h, int t, entity_t **ret_arr, int arr_sz)
{
    uint32 num_cmps = darr_num(wld->cmp_renderable_pool);
    int                         num         = 0;
    cmp_renderable_pool_item_t  *cmps       = wld->cmp_renderable_pool;
    cmp_renderable_t            *cmp;
    entity_t                    *e;

    for (uint32 i = 0; i < num_cmps && num < arr_sz; ++i)
    {
        cmp = &cmps[i].cmp;
        e   = cmp_renderable_get_entity(cmp);
        if (e->x >= x && e->x < x + w &&  e->y >= y && e->y < y + h
        &&  e->z >= z && e->z < z + t)
            ret_arr[num++] = e;
    }
    return num;
}

int
set_player_dir(entity_t *e, int dir)
{
    if (e->type != ENT_TYPE_PLAYER) return 1;
    if (!ENTITY_OK(e)) return 2;

    mobility_data_t *md = entity_get_mobility_data(e);
    if (md && md->walk_timer > 0.f) return 3;

    entity_set_dir(e, dir);
    return 0;
}

void
entity_walk(entity_t *e, int dir)
{

    if (!e) return;
    int32   x = 0;
    int32   y = 0;
    int8    z = 0;
    if (iso_dir_to_vec2(dir, &x, &y)) return;
    entity_walk_to(e, e->x + x, e->y + y, e->z + z);
}

void
entity_walk_to(entity_t *e, int32 x, int32 y, int8 z)
{
    if (!e) return;
    mobility_data_t *d = entity_get_mobility_data(e);

    if (d)
    {
        int32 dx = x - e->x;
        int32 dy = y - e->y;
        int32 dz = z - e->z;

        /* If moving more than ENTITY_WALK_SNAP_TRESHOLD in any direction,
         * snap last position */
        if (ABS(dx) <= ENTITY_WALK_SNAP_TRESHOLD
        &&  ABS(dy) <= ENTITY_WALK_SNAP_TRESHOLD
        &&  ABS(dz) <= ENTITY_WALK_SNAP_TRESHOLD)
        {
            d->last_x = e->x;
            d->last_y = e->y;
            d->last_z = e->z;
        } else
        {
            d->last_x = x;
            d->last_y = y;
            d->last_z = z;
        }

        d->walk_timer   = WC_WALK_SPEED_TO_SEC(d->walk_speed);
        d->time_idle    = 0.f;
    }

    if (e->x != x || e->y != y)
        entity_set_dir(e, vec2_to_iso_dir(x - e->x, y - e->y));
    _entity_set_pos(e, x, y, z);

    switch (e->type)
    {
    case ENT_TYPE_PLAYER:
    {
        cmp_player_ae_set_t *scmp = entity_get_cmp_player_ae_set(e);
        cmp_ae_animator_t   *acmp = entity_get_cmp_ae_animator(e);
        if (scmp && acmp)
            _cmp_ae_animator_play_no_restart(acmp, scmp->walk_asset, e->dir, 1);
    }
        break;
    case ENT_TYPE_CREATURE:
    {
        cmp_creature_ae_set_t   *scmp = entity_get_cmp_creature_ae_set(e);
        cmp_ae_animator_t       *acmp = entity_get_cmp_ae_animator(e);
        if (scmp && acmp)
            _cmp_ae_animator_play_no_restart(acmp, scmp->walk_asset, e->dir, 1);
    }
        break;
    }
}

int
world_init(world_t *w, int chunks_w, int chunks_h, int max_init_ents)
{
    int ret = 0;
    memset(w, 0, sizeof(world_t));

    if (!wc_all_defs_initialized())
        {ret = 1; goto failure;}
    if (map_init(&w->map, chunks_w, chunks_h))
        {ret = 2; goto failure;}
    obj_pool_init(&w->entities, max_init_ents, sizeof(entity_t));
    darr_reserve(w->removed_entities, 256);
    if (pguid_entity_table_init(&w->players, max_init_ents))
        {ret = 5; goto failure;}
    if (mobility_data_pool_init(&w->mobility_data_pool, max_init_ents))
        {ret = 7; goto failure;}
    if (cmp_renderable_pool_init(&w->cmp_renderable_pool, max_init_ents))
        {ret = 8; goto failure;}
    if (cmp_spritesheet_pool_init(&w->cmp_spritesheet_pool, max_init_ents, 4))
        {ret = 9; goto failure;}
    if (cmp_iso_animator_pool_init(&w->cmp_iso_animator_pool, max_init_ents))
        {ret = 10; goto failure;}
    if (cmp_ae_animator_pool_init(&w->cmp_ae_animator_pool, max_init_ents))
        {ret = 11; goto failure;}
    if (cmp_player_ae_set_pool_init(&w->cmp_player_ae_set_pool, max_init_ents,
        4))
        {ret = 12; goto failure;}
    if (cmp_creature_ae_set_pool_init(&w->cmp_creature_ae_set_pool,
        max_init_ents, 4))
        {ret = 13; goto failure;}
    if (fixed_stack_init(&w->frame_stack, 512))
        {ret = 14; goto failure;}
    if (cguid_entity_table_init(&w->creatures, max_init_ents))
        {ret = 15; goto failure;}
    world_event_listener_table_einit(&w->event_listeners.table, WEV_NUM);
    obj_pool_init(&w->event_listeners.pool, 32, sizeof(world_event_listener_t));
    w->event_listeners.arrays = 0;
    darr_reserve(w->events, 256);
    mutex_init(&w->event_mtx);

    int i, j, index;
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
        {
            index = j * 3 + i;
            w->map.cache_ptrs[index]    = &w->map.caches[index];
            chunk_cache_t *tc           = w->map.cache_ptrs[index];
            tc->chunk                   = &w->map.chunks[j * w->map.w + i];
        }

    w->initialized = 1;
    return 0;

    failure:
        world_destroy(w);
        printf("Error initializing world, code %d.\n", ret);
        return ret;
}

int
world_load_map_by_id(world_t *w, uint32 id)
{
    world_clear(w);

    int ret = 0;

    muta_map_db_entry_t *entry = muta_map_db_get_entry_by_id(&_map_db, id);
    if (!entry)
        {ret = 1; goto out;}

    muta_map_file_destroy(&w->map.file);
    memset(&w->map.file, 0, sizeof(w->map.file));

    muta_map_file_t f;
    if (muta_map_file_load(&f, entry->path))
        {ret = 2; goto out;}
    w->map.file = f;

    map_resize(&w->map, f.header.w, f.header.h);

    uint32      x, y;
    map_chunk_t *ch;

    for (x = 0; x < f.header.w; ++x)
        for (y = 0; y < f.header.h; ++y)
        {
            ch          = &GET_MAP_CHUNK(&w->map, x, y);
            ch->path    = muta_map_file_get_chunk_path(&f, x, y);
        }

    int i, j, index;
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
        {
            index = j * 3 + i;
            w->map.cache_ptrs[index]        = &w->map.caches[index];
            w->map.cache_ptrs[index]->chunk = &w->map.chunks[j * w->map.w + i];
            _load_chunk_cache_from_disk(&w->map.caches[index]);
        }

    _world_load_static_objects(w);

    world_event_t ev;
    ev.type = WEV_MAP_CHANGED;
    world_post_event(w, &ev);

    out:
        if (ret)
            printf("Error loading map id %u, code %d.\n", id, ret);
        return ret;
}

int
world_init_from_file(world_t *w, const char *fp)
{
    int ret = 0;

    muta_map_file_t f;
    if (muta_map_file_load(&f, fp))
        {ret = 1; goto cleanup;}

    if (world_init(w, f.header.w, f.header.h, 1024))
        {ret = 3; goto cleanup;}

    w->map.file = f;

    uint32      x, y;
    map_chunk_t *ch;

    for (x = 0; x < f.header.w; ++x)
        for (y = 0; y < f.header.h; ++y)
        {
            ch          = &GET_MAP_CHUNK(&w->map, x, y);
            ch->path    = muta_map_file_get_chunk_path(&f, x, y);
        }

    int i, j, index;
    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
        {
            index = j * 3 + i;
            w->map.cache_ptrs[index]        = &w->map.caches[index];
            w->map.cache_ptrs[index]->chunk = &w->map.chunks[j * w->map.w + i];
            _load_chunk_cache_from_disk(&w->map.caches[index]);
        }

    _world_load_static_objects(w);

    cleanup:
    {
        if (ret)
            DEBUG_PRINTF("%s failed with code %d.\n", __func__, ret);
        return ret;
    }
}

int
world_init_by_id(world_t *w, map_id_t id)
{
    muta_map_db_entry_t *entry = muta_map_db_get_entry_by_id(&_map_db, id);
    if (!entry) return -1;
    return world_init_from_file(w, entry->path);

}

void
world_destroy(world_t *w)
{
    DEBUG_PRINTF("%s: also free asset resources here!\n", __func__);
    map_destroy(&w->map);
    obj_pool_destroy(&w->entities);
    pguid_entity_table_destroy(&w->players);
    cguid_entity_table_destroy(&w->creatures);
    mobility_data_pool_destroy(&w->mobility_data_pool);
    cmp_renderable_pool_destroy(&w->cmp_renderable_pool);
    cmp_spritesheet_pool_destroy(&w->cmp_spritesheet_pool);
    cmp_iso_animator_pool_destroy(&w->cmp_iso_animator_pool);
    cmp_ae_animator_pool_destroy(&w->cmp_ae_animator_pool);
    cmp_player_ae_set_pool_destroy(&w->cmp_player_ae_set_pool);
    cmp_creature_ae_set_pool_destroy(&w->cmp_creature_ae_set_pool);
    darr_free(w->removed_entities);
    darr_free(w->events);
    fixed_stack_destroy(&w->frame_stack);
    uint32 num = darr_num(w->event_listeners.arrays);
    for (uint32 i = 0; i < num; ++i)
        darr_free(*w->event_listeners.arrays[i]);
    darr_free(w->event_listeners.arrays);
    world_event_listener_table_destroy(&w->event_listeners.table);
    obj_pool_destroy(&w->event_listeners.pool);
    mutex_destroy(&w->event_mtx);
    memset(w, 0, sizeof(world_t));
}

void
world_clear(world_t *w)
{
    DEBUG_PRINTFF("also free asset resources here!\n");
    obj_pool_clear(&w->entities);
    pguid_entity_table_clear(&w->players);
    cguid_entity_table_clear(&w->creatures);
    mobility_data_pool_clear(&w->mobility_data_pool);
    cmp_renderable_pool_clear(&w->cmp_renderable_pool);
    cmp_spritesheet_pool_clear(&w->cmp_spritesheet_pool);
    cmp_iso_animator_pool_clear(&w->cmp_iso_animator_pool);
    cmp_ae_animator_pool_clear(&w->cmp_ae_animator_pool);
    cmp_player_ae_set_pool_clear(&w->cmp_player_ae_set_pool);
    darr_clear(w->removed_entities);
    darr_clear(w->events);
    fixed_stack_clear(&w->frame_stack);
    w->tick_timer = 0.f;
    _world_load_static_objects(w);
}

static void
_load_chunk_cache_callback(void *args)
{
    chunk_cache_t *cache    = (chunk_cache_t*)args;
    map_chunk_t *chunk      = cache->chunk;

    if (chunk)
    {
        DEBUG_PRINTF("%s: beginning to load...\n", __func__);
        int res;
        mutex_lock(&cache->mtx);
        DEBUG_PRINTF("%s: locked mtx...\n", __func__);
        res = _load_chunk_cache_from_disk(cache);
        mutex_unlock(&cache->mtx);
        DEBUG_PRINTF("%s result: %d (%s).\n", __func__, res, chunk->path);
    } else
        DEBUG_PRINTF("%s: no chunk!\n", __func__);
}

bool32
world_set_cam_pos(world_t *w, int x, int y)
{
    int cx = CLAMP(x, 0, w->map.w - 3);
    int cy = CLAMP(y, 0, w->map.h - 3);

    if (cx == w->map.cam_x && cy == w->map.cam_y)
        return 0;

    int i, j, k, index;

    /* Find out which chunks will be loaded */
    map_chunk_t *req_chunks[9];
    bool32      use_flags[9];

    for (i = 0; i < 3; ++i)
        for (j = 0; j < 3; ++j)
            req_chunks[j * 3 + i] = &w->map.chunks[
                (cy + j) * w->map.w + (cx + i)];

    memset(w->map.cache_ptrs, 0, sizeof(w->map.cache_ptrs));
    memset(use_flags, 0, sizeof(use_flags));

    /* Set cache pointers to caches already loaded */
    for (i = 0; i < 3; ++i)
    {
        for (j = 0; j < 3; ++j)
        {
            index = j * 3 + i;
            for (k = 0; k < 9; ++k)
            {
                if (w->map.caches[k].chunk != req_chunks[index]) continue;
                w->map.cache_ptrs[index]    = &w->map.caches[k];
                req_chunks[index]           = 0;
                use_flags[k]                = 1; /* Mark cache as used */
                break;
            }
        }
    }

    /* Remove static objects from the chunks that will now be left out */
    for (i = 0; i < 9; ++i)
    {
        if (use_flags[i]) continue;
        _chunk_cache_purge_static_objs(&w->map.caches[i]);
    }

    kth_job_t   jobs[9];
    int         num_jobs = 0;

    /* Set the remaining cache pointers */
    for (i = 0; i < 9; ++i)
    {
        if (w->map.cache_ptrs[i]) continue;
        /* Find a free cache for this ptr */
        for (j = 0; j < 9; ++j)
        {
            if (use_flags[j]) continue;

            w->map.cache_ptrs[i]        = &w->map.caches[j];
            w->map.cache_ptrs[i]->chunk = req_chunks[j];
            use_flags[j]                = 1;

            int cache_x, cache_y;
            COMPUTE_CACHE_PTR_COORDS(&w->map, &w->map.cache_ptrs[i],
                &cache_x, &cache_y);

            w->map.cache_ptrs[i]->chunk = &GET_MAP_CHUNK(&w->map,
                cx + cache_x, cy + cache_y);

            jobs[num_jobs].func = _load_chunk_cache_callback;
            jobs[num_jobs].args = w->map.cache_ptrs[i];
            num_jobs++;

            break;
        }
    }

    DEBUG_PRINTF("%s: spawning %d load jobs.\n", __func__, num_jobs);

    kth_job_grp_t grp = kth_create_job_grp(jobs, num_jobs);
    int r = kth_job_grp_run_and_wait(&grp, &th_pool);
    if (r) muta_panic(-1, __func__);

    _world_load_static_objects(w);

    w->map.cam_x    = cx;
    w->map.cam_y    = cy;
    w->map.cam_tx   = cx * MAP_CHUNK_W;
    w->map.cam_ty   = cy * MAP_CHUNK_W;
    return 1;
}

void
world_update(world_t *w, world_render_props_t *rp, double dt)
{
    double tick_timer = w->tick_timer + dt;
    while (tick_timer >= WORLD_TIMESTEP)
    {
        w->render_props = *rp;
        _world_handle_events(w);
        _world_update_cmp_iso_animators(w);
        _world_update_cmp_ae_animators(w);
        _world_update_cmp_renderables(w);
        _world_update_mobility_components(w, WORLD_TIMESTEP);
        fixed_stack_clear(&w->frame_stack);
        tick_timer -= WORLD_TIMESTEP;
    }
    w->tick_timer = tick_timer;
    _world_purge_removed_entities(w);
}

entity_t *
world_spawn_static_object(world_t *w, static_obj_type_id_t type_id, int32 x,
    int32 y, int8 z, int dir)
{
    chunk_cache_t *ch = map_get_cache_of_tile(&w->map, x, y);
    if (!ch) return 0;
    uint32 file_index   = darr_num(ch->static_objs);
    entity_t *e         = _world_spawn_static_object_internal(w, type_id,
        file_index, x, y, z, dir);
    if (darr_push(ch->static_objs, e))
        goto fail;

    uint8 rx = (uint8)(x % MAP_CHUNK_W);
    uint8 ry = (uint8)(y % MAP_CHUNK_W);

    if (muta_chunk_file_push_static_obj(&ch->file, type_id, rx, ry, z))
    {
        darr_erase_last(ch->static_objs);
        goto fail;
    }

    return e;

    fail:
       e->type_data.static_obj.file_index = 0xFFFFFFFF;
       _remove_entity_internal(e);
       return 0;
}

entity_t *
world_spawn_player(world_t *w, player_guid_t id, const char *name, int x,
    int y, int z, int dir, player_race_id_t race, bool32 sex)
{
    entity_t *existing = world_get_player_by_id(w, id);
    muta_assert(!existing);

    int err;

    player_race_def_t *def = wc_get_player_race_def(race);
    if (!def) {err = 1; goto fail;}

    entity_t *e = _world_create_empty_entity(w);
    if (!e) {err = 2; goto fail;}

    e->type = ENT_TYPE_PLAYER;
    e->flags |= (sex ? ENT_SEX : 0);
    e->type_data.player.id          = id;
    e->type_data.player.race        = race;
    e->type_data.player.race_def    = def;

    if (name)
        strcpy(e->type_data.player.name, name);
    else
        e->type_data.player.name[0] = 0;

    _entity_set_pos(e, x, y, z);
    entity_set_dir(e, dir);
    pguid_entity_table_einsert(&w->players, id, e);

    e->type_data.player.id = id;

    {
        mobility_data_t cmp = {0};
        cmp.last_x          = x;
        cmp.last_y          = y;
        cmp.last_z          = z;
        cmp.walk_timer      = 0.f;
        cmp.walk_speed      = WC_PLAYER_BASE_WALK_SPEED;
        if (entity_attach_mobility_data(e, &cmp))
            {err = 4; goto fail;}
    }

    entity_attach_empty_cmp_renderable(e);
    entity_attach_empty_cmp_ae_animator(e);
    entity_attach_empty_cmp_player_ae_set(e);

    return e;

    fail:
        if (e)
        {
            pguid_entity_table_erase(&w->players, id);
            _remove_entity_internal(e);
        }
        DEBUG_PRINTFF("failed: %d\n", err);
        return 0;
}

static void
_world_despawn_player_deferred_internal(world_t *w, entity_t *e)
{
    if (!e) return;
    pguid_entity_table_erase(&w->players, e->type_data.player.id);
    DEBUG_PRINTF("%s: erased.\n", __func__);
    _world_defer_remove_entity(w, e);
}

void
world_despawn_player(world_t *w, entity_t *e)
    {_world_despawn_player_deferred_internal(w, e);}

void
world_despawn_player_by_id(world_t *w, player_guid_t id)
{
    _world_despawn_player_deferred_internal(w, world_get_player_by_id(w, id));
}

void
despawn_static_obj(entity_t *e)
{
    if (!e || !ENTITY_OK(e)) return;
    muta_assert(e->type == ENT_TYPE_STATIC_OBJ);
    _world_defer_remove_entity(e->w, e);
}

entity_t *
world_spawn_creature(world_t *w, creature_guid_t id, uint32 type_id, int x,
    int y, int z, int dir, uint8 flags)
{
    entity_t *existing = world_get_creature_by_id(w, id);
    muta_assert(!existing);

    int         err;
    entity_t    *e = 0;

    creature_def_t *def = wc_get_creature_def(type_id);
    if (!def)
    {
        printf("Cannot spawn creature: could not find creature definition "
            "%u!\n", type_id);
        err = 1;
        goto fail;
    }

    e = _world_create_empty_entity(w);
    if (!e) {err = 2; goto fail;}

    e->type                         = ENT_TYPE_CREATURE;
    e->type_data.creature.id        = id;
    e->type_data.creature.type_id   = type_id;
    /* TODO: add sex */

    _entity_set_pos(e, x, y, z);
    entity_set_dir(e, dir);
    cguid_entity_table_einsert(&w->creatures, id, e);

    {
        mobility_data_t cmp = {0};
        cmp.last_x      = x;
        cmp.last_y      = y;
        cmp.last_z      = z;
        cmp.walk_timer  = 0.f;
        cmp.walk_speed  = WC_PLAYER_BASE_WALK_SPEED;
        entity_attach_mobility_data(e, &cmp);
    }

    {
        cmp_renderable_t *rcmp = entity_attach_empty_cmp_renderable(e);

        switch (def->gfx_type)
        {
        case WC_GFX_TEXTURE:
        {
            tex_asset_t *ta = as_claim_tex_by_id(MATLOCK_ID, 0);
            if (ta)
            {
                float clip[4] = {0, 0, ta->tex.w, ta->tex.h};
                _cmp_renderable_set_gfx(rcmp, &ta->tex, clip);
                DEBUG_PRINTFF("Matlock clip set to %f, %f, %f, %f\n", clip[0],
                    clip[1], clip[2], clip[3]);
            } else
                DEBUG_PRINTFF("failed to find matlock! :(\n");
        }
            break;
        case WC_GFX_AE_SET:
            DEBUG_PRINTFF("WC_GFX_AE_SET\n");
            entity_attach_empty_cmp_ae_animator(e);
            entity_attach_empty_cmp_creature_ae_set(e);
            break;
        }
    }

    return e;

    fail:
        DEBUG_PRINTFF("failed: %d\n", err);
        if (e)
        {
            cguid_entity_table_erase(&w->creatures, id);
            _remove_entity_internal(e);
        }
        return 0;
}

void
world_despawn_creature(entity_t *e)
    {IMPLEMENTME();}

entity_t *
world_get_player_by_id(world_t *w, player_guid_t id)
{
    entity_t **e = pguid_entity_table_get_ptr(&w->players, id);
    return e ? *e : 0;
}

entity_t *
world_get_creature_by_id(world_t *w, creature_guid_t id)
{
    entity_t **e = cguid_entity_table_get_ptr(&w->creatures, id);
    return e ? *e : 0;
}

bool32
world_can_player_enter_tile(world_t *w, int x, int y, int z)
{
    int i = 0;
    for (tile_def_t *td = world_get_tile_def(w, x, y, z + i);
         i < 3;
         td = world_get_tile_def(w, x, y, z + i), ++i)
        if (!td->passthrough) return 0;
    return 1;
}

int
world_compute_chunk_cache_coords(world_t *w, chunk_cache_t *ch, int *ret_x,
    int *ret_y)
{
    if (!ch) return 1;
    COMPUTE_CHUNK_COORDS(&w->map, ch->chunk, ret_x, ret_y);
    return 0;
}

world_event_listener_t *
world_register_event_listener(world_t *w, int event, void *data,
    void (*callback)(world_t*, void*, world_event_t*))
{
    world_event_listener_t ***darr = world_event_listener_table_get_ptr(
        &w->event_listeners.table, event);
    if (!darr)
    {
        darr = world_event_listener_table_einsert_empty(
            &w->event_listeners.table, event);
        darr_reserve(*darr, 16);
        darr_push(w->event_listeners.arrays, darr);
    }
    world_event_listener_t *el = obj_pool_reserve(&w->event_listeners.pool);
    el->event       = event;
    el->index       = darr_num(*darr);
    el->data        = data;
    el->callback    = callback;
#ifdef _MUTA_DEBUG
    el->reserved = 1;
#endif
    darr_push(*darr, el);
    return el;
}

void
world_unregister_event_listener(world_t *w, world_event_listener_t *el)
{
#ifdef _MUTA_DEBUG
    muta_assert(el->reserved);
    el->reserved = 0;
#endif
    world_event_listener_t ***darr = world_event_listener_table_get_ptr(
        &w->event_listeners.table, el->event);
    uint32 last_index   = darr_num(*darr) - 1;
    uint32 index        = el->index;
    (*darr)[index]          = (*darr)[last_index];
    (*darr)[index]->index   = index;
    _darr_head(*darr)->num  = last_index;
    obj_pool_free(&w->event_listeners.pool, el);
}

void
world_post_event(world_t *w, world_event_t *ev)
{
    world_event_listener_t ***darr = world_event_listener_table_get_ptr(
        &w->event_listeners.table, ev->type);
    if (!darr)
        return;
    for (uint32 i = 0; i < darr_num(*darr); ++i)
        (*darr)[i]->callback(w, (*darr)[i]->data, ev);
}

bool32
world_get_solid_tile_by_px_pos(world_t *w, int x, int y, int ret_pos[3])
{
    world_render_props_t *rp = &w->render_props;
    int         tx, ty, nx, ny;
    int         z_limit = MAX(rp->tz, -1);
    int         i       = 0;
    tile_def_t  *td;
    r_compute_tiled_pos_from_px_pos(rp, x, y, &tx, &ty);

    /* TODO: limit start z to MAP_CHUNK_T */
    for (int nz = rp->tz + rp->tt - 1; nz >= z_limit; --nz, ++i)
    {
        nx = tx - i;
        ny = ty - i;
        td = world_get_tile_def(w, nx, ny, nz);
        if (!(!td->passthrough || nz < 0))
            continue;
        ret_pos[0] = nx;
        ret_pos[1] = ny;
        ret_pos[2] = nz;
        return 1;
    }
    return 0;
}

int
static_obj_set_pos(entity_t *e, int32 x, int32 y, int8 z)
{
    if (!ENTITY_OK(e)) return 1;
    if (e->type != ENT_TYPE_STATIC_OBJ) return 2;
    chunk_cache_t *och = map_get_cache_of_tile(&e->w->map, e->x, e->y);
    chunk_cache_t *nch = map_get_cache_of_tile(&e->w->map, x, y);
    if (!nch) return 3;
    if (och != nch)
    {
        if (darr_push(nch->static_objs, e))
            return 4;

        uint32 num = darr_num(och->static_objs);
        for (uint32 i = 0; i < num; ++i)
            if (och->static_objs[i] == e)
            {
                darr_erase(och->static_objs, i);
                break;
            }

    }
    _entity_set_pos(e, x, y, z);
    return 0;
}

int
player_set_pos(entity_t *e, int32 x, int32 y, int8 z)
{
    if (!ENTITY_OK(e)) return 1;
    if (e->type != ENT_TYPE_PLAYER) return 2;
    _entity_set_pos(e, x, y, z);
    return 0;
}

static entity_t *
_world_spawn_static_object_internal(world_t *w, static_obj_type_id_t type_id,
    uint32 file_index, int32 x, int32 y, int8 z, int dir)
{
    int err;

    entity_t *e = 0;
    static_obj_def_t *d = wc_get_static_obj_def(type_id);
    if (!d) {err = 1; goto fail;}
    e = _world_create_empty_entity(w);
    if (!e) {err = 2; goto fail;}

    e->type                             = ENT_TYPE_STATIC_OBJ;
    e->type_data.static_obj.type_id     = type_id;
    e->type_data.static_obj.file_index  = file_index;
    _entity_set_pos(e, x, y, z);
    entity_set_dir(e, dir);

    {
        cmp_renderable_t *cmp = entity_attach_empty_cmp_renderable(e);
        if (!cmp) {err = 3; goto fail;}
    }

    if (d->gfx_type == WC_GFX_SPRITESHEET)
    {
        cmp_spritesheet_t *cmp = entity_attach_empty_cmp_spritesheet(e);
        if (!cmp) goto fail;
        _cmp_spritesheet_set_asset(cmp, d->gfx_data.spritesheet.path,
             d->gfx_data.spritesheet.clip, d->gfx_data.spritesheet.ox,
             d->gfx_data.spritesheet.oy);
    }

    return e;

    fail:
        if (e) _world_defer_remove_entity(w, e);
        DEBUG_PRINTF("%s: error %d.\n", __func__, err);
        return 0;
}

static void
_world_update_mobility_components_callback(void *args)
{
    mobility_up_arg_t           *a      = args;
    mobility_data_pool_item_t   *items  = a->items;
    uint32                      first   = a->first;
    uint32                      num     = a->num;
    uint32                      last    = first + num;
    float                       fdt     = (float)a->dt;
    world_render_props_t        rp      = a->w->render_props;
    mobility_data_t             *md;
    cmp_renderable_t            *rcmp;
    entity_t                    *e;

    #define INCREMENT_XY(prcnt) \
        int     ltx = md->last_x - rp.tx; \
        int     lty = md->last_y - rp.ty; \
        int     ltz = md->last_z - rp.tz; \
        float   ox  = rp.bx + (float)rcmp->ox; \
        float   oy  = rp.by + (float)rcmp->oy; \
        float   lx  = R_COMPUTE_ISO_OBJ_PX_X(ltx, lty, ox); \
        float   ly  = R_COMPUTE_ISO_OBJ_PX_Y(ltx, lty, ltz, oy); \
        rcmp->x -= (rcmp->x - lx) * prcnt; \
        rcmp->y -= (rcmp->y - ly) * prcnt;

    for (uint32 i = first; i < last; ++i)
    {
        md = &items[i].cmp;

        if (md->walk_timer > 0.f)
        {
            e       = mobility_data_get_entity(md);
            rcmp    = entity_get_cmp_renderable(e);
            if (!rcmp) continue;

            float trvl_prcnt = md->walk_speed ? \
                md->walk_timer / WC_WALK_SPEED_TO_SEC(md->walk_speed) : 1.f;
            INCREMENT_XY(trvl_prcnt);

            md->walk_timer -= MIN(fdt, md->walk_timer);

            if (md->walk_timer > 0.f)
            {
                int trvl_dir = vec2_to_iso_dir(rcmp->tx - md->last_x,
                    rcmp->ty - md->last_y);
                rcmp->travel_dir = (int8)trvl_dir;
            } else
            {
                rcmp->travel_dir = INVALID_ISODIR;
                md->time_idle = 0.f;
            }
        } else
        {
            float old_time_idle = md->time_idle;
            md->time_idle       = old_time_idle + fdt;

            if (md->time_idle <  IDLE_ANIM_BEGIN_TIME) continue;
            if (old_time_idle >= IDLE_ANIM_BEGIN_TIME) continue;

            world_event_t ev;
            ev.type = WEV_WALK_FINISHED;
            ev.d.walk_finished.mobility_data_index = i;
            _world_post_event(a->w, ev);
        }
    }
    #undef INCREMENT_XY
}

static void
_world_update_mobility_components(world_t *w, double dt)
{
    uint32 num = darr_num(w->mobility_data_pool);
    if (num == 0)
        return;

    threaded_work_div_t div = _divide_threaded_work(num);
    if (div.num_jobs == 0)
        return;

    mobility_data_pool_item_t *items = w->mobility_data_pool;

    mobility_up_arg_t *args = (mobility_up_arg_t*)_world_stack_alloc(w,
        div.num_jobs * sizeof(mobility_up_arg_t));
    kth_job_t           *jobs = (kth_job_t*)_world_stack_alloc(w,
        div.num_jobs * sizeof(kth_job_t));

    muta_assert(args);
    muta_assert(jobs);

    mobility_up_arg_t *arg;
    for (uint32 i = 0; i < div.num_jobs; ++i)
    {
        arg = &args[i];
        arg->w      = w;
        arg->items  = items;
        arg->first  = i * div.num_per_job;
        arg->num    = div.num_per_job;
        arg->dt     = dt;
    }

    kth_job_t *job;
    for (uint32 i = 0; i < div.num_jobs; ++i)
    {
        job         = &jobs[i];
        job->func   = _world_update_mobility_components_callback;
        job->args   = &args[i];
    }

    args[div.num_jobs - 1].num = div.num_last_job;
    kth_job_grp_t wg = kth_create_job_grp(jobs, div.num_jobs);
    kth_job_grp_run_and_wait(&wg, &th_pool);
}

static void
_world_handle_events(world_t *w)
{
    uint32          num     = darr_num(w->events);
    world_event_t   *evs    = w->events;
    world_event_t   *ev;
    for (uint32 i = 0; i < num; ++i)
    {
        ev = &evs[i];
        switch (ev->type)
        {
        case WEV_WALK_FINISHED:
            _world_handle_wev_walk_finished(w, ev);
            break;
        }
    }
    darr_clear(w->events);
}

static void
_world_handle_wev_walk_finished(world_t *w, world_event_t *ev)
{
    mobility_data_t *md = &w->mobility_data_pool[
        ev->d.walk_finished.mobility_data_index].cmp;
    if (md->walk_timer > 0.f) return;
    entity_t *e = mobility_data_get_entity(md);

    switch (e->type)
    {
    case ENT_TYPE_PLAYER:
    {
        cmp_player_ae_set_t *scmp = entity_get_cmp_player_ae_set(e);
        cmp_ae_animator_t   *acmp = entity_get_cmp_ae_animator(e);
        if (acmp && scmp)
            _cmp_ae_animator_play(acmp, scmp->idle_asset, e->dir, 1);
    }
        break;
    case ENT_TYPE_CREATURE:
    {
        cmp_creature_ae_set_t   *scmp = entity_get_cmp_creature_ae_set(e);
        cmp_ae_animator_t       *acmp = entity_get_cmp_ae_animator(e);
        if (acmp && scmp)
            _cmp_ae_animator_play(acmp, scmp->idle_asset, e->dir, 1);
    }
        break;
    }
}

static entity_t *
_world_create_empty_entity(world_t *w)
{
    entity_t *e = obj_pool_reserve(&w->entities);
    e->w        = w;
    memset(e->cmps, 0xFF, sizeof(e->cmps));
    memset(e->hcmps, 0, sizeof(e->hcmps));
    e->target   = 0;
    e->x        = 0;
    e->y        = 0;
    e->z        = 0;
    e->flags    = ENT_RESERVED;
    e->type     = ENT_TYPE_UNKNOWN;
    e->dir      = 0;
    return e;
}

static void
_world_purge_removed_entities(world_t *w)
{
    entity_t            *e;
    entity_t            **items = w->removed_entities;
    uint32              num     = darr_num(items);
    for (uint32 i = 0; i < num; ++i)
    {
        e = items[i];
        muta_assert(!GET_BITFLAG(e->flags, ENT_RESERVED));
        _remove_entity_internal(e);
    }
    darr_clear(w->removed_entities);
}

static void
_world_update_cmp_renderables(world_t *w)
{
    cmp_renderable_pool_item_t  *cmps       = w->cmp_renderable_pool;
    uint32                      num_cmps    = darr_num(w->cmp_renderable_pool);
    world_render_props_t        rp          = w->render_props;
    cmp_renderable_t            *cmp;

    for (uint32 i = 0; i < num_cmps; ++i)
    {
        cmp = &cmps[i].cmp;
        float ox = rp.bx + (float)cmp->ox;
        float oy = rp.by + (float)cmp->oy;
        cmp->x = R_COMPUTE_ISO_OBJ_PX_X(cmp->tx - rp.tx, cmp->ty - rp.ty, ox);
        cmp->y = R_COMPUTE_ISO_OBJ_PX_Y(cmp->tx - rp.tx, cmp->ty - rp.ty,
            cmp->tz - rp.tz, oy);
    }
}

static void
_world_update_cmp_iso_animators(world_t *w)
{
    cmp_iso_animator_pool_item_t *cmps = w->cmp_iso_animator_pool;
    uint32              num_cmps = darr_num(w->cmp_iso_animator_pool);
    cmp_iso_animator_t  *cmp;
    iso_anim_frame_t    *f;
    entity_t            *e;
    cmp_renderable_t    *rcmp;

    for (uint32 i = 0; i < num_cmps; ++i)
    {
        cmp = &cmps[i].cmp;
        if (!cmp->num_frames) continue;

        e       = cmp_iso_animator_get_entity(cmp);
        rcmp    = entity_get_cmp_renderable(e);
        if (!rcmp) continue;

        if (cmp->paused) continue;

        cmp->time_passed += WORLD_TIMESTEP;
        if (cmp->time_passed < cmp->frame_dur)
            continue;

        cmp->frame_num      = (cmp->frame_num + 1) % cmp->num_frames;
        f                   = &cmp->asset->anim.frames[cmp->frame_num];
        cmp->time_passed    = 0;
        cmp->frame_dur      = f->dur;

        /* Finishing an unlooped animation */
        if (!cmp->frame_num && !cmp->loop)
        {
            _cmp_iso_animator_stop(0);
            continue;
        }

        int dir = e->dir;
        _cmp_renderable_set_gfx(rcmp, f->tex_areas[dir].tex,
            f->tex_areas[dir].clip);
        rcmp->ox = (int16)f->tex_areas[dir].ox;
        rcmp->oy = (int16)f->tex_areas[dir].oy;
    }
}

static void
_world_update_cmp_ae_animators(world_t *w)
{
    cmp_ae_animator_pool_t      *pool       = &w->cmp_ae_animator_pool;
    cmp_ae_animator_pool_item_t *items      = *pool;
    uint32                      num_items   = darr_num(*pool);
    cmp_ae_animator_t           *cmp;
    cmp_renderable_t            *rcmp;
    entity_t                    *e;

    for (uint i = 0; i < num_items; ++i)
    {
        cmp = &items[i].cmp;
        if (!cmp->num_frames) continue;

        ae_frame_t *f   = &cmp->frames[cmp->frame_num];
        cmp->frame_num  = (cmp->frame_num + 1) % cmp->num_frames;

        if (!cmp->loop && cmp->frame_num == 0)
            _cmp_ae_animator_stop(cmp);

        e       = cmp_ae_animator_get_entity(cmp);
        rcmp    = entity_get_cmp_renderable(e);
        if (!rcmp)
            continue;

        _cmp_renderable_set_gfx(rcmp, &f->layers[0].ta->tex, f->layers[0].clip);
        rcmp->ox    = (int16)(f->layers[0].ox + f->ox);
        rcmp->oy    = (int16)(f->layers[0].oy + f->oy);
        rcmp->rot   = f->layers[0].rot;
        rcmp->flip  = (uint8)f->layers[0].flip;
    }
}

static void
_world_defer_remove_entity(world_t *w, entity_t *e)
{
    DEBUG_PRINTF("%s: id %llu\n", __func__, (lluint)e->type_data.player.id);
    if (!GET_BITFLAG(e->flags, ENT_RESERVED)) return;
    SET_BITFLAG_OFF(e->flags, ENT_RESERVED);
    darr_push(w->removed_entities, e);
}

static void
_remove_entity_internal(entity_t *e)
{
    world_t *w = e->w;
    SET_BITFLAG_OFF(e->flags, ENT_RESERVED);

    /* Note: when a world sprite component is removed, it's resources
     * should also be unclaimed */
    FIXME();
    entity_remove_mobility_data(e);
    entity_remove_cmp_iso_animator(e);
    entity_remove_cmp_renderable(e);
    entity_remove_cmp_spritesheet(e);
    entity_remove_cmp_ae_animator(e);
    entity_remove_cmp_player_ae_set(e);
    entity_remove_cmp_creature_ae_set(e);

    switch (e->type)
    {
    case ENT_TYPE_STATIC_OBJ:
    {
        uint32 fi = e->type_data.static_obj.file_index;
        if (fi == 0xFFFFFFFF)
            break;
        chunk_cache_t *ch = world_get_cache_of_tile(w, e->x, e->y);
        if (!ch)
            break;
        muta_assert(
            darr_num(ch->static_objs) == ch->file.header.num_static_objs);
        darr_erase(ch->static_objs, fi);
        entity_t **es   = ch->static_objs;
        uint32 num      = darr_num(ch->static_objs);
        for (uint32 i = fi; i < num; ++i)
            es[i]->type_data.static_obj.file_index--;
        muta_chunk_file_erase_static_obj(&ch->file, fi);
    }
        break;
    case ENT_TYPE_PLAYER:
        break;
    default:
        IMPLEMENTME();
    }

    obj_pool_free(&w->entities, e);
}

static inline void *
_world_stack_alloc(world_t *w, size_t sz)
    {return fixed_stack_alloc(&w->frame_stack, sz);}

static int
_world_load_static_objects(world_t *w)
{
	if (!world_initialized(w))
		return 1;

    chunk_cache_t **ch;
    uint32              num;
    stored_static_obj_t *objs;
    int32               ctx, cty, cx, cy;
    entity_t            *e;

    for (int i = 0; i < 9; ++i)
    {
        ch = &w->map.cache_ptrs[i];
        COMPUTE_CACHE_PTR_COORDS(&w->map, ch, &cx, &cy);
        ctx     = cx * MAP_CHUNK_W;
        cty     = cy * MAP_CHUNK_W;
        num     = (*ch)->file.header.num_static_objs;
        objs    = (*ch)->file.static_objs;

        for (uint32 j = 0; j < num; ++j)
        {
            e = _world_spawn_static_object_internal(w, objs[j].type_id, j,
                ctx + (int32)objs[j].x, cty + (int32)objs[j].y, objs[j].z,
                objs[j].dir);
            if (!e)
                return 2;
            if (darr_push((*ch)->static_objs, e))
                return 3;
        }
    }

    return 0;
}

static threaded_work_div_t
_divide_threaded_work(uint32 num_items)
{
    threaded_work_div_t r = {0};
    r.num_jobs = MIN((uint32)th_pool.threads_num, num_items);
    if (r.num_jobs == 0)
        return r;
    r.num_per_job       = num_items / r.num_jobs;
    uint32 left_over    = num_items - r.num_jobs * r.num_per_job;
    r.num_last_job      = r.num_per_job + left_over;
    return r;
}

static void
_cmp_spritesheet_clear(cmp_spritesheet_t *cmp)
{
    if (!cmp->asset) return;
    as_unclaim_spritesheet(cmp->asset);
    cmp->asset = 0;

    /* At this point, also clear the gfx from the world sprite component */
    IMPLEMENTME();
}

static int
_cmp_spritesheet_set_asset(cmp_spritesheet_t *cmp, const char *fp,
    uint32 clip, int16 ox, int16 oy)
{
    _cmp_spritesheet_clear(cmp);

    int err;

    spritesheet_t *ss = as_claim_spritesheet_by_name(fp, 1);
    if (!ss) {err = 1; goto fail;}

    tex_t *tex  = spritesheet_get_tex(ss);
    float *c    = spritesheet_get_clip(ss, clip);

    cmp->asset  = ss;
    cmp->clip   = clip;

    cmp_renderable_t *rcmp = entity_get_cmp_renderable(
        cmp_spritesheet_get_entity(cmp));
    if (rcmp)
    {
        _cmp_renderable_set_gfx(rcmp, tex, c);
        rcmp->ox = ox;
        rcmp->oy = oy;
    }

    return 0;

    fail:
        as_unclaim_spritesheet(ss);
        return err;
}

static void
_cmp_renderable_set_gfx(cmp_renderable_t *cmp, tex_t *tex, float *clip)
{
    cmp->tex = tex;
    if (clip) {memcpy(cmp->clip, clip, 4 * sizeof(float)); return;}
    cmp->clip[0] = 0;
    cmp->clip[1] = 0;
    if (tex)
        {cmp->clip[2] = tex->w; cmp->clip[3] = tex->h;}
    else
        {cmp->clip[2] = 0; cmp->clip[3] = 0;}
}

static void
_cmp_renderable_get_rect(cmp_renderable_t *cmp, int ret_rect[4])
{
    ret_rect[0] = (int)cmp->x;
    ret_rect[1] = (int)cmp->y;
    ret_rect[2] = ret_rect[0] + (int)(cmp->clip[2] - cmp->clip[0]);
    ret_rect[3] = ret_rect[1] + (int)(cmp->clip[3] - cmp->clip[1]);
}

static void
_cmp_iso_animator_update_dir(cmp_iso_animator_t *cmp, uint8 dir)
{
    muta_assert(dir < NUM_ISODIRS);
    if (!cmp->num_frames) return;

    entity_t *e             = cmp_iso_animator_get_entity(cmp);
    cmp_renderable_t *rcmp  = entity_get_cmp_renderable(e);
    if (!rcmp) return;

    iso_anim_frame_t *f = &cmp->asset->anim.frames[cmp->frame_num];
    _cmp_renderable_set_gfx(rcmp, f->tex_areas[dir].tex,
        f->tex_areas[dir].clip);
}

#if 0
static void
_cmp_iso_animator_play(cmp_iso_animator_t *cmp, const char *fp,
    bool32 loop)
{
    /*cmp_iso_animator_stop(*/
    FIXME();

    iso_anim_asset_t *asset = claim_iso_anim_asset(fp);
    if (!asset) return;

    if (!asset->anim.num_frames)
        {unclaim_iso_anim_asset(asset); return;}

    cmp->asset          = asset;
    cmp->frame_num      = 0;
    cmp->num_frames     = asset->anim.num_frames;
    cmp->frame_dur      = asset->anim.frames[0].dur;
    cmp->time_passed    = 0.f;
    cmp->loop           = loop ? 1 : 0;

    entity_t *e = cmp_iso_animator_get_entity(cmp);

    cmp_renderable_t *rcmp = entity_get_cmp_renderable(e);
    if (!rcmp) return;

    iso_anim_frame_t *f = &asset->anim.frames[0];
    _cmp_renderable_set_gfx(rcmp, f->tex_areas[e->dir].tex,
        f->tex_areas[e->dir].clip);
    rcmp->ox = (int16)f->tex_areas[e->dir].ox;
    rcmp->oy = (int16)f->tex_areas[e->dir].oy;
}
#endif

static void
_cmp_iso_animator_stop(cmp_iso_animator_t *cmp)
{
    cmp->asset      = 0;
    cmp->num_frames = 0;
}

static void
_cmp_ae_animator_play(cmp_ae_animator_t *cmp, ae_asset_t *asset, int dir,
    bool32 loop)
{
    _cmp_ae_animator_stop(cmp);
    cmp->loop   = loop;
    cmp->asset  = asset;
    _cmp_ae_animator_set_dir(cmp, dir);
}

static void
_cmp_ae_animator_play_no_restart(cmp_ae_animator_t *cmp, ae_asset_t *asset,
    int dir, bool32 loop)
{
    cmp->loop = loop;
    if (!asset) {_cmp_ae_animator_stop(cmp); return;}

    if (!_cmp_ae_animator_is_stopped(cmp))
    {
        if (asset != cmp->asset)
            _cmp_ae_animator_stop(cmp);
        else
            return;
    }
    cmp->asset = asset;
    _cmp_ae_animator_set_dir(cmp, dir);
}

static void
_cmp_ae_animator_stop(cmp_ae_animator_t *cmp)
{
    cmp->asset      = 0;
    cmp->num_frames = 0;
    cmp->frames     = 0;
    cmp->frame_num  = 0;
}

static inline bool32
_cmp_ae_animator_is_stopped(cmp_ae_animator_t *cmp)
    {return cmp->asset ? 0 : 1;}

static void
_cmp_ae_animator_set_dir(cmp_ae_animator_t *cmp, int dir)
{
    muta_assert(dir >= 0 && dir < NUM_ISODIRS);
    ae_asset_t *asset = cmp->asset;
    if (!asset) return;

    ae_animation_t *a = &asset->ae.anims[dir];
    cmp->frames     = a->frames;
    cmp->num_frames = (uint16)a->num_frames;
    cmp->frame_num  = 0;

    entity_t *e             = cmp_ae_animator_get_entity(cmp);
    cmp_renderable_t *rcmp  = entity_get_cmp_renderable(e);
    if (!rcmp) return;

    if (a->num_frames > 0)
    {
        _cmp_renderable_set_gfx(rcmp, &cmp->frames->layers[0].ta->tex,
            cmp->frames->layers[0].clip);
        rcmp->ox = (int16)(cmp->frames[0].layers[0].ox + cmp->frames[0].ox);
        rcmp->oy = (int16)(cmp->frames[0].layers[0].oy + cmp->frames[0].oy);
    } else
        _cmp_renderable_set_gfx(rcmp, 0, 0);
}

static void
_cmp_player_ae_set_init(cmp_player_ae_set_t *cmp, entity_t *e)
{
    player_race_def_t *d = wc_get_player_race_def(e->type_data.player.race);
    if (!d)
        return;
    ae_set_t *ae_set = as_get_ae_set(d->ae_set);
    muta_assert(ae_set);
    if (!ae_set)
        return;

    cmp->idle_fp    = ae_set->idle;
    cmp->walk_fp    = ae_set->walk;
    cmp->idle_asset = as_claim_ae_by_name(cmp->idle_fp, 1);
    cmp->walk_asset = as_claim_ae_by_name(cmp->walk_fp, 1);

    cmp_ae_animator_t *acmp = entity_get_cmp_ae_animator(e);
    _cmp_ae_animator_play(acmp, cmp->idle_asset, e->dir, 1);
}

static void
_cmp_creature_ae_set_init(cmp_creature_ae_set_t *cmp, entity_t *e)
{
    creature_def_t *def = wc_get_creature_def(e->type_data.creature.type_id);
    if (!def)
    {
        muta_assert(0);
        return;
    }
    cmp->ae_set = as_get_ae_set(def->gfx_data.ae_set_name);
    if (!cmp->ae_set)
    {
        muta_assert(0);
        return;
    }
    cmp->idle_asset = as_claim_ae_by_name(cmp->ae_set->idle, 1);
    cmp->walk_asset = as_claim_ae_by_name(cmp->ae_set->walk, 1);

    cmp_ae_animator_t *acmp = entity_get_cmp_ae_animator(e);
    _cmp_ae_animator_play(acmp, cmp->idle_asset, e->dir, 1);
}
