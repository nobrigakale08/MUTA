#define LOG(str)        puts(str)
#define LOGF(str, ...)  printf(str "\n", ##__VA_ARGS__)

#define SET_BYTE_COLOR(c, r, g, b, a) \
    (c)[0] = (r); \
    (c)[1] = (g); \
    (c)[2] = (b); \
    (c)[3] = (a);

/* For getting the dimensions of the bars surrounding a rect on the screen
 * (for example, for screen scissors) */
#define WIN_RECT_TOP_Y(r)       ((r)[1])
#define WIN_RECT_BOTTOM_Y(r)    (main_window.h - (r)[1] - (r)[3])
#define WIN_RECT_LEFT_X(r)      ((r)[0])
#define WIN_RECT_RIGHT_X(r)     (main_window.w - (r)[0] - (r)[2])
