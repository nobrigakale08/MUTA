#ifndef MUTA_SPRITE_CONTEXT_H
#define MUTA_SPRITE_CONTEXT_H

#include "../../shared/containers.h"

/* Forward declaration(s) */
typedef struct anim_t   anim_t;
typedef struct tex_t    tex_t;

/* Types defined here */
typedef struct sprite_t         sprite_t;
typedef struct sprite_context_t sprite_context_t;

enum sprite_flags
{
    SPRITE_ANIM_LOOP    = (1 << 0),
    SPRITE_ANIM_PAUSED  = (1 << 1)
};

struct sprite_t
{
    anim_t      *anim;
    float       speed;
    float       time_passed;
    sprite_t    *next;
    uint16      frame_num;
    uint8       flags;
};

DYNAMIC_OBJ_POOL_DEFINITION(sprite_t, sprite_pool, 16);

struct sprite_context_t
{
    sprite_pool_t   sprite_pool;
    sprite_t        *res_sprites;
    double          anim_accum;
};

int
sprite_context_init(sprite_context_t *sc, int num_sprites);

void
sprite_context_destroy(sprite_context_t *sc);

void
sprite_context_clear(sprite_context_t *sc);

sprite_t *
create_sprite(sprite_context_t *sc);

void
free_sprite(sprite_t *s, sprite_context_t *sc);

void
sprite_context_update(sprite_context_t *sc, double dt);

void
sprite_play_anim(sprite_t *s, anim_t *a);

tex_t *
sprite_get_cur_tex(sprite_t *s);

static inline void
sprite_set_pause(sprite_t *s, bool32 opt);

static inline void
sprite_set_loop(sprite_t *s, bool32 opt);

#define sprite_is_anim_finished(s) \
    ((s) && (s)->anim && (s)->anim->frames \
        && (s)->frame_num == (s)->anim->num_frames - 1 \
        && (s)->time_passed >= \
            (s)->anim->frames[(s)->anim->num_frames - 1].dur * (s)->speed)




static inline void
sprite_set_pause(sprite_t *s, bool32 opt)
{
    if (!s) return;
    if (opt)
        SET_BITFLAG_ON(s->flags, SPRITE_ANIM_PAUSED);
    else
        SET_BITFLAG_OFF(s->flags, SPRITE_ANIM_PAUSED);
}

static inline void
sprite_set_loop(sprite_t *s, bool32 opt)
{
    if (!s) return;
    if (opt)
        SET_BITFLAG_ON(s->flags, SPRITE_ANIM_LOOP);
    else
        SET_BITFLAG_OFF(s->flags, SPRITE_ANIM_LOOP);
}

#endif /* MUTA_SPRITE_CONTEXT_H */
