#include "characterselectscreen.h"
#include "core.h"
#include "gui.h"
#include "render.h"
#include "assets.h"
#include "gamestate.h"

static uint32 _selected_character_index;
static bool32 _logging_in = 0;

static void
_draw_selected_character_window();

static void
_draw_logging_in_window();

void
character_select_screen_update(double dt)
{
    r_viewport(0, 0, main_window.w, main_window.h);
    r_scissor(0, 0, main_window.w, main_window.h);
    r_color(0.0f, 0.0f, 0.0f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    gui_begin();

    gui_font(&sf_fancy);
    gui_button_style(menu_button_style);
    gui_win_style(menu_window_style);

    gui_origin(GUI_TOP_CENTER);
    gui_text("Characters", 0, 0, 30);

    gui_origin(GUI_CENTER_CENTER);

    uint32 num_char_props = core_num_character_props;

    if (num_char_props)
    {
        for (uint32 i = 0; i < num_char_props; ++i)
            if (gui_button(core_character_props[i].name, 0, i * 28, 96, 24, 0))
                _selected_character_index = i;
    }
    else
        gui_text("No characters created", 0, 0, 0);

    gui_origin(GUI_BOTTOM_LEFT);
    if (num_char_props < MAX_CHARACTERS_PER_ACC
    && gui_button("Create new character", 10, 10, 256, 32, 0))
        core_set_screen(&character_create_screen);

    gui_origin(GUI_BOTTOM_RIGHT);
    if (gui_button("Cancel##screen", 10, 10, 256, 32, 0))
    {
        core_disconnect_from_sv();
        core_set_screen(&main_menu_screen);
    }

    if (_selected_character_index < num_char_props)
    {
        character_props_t *cp =
            &core_character_props[_selected_character_index];

        gui_origin(GUI_CENTER_RIGHT);
        _draw_selected_character_window(cp, 0, 0);

        gui_origin(GUI_BOTTOM_CENTER);
        if (gui_button("Log in", 0, 0, 64, 24, 0))
        {
            if (core_send_log_in_character_msg(cp->id))
                core_disconnect_from_sv();
            else
                _logging_in = 1;
        }
    }

    if (_logging_in)
    {
        _draw_logging_in_window();
        if (gs_in_session())
            core_set_screen(&game_screen);
    }

    gui_end();
    r_swap_buffers(&main_window);

    if (c_conn_status != CONNSTATUS_CONNECTED)
        core_set_screen(&main_menu_screen);
}

void
character_select_screen_open()
{
    _selected_character_index   = 0;
    _logging_in                 = 0;
}

void
character_select_screen_close()
{
}

void
character_select_screen_text_input(const char *text)
{
}

void
character_select_screen_keydown(int key, bool32 is_repeat)
{
}

static void
_draw_selected_character_window(character_props_t *cp, int x, int y)
{
    gui_begin_win("Selected character", x, y, 256, 256, 0);
    gui_origin(GUI_TOP_LEFT);
    gui_textf(
        "Name: %s\n"
        "Race: %d\n"
        "Sex: %d",
        0, 5, 5, cp->name, cp->race, cp->sex);
    gui_end_win();
}

static void
_draw_logging_in_window()
{
    gui_origin(GUI_CENTER_CENTER);
    gui_begin_empty_win("log in layer", 0, 0, RESOLUTION_W, RESOLUTION_H, 0);
    gui_begin_win("##log in window", 0, 0, 256, 256, 0);
    gui_text("Logging in...", 0, 0, 0);
    gui_origin(GUI_BOTTOM_CENTER);
    if (gui_button("Cancel##log in win", 0, 0, 64, 24, 0))
        core_disconnect_from_sv();
    gui_end_win();
    gui_end_win();
}
