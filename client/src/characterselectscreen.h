#ifndef MUTA_CHARACTER_SELECTION_SCREEN_H
#define MUTA_CHARACTER_SELECTION_SCREEN_H

#include "../../shared/types.h"

void
character_select_screen_update(double dt);

void
character_select_screen_open();

void
character_select_screen_close();

void
character_select_screen_text_input(const char *text);

void
character_select_screen_keydown(int key, bool32 is_repeat);

#endif /* MUTA_CHARACTER_SELECTION_SCREEN_H */
