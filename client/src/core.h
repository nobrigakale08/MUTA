#ifndef MUTA_CORE_H
#define MUTA_CORE_H

#include <SDL2/SDL.h>
#include "../../shared/common_defs.h"
#include "../../shared/kthp.h"
#include "world.h"

#define RESOLUTION_W            1920
#define RESOLUTION_H            1080
#define WIN_POS_ANY             SDL_WINDOWPOS_UNDEFINED
#define KEYCODE(key)            SDLK_##key
#define KEY_SCANCODE(key)       SDL_SCANCODE_##key
#define MOUSE_LEFT              SDL_BUTTON_LEFT
#define MOUSE_RIGHT             SDL_BUTTON_RIGHT
#define MOUSE_LEFT              SDL_BUTTON_LEFT
#define MOUSE_MIDDLE            SDL_BUTTON_MIDDLE
#define MOUSE_SIDE_1            SDL_BUTTON_X1
#define MOUSE_SIDE_2            SDL_BUTTON_X2
#define CORE_MAX_SLASH_CMD_SHORT_NAME_LEN   2
#define CORE_MAX_SLASH_CMD_NAME_LEN         32
#define CORE_MAX_SHARDS         64

/* Forward declaration(s) */
typedef struct tile_sprite_def_t    tile_sprite_def_t;
typedef struct spritesheet_t        spritesheet_t;
typedef struct gui_win_style_t      gui_win_style_t;
typedef struct gui_button_style_t   gui_button_style_t;
typedef struct sprite_context_t     sprite_context_t;
typedef struct cryptchan_t          cryptchan_t;

/* Types defined here */
typedef struct window_t             window_t;
typedef struct perf_clock_t         perf_clock_t;
typedef struct mouse_state_t        mouse_state_t;
typedef struct screen_t             screen_t;
typedef struct game_config_t        game_config_t;
typedef struct slash_cmd_t          slash_cmd_t;
typedef struct character_props_t    character_props_t;
typedef struct shard_info_t         shard_info_t;

enum dc_reason_t
{
    DC_REASON_NONE = 0,
    DC_REASON_CHAR_LOGIN_FAIL,
    DC_REASON_SERVER_CLOSED_CONNECTION
};

enum conn_status_t
{
    CONNSTATUS_DISCONNECTED,
    CONNSTATUS_CONNECTING,
    CONNSTATUS_HANDSHAKING,
    CONNSTATUS_WAITING_FOR_SHARD_LIST,
    CONNSTATUS_SELECTING_SHARD,
    CONNSTATUS_WAITING_FOR_CHARACTER_LIST,
    CONNSTATUS_CONNECTED
};

struct window_t
{
    SDL_Window  *sdl_win;
    int         w, h;
};

struct perf_clock_t
{
    double      delta;      /* Seconds */
    uint64      last_tick;
    uint32      fps;
    uint32      target_fps;
    uint64      frame_num;
};

struct mouse_state_t
{
    uint32  buttons;
    uint32  last_buttons;
    int     x, y, last_x, last_y, down_x, down_y;
};

/* A screen is a structure to allow easier create of different program states,
 * ie. "menu screen", "gameplay screen".
 * Set up the function pointers of the structure and pass in to the set_screen()
 * function to set a screen as current. */
struct screen_t
{
    const char *name;
    void (*update)(double dt);
    void (*open)();
    void (*close)();
    void (*os_quit)();
    void (*text_input)(const char *text);
    void (*keydown)(int key, bool32 is_repeat);
    void (*keyup)(int key);
    void (*mousebuttondown)(uint8 button, int x, int y);
    void (*mousebuttonup)(uint8 button, int x, int y);
    void (*mousewheel)(int x, int y);
    void (*file_drop)(const char *path);
};

struct game_config_t
{
    struct
    {
        int     win_size[2];
        bool32  win_maximized;
        bool32  win_fullscreen;
        bool32  win_windowed;
    } display;
    struct
    {
        dchar *tile_path;
        dchar *player_race_path;
        dchar *static_obj_path;
        dchar *dynamic_obj_path;
        dchar *creature_path;
        dchar *map_path;
        dchar *map_db_path;
    } game_data;
    dchar   *tile_tex_path;
    dchar   *tile_sprite_def_path;
    bool32  vsync;
    int     fps_limit;
    float   sound_volume;
    float   music_volume;
    int     sprite_batch_size;
    char    account_name[MAX_ACC_NAME_LEN + 1];
    int     tile_px_w;
    int     tile_px_h;
    int     tile_top_px_h;
};

union slash_cmd_arg_t
{
    uint64 arg_uint64;
};

struct slash_cmd_t
{
    char                    short_name[CORE_MAX_SLASH_CMD_SHORT_NAME_LEN];
    char                    name[CORE_MAX_SLASH_CMD_NAME_LEN + 1]; /* Without the / */
    const char              *description;
    int                     (*callback)(slash_cmd_t *cmd,
                                union slash_cmd_arg_t arg, const char *args);
    union slash_cmd_arg_t   arg;
};

struct character_props_t
{
    uint64  id;
    char    name[MAX_CHARACTER_NAME_LEN + 1];
    uint32  name_len;
    int     race;
    int     sex;
};

struct shard_info_t
{
    bool32  online;
    char    name[MAX_SHARD_NAME_LEN + 1];
};

/* Globals */
extern window_t             main_window;
extern perf_clock_t         main_perf_clock;
extern mouse_state_t        mouse_state;
extern screen_t             main_menu_screen;
extern screen_t             game_screen;
extern screen_t             editor_screen;
extern screen_t             tile_editor_screen;
extern screen_t				sprite_editor_screen;
extern screen_t             character_select_screen;
extern screen_t             character_create_screen;
extern screen_t             animator_screen;
extern game_config_t        game_cfg;
extern gui_win_style_t      *menu_window_style;
extern gui_button_style_t   *menu_button_style;
extern kth_pool_t           th_pool;
extern spritesheet_t        *icons_ss;
extern sprite_context_t     *shared_sprite_ctx;
extern const uint8          *keyboard_state;
extern int                  num_keyboard_keys;
extern entity_handle_t      target_entity;
extern bool32               player_sex;
extern uint8                bcolor_white[4];
extern uint8                bcolor_blue [4];
extern uint8                bcolor_black[4];
extern int                  core_dc_reason;
extern const char *         core_dc_reason_str;
extern character_props_t    core_character_props[MAX_CHARACTERS_PER_ACC];
extern uint32               core_num_character_props;
extern bool32               core_character_creation_in_progress;
extern int                  core_character_creation_error;
extern shard_info_t         core_shards[CORE_MAX_SHARDS];
extern uint32               core_num_shards;
extern uint32               core_selected_shard;

#ifdef _MUTA_DEBUG
extern const char *core_auto_login_name;
extern const char *core_auto_login_pw;
#endif

/* Client vars */
extern enum conn_status_t   c_conn_status;
extern bool32               c_authed;
extern cryptchan_t          *c_cryptchan;

void
perf_clock_init(perf_clock_t *clock, uint32 fps_limit);

void
perf_clock_tick(perf_clock_t *clock);

int
core_init(int argc, char **argv);

int
core_run(int argc, char **argv, screen_t *first_screen);

void
core_update();

int
core_shutdown();

void
core_execute_click_events();
/* May be used to prematurely execute click events before a screen's update
 * function is finished. */

float
core_compute_pos_scaled_by_target_viewport(int *x, int *y);

void
core_set_screen(screen_t *screen);

int
core_connect_to_sv(const char *acc_name, const char *pw);

int
core_disconnect_from_sv();

int
core_maximize_window();

int
core_read_config(const char *path);

int
core_get_main_thread_fps();

byte_buf_t *
core_send_msg_to_sv(int len);
/* Size need not contain sizeof msg_t. Automatically disconnects if sending
 * fails. */

int
core_send_move_msg_to_sv(int dir);

int
core_send_find_path_msg_to_sv(int32 x, int32 y, int8 z);

int
core_send_chat_msg_to_sv(const char *msg);

int
core_send_create_character_msg_to_sv(character_props_t *cp);

int
core_send_log_in_character_msg(uint64 id);

int
core_select_shard(uint32 index);

int
core_cancel_select_shard();

int
core_cancel_character_creation();

float
core_compute_target_viewport_scale();

float
core_compute_target_viewport(int *ret);
/* Return value is the scale of the viewport in comparison to window w/h */

void
core_window_to_gui_coords(int *ret_in_x, int *ret_in_y);

int
core_chat_print(const char *msg);

int
core_chat_printf(const char *fmt, ...);

int
core_register_slash_cmd(const char *name, const char *desc, bool32 dynamic_desc,
    union slash_cmd_arg_t *arg,
    int (*callback)(slash_cmd_t *cmd, union slash_cmd_arg_t arg,
        const char *args));

int
core_set_cursor(const char *name);

int
core_execute_slash_cmd_from_chat(const char *msg);
/* Temporarily in core, move to gamestate */

#define BUTTON_BIT(btn) SDL_BUTTON(btn)
#define BUTTON_LEFT     SDL_BUTTON_LEFT
#define BUTTON_RIGHT    SDL_BUTTON_RIGHT
#define BUTTON_MIDDLE   SDL_BUTTON_MIDDLE

/* Mouse button macros
 * Example:
 *
 * if (BUTTON_DOWN(BUTTON_BIT(BUTTON_LEFT)))
 *     do_something(); */

#define BUTTON_DOWN(btn_bit) (GET_BITFLAG(mouse_state.buttons, (btn_bit)))

#define BUTTON_DOWN_NOW(btn_bit) \
    (GET_BITFLAG(mouse_state.buttons, (btn_bit)) && \
    !GET_BITFLAG(mouse_state.last_buttons, (btn_bit)))

#define BUTTON_UP_NOW(btn_bit) \
    (GET_BITFLAG(mouse_state.last_buttons, (btn_bit)) && \
    !GET_BITFLAG(mouse_state.buttons, (btn_bit)))

/* Keyboard macros:
 * IS_KEY_PRESSED() is used for polling the state of different keyboard keys
 * (most likely * in a screen's update() function). It can also be used to
 * check the state of different modifiers during a screen's keydown() or
 * keyup() function.
 * Example:
 *
 * if (IS_KEY_PRESSED(KEY_SCANCODE(F)))
 *     do_something(); */

#define IS_KEY_PRESSED(scancode) \
    ((scancode) >= 0 && (scancode) < num_keyboard_keys \
        ? keyboard_state[(scancode)] : 0)

#endif /* MUTA_CORE_H */
