#include "core.h"
#include "assets.h"
#include "render.h"
#include "utils.inl"

int main(int argc, char **argv)
{
    int r = core_init(argc, argv);
    if (r)
    {
        printf("core_init() returned %d.\n", r);
        return 1;
    }

    int         tile_px_w               = game_cfg.tile_px_w;
    int         tile_px_h               = game_cfg.tile_px_h;
    int         tile_top_px_h           = game_cfg.tile_top_px_h;
    tex_asset_t *ta = as_claim_tex_by_name("tilesheet", 1);
    int tp_res = r_set_tile_props(ta ? &ta->tex : 0, ta ? &ta->bitmap : 0,
        tile_px_w, tile_px_h, tile_top_px_h);
    if (tp_res)
        LOGF("Failed to load tile sprites, code %d.", tp_res);

    screen_t *s = &main_menu_screen;
    if (core_run(argc, argv, s))
        return 2;
    return 0;
}
