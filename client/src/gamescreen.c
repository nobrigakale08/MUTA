#include <math.h>
#include "world_internal.h"
#include "gamestate.h"
#include "render.h"
#include "core.h"

void
gamescreen_update(double dt)
{
    r_viewport(0, 0, main_window.w, main_window.h);
    r_scissor(0, 0, main_window.w, main_window.h);
    r_color(0.0f, 0.0f, 0.0f, 1.0f);
    r_clear(R_CLEAR_COLOR_BIT);

    static float xos, yos, wos, hos;

    float s = core_compute_target_viewport_scale();
    int x = (int)xos + (main_window.w - (int)((float)RESOLUTION_W * s)) / 2;
    int y = (int)yos + (main_window.h - (int)((float)RESOLUTION_H * s)) / 2;
    int w = (int)wos + (int)((float)RESOLUTION_W * s);
    int h = (int)hos + (int)((float)RESOLUTION_H * s);

#if 0
    /* Some debugging keybinds for moving the viewport */
    if (!IS_KEY_PRESSED(KEY_SCANCODE(LSHIFT)))
    {
        if (IS_KEY_PRESSED(KEY_SCANCODE(LEFT)))
            xos -= (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(RIGHT)))
            xos += (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(UP)))
            yos -= (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(DOWN)))
            yos += (float)dt * 200.f;
    } else
    {
        if (IS_KEY_PRESSED(KEY_SCANCODE(LEFT)))
            wos -= (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(RIGHT)))
            wos += (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(UP)))
            hos -= (float)dt * 200.f;
        if (IS_KEY_PRESSED(KEY_SCANCODE(DOWN)))
            hos += (float)dt * 200.f;
    }
#endif

    if (c_conn_status != CONNSTATUS_DISCONNECTED)
    {
        gs_update_and_render(dt, x, y, w, h);
        core_execute_click_events();
    } else
        core_set_screen(&main_menu_screen);
    /* Note: we use the world's render_props member here, because it is
     * possible it has not been updated in the last frame. */

    r_swap_buffers(&main_window);
}

void
gamescreen_open()
{
}

void
gamescreen_close()
{
    core_disconnect_from_sv();
}

void
gamescreen_os_quit()
    {core_shutdown();}

void
gamescreen_keydown(int key, bool32 is_repeat)
    {gs_key_down(key, is_repeat);}

void
gamescreen_keyup(int key)
    {gs_key_up(key);}

void
gamescreen_text_input(const char *text)
{
}

void
gamescreen_mousebuttonup(uint8 button, int x, int y)
    {gs_mouse_down((int)button, x, y);}

void
gamescreen_mousebuttondown(uint8 button, int x, int y)
    {gs_mouse_up((int)button, x, y);}
