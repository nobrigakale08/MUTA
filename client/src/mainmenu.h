#ifndef MUTA_MAINMENU_H
#define MUTA_MAINMENU_H

void
main_menu_update(double dt);

void
main_menu_open();

void
main_menu_close();

void
main_menu_keydown(int key, bool32 is_repeat);

void
main_menu_text_input(const char *text);

void
main_menu_os_quit();

#endif /* MUTA_MAINMENU_H */
