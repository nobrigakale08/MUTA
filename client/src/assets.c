#include "assets.h"
#include "audio.h"
#include "utils.inl"
#include "../../shared/common_utils.h"
#include "../../shared/mdb.h"
#include "render.h"
#include "core.h"
#include "asset_container.h"

typedef struct font_load_context_t font_load_context_t;

struct font_load_context_t
{
    sfont_t *font;
    dchar   *path;
    int     size;
    enum
    {
        FONT_DEFAULT_NONE,
        FONT_DEFAULT,
        FONT_DEFAULT_PIXEL
    }       default_type;
};

#define TEX_ASSET_ITEM(ta_) \
    ((tex_asset_container_item_t*)((char*)(ta_) - \
    offsetof(tex_asset_container_item_t, asset)))
#define ANIM__ASSET_ITEM(aa_) \
    ((anim_asset_container_item_t*)((char*)(aa_) - \
    offsetof(anim_asset_container_item_t, asset)))
#define AE_ASSET_ITEM(aa_) \
    ((ae_asset_container_item_t*)((char*)(aa_) - \
    offsetof(ae_asset_container_item_t, asset)))
#define SPRITESHEET_ITEM(ss_) \
    ((spritesheet_container_item_t*)((char*)(ss_) - \
    offsetof(spritesheet_container_item_t, asset)))
#define SOUND_ASSET_ITEM(sa_) \
    ((sound_asset_container_item_t*)((char*)(sa_) - \
    offsetof(sound_asset_container_item_t, asset)))
#define CURSOR_BUCKET_SZ 2

typedef struct unclaimed_asset_t    unclaimed_asset_t;
typedef unclaimed_asset_t           unclaimed_asset_darr_t;

enum asset_types
{
    ASSET_TEX,
    ASSET_SOUND
};

struct unclaimed_asset_t
{
    void    *asset;
    uint32  time;
    int8    type;
};

static struct
{
    tex_job_t   *jobs;
    int         num, max;
    mutex_t     mtx;
} tex_jobs;

ASSET_CONTAINER_DEFINITION(tex_asset_t, tex_asset_container);
ASSET_CONTAINER_DEFINITION(anim_asset_t, anim_asset_container);
ASSET_CONTAINER_DEFINITION(ae_asset_t, ae_asset_container);
ASSET_CONTAINER_DEFINITION(spritesheet_t, spritesheet_container);
ASSET_CONTAINER_DEFINITION(sound_asset_t, sound_asset_container);
DYNAMIC_HASH_TABLE_DEFINITION(str_ae_set_table, ae_set_t*, const char*, uint64,
    fnv_hash64_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(str_cursor_table, cursor_t, const char*, uint32,
    fnv_hash32_from_str, CURSOR_BUCKET_SZ);
DYNAMIC_HASH_TABLE_DEFINITION(str_sfont_table, sfont_t*, const char*, uint32,
    fnv_hash32_from_str, CURSOR_BUCKET_SZ);

/* Externs */
sfont_t     sf_default;
sfont_t     sf_fancy;
sfont_t     sf_fancy_small;

static tex_asset_container_t        _tex_assets;
static anim_asset_container_t       _anim_assets;
static ae_asset_container_t         _ae_assets;
static spritesheet_container_t      _spritesheets;
static sound_asset_container_t      _sound_assets;
static unclaimed_asset_darr_t       *_unclaimed_assets;
static tex_asset_container_item_t   _matlock;

static const char *_ae_sets_path    = "muta-assets/ae_sets.def";
static const char *_fonts_path      = "muta-assets/fonts.def";

static struct
{
    obj_pool_t          pool;
    str_ae_set_table_t  table;
} _ae_sets;

static struct
{
    obj_pool_t          pool;
    str_sfont_table_t   table;
    sfont_t             *default_font;
    sfont_t             *default_pixel_font;
} _fonts;

static str_cursor_table_t _cursors;

static int
_ae_asset_load(ae_asset_t *aes, bool32 async);

static int
_spritesheet_load(spritesheet_t *sheet, bool32 async);

static int
_anim_asset_load(anim_asset_t *aa, bool32 async);

static int
_sfont_from_file_quick_and_dirty(sfont_t *sf, const char *path, int font_size);

static void
_tex_asset_async_callback(void *args);

static int
_load_tex_assets();

static int
_load_anim_assets();

static int
_load_ae_assets();

static int
_load_ae_sets();

static int
_load_fonts();

static int
_load_spritesheets();

static int
_load_cursors();

static void
_destroy_cursors();

static int
_load_sound_assets();

static void
_destroy_sound_assets();

static void
_destroy_tex_assets();

static void
_destroy_anim_assets();

static void
_destroy_ae_assets();

static void
_destroy_ae_sets();

static void
_destroy_fonts();

static void
_destroy_spritesheets();

static int
_tex_asset_load_bitmap(tex_asset_t *ta);

static int
_tex_asset_generate_gl_texture(tex_asset_t *ta);

static int
_tex_asset_load_bitmap_and_generate_texture(tex_asset_t *ta);

static void
_spritesheet_unload(spritesheet_t *ss);

static void
_anim_asset_unload(anim_asset_t *a);

int
as_load()
{
    tex_jobs.jobs   = malloc(sizeof(tex_job_t) * 64);
    if (!tex_jobs.jobs) return 9;
    tex_jobs.max    = 64;
    tex_jobs.num    = 0;
    mutex_init(&tex_jobs.mtx);
    darr_reserve(_unclaimed_assets, 128);

    _matlock.row                = 0xFFFFFFFF;
    _matlock.asset.keep_loaded  = 1;
    _matlock.asset.loaded       = 1;
    _matlock.asset.keep_bitmap  = 1;
    img_load(&_matlock.asset.bitmap, "assets/textures/misc/matlock.png");
    tex_from_img(&_matlock.asset.tex, &_matlock.asset.bitmap);

    _load_sound_assets();
    _load_tex_assets();
    _load_anim_assets();
    _load_ae_assets();
    _load_ae_sets();
    _load_fonts();
    _load_spritesheets();
    _load_cursors();

    if (_sfont_from_file_quick_and_dirty(&sf_default, "assets/fonts/munro.ttf",
        16))
        DEBUG_PRINTFF("failed to load default font.\n");
    if (_sfont_from_file_quick_and_dirty(&sf_fancy,
        "assets/fonts/metamorphous_regular.ttf", 16))
        DEBUG_PRINTFF("failed to font fancy.");
    if (_sfont_from_file_quick_and_dirty(&sf_fancy_small,
        "assets/fonts/metamorphous_regular.ttf", 11))
        DEBUG_PRINTFF("failed to load font fancy small\n");

    return 0;
}

void
as_destroy()
{
    _destroy_sound_assets();
    _destroy_anim_assets();
    _destroy_ae_assets();
    _destroy_ae_sets();
    _destroy_fonts();
    _destroy_spritesheets();
    _destroy_tex_assets();
    _destroy_cursors();

    tex_free(&_matlock.asset.tex);
    img_free(&_matlock.asset.bitmap);

    sfont_free(&sf_default);
    sfont_free(&sf_fancy);
    sfont_free(&sf_fancy_small);
}

void
as_update()
{
    uint32              time_now            = SDL_GetTicks();
    uint32              num                 = darr_num(_unclaimed_assets);
    uint32              sub;
    tex_asset_t         *ta;
    sound_asset_t       *sa;
    for (int32 i = 0; i < (int32)num; ++i)
    {
        sub = time_now - _unclaimed_assets[i].time;

        switch (_unclaimed_assets[i].type)
        {
        case ASSET_TEX:
        {
            ta  = _unclaimed_assets[i].asset;
            if (sub < 5000 || ta->loading || !ta->loaded)
                continue;
            if (ta->user_count == 0)
            {
                if (ta->keep_bitmap)
                    img_free(&ta->bitmap);
                tex_free(&ta->tex);
                ta->loaded = 0;
                DEBUG_PRINTFF("freed tex %s.\n", tex_asset_get_path(ta));
            }
            ta->in_unclaimed_queue = 0;
        }
            break;
        case ASSET_SOUND:
        {
            sa = _unclaimed_assets[i].asset;
            if (sub < 15000 || sa->loading || !sa->loaded)
                continue;
            if (sa->user_count == 0)
            {
                dequeue_sound_asset(sa);
                au_destroy_buffer(&sa->sound.buffer);
                sa->loaded = 0;
                DEBUG_PRINTFF("freed sound %s.\n", sound_asset_get_path(sa));
            }
            sa->in_unclaimed_queue = 0;
        }
            break;
        }
        _unclaimed_assets[i] = _unclaimed_assets[--num];
    }
    assert(_unclaimed_assets);
    darr_head(_unclaimed_assets)->num = num;
}

static int
_push_gen_tex_job(tex_asset_t *ta)
{
    int ret = 0;

    mutex_lock(&tex_jobs.mtx);

    if (ta->tex.id != 0)
        goto close;

    for (int i = 0; i < tex_jobs.num; ++i)
        if (tex_jobs.jobs[i].ta == ta)
            goto close;

    if (tex_jobs.num == tex_jobs.max)
    {
        uint32 new_max = tex_jobs.max * 2;
        uint32 req_max = tex_jobs.max + 4;
        if (new_max < req_max)
            new_max = req_max;
        tex_job_t *new_jobs = realloc(tex_jobs.jobs,
            (new_max) * sizeof(tex_job_t));

        if (new_jobs == 0)
        {
            ret = 1;
            goto close;
        }

        tex_jobs.jobs = new_jobs;
        tex_jobs.max  = new_max;
    }

    tex_jobs.jobs[tex_jobs.num++].ta = ta;

    close:
        mutex_unlock(&tex_jobs.mtx);
        return ret;
}

int
bind_tex_jobs()
{
    if (tex_jobs.num <= 0)
        return 0;
    mutex_lock(&tex_jobs.mtx);
    for (int i = 0; i < tex_jobs.num; ++i)
    {
        _tex_asset_generate_gl_texture(tex_jobs.jobs[i].ta);
        tex_jobs.jobs[i].ta->loading    = 0;
        tex_jobs.jobs[i].ta->loaded     = 1;
    }
    tex_jobs.num = 0;
    mutex_unlock(&tex_jobs.mtx);
    return 0;
}

char *
get_layer_type_string(int layer)
{
    switch (layer)
    {
    case LAYER_BODY:         return "Body";
    case LAYER_HEAD:         return "Head";
    case LAYER_LHAND:        return "Left hand";
    case LAYER_RHAND:        return "Right hand";
    case LAYER_LEGS:         return "Legs";
    case LAYER_FEET:         return "Feet";
    case LAYER_BODY_ARMOUR:  return "Body Armour";
    case LAYER_HEAD_ARMOUR:  return "Helmet";
    case LAYER_LHAND_ARMOUR: return "Left hand Armour";
    case LAYER_RHAND_ARMOUR: return "Right hand Armour";
    case LAYER_LEGS_ARMOUR:  return "Leg Armour";
    case LAYER_FEET_ARMOUR:  return "Foot Armour";
    case LAYER_LHAND_ITEM:   return "Left hand item";
    case LAYER_RHAND_ITEM:   return "Right hand item";
    }
    return "Other";
}

static int
_spritesheet_load(spritesheet_t *sheet, bool32 async)
{
    const char *fp = spritesheet_get_path(sheet);
    memset(sheet, 0, sizeof(spritesheet_t));

    FILE *f = fopen(fp, "r");
    if (!f) {return 1;}

    int ret = 0;

    char  line[512];
    char  str_buf[512];
    int   line_len  = 512;
    int   num_clips = 0;
    int   index     = 0;
    float tmpf;
    float clip[4];

    while(fgets(line, line_len, f))
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &tmpf, &tmpf, &tmpf,
            &tmpf) == 5)
            ++num_clips;

    sheet->clips        = calloc(num_clips, sizeof(float) * 4);
    sheet->clip_names   = calloc(num_clips, sizeof(char*));
    if(!sheet->clips || !sheet->clip_names)
        {ret = 2; goto out;}

    sheet->num_clips = num_clips;

    rewind(f);

    fgets(line, line_len, f);

    if (strncmp(line, "PATH: ", 6))
        {ret = 3; goto out;}

    strncpy(str_buf, line + 6, 512);
    str_strip_trailing_spaces(str_buf);
    str_strip_non_ascii(str_buf);
    str_strip_ctrl_chars(str_buf);

    sheet->ta = as_claim_tex_by_name(str_buf, async);
    if (!sheet->ta)
        {ret = 4; goto out;}

    while (fgets(line, line_len, f))
    {
        if (sscanf(line, "%511s %f %f %f %f", str_buf, &clip[0], &clip[1],
            &clip[2], &clip[3]) == 5)
        {
            sheet->clips[index][0]      = clip[0];
            sheet->clips[index][1]      = clip[1];
            sheet->clips[index][2]      = clip[2];
            sheet->clips[index][3]      = clip[3];
            sheet->clip_names[index]    = create_dynamic_str(str_buf);
            if (!sheet->clip_names[index]) {ret = 5; goto out;}
            index++;
        } else
            DEBUG_PRINTFF("failed to scan clip %d.\n", index);
    }

    out:
        if (ret)
        {
            for (int i = 0; i < sheet->num_clips; ++i)
                free_dynamic_str(sheet->clip_names[i]);
            free(sheet->clip_names);
            free(sheet->clips);
            as_unclaim_tex_asset(sheet->ta);
            DEBUG_PRINTFF("failed to load '%s' with code %d!\n", fp, ret);
        }
        safe_fclose(f);
        return ret;
}

static int
_anim_asset_load(anim_asset_t *aa, bool32 async)
{
    anim_t *anim        = &aa->anim;
    const char *path    = anim_asset_get_path(aa);

    FILE *fp = 0;
    fp = fopen(path, "r");

    if(!fp)
        return 1;

    int ret = 0;

    char name[128] = {0};
    fgets(name, 128, fp);

    for (int i = 0; i < 128; ++i)
        if (name[i] == '\n') {name[i] = '\0'; break;}

    char line[1024];

    anim->num_frames = 0;

    int num_frames = -1;

    fgets(line, 1024, fp);
    sscanf(line, "%d", &num_frames);

    if (num_frames <= 0)
        {ret = 1; goto out;}

    anim->num_frames = num_frames;
    anim_frame_t *frames_for_anim = calloc(num_frames, sizeof(anim_frame_t));
    anim->frames = frames_for_anim;
    anim->total_dur = 0.f;

    for (int i = 0; i < num_frames; ++i)
    {
        char texture_name[128] = {0};
        fgets(texture_name, 128, fp);

        for (int j = 0; j < 128; ++j)
            if (texture_name[j] == '\n') {texture_name[j] = '\0'; break;}

        int x, y, w, h, offx, offy;

        fgets(line, 1024, fp);
        sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);

        float dur = 0.016667f;

        fgets(line, 1024, fp);
        sscanf(line, "%f", &dur);

        tex_asset_t *frame_asset = as_claim_tex_by_name(texture_name,
            async);
        if (frame_asset == &_matlock.asset)
        {
            x = y = offx = offy = 0;
            w = 128;
            h = 144;
        }

        anim->frames[i].tex = &frame_asset->tex;
        anim->frames[i].clip[0] = (float)x;
        anim->frames[i].clip[1] = (float)y;
        anim->frames[i].clip[2] = (float)w;
        anim->frames[i].clip[3] = (float)h;
        anim->frames[i].ox      = (float)offx;
        anim->frames[i].oy      = (float)offy;
        anim->frames[i].dur     = dur;

        anim->total_dur += dur;
    }

    out:
        fclose(fp);
        return ret;
}

static int
_ae_asset_load(ae_asset_t *aes, bool32 async)
{
    animated_entity_t   *ae     = &aes->ae;
    const char          *path   = ae_asset_get_path(aes);

    FILE *f;
    f = fopen(path, "r");
    if (!f) return 1;

    char name[512];
    char line[512];

    fgets(line, 512, f);
    sscanf(line, "%511s", name);

    int error_code = 0;

    for (int a = 0; a < 8; ++a)
    {
        int fc = 0;
        fgets(line, 512, f);
        fgets(line, 512, f);
        fgets(line, 512, f);
        sscanf(line, "Frames %d", &fc);
        if (fc <= 0) { error_code = 1; goto error; }
        ae_animation_t *anim = &ae->anims[a];
        anim->frames      = (ae_frame_t*)calloc(fc, sizeof(ae_frame_t));
        anim->num_frames  = fc;

        for (int i = 0; i < fc; ++i)
        {
            ae_frame_t *frame = &anim->frames[i];

            fgets(line, 512, f);
            fgets(line, 512, f);
            fgets(line, 512, f);
            int lc = 0;
            if (sscanf(line, "Layers %d", &lc) != 1)
            { error_code = 2; goto error; }

            int fox = 0, foy = 0;
            fgets(line, 512, f);
            if (sscanf(line, "Offsets %d %d", &fox, &foy) != 2)
            { error_code = 3; goto error; }

            frame->layers      = (ae_layer_t*)malloc(sizeof(ae_layer_t) * lc);
            frame->num_layers  = lc;
            frame->ox          = (float)fox;
            frame->oy          = (float)foy;

            for (int j = 0; j < lc; ++j)
            {
                ae_layer_t *layer = &frame->layers[j];

                fgets(line, 512, f);
                char tex_name[512];
                fgets(line, 512, f);
                if (strncmp(line, "Texture ", 8) || !line[8])
                    {error_code = 4; goto error;}

                strncpy(tex_name, line + 8, 512);
                str_strip_trailing_spaces(tex_name);

                int color[4];
                fgets(line, 512, f);
                if (sscanf(line, "Color %d %d %d %d",
                    &color[0], &color[1], &color[2], &color[3]) != 4)
                { error_code = 5; goto error; }

                int clip[4];
                fgets(line, 512, f);
                if (sscanf(line, "Clip %d %d %d %d",
                    &clip[0], &clip[1], &clip[2], &clip[3]) != 4)
                { error_code = 6; goto error; }

                int lox = 0, loy = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Offsets %d %d", &lox, &loy) != 2)
                { error_code = 7; goto error; }

                float rot = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Rotation %f", &rot) != 1)
                { error_code = 8; goto error; }

                int flip = 0;
                fgets(line, 512, f);
                if (sscanf(line, "Flip %d", &flip) != 1)
                { error_code = 9; goto error; }

                int type = NUM_LAYER_TYPES;
                fgets(line, 512, f);
                if (sscanf(line, "Type %d", &type) != 1)
                { error_code = 10; goto error; }

                tex_asset_t *ta = as_claim_tex_by_name(tex_name, async);
                if (ta == 0)
                { error_code = 11; goto error; }

                layer->ta       = ta;
                layer->color[0] = (uint8)color[0];
                layer->color[1] = (uint8)color[1];
                layer->color[2] = (uint8)color[2];
                layer->color[3] = (uint8)color[3];
                layer->clip[0]  = (float)clip[0];
                layer->clip[1]  = (float)clip[1];
                layer->clip[2]  = (float)clip[2];
                layer->clip[3]  = (float)clip[3];
                layer->ox       = (float)lox;
                layer->oy       = (float)loy;
                layer->rot      = rot;
                layer->flip     = flip;
                layer->type     = type;
            }
        }
    }

    ae->cur_anim = &ae->anims[0];
    safe_fclose(f);
    return 0;

    error:
    safe_fclose(f);
    char *errors = "Something went wrong";
    switch (error_code)
    {
    case 1:  errors = "Could not get frame count";      break;
    case 2:  errors = "Could not get layer count";      break;
    case 3:  errors = "Could not get frame offsets";    break;
    case 4:  errors = "Could not get texture name";     break;
    case 5:  errors = "Could not get layer color";      break;
    case 6:  errors = "Could not get texture clip";     break;
    case 7:  errors = "Could not get layer offsets";    break;
    case 8:  errors = "Could not get layer rotation";   break;
    case 9:  errors = "Could not get layer flip";       break;
    case 10: errors = "Could not get layer type";       break;
    case 11: errors = "Could not load texture";         break;
    }

    DEBUG_PRINTF("Failed to load Entity Animation, error code %d (%s)\n",
                error_code, errors);

    for (int i = 0; i < 8; ++i)
    {
        ae_animation_t *anim = &ae->anims[i];
        if (!anim || !anim->frames)
            continue;
        for (int j = 0; j < anim->num_frames; ++j)
        {
            ae_frame_t *frame = &anim->frames[j];
            free(frame->layers);
            frame->layers     = 0;
            frame->num_layers = 0;
        }
        free(anim->frames);
    }
    return error_code;
}

static void
_sound_asset_load_callback(void *args)
{
    sound_asset_t *sa = args;
    muta_assert(!sa->loaded);
    int r;
    if ((r = create_buffer(&sa->sound.buffer, sound_asset_get_path(sa))))
    {
        DEBUG_PRINTFF("error loading (code %d).\n", r);
        sa->errors_loading = 1;
    } else
        sa->errors_loading = 0;
    sa->loaded  = 1;
    sa->loading = 0;
}

static int
_sound_asset_load(sound_asset_t *sa, bool32 async)
{
    muta_assert(!sa->loaded);
    const char *fp = sound_asset_get_path(sa);
    if (!fp) return 1;

    muta_assert(!sa->loading);
    if (async)
    {
        sa->loading = 1;
        if (kth_pool_add_job(&th_pool, _sound_asset_load_callback, sa))
        {
            sa->loading = 0;
            return 2;
        }
    } else
    if (create_buffer(&sa->sound.buffer, fp))
    {
        sa->loaded = 1;
        return 3;
    }
    return 0;
}

static void
_tex_asset_async_callback(void *args)
{
    tex_asset_t *ta = args;
    muta_assert(!ta->loaded);
    if (!_tex_asset_load_bitmap(ta))
    {
        if (!_push_gen_tex_job(ta))
            return;
        img_free(&ta->bitmap);
    }
    DEBUG_PRINTFF("failed.");
    ta->loading = 0;
}

static int
_launch_tex_asset_async_load(tex_asset_t *ta)
{
    muta_assert(!ta->loading);
    ta->loading = 1;
    if (!kth_pool_add_job(&th_pool, _tex_asset_async_callback, ta))
        return 0;
    ta->loading = 0;
    return 1;
}

static int
_load_tex_assets()
{
    int ret = 0;

    if (tex_asset_container_init(&_tex_assets, "muta-assets/textures.mdb"))
        {ret = 1; goto out;}

    uint32 keep_loaded_i;
    uint32 keep_bitmap_i;
    uint32 min_filter_i;
    uint32 mag_filter_i;

    int i;
    i = mdb_get_col_index(&_tex_assets.mdb, "keep loaded");
    if (i < 0) {ret = 2; goto out;}
    keep_loaded_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "keep bitmap");
    if (i < 0) {ret = 3; goto out;}
    keep_bitmap_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "min filter");
    if (i < 0) {ret = 4; goto out;}
    min_filter_i = i;
    i = mdb_get_col_index(&_tex_assets.mdb, "mag filter");
    if (i < 0) {ret = 5; goto out;}
    mag_filter_i = i;

    int8 keep_loaded, keep_bitmap, min_filter, mag_filter;

    tex_asset_container_item_t  *item;
    tex_asset_t                 *ta;
    for (item = tex_asset_container_pool_first(&_tex_assets.pool);
         item;
         item = item->next)
    {
        ta = &item->asset;

        keep_loaded = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            keep_loaded_i);
        keep_bitmap = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            keep_bitmap_i);
        min_filter = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            min_filter_i);
        mag_filter = *(int8*)mdb_get_field(&_tex_assets.mdb, item->row,
            mag_filter_i);

        ta->keep_loaded = keep_loaded ? 1 : 0;
        ta->keep_bitmap = keep_bitmap ? 1 : 0;
        ta->min_filter  = min_filter == 1 ? TEX_FILTER_NEAREST :
            TEX_FILTER_LINEAR;
        ta->mag_filter  = mag_filter == 1 ? TEX_FILTER_NEAREST :
            TEX_FILTER_LINEAR;
        ta->wrapping    = TEX_WRAP_CLAMP_TO_EDGE; /* tmp */

        /* Load asynchronously */
        if (keep_loaded)
            _launch_tex_asset_async_load(ta);
    }

    out:
        if (ret)
        {
            DEBUG_PRINTFF("failed with code %d.\n", ret);
            tex_asset_container_destroy(&_tex_assets);
        } else
            DEBUG_PRINTFF("loaded %u items.\n", _tex_assets.mdb.num_rows);
        return ret;
}

static int
_load_anim_assets()
{
    int ret = 0;
    if (anim_asset_container_init(&_anim_assets, "muta-assets/animations.mdb"))
        {ret = 1; goto out;}

    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {ret = 2; goto out;}

    uint32 keep_loaded_i = index;

    anim_asset_container_item_t *item;
    for (item = anim_asset_container_pool_first(&_anim_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_anim_assets.mdb, item->row,
            _anim_assets.path_mdb_index);

        if (!path)
            continue;

        bool32 keep_loaded = *(int8*)mdb_get_field(&_anim_assets.mdb, item->row,
            keep_loaded_i);

        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _anim_asset_load(&item->asset, 1);
    }

    out:
        if (ret)
        {
            DEBUG_PRINTFF("failed with code %d.\n", ret);
            anim_asset_container_destroy(&_anim_assets);
        } else
            DEBUG_PRINTFF("loaded %u items.\n", _anim_assets.mdb.num_rows);
        return ret;
}

static int
_load_ae_assets()
{
    int ret = 0;

    if (ae_asset_container_init(&_ae_assets,
        "muta-assets/iso-animations.mdb"))
        {ret = 1; goto out;}

    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {ret = 2; goto out;}

    uint32 keep_loaded_i = index;

    ae_asset_container_item_t *item;
    for (item = ae_asset_container_pool_first(&_ae_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_ae_assets.mdb, item->row,
            _ae_assets.path_mdb_index);

        if (!path)
            continue;

        bool32 keep_loaded = *(int8*)mdb_get_field(&_ae_assets.mdb,
            item->row, keep_loaded_i);

        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _ae_asset_load(&item->asset, 1);
    }

    out:
        if (ret)
        {
            DEBUG_PRINTFF("failed with code %d.\n", ret);
            ae_asset_container_destroy(&_ae_assets);
        } else
            DEBUG_PRINTFF("loaded %u items.\n", _ae_assets.mdb.num_rows);
        return 0;
}

static int
_ae_set_on_def(void *ctx, const char *def, const char *val)
{
    printf("def: %s\n", def);
    if (!streq(def, "ae_set"))
        return 1;
    if (str_ae_set_table_exists(&_ae_sets.table, def))
    {
        printf("Error: ae set '%s' defined twice in %s.\n", def, _ae_sets_path);
        return 2;
    }
    ae_set_t **set = ctx;
    *set = obj_pool_reserve(&_ae_sets.pool);
    memset(*set, 0, sizeof(ae_set_t));
    str_ae_set_table_einsert(&_ae_sets.table, val, *set);
    return 0;
}

static int
_ae_set_on_opt(void *ctx, const char *opt, const char *val)
{
    ae_set_t **set = ctx;
    if (!*set)
        return 1;
    if (streq(opt, "idle"))
        dstr_set(&(*set)->idle, val);
    else if (streq(opt, "walk"))
        dstr_set(&(*set)->walk, val);
    else
        return 2;
    return 0;
}

static int
_load_ae_sets()
{
    obj_pool_init(&_ae_sets.pool, 64, sizeof(ae_set_t));
    str_ae_set_table_einit(&_ae_sets.table, 64);
    ae_set_t *set = 0;
    int r = parse_def_file(_ae_sets_path, _ae_set_on_def,
        _ae_set_on_opt, &set);
    if (r)
        printf("Errors loading ae sets from file '%s'.\n", _ae_sets_path);
    return r;
}

static int
_font_on_def(void *ctx, const char *def, const char *val)
{
    int default_type = FONT_DEFAULT_NONE;
    if (!streq(def, "font"))
    {
        if (streq(def, "default_font"))
            default_type = FONT_DEFAULT;
        else if (streq(def, "default_pixel_font"))
            default_type = FONT_DEFAULT_PIXEL;
        else
            return 1;
    }
    if (str_sfont_table_exists(&_fonts.table, val))
    {
        printf("Warning: font '%s' defined twice.\n", val);
        return 2;
    }
    font_load_context_t *lc = ctx;
    sfont_t *font = obj_pool_reserve(&_fonts.pool);
    memset(font, 0, sizeof(sfont_t));
    str_sfont_table_einsert(&_fonts.table, val, font);
    lc->font = font;
    set_dynamic_str_len(lc->path, 0);
    lc->size            = -1;
    lc->default_type    = default_type;
    return 0;
}

static int
_font_on_val(void *ctx, const char *opt, const char *val)
{
    font_load_context_t *lc = ctx;
    if (!lc->font)
        return 3;
    if (streq(opt, "path"))
        dstr_set(&lc->path, val);
    else if (streq(opt, "size"))
        lc->size = atoi(val);
    else
        return 3;

    if (dstr_len(lc->path) && lc->size > 0)
    {
        int r = _sfont_from_file_quick_and_dirty(lc->font, lc->path, lc->size);
        if (!r)
        {
            if (lc->default_type == FONT_DEFAULT || !_fonts.default_font)
            {
                _fonts.default_font = lc->font;
                if (!_fonts.default_pixel_font)
                    _fonts.default_pixel_font = lc->font;
            } else
            if (lc->default_type == FONT_DEFAULT_PIXEL)
                _fonts.default_pixel_font = lc->font;
        }
        lc->font            = 0;
        lc->default_type    = FONT_DEFAULT_NONE;
    }

    return 0;
}

static int
_load_fonts()
{
    obj_pool_init(&_fonts.pool, 32, sizeof(sfont_t));
    str_sfont_table_einit(&_fonts.table, 64);
    font_load_context_t ctx = {0};
    ctx.path = dstr_create_empty(255);
    int r = parse_def_file(_fonts_path, _font_on_def, _font_on_val, &ctx);
    if (r)
        printf("Error %d loading fonts from file '%s'.\n", r, _fonts_path);
    dstr_free(&ctx.path);
    return 0;
}

static int
_load_spritesheets()
{
    int ret = 0;

    if (spritesheet_container_init(&_spritesheets,
        "muta-assets/spritesheets.mdb"))
        {ret = 1; goto out;}

    int index = mdb_get_col_index(&_anim_assets.mdb, "keep loaded");
    if (index < 0)
        {ret = 2; goto out;}

    uint32 keep_loaded_i = index;

    spritesheet_container_item_t *item;
    for (item = spritesheet_container_pool_first(&_spritesheets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_spritesheets.mdb, item->row,
            _spritesheets.path_mdb_index);

        if (!path)
            continue;

        bool32 keep_loaded = *(int8*)mdb_get_field(&_spritesheets.mdb,
            item->row, keep_loaded_i);

        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _spritesheet_load(&item->asset, 1);
    }

    out:
        if (ret)
        {
            DEBUG_PRINTFF("failed with code %d.\n", ret);
            spritesheet_container_destroy(&_spritesheets);
        } else
            DEBUG_PRINTFF("loaded %u items.\n", _spritesheets.mdb.num_rows);
        return ret;
}

static void
_load_cursors_callback(void *ctx, const char *opt, const char *val)
{
    cursor_t *cursor = as_get_cursor(opt);

    if (cursor)
    {
        if (streq(opt, cursor->name))
            printf("Warning: cursor '%s' defined twice.\n", opt);
        else
            printf("Warning: cursor definition '%s' collides with an existing "
             "definition (same hash). Try changing the name.\n", opt);
        return;
    }

    cursor = str_cursor_table_einsert_empty(&_cursors, opt);

    img_t img;
    if (img_load(&img, val))
    {
        printf("Error: failed to load cursor '%s' from path '%s'.\n", opt, val);
        return;
    }

    uint32 r = TO_SYS_ENDIAN32(0x000000ff);
    uint32 g = TO_SYS_ENDIAN32(0x0000ff00);
    uint32 b = TO_SYS_ENDIAN32(0x00ff0000);
    uint32 a = TO_SYS_ENDIAN32(0xff000000);

    int err = 0;
    SDL_Surface *surface  = SDL_CreateRGBSurfaceFrom(img.pixels, img.w, img.h,
        32, 4 * 17, r, g, b, a);

    if (!surface)
        {err = 2; goto out;}

    cursor->data = SDL_CreateColorCursor(surface, 0, 0);
    if (!cursor->data)
        {err = 3; goto out;}

    out:
        if (err)
        {
            str_cursor_table_erase(&_cursors, opt);
            printf("Error loading cursor '%s', code %d.\n", opt, err);
        }
        SDL_FreeSurface(surface);
        img_free(&img);

}

static int
_load_cursors()
{
    const char *fp = "muta-assets/cursors.cfg";
    str_cursor_table_einit(&_cursors, 8);
    int r = parse_cfg_file(fp, _load_cursors_callback, 0);
    if (r)
    {
        printf("Failed to open %s- cursors will not be loaded.\n", fp);
        _destroy_cursors();
    }
    return r;
}

static void
_destroy_cursors()
{
    for (uint32 i = 0; i < _cursors.num_buckets; ++i)
        for (uint32 j = 0; j < CURSOR_BUCKET_SZ; ++j)
            if (_cursors.buckets[i].res_flags & j)
                SDL_FreeCursor(_cursors.buckets[i].items[j].val.data);
    str_cursor_table_destroy(&_cursors);
}

static int
_load_sound_assets()
{
    int ret = 0;

    if (sound_asset_container_init(&_sound_assets, "muta-assets/sounds.mdb"))
        {ret = 1; goto out;}
    int index = mdb_get_col_index(&_sound_assets.mdb, "keep loaded");
    if (index < 0)
        {ret = 2; goto out;}

    uint32 keep_loaded_i = index;

    sound_asset_container_item_t *item;
    for (item = sound_asset_container_pool_first(&_sound_assets.pool);
         item;
         item = item->next)
    {
        dchar *path = *(dchar**)mdb_get_field(&_sound_assets.mdb, item->row,
            _sound_assets.path_mdb_index);

        if (!path)
            continue;

        bool32 keep_loaded = *(int8*)mdb_get_field(&_sound_assets.mdb,
            item->row, keep_loaded_i);

        item->asset.keep_loaded = keep_loaded ? 1 : 0;
        if (keep_loaded)
            _sound_asset_load(&item->asset, 1);
    }

    out:
        if (ret)
        {
            DEBUG_PRINTFF("failed with code %d.\n", ret);
            sound_asset_container_destroy(&_sound_assets);
        } else
            DEBUG_PRINTFF("loaded %u items.\n", _sound_assets.mdb.num_rows);
        return ret;
}


static void
_destroy_sound_assets()
{
    sound_asset_container_item_t *item;
    for (item = _sound_assets.pool.res; item; item = item->next)
    {
        if (!item->asset.loaded)
            continue;
        dequeue_sound_asset(&item->asset);
        au_destroy_buffer(&item->asset.sound.buffer);
        item->asset.loaded = 0;
    }
    sound_asset_container_destroy(&_sound_assets);
}

static void
_destroy_tex_assets()
{
    tex_asset_container_item_t *item;
    for (item = _tex_assets.pool.res; item; item = item->next)
    {
        if (!item->asset.loaded)
            continue;
        if (item->asset.keep_bitmap)
            img_free(&item->asset.bitmap);
        tex_free(&item->asset.tex);
        item->asset.loaded = 0;
    }
    tex_asset_container_destroy(&_tex_assets);
}

static void
_destroy_anim_assets()
{
    anim_asset_container_item_t *item;
    for (item = _anim_assets.pool.res; item; item = item->next)
        _anim_asset_unload(&item->asset);
    anim_asset_container_destroy(&_anim_assets);
}

static void
_destroy_ae_assets()
{
    ae_asset_container_item_t *item;
    for (item = _ae_assets.pool.res; item; item = item->next)
        IMPLEMENTME();
    ae_asset_container_destroy(&_ae_assets);
}

static void
_destroy_ae_sets()
{
    IMPLEMENTME();
    obj_pool_destroy(&_ae_sets.pool);
    str_ae_set_table_destroy(&_ae_sets.table);
}

static void
_destroy_fonts()
{
    IMPLEMENTME();
}

static void
_destroy_spritesheets()
{
    spritesheet_container_item_t *item;
    for (item = _spritesheets.pool.res; item; item = item->next)
        _spritesheet_unload(&item->asset);
    spritesheet_container_destroy(&_spritesheets);
}


static int
_tex_asset_claim(tex_asset_t *ta, bool32 async)
{
    if (ta->user_count++ > 0 || ta->keep_loaded || ta->loading)
        return 0;
    if (ta->loaded)
        return 0;
    int ret = 0;
    if (async)
    {
        if (_launch_tex_asset_async_load(ta))
            {ret = 1; goto fail;}
    } else
    if (_tex_asset_load_bitmap_and_generate_texture(ta))
        {ret = 2; goto fail;}
    return ret;
    fail:
        ta->user_count--;
        return ret;
}

tex_asset_t *
as_claim_tex_by_name(const char *name, bool32 async)
{
    tex_asset_container_item_t **item = tex_asset_container_name_table_get_ptr(
        &_tex_assets.name_table, name);
    if (!item) return &_matlock.asset;
    return _tex_asset_claim(&(*item)->asset, async) ? 0 : &(*item)->asset;
}

tex_asset_t *
as_claim_tex_by_id(uint32 id, bool32 async)
{
    tex_asset_container_item_t **item = tex_asset_container_id_table_get_ptr(
        &_tex_assets.id_table, id);
    if (!item) return &_matlock.asset;
    return _tex_asset_claim(&(*item)->asset, async) ? 0 : &(*item)->asset;
}

const dchar *
tex_asset_get_path(tex_asset_t *ta)
{
    if (!ta) return 0;
    return *(dchar**)mdb_get_field(&_tex_assets.mdb, TEX_ASSET_ITEM(ta)->row,
        _tex_assets.path_mdb_index);
}

const dchar *
anim_asset_get_path(anim_asset_t *aa)
{
    if (!aa) return 0;
    return *(dchar**)mdb_get_field(&_anim_assets.mdb, ANIM__ASSET_ITEM(aa)->row,
        _anim_assets.path_mdb_index);
}

const dchar *
ae_asset_get_path(ae_asset_t *aa)
{
    if (!aa) return 0;
    return *(dchar**)mdb_get_field(&_ae_assets.mdb, AE_ASSET_ITEM(aa)->row,
        _ae_assets.path_mdb_index);
}

const dchar *
spritesheet_get_path(spritesheet_t *ss)
{
    if (!ss) return 0;
    return *(dchar**)mdb_get_field(&_spritesheets.mdb,
        SPRITESHEET_ITEM(ss)->row, _spritesheets.path_mdb_index);
}

const dchar *
sound_asset_get_path(sound_asset_t *sa)
{
    if (!sa) return 0;
    return *(dchar**)mdb_get_field(&_sound_assets.mdb,
        SOUND_ASSET_ITEM(sa)->row, _sound_assets.path_mdb_index);
}

void
as_unclaim_tex_asset(tex_asset_t *ta)
{
    if (!ta)
        return;
    muta_assert(ta->user_count > 0);
    ta->user_count--;
    if (ta->user_count > 0 || ta->keep_loaded || ta->in_unclaimed_queue)
        return;
    ta->in_unclaimed_queue = 1;
    unclaimed_asset_t ua;
    ua.asset    = ta;
    ua.time     = SDL_GetTicks();
    ua.type     = ASSET_TEX;
    darr_push(_unclaimed_assets, ua);
}

static spritesheet_t *
_spritesheet_claim(spritesheet_t *ss, bool32 async)
{
    if (!ss->user_count && !ss->keep_loaded && _spritesheet_load(ss, async))
        return 0;
    ss->user_count++;
    return ss;
}

spritesheet_t *
as_claim_spritesheet_by_name(const char *name, bool32 async)
{
    spritesheet_container_item_t **item =
        spritesheet_container_name_table_get_ptr(&_spritesheets.name_table,
            name);
    return item ? _spritesheet_claim(&(*item)->asset, async) : 0;
}

spritesheet_t *
as_claim_spritesheet_by_id(uint32 id, bool32 async)
{
    spritesheet_container_item_t **item =
        spritesheet_container_id_table_get_ptr(&_spritesheets.id_table,
            id);
    return item ? _spritesheet_claim(&(*item)->asset, async) : 0;
}

void
as_unclaim_spritesheet(spritesheet_t *ss)
{
    if (!ss)
        return;
    assert(ss->user_count > 0);
    if (!--ss->user_count && !ss->keep_loaded)
        _spritesheet_unload(ss);
}

static anim_asset_t *
_anim_asset_claim(anim_asset_t *aa, bool32 async)
{
    if (!aa->anim.num_frames && _anim_asset_load(aa, async))
        return 0;
    aa->user_count++;
    return aa;
}

anim_asset_t *
as_claim_anim_asset_by_name(const char *name, bool32 async)
{
    anim_asset_container_item_t **item =
        anim_asset_container_name_table_get_ptr(&_anim_assets.name_table, name);
    if (!item) return 0;
    return _anim_asset_claim(&(*item)->asset, async);
}

anim_asset_t *
as_claim_anim_asset_by_id(uint32 id, bool32 async)
{
    anim_asset_container_item_t **item =
        anim_asset_container_id_table_get_ptr(&_anim_assets.id_table, id);
    if (!item) return 0;
    return _anim_asset_claim(&(*item)->asset, async);
}

static ae_asset_t *
_ae_asset_claim_by_name(const char *name, bool32 async)
{
    if (!name)
        return 0;
    ae_asset_container_item_t **item =
        ae_asset_container_name_table_get_ptr(&_ae_assets.name_table, name);
    if (!item) return 0;
    ae_asset_t *aa = &(*item)->asset;
    if (!aa->user_count && _ae_asset_load(aa, async))
        return 0;
    aa->user_count++;
    return aa;
}

static ae_asset_t *
_ae_asset_claim_by_id(uint32 id, bool32 async)
{
    ae_asset_container_item_t **item =
        ae_asset_container_id_table_get_ptr(&_ae_assets.id_table, id);
    if (!item) return 0;
    ae_asset_t *aa = &(*item)->asset;
    if (!aa->user_count && _ae_asset_load(aa, async))
        return 0;
    aa->user_count++;
   return aa;
}

ae_asset_t *
as_claim_ae_by_name(const char *name, bool32 async)
    {return _ae_asset_claim_by_name(name, async);}

ae_asset_t *
as_claim_ae_by_id(uint32 id, bool32 async)
    {return _ae_asset_claim_by_id(id, async);}

void
as_unclaim_ae(ae_asset_t *ae)
{
    if (!ae)
        return;
    muta_assert(ae->user_count > 0);
    IMPLEMENTME();
    ae->user_count--;
}

ae_set_t *
as_get_ae_set(const char *name)
{
    ae_set_t **ret = str_ae_set_table_get_ptr(&_ae_sets.table, name);
    return ret ? *ret : 0;
}

sfont_t *
as_get_font(const char *name)
{
    sfont_t **ret = str_sfont_table_get_ptr(&_fonts.table, name);
    return ret ? *ret : 0;
}

sfont_t *
as_get_default_font()
    {return _fonts.default_font;}

sfont_t *
as_get_default_pixel_font()
    {return _fonts.default_pixel_font;}

void
as_unclaim_anim_asset(anim_asset_t *a)
{
    if (!a)
        return;
    muta_assert(a->user_count > 0);
    if (--a->user_count)
        return;
    _anim_asset_unload(a);
}

static int
_claim_sound(sound_asset_t *sa, bool32 async)
{
    if (sa->user_count++ > 0)
        return 0;
    if (sa->loaded)
        return 0;
    if (!sa->loading && _sound_asset_load(sa, async))
    {
        sa->user_count--;
        return 1;
    }
    return 0;
}

sound_asset_t *
as_claim_sound_by_name(const char *name, bool32 async)
{
    sound_asset_container_item_t **item =
        sound_asset_container_name_table_get_ptr(
        &_sound_assets.name_table, name);
    if (!item) return 0;
    if (_claim_sound(&(*item)->asset, async))
        return 0;
    return &(*item)->asset;
}

sound_asset_t *
as_claim_sound_by_id(uint32 id, bool32 async)
{
    sound_asset_container_item_t **item =
        sound_asset_container_id_table_get_ptr(
        &_sound_assets.id_table, id);
    if (!item) return 0;
    if (_claim_sound(&(*item)->asset, async))
        return 0;
    return &(*item)->asset;
}

void
as_unclaim_sound(sound_asset_t *sa)
{
    if (!sa) return;
    muta_assert(sa->user_count > 0);
    if (--sa->user_count > 0 || sa->keep_loaded || sa->in_unclaimed_queue)
        return;
    sa->in_unclaimed_queue   = 1;
    unclaimed_asset_t ua;
    ua.asset    = sa;
    ua.time     = SDL_GetTicks();
    ua.type     = ASSET_SOUND;
    darr_push(_unclaimed_assets, ua);
}

cursor_t *
as_get_cursor(const char *name)
    {return str_cursor_table_get_ptr(&_cursors, name);}

float *
spritesheet_get_clip_by_name(spritesheet_t *ss, const char *name)
{
    int num         = ss->num_clips;
    char **names    = ss->clip_names;
    for (int i = 0; i < num; ++i)
        if (streq(names[i], name))
            return ss->clips[i];
    return 0;
}

static int
_sfont_from_file_quick_and_dirty(sfont_t *sf, const char *path, int font_size)
{
    ttf_t ttf;
    if (ttf_load(&ttf, path, font_size, 2, 2) != 0)
        return 1;
    int ret = 0;
    if (sfont_from_ttf(sf, &ttf) != 0)
        ret = 2;
    ttf_free(&ttf);
    return ret;
}

static int
_tex_asset_load_bitmap(tex_asset_t *ta)
{
    if (ta->bitmap.pixels) return 0;
    return img_load(&ta->bitmap, tex_asset_get_path(ta)) ? 1 : 0;
}

static int
_tex_asset_generate_gl_texture(tex_asset_t *ta)
{
    if (tex_from_img_ext(&ta->tex, &ta->bitmap,
        ta->min_filter, ta->mag_filter, ta->wrapping))
        return 2;
    if (!ta->keep_bitmap)
        img_free(&ta->bitmap);
    return 0;
}

static int
_tex_asset_load_bitmap_and_generate_texture(tex_asset_t *ta)
{
    if (_tex_asset_load_bitmap(ta))
        return 1;
    if (_tex_asset_generate_gl_texture(ta))
        return 2;
    ta->loaded = 1;
    return 0;
}

static void
_spritesheet_unload(spritesheet_t *ss)
{
    as_unclaim_tex_asset(ss->ta);
    free(ss->clips);
    if (ss->clip_names)
        for (int i = 0; i < ss->num_clips; ++i)
            dstr_free(&ss->clip_names[i]);
    free(ss->clip_names);
    ss->clips       = 0;
    ss->ta          = 0;
    ss->clip_names  = 0;
    ss->num_clips   = 0;

}

static void
_anim_asset_unload(anim_asset_t *a)
{
    tex_asset_t *ta;
    for (int i = 0; i < a->anim.num_frames; ++i)
    {
        if (!a->anim.frames[i].tex)
            continue;
        char *tmp   = (char*)a->anim.frames[i].tex - offsetof(tex_asset_t, tex);
        ta          = (tex_asset_t*)tmp;
        as_unclaim_tex_asset(ta);
    }
    free(a->anim.frames);
    a->anim.frames      = 0;
    a->anim.num_frames  = 0;
    a->anim.total_dur   = 0.f;
}

#if 0 /* Kept for legacy reasons */
static int
_load_iso_anim(iso_anim_t *anim, const char *path)
{
    FILE *fp = fopen(path, "r");
    if(!fp) return 1;

    char line[1024];

    int num_frames = 0;

    fgets(line, 1024, fp);
    fgets(line, 1024, fp);
    fgets(line, 1024, fp);
    sscanf(line, "%d", &num_frames);
    rewind(fp);

    if (num_frames <= 0)
    {
        LOGF("Animation '%s' does not have any frames!\n", path);
        safe_fclose(fp);
        return 2;
    }

    char name[128] = { 0 };
    fgets(name, 128, fp);

    for (int i = 0; i < 128; ++i)
    {
        if (name[i] == '\n')
            {name[i] = '\0'; break;}
    }

	iso_anim_frame_t *frames_for_anim = calloc(num_frames,
        sizeof(iso_anim_frame_t));
    anim->frames        = frames_for_anim;
    anim->num_frames    = num_frames;
    anim->base_dur      = 0.1f;

    for (int clips = 0; clips < 8; ++clips)
    {
        int dir = -1;

        fgets(line, 1024, fp);
        sscanf(line, "%d", &dir);

        if (dir <= -1 || dir >= 8)
            {safe_fclose(fp); return 3;}

        fgets(line, 1024, fp);

        for (int i = 0; i < num_frames; ++i)
        {
            char texture_name[128] = { 0 };
            fgets(texture_name, 128, fp);

            for (int j = 0; j < 128; ++j)
            {
                if (texture_name[j] == '\n')
                {
                    texture_name[j] = '\0'; 
                    break;
                }
            }

            int x, y, w, h, offx, offy;

            fgets(line, 1024, fp);
            sscanf(line, "%d %d %d %d %d %d", &x, &y, &w, &h, &offx, &offy);

            float dur;

            fgets(line, 1024, fp);
            sscanf(line, "%f", &dur);

			tex_asset_t *frame_asset = as_claim_tex_async_by_name(
                texture_name);
            if (frame_asset == matlock)
            {
                x = y = offx = offy = 0;
                w = 128;
                h = 144;
            }

            anim->frames[i].dur = dur;
            anim->frames[i].tex_areas[dir].tex      = &frame_asset->tex;
            anim->frames[i].tex_areas[dir].clip[0]  = (float)x;
            anim->frames[i].tex_areas[dir].clip[1]  = (float)y;
            anim->frames[i].tex_areas[dir].clip[2]  = (float)w;
            anim->frames[i].tex_areas[dir].clip[3]  = (float)h;
            anim->frames[i].tex_areas[dir].ox       = (float)offx;
            anim->frames[i].tex_areas[dir].oy       = (float)offy;
        }
    }

    safe_fclose(fp);
    return 0;
}
#endif

