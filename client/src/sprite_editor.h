#ifndef MUTA_SPRITE_EDITOR_H
#define MUTA_SPRITE_EDITOR_H

#include"../../shared/types.h"

void
sprite_editor_update(double dt);

void
sprite_editor_open();

void
sprite_editor_close();

void
sprite_editor_keydown(int key, bool32 is_repeat);

void
sprite_editor_mousewheel(int x, int y);

void
sprite_editor_mousebuttonup(uint8 button, int x, int y);

void
sprite_editor_mousebuttondown(uint8 button, int x, int y);

void
sprite_editor_text_input(const char *text);

void
sprite_editor_file_drop(const char *path);

#endif /* MUTA_SPRITE_EDITOR_H */