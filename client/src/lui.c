#include "lui.h"
#include "gui.h"
#include "assets.h"
#include "core.h"
#include "render.h"
#include "hotkey.h"
#include "utils.inl"
#include "gamestate.h"
#include "../../shared/common_utils.h"
#include "../../shared/containers.h"
#include "../../shared/lua/lauxlib.h"
#include "../../shared/lua/lualib.h"
#include "../../shared/lua/lapi.h"

#define LUI_ERROR_LOG(fmt, ...) ((void)0)

typedef struct lui_window_obj_t         lui_window_obj_t;
typedef struct lui_window_obj_data_t    lui_window_obj_data_t;
typedef struct lui_window_t             lui_window_t;
typedef struct lui_texture_t            lui_texture_t;
typedef struct lui_text_input_t         lui_text_input_t;
typedef struct lui_button_t             lui_button_t;
typedef struct lui_text_t               lui_text_t;
typedef struct addon_parse_ctx_t        addon_parse_ctx_t;
typedef struct addon_t                  addon_t;
typedef struct lui_listener_list_t      lui_listener_list_t;

enum lui_window_obj_types
{
    LUI_WINDOW_OBJ_NONE,
    LUI_WINDOW_OBJ_TEXT,
    LUI_WINDOW_OBJ_BUTTON,
    LUI_WINDOW_OBJ_TEXTURE,
    LUI_WINDOW_OBJ_TEXT_INPUT
};

struct lui_window_t
{
    dchar                   *title;
    int                     origin;
    int                     x, y, w, h;
    uint32                  index;          /* Index in _wins.all */
    uint32                  style_index;    /* Index in _window_styles */
    lui_window_obj_data_t   *objs;
    uint32                  parent; /* 0xFFFFFFFF if none */
    uint32_darr_t           *children;
    uint8                   listening_to[LUI_NUM_EVENTS];
    struct
    {
        uint32 shown;
    } flags;
};

struct lui_button_t
{
    dchar   *name;
    int     w, h;
    uint32  style_index;
};

struct lui_text_t
{
    dchar   *string;
    int     wrap;
    float   scale;
    sfont_t *font;
    int     w, h;
};

struct lui_texture_t
{
    tex_t   *tex;
    float   scale_x;
    float   scale_y;
    float   clip[4];
};

struct lui_text_input_t
{
    dchar   *title;
    char    *buf;
    uint32  buf_size;
    int     w, h;
};

struct lui_window_obj_t
{
    int     type;
    uint32  id;
    uint32  window;
    uint32  index_in_window;
};
/* Window objects are accessed by their id which is why this structure is stored
 * inside a hashtable. */

union lui_window_obj_type_data_t
{
    lui_text_t          text;
    lui_button_t        button;
    lui_texture_t       texture;
    lui_text_input_t    text_input;
};

struct lui_window_obj_data_t
{
    int                                 type;
    lui_window_obj_t                    *obj;
    int                                 origin;
    int                                 x, y;
    union lui_window_obj_type_data_t    data;
};
/* This is the actual window object structure stored in a lui_window_t */

DYNAMIC_HASH_TABLE_DEFINITION(lui_window_obj_table, lui_window_obj_t, uint32,
    uint32, HASH_FROM_NUM, 1);
DYNAMIC_HASH_TABLE_DEFINITION(str_tex_table, tex_t*, const char*, uint32,
    fnv_hash32_from_str, 2);

struct addon_parse_ctx_t
{
    int         error;
    addon_t     *addon;
    const char  *dir_path;
    dchar       *lua_file_path;
};

struct addon_t
{
    dchar *name;
    dchar *lua_file_path;
};

struct lui_listener_list_t
{
    uint32_darr_t *windows;
};

static struct
{
    obj_pool_t      pool;
    lui_window_t    **all;
    lui_window_t    **shown;
} _wins;

static bool32                   _in_use;
static const char               *_window_table_key  = "__MUTA_WINDOW_TABLE";
static const char               *_button_table_key  = "__MUTA_BUTTON_TABLE";
static const char               *_hotkey_table_key  = "__MUTA_HOTKEY_TABLE";
static const char *_key_callback_key = "__MUTA_KEY_CALLBACK_TABLE";
static const char *_text_input_key = "__MUTA_TEXT_INPUT_TABLE";
static const char               *_button_id_str     = "_MUTA_BUTTON";
static const char               *_text_input_submit_key = "submit";
static lua_State                *_lua;
static addon_t                  *_addons;
static lui_event_t              *_events;
static lui_listener_list_t      _listener_lists[LUI_NUM_EVENTS];
static gui_win_style_t          *_window_styles;
static gui_button_style_t       *_button_styles;
static uint32                   _running_key_callback_id;
static int                      _active_window_index;   /* -1 if none */
static int                      _hovered_window_index;  /* -1 if none */
static int                      _keyboard_grab_window_index; /* -1 if none. */

static struct
{
    lui_window_obj_table_t  table;
    uint32                  running_id;
} _window_objs;

static struct
{
    obj_pool_t          pool;
    str_tex_table_t     table;
    tex_t               **all;
} _textures;

#define _get_window_obj(id_) \
    lui_window_obj_table_get_ptr(&_window_objs.table, (id_))

static inline lui_window_obj_data_t *
_get_window_obj_data(uint32 id)
{
    lui_window_obj_t *o = _get_window_obj(id);
    if (!o)
        return 0;
    return &_wins.all[o->window]->objs[o->index_in_window];
}

static int
_luiapi_get_window(lua_State *lua);

static int
_luiapi_create_window(lua_State *lua);

static int
_luiapi_show_window(lua_State *lua);

static int
_luiapi_hide_window(lua_State *lua);

static int
_luiapi_is_window_shown(lua_State *lua);

static int
_luiapi_set_window_position(lua_State *lua);

static int
_luiapi_set_window_width(lua_State *lua);

static int
_luiapi_set_window_height(lua_State *lua);

static int
_luiapi_set_window_origin(lua_State *lua);

static int
_luiapi_set_window_parent(lua_State *lua);

static int
_luiapi_set_window_title(lua_State *lua);

static int
_luiapi_is_window_active(lua_State *lua);

static int
_luiapi_is_window_hovered(lua_State *lua);

static int
_luiapi_create_button(lua_State *lua);

static int
_luiapi_create_text(lua_State *lua);

static int
_luiapi_create_texture(lua_State *lua);

static int
_luiapi_create_text_input(lua_State *lua);

static int
_luiapi_listen_to_event(lua_State *lua);

static int
_luiapi_unlisten_to_event(lua_State *lua);

static int
_luiapi_set_event_callback(lua_State *lua);

static int
_luiapi_get_window_position(lua_State *lua);

static int
_luiapi_get_window_width(lua_State *lua);

static int
_luiapi_get_window_height(lua_State *lua);

static int
_luiapi_set_window_obj_origin(lua_State *lua);

static int
_luiapi_set_window_obj_position(lua_State *lua);

static int
_luiapi_set_text_string(lua_State *lua);

static int
_luiapi_get_text_string(lua_State *lua);

static int
_luiapi_get_text_width(lua_State *lua);

static int
_luiapi_get_text_height(lua_State *lua);

static int
_luiapi_set_text_font(lua_State *lua);

static int
_luiapi_set_text_scale(lua_State *lua);

static int
_luiapi_set_button_width(lua_State *lua);

static int
_luiapi_set_button_height(lua_State *lua);

static int
_luiapi_set_button_callback(lua_State *lua);

static int
_luiapi_set_button_title(lua_State *lua);

static int
_luiapi_set_texture_clip(lua_State *lua);

static int
_luiapi_set_texture_scale(lua_State *lua);

static int
_luiapi_set_texture_vertical_scale(lua_State *lua);

static int
_luiapi_set_texture_horizontal_scale(lua_State *lua);

static int
_luiapi_set_texture_file(lua_State *lua);

static int
_luiapi_set_text_input_width(lua_State *lua);

static int
_luiapi_set_text_input_height(lua_State *lua);

static int
_luiapi_get_text_input_content(lua_State *lua);

static int
_luiapi_set_text_input_submit_callback(lua_State *lua);

static int
_luiapi_clear_text_input(lua_State *lua);

static int
_luiapi_set_active_text_input(lua_State *lua);

static int
_luiapi_is_text_input_active(lua_State *lua);

static int
_luiapi_create_window_style(lua_State *lua);

static int
_luiapi_set_window_style_background_color(lua_State *lua);

static int
_luiapi_set_window_style_title_color(lua_State *lua);

static int
_luiapi_get_fps(lua_State *lua);

static int
_luiapi_create_key_bind(lua_State *lua);

static int
_luiapi_bind_key(lua_State *lua);

static int
_luiapi_log_out(lua_State *lua);

static int
_luiapi_exit_game(lua_State *lua);

static int
_luiapi_get_key_bindings(lua_State *lua);

static int
_luiapi_is_shift_down(lua_State *lua);

static int
_luiapi_is_alt_down(lua_State *lua);

static int
_luiapi_is_ctrl_down(lua_State *lua);

static int
_luiapi_grab_keyboard_for_window(lua_State *lua);

static int
_luiapi_ungrab_keyboard_for_window(lua_State *lua);

static int
_luiapi_send_chat_message(lua_State *lua);

static void
_parse_addon_file_callback(void *ctx, const char *opt, const char *val);

static int
_load_addons();

static void
_load_addon_directory(const char *dp);

static addon_t *
_get_addon(const char *name);

static lui_window_t *
_get_window(const char *name);

static lui_window_t *
_create_window(const char *name);

static lui_window_obj_data_t *
_create_window_obj(lui_window_t *win);
/* Create a window object (text, button, etc.) as the given window's child */

static inline lui_window_t *
_get_window_arg(int stack_index);

static inline lui_text_t *
_get_text(uint32 id);

static inline lui_button_t *
_get_button(uint32 id);

static inline lui_texture_t *
_get_texture(uint32 id);

static inline lui_text_input_t *
_get_text_input(uint32 id);

static inline gui_win_style_t *
_get_window_style(uint32 id);

static void
_propagate_event_to_window(lui_window_t *win, lui_event_t *event);

static int
_str_to_event_enum(const char *str);

static int
_str_to_gui_origin(const char *str);

static inline void
_post_event(lui_event_t *event);

static tex_t *
_load_texture_file(const char *path);
/* Call this only if you've checked the texture doesn't already exist */

static int
_lua_stack_trace(lua_State *lua);

static void
_create_lua_table_for_hotkey_action(hk_action_t *action);

static int
_is_window_hovered_or_active(lua_State *lua, int *index_val);

static void
_remove_window_from_shown(lui_window_t *win);

static inline void
_update_text_wh(lui_text_t *text);

static inline lui_window_t *
_get_window_of_text(lui_text_t *text);

static inline lui_window_t *
_get_window_of_texture(lui_texture_t *texture);

static inline lui_window_t *
_get_window_of_button(lui_button_t *button);

static inline lui_window_t *
_get_window_of_text_input(lui_text_input_t *ti);

int
lui_init()
{
    _active_window_index        = -1;
    _hovered_window_index       = -1;
    _keyboard_grab_window_index = -1;

    _lua = luaL_newstate();
    if (!_lua)
        return 1;

    /*-- Load Lua libraries --*/
    luaL_Reg libs[] =
    {
      {"_G", luaopen_base},
      {LUA_LOADLIBNAME, luaopen_package},
      {LUA_COLIBNAME, luaopen_coroutine},
      {LUA_TABLIBNAME, luaopen_table},
      {LUA_STRLIBNAME, luaopen_string},
      {LUA_MATHLIBNAME, luaopen_math},
      {LUA_UTF8LIBNAME, luaopen_utf8},
      {LUA_DBLIBNAME, luaopen_debug},
      {NULL, NULL}
    };

    for (luaL_Reg *lib = libs; lib->func; ++lib)
    {
        luaL_requiref(_lua, lib->name, lib->func, 1);
        lua_pop(_lua, 1);
    }

    obj_pool_init(&_wins.pool, 64, sizeof(lui_window_t));
    darr_reserve(_wins.shown, 64);
    darr_reserve(_events, 256);
    lui_window_obj_table_einit(&_window_objs.table, 512);
    _window_objs.running_id = 0;

    /*-- Create the global window table --*/
    lua_pushstring(_lua, _window_table_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Create the global button table --*/
    lua_pushstring(_lua, _button_table_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Create the global key callback table --*/
    lua_pushstring(_lua, _key_callback_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);
    _running_key_callback_id = 0;

    /*-- Create the global text input callback table --*/
    lua_pushstring(_lua, _text_input_key);
    lua_newtable(_lua);
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /*-- Textures --*/
    obj_pool_init(&_textures.pool, 64, sizeof(tex_t));
    str_tex_table_init(&_textures.table, 64);
    darr_reserve(_textures.all, 64);

    /*-- Window styles --*/
    darr_reserve(_window_styles, 16);
    gui_win_style_t win_style = gui_create_win_style();
    gui_win_state_style_t *wss = &win_style.active;
    SET_BYTE_COLOR(wss->bg_col,  13, 13, 13, 255);
    SET_BYTE_COLOR(wss->border.color,  33, 33, 33, 255);
    SET_BYTE_COLOR(wss->title_col,  255, 255, 255, 255);
    wss->title_font = as_get_default_pixel_font();
    muta_assert(wss->title_font);
    wss->border.widths[GUI_EDGE_TOP]    = 22;
    wss->border.widths[GUI_EDGE_BOTTOM] = 2;
    wss->border.widths[GUI_EDGE_LEFT]   = 2;
    wss->border.widths[GUI_EDGE_RIGHT]  = 2;
    memcpy(&win_style.hovered, wss, sizeof(gui_win_state_style_t));
    memcpy(&win_style.inactive, wss, sizeof(gui_win_state_style_t));
    darr_push(_window_styles, win_style);

    /*-- Button styles --*/
    darr_reserve(_button_styles, 16);
    gui_button_style_t bs = gui_create_button_style();
    SET_BYTE_COLOR(bs.normal.bg_col,  23, 23, 23, 255);
    SET_BYTE_COLOR(bs.hovered.bg_col, 13, 43, 43, 255);
    SET_BYTE_COLOR(bs.pressed.bg_col, 13, 83, 13, 255);
    SET_BYTE_COLOR(bs.normal.border.color,  13, 13, 13, 255);
    SET_BYTE_COLOR(bs.hovered.border.color, 33, 33, 33, 255);
    SET_BYTE_COLOR(bs.pressed.border.color, 43, 43, 43, 255);
    bs.normal.border.widths[GUI_EDGE_TOP]  = 2;
    bs.normal.border.widths[GUI_EDGE_BOTTOM]  = 2;
    bs.normal.border.widths[GUI_EDGE_LEFT]  = 2;
    bs.normal.border.widths[GUI_EDGE_RIGHT]  = 2;
    bs.hovered.border.widths[GUI_EDGE_TOP] = 2;
    bs.hovered.border.widths[GUI_EDGE_BOTTOM] = 2;
    bs.hovered.border.widths[GUI_EDGE_LEFT] = 2;
    bs.hovered.border.widths[GUI_EDGE_RIGHT] = 2;
    bs.pressed.border.widths[GUI_EDGE_TOP] = 2;
    bs.pressed.border.widths[GUI_EDGE_BOTTOM] = 2;
    bs.pressed.border.widths[GUI_EDGE_LEFT] = 2;
    bs.pressed.border.widths[GUI_EDGE_RIGHT] = 2;
    darr_push(_button_styles, bs);

    /*-- Register API functions --*/
    lua_register(_lua, "create_window", _luiapi_create_window);
    lua_register(_lua, "get_window", _luiapi_get_window);
    lua_register(_lua, "show_window", _luiapi_show_window);
    lua_register(_lua, "hide_window", _luiapi_hide_window);
    lua_register(_lua, "is_window_shown", _luiapi_is_window_shown);
    lua_register(_lua, "set_window_position", _luiapi_set_window_position);
    lua_register(_lua, "set_window_width", _luiapi_set_window_width);
    lua_register(_lua, "set_window_height", _luiapi_set_window_height);
    lua_register(_lua, "create_button", _luiapi_create_button);
    lua_register(_lua, "listen_to_event", _luiapi_listen_to_event);
    lua_register(_lua, "unlisten_to_event", _luiapi_unlisten_to_event);
    lua_register(_lua, "set_event_callback", _luiapi_set_event_callback);
    lua_register(_lua, "get_window_position", _luiapi_get_window_position);
    lua_register(_lua, "get_window_width", _luiapi_get_window_width);
    lua_register(_lua, "get_window_height", _luiapi_get_window_height);
    lua_register(_lua, "set_window_origin", _luiapi_set_window_origin);
    lua_register(_lua, "set_window_parent", _luiapi_set_window_parent);
    lua_register(_lua, "set_window_title", _luiapi_set_window_title);
    lua_register(_lua, "is_window_active", _luiapi_is_window_active);
    lua_register(_lua, "is_window_hovered", _luiapi_is_window_hovered);
    lua_register(_lua, "create_text", _luiapi_create_text);
    lua_register(_lua, "set_text_string", _luiapi_set_text_string);
    lua_register(_lua, "get_text_string", _luiapi_get_text_string);
    lua_register(_lua, "get_text_width", _luiapi_get_text_width);
    lua_register(_lua, "get_text_height", _luiapi_get_text_height);
    lua_register(_lua, "set_text_position", _luiapi_set_window_obj_position);
    lua_register(_lua, "set_text_origin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "set_text_font", _luiapi_set_text_font);
    lua_register(_lua, "set_text_scale", _luiapi_set_text_scale);
    lua_register(_lua, "set_button_position", _luiapi_set_window_obj_position);
    lua_register(_lua, "set_button_origin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "set_button_width", _luiapi_set_button_width);
    lua_register(_lua, "set_button_height", _luiapi_set_button_height);
    lua_register(_lua, "set_button_callback", _luiapi_set_button_callback);
    lua_register(_lua, "set_button_title", _luiapi_set_button_title);
    lua_register(_lua, "create_texture", _luiapi_create_texture);
    lua_register(_lua, "set_texture_position", _luiapi_set_window_obj_position);
    lua_register(_lua, "set_texture_clip", _luiapi_set_texture_clip);
    lua_register(_lua, "set_texture_scale", _luiapi_set_texture_scale);
    lua_register(_lua, "set_texture_vertical_scale",
        _luiapi_set_texture_vertical_scale);
    lua_register(_lua, "set_texture_horizontal_scale",
        _luiapi_set_texture_horizontal_scale);
    lua_register(_lua, "set_texture_origin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "set_texture_file", _luiapi_set_texture_file);
    lua_register(_lua, "create_text_input", _luiapi_create_text_input);
    lua_register(_lua, "set_text_input_width", _luiapi_set_text_input_width);
    lua_register(_lua, "set_text_input_height", _luiapi_set_text_input_height);
    lua_register(_lua, "get_text_input_content",
        _luiapi_get_text_input_content);
    lua_register(_lua, "set_text_input_origin", _luiapi_set_window_obj_origin);
    lua_register(_lua, "set_text_input_submit_callback",
        _luiapi_set_text_input_submit_callback);
    lua_register(_lua, "clear_text_input", _luiapi_clear_text_input);
    lua_register(_lua, "set_active_text_input", _luiapi_set_active_text_input);
    lua_register(_lua, "is_text_input_active", _luiapi_is_text_input_active);
    lua_register(_lua, "create_window_style", _luiapi_create_window_style);
    lua_register(_lua, "set_window_style_background_color",
        _luiapi_set_window_style_background_color);
    lua_register(_lua, "set_window_style_title_color",
        _luiapi_set_window_style_title_color);
    lua_register(_lua, "get_fps", _luiapi_get_fps);
    lua_register(_lua, "create_key_bind", _luiapi_create_key_bind);
    lua_register(_lua, "bind_key", _luiapi_bind_key);
    lua_register(_lua, "log_out", _luiapi_log_out);
    lua_register(_lua, "exit_game", _luiapi_exit_game);
    lua_register(_lua, "get_key_bindings", _luiapi_get_key_bindings);
    lua_register(_lua, "is_shift_down", _luiapi_is_shift_down);
    lua_register(_lua, "is_alt_down", _luiapi_is_alt_down);
    lua_register(_lua, "is_ctrl_down", _luiapi_is_ctrl_down);
    lua_register(_lua, "grab_keyboard_for_window",
        _luiapi_grab_keyboard_for_window);
    lua_register(_lua, "ungrab_keyboard_for_window",
        _luiapi_ungrab_keyboard_for_window);
    lua_register(_lua, "send_chat_message", _luiapi_send_chat_message);

    /* Create the key bindings table */
    uint32      num_hka;
    hk_action_t **hka = hk_get_actions(&num_hka);
    lua_pushstring(_lua, _hotkey_table_key);
    lua_newtable(_lua);
    for (uint32 i = 0; i < num_hka; ++i)
    {
        lua_pushstring(_lua, hka[i]->name);
        _create_lua_table_for_hotkey_action(hka[i]);
        lua_settable(_lua, -3);
    }
    lua_settable(_lua, LUA_REGISTRYINDEX);

    /* Load the default UI before any addons */
    _load_addon_directory("ui/default");
    _load_addons();
    _in_use                     = 1;
    return 0;
}

void
lui_destroy()
{
    if (_lua)
        lua_close(_lua);
    _lua = 0;
    for (uint32 i = 0; i < darr_num(_wins.all); ++i)
    {
        lui_window_t *win = _wins.all[i];
        for (uint32 j = 0; j < darr_num(win->objs); ++j)
        {
            switch (win->objs[j].type)
            {
            case LUI_WINDOW_OBJ_TEXT:
                dstr_free(&win->objs[j].data.text.string);
                break;
            case LUI_WINDOW_OBJ_BUTTON:
                dstr_free(&win->objs[j].data.button.name);
                break;
            case LUI_WINDOW_OBJ_TEXT_INPUT:
                free(win->objs[j].data.text_input.buf);
                dstr_free(&win->objs[j].data.text_input.title);
                break;
            }
        }
        darr_free(win->objs);
        darr_free(win->children);
        dstr_free(&win->title);
    }
    darr_free(_wins.shown);
    darr_free(_wins.all);
    obj_pool_destroy(&_wins.pool);
    for (uint32 i = 0; i < darr_num(_addons); ++i)
    {
        dstr_free(&_addons[i].name);
        dstr_free(&_addons[i].lua_file_path);
    }
    darr_free(_addons);
    darr_free(_events);
    for (int i = 0; i < LUI_NUM_EVENTS; ++i)
        darr_free(_listener_lists[i].windows);
    memset(_listener_lists, 0, sizeof(_listener_lists));

    /*-- Destroy window objects --*/
    lui_window_obj_table_destroy(&_window_objs.table);

    /*-- Destroy textures --*/
    for (uint32 i = 0; i < darr_num(_textures.all); ++i)
        tex_free(_textures.all[i]);
    darr_free(_textures.all);
    str_tex_table_destroy(&_textures.table);
    obj_pool_destroy(&_textures.pool);

    _in_use = 0;
}

void
lui_reload()
{
    lui_destroy();
    lui_init();
}

static void
_update_window(lui_window_t *win)
{
    if (!win->flags.shown)
        return;
    gui_origin(win->origin);
    gui_win_style(&_window_styles[win->style_index]);
    gui_begin_win(win->title, win->x, win->y, win->w, win->h, 0);
    if (gui_is_window_active())
        _active_window_index = win->index;
    if (gui_is_window_hovered())
        _active_window_index = win->index;
    for (uint32 j = 0; j < darr_num(win->objs); ++j)
    {
        lui_window_obj_data_t *d = &win->objs[j];
        switch (d->type)
        {
        case LUI_WINDOW_OBJ_TEXT:
        {
            lui_text_t *t = &d->data.text;
            gui_font(t->font);
            gui_origin(d->origin);
            gui_text_s(t->string, t->wrap, d->x, d->y, t->scale);
        }
            break;
        case LUI_WINDOW_OBJ_BUTTON:
        {
            lui_button_t *b = &d->data.button;
            gui_button_style(&_button_styles[b->style_index]);
            gui_origin(d->origin);
            if (!gui_button(b->name, d->x, d->y, b->w, b->h, 0))
                break;
            lua_pushstring(_lua, _button_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_assert(!lua_isnil(_lua, -1));
            lua_pushnumber(_lua, (lua_Number)win->objs[j].obj->id);
            lua_gettable(_lua, -2);
            lua_assert(!lua_isnil(_lua, -1));
            if (!lua_isnil(_lua, -1))
                lua_pcall(_lua, 0, 0, 0);
            else
                DEBUG_PRINTFF("button '%s' function was nil.\n", b->name);
            lua_pop(_lua, 2);
        }
            break;
        case LUI_WINDOW_OBJ_TEXTURE:
        {
            lui_texture_t *t = &d->data.texture;
            gui_origin(d->origin);
            gui_texture_s(t->tex, t->clip, d->x, d->y, t->scale_x,
                t->scale_y);
        }
            break;
        case LUI_WINDOW_OBJ_TEXT_INPUT:
        {

            lui_text_input_t *ti = &d->data.text_input;
            gui_origin(d->origin);
            gui_text_input2(ti->title, ti->buf, ti->buf_size, d->x, d->y,
                ti->w, ti->h);
            if (!gui_text_input_enter_pressed())
                break;
            lua_pushstring(_lua, _text_input_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushnumber(_lua, (lua_Number)d->obj->id);
            lua_gettable(_lua, -2);
            lua_pushstring(_lua, _text_input_submit_key);
            lua_gettable(_lua, -2);
            if (!lua_isnil(_lua, -1))
            {
                lua_pushnumber(_lua, (lua_Number)d->obj->id);
                lua_pcall(_lua, 1, 0, 0);
            }
            lua_pop(_lua, 2);
            gui_set_active_text_input(0);
            /* Flag so that the key up event will not trigger */
            gs_flag_special_key_executed(GS_SPECIAL_KEY_ENTER);
        }
            break;
        }
    }
    for (uint32 i = 0; i < darr_num(win->children); ++i)
        _update_window(_wins.all[win->children[i]]);
    gui_end_win();
}

void
lui_update_and_render(double dt)
{
    gui_begin();

    lui_window_t    **shown   = _wins.shown;
    uint32          num_shown = darr_num(shown);

    /*-- Propagate events --*/
    lui_event_t update_event;
    update_event.type           = LUI_EVENT_UPDATE;
    update_event.update.delta   = dt;
    _post_event(&update_event);

    lui_event_t *evs        = _events;
    uint32      num_events  = darr_num(evs);

    for (uint32 i = 0; i < num_events; ++i)
    {
        switch (evs[i].type)
        {
        case LUI_EVENT_KEY_CALLBACK:
        {
            lua_pushstring(_lua, _key_callback_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushnumber(_lua,
                (lua_Number)evs[i].key_callback.callback_id);
            lua_gettable(_lua, -2);
            if (lua_isfunction(_lua, -1))
                lua_pcall(_lua, 0, 0, 0);
            lua_pop(_lua, 1);
            continue; /* Skip */
        }
            break;
        case LUI_EVENT_KEY_BIND_CHANGED:
        {
            /* Register the key with the global hotkey table in the Lua
             * instance. Propagate the event later to listening windows. */
            lui_key_bind_changed_event_t *ed = &evs[i].key_bind_changed;
            lua_pushstring(_lua, _hotkey_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushstring(_lua, ed->action->name);
            _create_lua_table_for_hotkey_action(ed->action);
            lua_settable(_lua, -3);
        }
            break;
        case LUI_EVENT_NEW_HOTKEY_ACTION:
        {
            /* Register the key with the global hotkey table in the Lua
             * instance. Propagate the event later to listening windows. */
            lui_new_hotkey_action_event_t *ed = &evs[i].new_hotkey_action;
            lua_pushstring(_lua, _hotkey_table_key);
            lua_gettable(_lua, LUA_REGISTRYINDEX);
            lua_pushstring(_lua, ed->action->name);
            _create_lua_table_for_hotkey_action(ed->action);
            lua_settable(_lua, -3);
        }
            break;
        case LUI_EVENT_KEY_UP:
            if (streq(evs[i].key.key_combo_name, "Escape"))
                gui_set_active_text_input(0);
            break;
        }
        muta_assert(evs[i].type < LUI_NUM_EVENTS);
        uint32 *windows     = _listener_lists[evs[i].type].windows;
        uint32 num_windows  = darr_num(windows);
        for (uint32 j = 0; j < num_windows; ++j)
            _propagate_event_to_window(_wins.all[windows[j]], &evs[i]);
    }

    uint32 num_events_now = darr_num(_events);
    if (num_events_now == num_events)
        darr_clear(_events);
    else
    {
        uint32 num_remaining_events = (num_events_now - num_events);
        memmove(_events, _events + num_events,
             num_remaining_events * sizeof(*_events));
        _darr_head(_events)->num = num_remaining_events;
    }

    /*-- Actual gui.h api calls --*/
    for (uint32 i = 0; i < num_shown; ++i)
        _update_window(shown[i]);
    gui_end();
}

void
lui_post_event(lui_event_t *event)
{
    if (!_in_use)
        return;
    muta_assert(event->type < LUI_NUM_EVENTS);
    darr_push(_events, *event);
}

bool32
lui_is_grabbing_keyboard()
    {return _keyboard_grab_window_index != -1 || gui_have_active_text_input();}

static int
_luiapi_get_window(lua_State *lua)
{
    lui_window_t *win = _get_window(lua_tostring(lua, 1));
    if (win)
        lua_pushnumber(lua, (lua_Number)win->index);
    else
        lua_pushnil(lua);
    return 1;
}

static int
_luiapi_create_window(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        goto fail;
    const char *name = lua_tostring(lua, -1);
    lui_window_t *win = _create_window(name);
    if (!win)
        goto fail;
    lua_pushstring(lua, _window_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushvalue(lua, -2);
    lua_newtable(lua);
    lua_settable(lua, -3);
    lua_pushnumber(_lua, (lua_Number)win->index);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_show_window(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win || win->flags.shown)
        return 0;
    win->flags.shown = 1;
    darr_push(_wins.shown, win);
    return 0;
}

static int
_luiapi_hide_window(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win || !win->flags.shown)
        return 0;
    win->flags.shown = 0;
    uint32 num = darr_num(_wins.shown);
    for (uint32 i = 0; i < num; ++i)
    {
        if (_wins.shown[i] != win)
            continue;
        darr_erase(_wins.shown, i);
        return 0;
    }
    muta_assert(0);
    return 0;
}

static int
_luiapi_is_window_shown(lua_State *lua)
{
    int v = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto out;
    v = win->flags.shown ? 1 : 0;
    out:
        lua_pushboolean(lua, v);
        return 1;
}

static int
_luiapi_set_window_position(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -3))
        return 0;
    lui_window_t *win = _get_window_arg(-3);
    if (!win)
        return 0;
    win->x = (int)lua_tonumber(lua, -2);
    win->y = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_window_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    win->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_window_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    win->h = (int)lua_tonumber(lua, 2);
    return 0;
}

static int
_luiapi_set_window_origin(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    int origin = _str_to_gui_origin(lua_tostring(lua, -1));
    if (origin != NUM_GUI_ORIGINS)
        win->origin = origin;
    return 0;
}

static int
_luiapi_set_window_parent(lua_State *lua)
{
    if (!lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    if (lua_isnumber(lua, -1))
    {
        lui_window_t *p = _get_window_arg(-1);
        if (!p)
            return 0;
        /* Check of already a child of the parent */
        uint32 index        = win->index;
        uint32 *children    = p->children;
        uint32 num_children = darr_num(children);
        for (uint32 i = 0; i < num_children; ++i)
            if (p->children[i] == index)
                return 0;
        darr_push(children, index);
        p->children = children;
        /* Remove from exiting parent */
        uint32 old_parent_index = win->parent;;
        if (old_parent_index != 0xFFFFFFFF)
        {
            lui_window_t    *ep         = _wins.all[old_parent_index];
            uint32          child_index = 0xFFFFFFFF;
            children        = ep->children;
            num_children    = darr_num(children);
            for (uint32 i = 0; i < num_children; ++i)
            {
                if (children[i] != index)
                    continue;
                child_index = i;
                break;
            }
            muta_assert(child_index != 0xFFFFFFFF);
            darr_erase(children, child_index);
            ep->children = children;
        } else if (win->flags.shown) /* Remove from shown */
            _remove_window_from_shown(win);
        win->parent = p->index;
    } else
    if (lua_isnil(lua, -1) && win->parent != 0xFFFFFFFF) /* Rem from parent */
    {
        lui_window_t    *p              = _wins.all[win->parent];
        uint32          index           = win->index;
        uint32          *children       = p->children;
        uint32          num_children    = darr_num(children);
        for (uint32 i = 0; i < num_children; ++i)
        {
            if (children[i] != index)
                continue;
            darr_erase(children, index);
            p->children = children;
            p->parent   = 0xFFFFFFFF;
            return 0;
        }
        muta_assert(0);
    }
    return 0;
}

static int
_luiapi_set_window_title(lua_State *lua)
{
    if (!lua_isnumber(lua, -2))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    const char *title;
    if (lua_isstring(lua, -1))
        title = lua_tostring(lua, -1);
    else if (lua_isnil(lua, -1))
        title = 0;
    else
        return 0;
    dstr_set(&win->title, title);
    return 0;
}

static int
_luiapi_is_window_active(lua_State *lua)
    {return _is_window_hovered_or_active(lua, &_active_window_index);}

static int
_luiapi_is_window_hovered(lua_State *lua)
    {return _is_window_hovered_or_active(lua, &_hovered_window_index);}

static int
_luiapi_create_button(lua_State *lua)
{
    if (!lua_isnumber(lua, -2))
        return 0;
    if (!lua_isnil(lua, -1) && !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    const char *name = lua_tostring(lua, -1);
    if (!name)
        return 0;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type                     = LUI_WINDOW_OBJ_BUTTON;
    d->data.button.name         = 0;
    dstr_setf(&d->data.button.name, "%s##%s%u", name, _button_id_str,
        d->obj->id);
    d->data.button.style_index  = 0;
    d->obj->type                = LUI_WINDOW_OBJ_BUTTON;
    lua_pushstring(lua, _button_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    lua_pushnil(lua);
    lua_settable(lua, -3);
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    return 1;
}

static int
_luiapi_create_text(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        goto fail;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type             = LUI_WINDOW_OBJ_TEXT;
    d->data.text.string = dstr_create(lua_tostring(lua, -1));
    d->data.text.font   = as_get_default_font();
    d->data.text.scale  = 1.f;
    d->obj->type        = LUI_WINDOW_OBJ_TEXT;
    _update_text_wh(&d->data.text);
    lua_pushnumber(lua, d->obj->id);
    return 1;

    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_create_texture(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        goto fail;
    const char  *path       = lua_tostring(lua, -1);
    tex_t       *tex        = 0;
    tex_t       **existing  = str_tex_table_get_ptr(&_textures.table, path);
    if (existing)
        tex = *existing;
    else if (!(tex = _load_texture_file(path)))
        goto fail;
    lui_window_obj_data_t *d = _create_window_obj(win);
    d->type                 = LUI_WINDOW_OBJ_TEXTURE;
    d->data.texture.tex     = tex;
    d->data.texture.scale_x = 1.f;
    d->data.texture.scale_y = 1.f;
    d->data.texture.clip[0] = 0.f;
    d->data.texture.clip[1] = 0.f;
    d->data.texture.clip[2] = (float)tex->w;
    d->data.texture.clip[3] = (float)tex->h;
    d->obj->type            = LUI_WINDOW_OBJ_TEXTURE;
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_create_text_input(lua_State *lua)
{
    if (!lua_isnumber(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isnumber(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-3);
    if (!win)
        goto fail;
    lua_Number sizef = lua_tonumber(lua, -1);
    if (sizef < 1.f)
        return 0;
    uint32                  size    = (uint32)sizef + 1;
    lui_window_obj_data_t   *d      = _create_window_obj(win);
    dstr_setf(&d->data.text_input.title, "%s##%s%u", lua_tostring(lua, -2),
        _button_id_str, d->obj->id);
    d->type                         = LUI_WINDOW_OBJ_TEXT_INPUT;
    d->obj->type                    = LUI_WINDOW_OBJ_TEXT_INPUT;
    d->data.text_input.buf          = emalloc(size);
    d->data.text_input.buf_size     = size;
    d->data.text_input.buf[0]       = 0;
    lua_pushstring(lua, _text_input_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    lua_newtable(lua);
    lua_settable(lua, -3);
    lua_pushnumber(lua, (lua_Number)d->obj->id);
    return 1;
    fail:
        muta_assert(0);
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_position(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto fail;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        goto fail;
    lua_newtable(lua);
    lua_pushstring(lua, "x");
    lua_pushnumber(lua, (lua_Number)win->x);
    lua_settable(lua, -3);
    lua_pushstring(lua, "y");
    lua_pushnumber(lua, (lua_Number)win->y);
    lua_settable(lua, -3);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_window_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    lua_pushnumber(lua, (lua_Number)win->w);
    return 0;
}

static int
_luiapi_get_window_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    lua_pushnumber(lua, (lua_Number)win->h);
    return 0;
}

static int
_luiapi_set_window_obj_origin(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    int origin = _str_to_gui_origin(lua_tostring(lua, -1));
    if (origin == NUM_GUI_ORIGINS)
        return 0;
    lui_window_obj_t *o = _get_window_obj((uint32)lua_tonumber(lua, -2));
    if (o)
        _wins.all[o->window]->objs[o->index_in_window].origin = origin;
    return 0;
}

static int
_luiapi_set_window_obj_position(lua_State *lua)
{
    if (!lua_isnumber(lua, -1) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -3))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -3);
    lui_window_obj_data_t *d = _get_window_obj_data(id);
    if (!d)
        return 0;
    d->x = (int)lua_tonumber(lua, -2);
    d->y = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_text_string(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -2));
    if (!text)
        return 0;
    dstr_set(&text->string, lua_tostring(lua, -1));
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_get_text_string(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    if (!text->string)
        goto out;
    lua_pushstring(lua, text->string);
    return 1;
    out:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_get_text_width(lua_State *lua)
{
    int w = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    w = text->w;
    out:
        lua_pushnumber(lua, w);
        return 1;
}

static int
_luiapi_get_text_height(lua_State *lua)
{
    int h = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -1));
    if (!text)
        goto out;
    h = text->h;
    out:
        lua_pushnumber(lua, h);
        return 1;
}

static int
_luiapi_set_text_font(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    const char *name = lua_tostring(lua, -1);
    if (!name)
        return 0;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -2));
    if (!text)
        return 0;
    sfont_t *font = as_get_font(name);
    if (!font)
        font = as_get_default_font();
    text->font = font;
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_set_text_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_t *text = _get_text((uint32)lua_tonumber(lua, -2));
    if (!text)
        return 0;
    text->scale = (float)lua_tonumber(lua, -1);
    _update_text_wh(text);
    return 0;
}

static int
_luiapi_set_button_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_button_t *button = _get_button((uint32)lua_tonumber(lua, -2));
    if (!button)
        return 0;
    button->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_button_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_button_t *button = _get_button((uint32)lua_tonumber(lua, -2));
    if (!button)
        return 0;
    button->h = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_button_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isfunction(lua, -1))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_button_t *b = _get_button(id);
    if (!b)
        return 0;
    lua_pushstring(lua, _button_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));
    lua_pushnumber(lua, (lua_Number)id);
    muta_assert(lua_isfunction(lua, -3));
    lua_pushvalue(lua, -3);
    lua_settable(lua, -3);
    return 0;
}

static int
_luiapi_set_button_title(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_button_t *b = _get_button(id);
    if (!b)
        return 0;
    dstr_setf(&b->name, "%s##%s%u", lua_tostring(lua, -1), _button_id_str, id);
    DEBUG_PRINTFF("setting button title to %s...\n", b->name);
    return 0;
}

static int
_luiapi_set_texture_clip(lua_State *lua)
{
    if (!lua_isnumber(lua, -5) || !lua_isnumber(lua, -4) ||
        !lua_isnumber(lua, -3) || !lua_isnumber(lua, -2) ||
        !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -5));
    if (!t)
        return 0;
    t->clip[0] = (float)lua_tonumber(lua, -4);
    t->clip[1] = (float)lua_tonumber(lua, -3);
    t->clip[2] = (float)lua_tonumber(lua, -2);
    t->clip[3] = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_x = (float)lua_tonumber(lua, -1);
    t->scale_y = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_vertical_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_x = (float)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_texture_horizontal_scale(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    t->scale_y = (float)lua_tonumber(lua, -1);
    return 0;

}

static int
_luiapi_set_texture_file(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_texture_t *t = _get_texture((uint32)lua_tonumber(lua, -2));
    if (!t)
        return 0;
    tex_t **existing = str_tex_table_get_ptr(&_textures.table,
        lua_tostring(lua, -1));
    if (existing)
        t->tex = *existing;
    else
        t->tex = _load_texture_file(lua_tostring(lua, -1));
    t->clip[0] = 0.f;
    t->clip[1] = 0.f;
    if (t->tex)
    {
        t->clip[2] = (float)t->tex->w;
        t->clip[3] = (float)t->tex->h;
    }
    return 0;
}

static int
_luiapi_set_text_input_width(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -2));
    if (ti)
        ti->w = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_set_text_input_height(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -2));
    if (ti)
        ti->h = (int)lua_tonumber(lua, -1);
    return 0;
}

static int
_luiapi_get_text_input_content(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        goto fail;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        goto fail;
    lua_pushstring(lua, ti->buf);
    return 1;
    fail:
        lua_pushnil(lua);
        return 1;
}

static int
_luiapi_set_text_input_submit_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) ||
        (!lua_isfunction(lua, -1) && !lua_isnil(lua, -1)))
        return 0;
    uint32 id = (uint32)lua_tonumber(lua, -2);
    lui_text_input_t *ti = _get_text_input(id);
    if (!ti)
        return 0;
    lua_pushstring(lua, _text_input_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    lua_pushnumber(lua, (lua_Number)id);
    lua_gettable(lua, -2);
    lua_pushstring(lua, _text_input_submit_key);
    lua_pushvalue(lua, -4);
    lua_settable(lua, -3);
    lua_pop(lua, 2);
    return 0;
}

static int
_luiapi_clear_text_input(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        return 0;
    if (ti->buf)
        ti->buf[0] = 0;
    return 0;
}

static int
_luiapi_set_active_text_input(lua_State *lua)
{
    if (lua_isnumber(lua, -1))
    {
        lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
        if (!ti)
            return 0;
        gui_set_active_win(_get_window_of_text_input(ti)->title);
        gui_set_active_text_input(ti->title);
    } else if (lua_isnil(lua, -1))
        gui_set_active_text_input(0);
    return 0;
}

static int
_luiapi_is_text_input_active(lua_State *lua)
{
    int val = 0;
    if (!lua_isnumber(lua, -1))
        goto out;
    lui_text_input_t *ti = _get_text_input((uint32)lua_tonumber(lua, -1));
    if (!ti)
        goto out;
    return gui_is_text_input_active(ti->title);
    out:
        lua_pushboolean(lua, val);
        return 1;
}

static int
_luiapi_create_window_style(lua_State *lua)
{
    uint32 index = darr_num(_window_styles);
    gui_win_style_t style = gui_create_win_style();
    memcpy(&style, &_window_styles[0], sizeof(gui_win_style_t));
    darr_push(_window_styles, style);
    lua_pushnumber(lua, (lua_Number)index);
    return 1;
}

static gui_win_state_style_t *
_get_window_state_style(uint32 style_id, const char *state_name)
{
    gui_win_style_t *style = _get_window_style(style_id);
    if (!style)
        return 0;
    if (streq(state_name, "inactive"))
        return &style->inactive;
    else if (streq(state_name, "hovered"))
        return &style->hovered;
    else if (streq(state_name, "active"))
        return  &style->active;
    return 0;
}

static int
_luiapi_set_window_style_background_color(lua_State *lua)
{
    if (!lua_isnumber(lua, -6) || !lua_isstring(lua, -5))
        return 0;
    for (int i = -4; i < 0; ++i)
        if (!lua_isnumber(lua, i))
            return 0;
    gui_win_state_style_t *ss = _get_window_state_style(
        (uint32)lua_tonumber(lua, -6), lua_tostring(lua, -5));
    if (!ss)
        return 0;
    ss->bg_col[0] = (uint8)CLAMP(lua_tonumber(lua, -4) * 255.f, 0, 255.f);
    ss->bg_col[1] = (uint8)CLAMP(lua_tonumber(lua, -3) * 255.f, 0, 255.f);
    ss->bg_col[2] = (uint8)CLAMP(lua_tonumber(lua, -2) * 255.f, 0, 255.f);
    ss->bg_col[3] = (uint8)CLAMP(lua_tonumber(lua, -1) * 255.f, 0, 255.f);
    return 0;
}

static int
_luiapi_set_window_style_title_color(lua_State *lua)
{
    if (!lua_isnumber(lua, -6) || !lua_isstring(lua, -5))
        return 0;
    for (int i = -4; i < 0; ++i)
        if (!lua_isnumber(lua, i))
            return 0;
    gui_win_state_style_t *ss = _get_window_state_style(
        (uint32)lua_tonumber(lua, -6), lua_tostring(lua, -5));
    if (!ss)
        return 0;
    ss->title_col[0] = (uint8)CLAMP(lua_tonumber(lua, -4) * 255.f, 0, 255.f);
    ss->title_col[1] = (uint8)CLAMP(lua_tonumber(lua, -3) * 255.f, 0, 255.f);
    ss->title_col[2] = (uint8)CLAMP(lua_tonumber(lua, -2) * 255.f, 0, 255.f);
    ss->title_col[3] = (uint8)CLAMP(lua_tonumber(lua, -1) * 255.f, 0, 255.f);
    return 0;
}

static int
_luiapi_get_fps(lua_State *lua)
{
    lua_pushnumber(lua, (lua_Number)main_perf_clock.fps);
    return 1;
}

static int
_luiapi_create_key_bind(lua_State *lua)
{
    if (!lua_isstring(lua, -2) || !lua_isfunction(lua, -1))
        return 0;

    lua_pushstring(lua, _key_callback_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(lua, -1));

    uint32 id = _running_key_callback_id++;
    lua_pushnumber(lua, (lua_Number)id);
    lua_pushvalue(lua, -3);
    muta_assert(lua_isfunction(lua, -1));
    lua_settable(lua, -3);

    hk_action_callback_t callback;
    callback.type   = HK_ACTION_CALLBACK_LUA;
    callback.lua.id = id;

    muta_assert(lua_isstring(lua, -3));
    hk_new_action(lua_tostring(lua, -3), callback, HK_PRESS_UP);

    DEBUG_PRINTFF("created hotkey %s.\n", lua_tostring(lua, -3));
    return 0;
}

static int
_luiapi_bind_key(lua_State *lua)
{
    if (!lua_isstring(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isnumber(lua, -1))
        return 0;
    const char  *action_name    = lua_tostring(lua, -3);
    const char  *keys           = lua_tostring(lua, -2);
    hk_action_t *action         = hk_get_action_by_name(action_name);
    int         index           = 1;
    int         err;
    if (!action)
        {err = 1; goto err_out;}
    if (!lua_isnumber(_lua, -1))
    {
        if (action->keys[0].keycode != -1 && action->keys[1].keycode != -1)
            {err = 2; goto err_out;}
        index = action->keys[0].keycode == -1 ? 1 : 2;
    } else if ((index = (int)lua_tonumber(_lua, -1)) < 1 || index > 2)
        {err = 3; goto err_out;}
    int r = hk_bind_key_from_str(action_name, index - 1, keys);
    if (r)
    {
        DEBUG_PRINTFF("failed to bind %s to %s, code %d.\n", action_name, keys,
            r);
        err = 4;
        goto err_out;
    }
    DEBUG_PRINTFF("bound hotkey %s to %s.\n", action_name, keys);
    return 0;
    err_out:
        DEBUG_PRINTFF("error %d.\n", err);
        return 0;
}

static int
_luiapi_log_out(lua_State *lua)
    {core_set_screen(&main_menu_screen); return 0;}

static int
_luiapi_exit_game(lua_State *lua)
    {core_shutdown(); return 0;}

static int
_luiapi_get_key_bindings(lua_State *lua)
{
    lua_pushstring(_lua, _hotkey_table_key);
    lua_gettable(_lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(_lua, -1));
    muta_assert(lua_istable(_lua, -1));
    return 1;
}

static int
_luiapi_is_shift_down(lua_State *lua)
{
    int v = keyboard_state[KEY_SCANCODE(LSHIFT)] ||
        keyboard_state[KEY_SCANCODE(RSHIFT)];
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_is_alt_down(lua_State *lua)
{
    int v = keyboard_state[KEY_SCANCODE(LALT) || KEY_SCANCODE(RALT)];
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_is_ctrl_down(lua_State *lua)
{
    int v = keyboard_state[KEY_SCANCODE(LCTRL)] ||
        keyboard_state[KEY_SCANCODE(RCTRL)];
    lua_pushboolean(lua, v);
    return 1;
}

static int
_luiapi_grab_keyboard_for_window(lua_State *lua)
{
    if (lua_isnil(lua, -1))
    {
        _keyboard_grab_window_index = -1;
        return 0;
    }
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    _keyboard_grab_window_index = (int)win->index;
    return 0;
}

static int
_luiapi_ungrab_keyboard_for_window(lua_State *lua)
{
    if (!lua_isnumber(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    if (_keyboard_grab_window_index != (int)win->index)
        return 0;
    _keyboard_grab_window_index = -1;
    return 0;
}

static int
_luiapi_send_chat_message(lua_State *lua)
{
    if (!lua_isstring(lua, -1))
        return 0;
    gs_send_chat_msg_to_sv(lua_tostring(lua, -1));
    return 0;
}

static int
_luiapi_listen_to_event(lua_State *lua)
{
    if (!lua_isnumber(lua, -2) || !lua_isstring(lua, -1))
        return 0;
    lui_window_t *win = _get_window_arg(-2);
    if (!win)
        return 0;
    int type = _str_to_event_enum(lua_tostring(lua, -1));
    if (type == LUI_NUM_EVENTS || win->listening_to[type])
        return 0;
    win->listening_to[type] = 1;
    darr_push(_listener_lists[type].windows, win->index);
    return 0;
}

static int
_luiapi_unlisten_to_event(lua_State *lua)
{
    IMPLEMENTME();
    muta_assert(0);
    return 0;
}

static int
_luiapi_set_event_callback(lua_State *lua)
{
    if (!lua_isnumber(lua, -3) || !lua_isstring(lua, -2) ||
        !lua_isfunction(lua, -1)) // TODO: make nil a valid value for callback
        return 0;

    lui_window_t *win = _get_window_arg(-3);
    if (!win)
    {
        DEBUG_PRINTFF("window not found.\n");
        return 0;
    }

    const char  *event_name = lua_tostring(lua, -2);
    int         event       = _str_to_event_enum(event_name);
    if (event == LUI_NUM_EVENTS)
    {
        DEBUG_PRINTFF("invalid event '%s'.\n", event_name);
        return 0;
    }

    lua_pushstring(lua, _window_table_key);
    lua_gettable(lua, LUA_REGISTRYINDEX);

    muta_assert(!lua_isnil(lua, -1));

    lua_pushstring(lua, win->title);
    lua_gettable(lua, -2);

    muta_assert(!lua_isnil(lua, -1));

    lua_pushnumber(lua, event);
    lua_pushvalue(lua, -4);
    lua_settable(lua, -3);

    DEBUG_PRINTFF("event handler of event '%s' changed for window '%s'.\n",
        event_name, win->title);
    return 0;
}

static void
_parse_addon_file_callback(void *ctx, const char *opt, const char *val)
{
    addon_parse_ctx_t *pc = ctx;
    if (streq(opt, "name"))
    {
        if (pc->addon->name)
        {
            printf("Addon name '%s' defined twice in the same file.\n", val);
            pc->error = 1;
            return;
        }
        if (!val)
        {
            printf("Error: defines an empty name.\n");
            pc->error = 2;
            return;
        }
        if (_get_addon(val))
        {
            printf("Warning: addon %s already exists.\n", val);
            pc->error = 1;
            return;
        }
        pc->addon->name = dstr_create(val);
    } else
    if (streq(opt, "file"))
    {
        dstr_setf(&pc->lua_file_path, pc->dir_path);
        dstr_append(&pc->lua_file_path, "/");
        dstr_append(&pc->lua_file_path, val);
        if (!file_exists(pc->lua_file_path))
        {
            printf("Warning: lua file '%s' not found (as defined for addon "
                "'%s'", pc->lua_file_path, opt);
            pc->error = 3;
        }
        pc->addon->lua_file_path = dstr_create(pc->lua_file_path);
    } else
        pc->error = 4;
}

static int
_load_addons()
{
    dir_entry_t     de;
    dir_handle_t    dh = open_directory("ui/addons", &de);

    if (!dh)
    {
        puts("ui/addons directory not found!");
        return 1;
    }

    dchar *dp = dstr_create_empty((uint)strlen("ui/addons/") + 64);

    while (!get_next_file_in_directory(dh, &de))
    {
        dstr_set(&dp, "ui/addons/");
        dstr_append(&dp, get_dir_entry_name(&de));
        _load_addon_directory(dp);
    }

    dstr_free(&dp);
    close_directory(dh);

    addon_t *addons     = _addons;
    uint32  num_addons  = darr_num(addons);

    for (uint32 i = 0; i < num_addons; ++i)
    {
        printf("Attempting to run UI Lua file %s\n", addons[i].lua_file_path);
        lua_pushcfunction(_lua, _lua_stack_trace);
        muta_assert(lua_isfunction(_lua, -1));
        luaL_loadfile(_lua, addons[i].lua_file_path);
        muta_assert(lua_isfunction(_lua, -2));
        if (!lua_pcall(_lua, 0, 0, -2))
            printf("UI Lua file ran successfully.\n");
        else
            printf("Errors running UI Lua file!\n");
    }

    printf("Loaded %u addons.\n", darr_num(_addons));
    return 0;
}


static void
_load_addon_directory(const char *dp)
{
    int len = (int)strlen(dp);

    /* Check for directories . and .. */
    if (len >= 2 && (streq(dp + len - 2, "/.") || streq(dp + len - 2, "..")))
        return;

    DEBUG_PRINTFF("attempting to load from directory %s.\n", dp);

    dir_entry_t         de;
    dir_handle_t        dh          = open_directory(dp, &de);
    const int           postfix_len = (int)strlen(".addon");
    bool32              success     = 0;
    addon_parse_ctx_t   ctx         = {0};

    while (!get_next_file_in_directory(dh, &de))
    {
        const char  *file_name      = get_dir_entry_name(&de);
        int         file_name_len   = (int)strlen(file_name);

        if (file_name_len < postfix_len)
            continue;
        if (!streq(file_name + file_name_len - postfix_len, ".addon"))
            continue;

        addon_t addon = {0};

        ctx.error           = 0;
        ctx.addon           = &addon;
        ctx.dir_path        = dp;
        ctx.lua_file_path   = set_dynamic_str_len(ctx.lua_file_path, 0);

        uint32  dp_len  = (uint32)strlen(dp);
        char    *fp     = stack_alloc(dp_len + strlen(file_name) + 2);
        memcpy(fp, dp, dp_len);
        fp[dp_len] = '/';
        memcpy(fp + dp_len + 1, file_name, file_name_len);
        fp[dp_len + file_name_len + 1] = 0;

        bool32 have_err = parse_cfg_file(fp, _parse_addon_file_callback, &ctx)
            || ctx.error;
        if (have_err)
        {
            printf("Error loading addon '%s'.\n", fp);
            dstr_free(&addon.name);
            dstr_free(&addon.lua_file_path);
            continue;
        }
        darr_push(_addons, addon);
        success = 1;
    }
    dstr_free(&ctx.lua_file_path);
    if (success)
        DEBUG_PRINTFF("loaded addon %s.\n", dp);
    else
        printf("Error loading addon '%s'.\n", dp);
    close_directory(dh);
}

static addon_t *
_get_addon(const char *name)
{
    addon_t *addons     = _addons;
    uint32  num_addons  = darr_num(addons);
    for (uint32 i = 0; i < num_addons; ++i)
        if (streq(addons[i].name, name))
            return &addons[i];
    return 0;
}

static lui_window_t *
_get_window(const char *name)
{
    lui_window_t    **wins      = _wins.all;
    uint32          num_wins    = darr_num(wins);
    for (uint32 i = 0; i < num_wins; ++i)
        if (streq(name, wins[i]->title))
            return wins[i];
    return 0;
}

static lui_window_t *
_create_window(const char *title)
{
    if (_get_window(title))
        return 0;
    lui_window_t *ret = obj_pool_reserve(&_wins.pool);
    memset(ret, 0, sizeof(lui_window_t));
    ret->title          = dstr_create(title);
    ret->flags.shown    = 0;
    ret->index          = darr_num(_wins.all);
    ret->origin         = GUI_TOP_LEFT;
    ret->style_index    = 0;
    ret->parent         = 0xFFFFFFFF;
    darr_push(_wins.all, ret);
    return ret;
}

static lui_window_obj_data_t *
_create_window_obj(lui_window_t *win)
{
    uint32 id = _window_objs.running_id++;
    lui_window_obj_t *obj = lui_window_obj_table_einsert_empty(
        &_window_objs.table, id);
    obj->type               = LUI_WINDOW_OBJ_NONE;
    obj->id                 = id;
    obj->window             = win->index;
    obj->index_in_window    = darr_num(win->objs);

    lui_window_obj_data_t data = {0};
    data.type   = LUI_WINDOW_OBJ_NONE;
    data.obj    = obj;
    data.origin = GUI_TOP_LEFT;

    darr_push(win->objs, data);
    return &win->objs[obj->index_in_window];
}

static inline lui_window_t *
_get_window_arg(int stack_index)
{
    uint32 index = (uint32)lua_tonumber(_lua, stack_index);
    if (index >= darr_num(_wins.all))
        return 0;
    return _wins.all[index];
}

static inline lui_text_t *
_get_text(uint32 id)
{
    lui_window_obj_t *o = lui_window_obj_table_get_ptr(&_window_objs.table, id);
    if (!o || o->type != LUI_WINDOW_OBJ_TEXT)
        return 0;
    lui_window_t *win = _wins.all[o->window];
    return &win->objs[o->index_in_window].data.text;
}

static inline lui_button_t *
_get_button(uint32 id)
{
    lui_window_obj_t *o = lui_window_obj_table_get_ptr(&_window_objs.table, id);
    if (!o || o->type != LUI_WINDOW_OBJ_BUTTON)
        return 0;
    lui_window_t *win = _wins.all[o->window];
    return &win->objs[o->index_in_window].data.button;
}

static inline lui_texture_t *
_get_texture(uint32 id)
{
    lui_window_obj_t *o = lui_window_obj_table_get_ptr(&_window_objs.table, id);
    if (!o || o->type != LUI_WINDOW_OBJ_TEXTURE)
        return 0;
    lui_window_t *win = _wins.all[o->window];
    return &win->objs[o->index_in_window].data.texture;
}

static inline lui_text_input_t *
_get_text_input(uint32 id)
{
    lui_window_obj_t *o = lui_window_obj_table_get_ptr(&_window_objs.table, id);
    if (!o || o->type != LUI_WINDOW_OBJ_TEXT_INPUT)
        return 0;
    lui_window_t *win = _wins.all[o->window];
    return &win->objs[o->index_in_window].data.text_input;
}

static inline gui_win_style_t *
_get_window_style(uint32 id)
{
    if (id >= darr_num(_window_styles))
        return 0;
    return &_window_styles[id];
}

static int
_get_window_event_callback(lui_window_t *win, lui_event_t *event)
{
    lua_pushstring(_lua, _window_table_key);
    lua_gettable(_lua, LUA_REGISTRYINDEX);
    muta_assert(!lua_isnil(_lua, -1));

    lua_pushstring(_lua, win->title);
    lua_gettable(_lua, -2);

    lua_pushnumber(_lua, (lua_Number)event->type);
    lua_gettable(_lua, -2);

    if (lua_isnil(_lua, -1))
    {
        DEBUG_PRINTFF("event callback for event %d in window '%s' was nil!\n",
            event->type, win->title);
        return 1;
    }
    return 0;
}

static void
_propagate_event_to_window(lui_window_t *win, lui_event_t *event)
{
    int top, new_top;
    top = lua_gettop(_lua);
    if (_get_window_event_callback(win, event))
        goto out;
    switch (event->type)
    {
    case LUI_EVENT_UPDATE:
    {
        lua_newtable(_lua);
        lua_pushstring(_lua, "delta");
        lua_pushnumber(_lua, event->update.delta);
        lua_settable(_lua, -3);
        muta_assert(lua_isfunction(_lua, -2));
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_APP_WINDOW_SIZE:
    {
        lua_newtable(_lua);
        lua_pushstring(_lua, "width");
        lua_pushnumber(_lua, event->app_window.w);
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "height");
        lua_pushnumber(_lua, event->app_window.h);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_NEW_HOTKEY_ACTION:
    {
        lua_pushstring(_lua,_hotkey_table_key);
        lua_gettable(_lua, LUA_REGISTRYINDEX);
        lua_pushstring(_lua, event->new_hotkey_action.action->name);
        lua_gettable(_lua, -2);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_KEY_DOWN:
    {
        if (_keyboard_grab_window_index != -1 &&
            win->index != _keyboard_grab_window_index)
            break;
        lua_newtable(_lua);
        lua_pushstring(_lua, "key");
        lua_pushstring(_lua, event->key.key_combo_name);
        lua_settable(_lua, -3);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_KEY_UP:
    {
        if (_keyboard_grab_window_index != -1 &&
            win->index != _keyboard_grab_window_index)
            break;
        lua_newtable(_lua);
        lua_pushstring(_lua, "key");
        lua_pushstring(_lua, event->key.key_combo_name);
        lua_settable(_lua, -3);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_KEY_BIND_CHANGED:
    {
        lua_pushstring(_lua,_hotkey_table_key);
        lua_gettable(_lua, LUA_REGISTRYINDEX);
        lua_pushstring(_lua, event->key_bind_changed.action->name);
        lua_gettable(_lua, -2);
        lua_remove(_lua, -2);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    case LUI_EVENT_CHAT_LOG_ENTRY:
    {
        DEBUG_PRINTFF("LUI_EVENT_CHAT_LOG_ENTRY\n");
        lua_newtable(_lua);
        lua_pushstring(_lua, "type");
        lua_pushstring(_lua, "GLOBAL_CHAT_BROADCAST");
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "sender");
        lua_pushstring(_lua, event->chat_log_entry.sender);
        lua_settable(_lua, -3);
        lua_pushstring(_lua, "message");
        lua_pushstring(_lua, event->chat_log_entry.msg);
        lua_settable(_lua, -3);
        lua_pcall(_lua, 1, 0, 0);
    }
        break;
    default:
        DEBUG_PRINTFF("warning: unhandled event type %d.\n", event->type);
    }
    out:
        new_top = lua_gettop(_lua);
        if (new_top > top)
            lua_pop(_lua, new_top - top);
}

static int
_str_to_event_enum(const char *str)
{
    if (!str)
        return LUI_NUM_EVENTS;
    if (streq(str, "UPDATE"))
        return LUI_EVENT_UPDATE;
    else if (streq(str, "APP_WINDOW_SIZE"))
        return LUI_EVENT_APP_WINDOW_SIZE;
    else if (streq(str, "KEY_CALLBACK"))
        return LUI_EVENT_KEY_CALLBACK;
    else if (streq(str, "NEW_KEY_BINDING"))
        return LUI_EVENT_NEW_HOTKEY_ACTION;
    else if (streq(str, "KEY_DOWN"))
        return LUI_EVENT_KEY_DOWN;
    else if (streq(str, "KEY_UP"))
        return LUI_EVENT_KEY_UP;
    else if (streq(str, "KEY_BIND_CHANGED"))
        return LUI_EVENT_KEY_BIND_CHANGED;
    else if (streq(str, "CHAT_LOG_ENTRY"))
        return LUI_EVENT_CHAT_LOG_ENTRY;
    return LUI_NUM_EVENTS;
}

static int
_str_to_gui_origin(const char *str)
{
    if (streq(str, "TOP_LEFT"))
        return GUI_TOP_LEFT;
    if (streq(str, "TOP_RIGHT"))
        return GUI_TOP_RIGHT;
    if (streq(str, "TOP_CENTER"))
        return GUI_TOP_CENTER;
    if (streq(str, "BOTTOM_LEFT"))
        return GUI_BOTTOM_LEFT;
    if (streq(str, "BOTTOM_RIGHT"))
        return GUI_BOTTOM_RIGHT;
    if (streq(str, "BOTTOM_CENTER"))
        return GUI_BOTTOM_CENTER;
    if (streq(str, "CENTER_LEFT"))
        return GUI_CENTER_LEFT;
    if (streq(str, "CENTER_RIGHT"))
        return GUI_CENTER_RIGHT;
    if (streq(str, "CENTER_CENTER"))
        return GUI_CENTER_CENTER;
    return NUM_GUI_ORIGINS;
}

static inline void
_post_event(lui_event_t *event)
{
    muta_assert(event->type < LUI_NUM_EVENTS);
    darr_push(_events, *event);
}

static tex_t *
_load_texture_file(const char *path)
{
    tex_t *tex = 0;
    img_t img;
    if (img_load(&img, path))
        return 0;
    tex_t new_tex;
    if (!tex_from_img(&new_tex, &img))
    {
        tex = obj_pool_reserve(&_textures.pool);
        *tex = new_tex;
        str_tex_table_einsert(&_textures.table, path, tex);
    }
    img_free(&img);
    return tex;
}

static int
_lua_stack_trace(lua_State *lua)
{
    luaL_traceback(_lua, _lua, "UI Lua error!", 1);
    int index = -1;
    for (; lua_isstring(_lua, index); --index)
        printf("%s\n", lua_tostring(_lua, index));
    return 0;
}

static void
_create_lua_table_for_hotkey_action(hk_action_t *action)
{
    char    buf[HK_MAX_KEY_COMBO_NAME_LEN + 1];
    int     r;
    lua_newtable(_lua);
    lua_pushstring(_lua, "name");
    lua_pushstring(_lua, action->name);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "key1");
    r = hk_key_combo_to_str(action->keys[0].keycode, action->keys[0].mods, buf);
    muta_assert(!r);
    DEBUG_PRINTFF("buf(1): %s from key %d, mods %d\n", buf,
        action->keys[0].keycode, action->keys[0].mods);
    lua_pushstring(_lua, buf);
    lua_settable(_lua, -3);
    lua_pushstring(_lua, "key2");
    r = hk_key_combo_to_str(action->keys[1].keycode, action->keys[1].mods, buf);
    muta_assert(!r);
    lua_pushstring(_lua, buf);
    lua_settable(_lua, -3);
    DEBUG_PRINTFF("buf(2): %s from key %d, mods %d\n", buf,
        action->keys[1].keycode, action->keys[1].mods);
}

static int
_is_window_hovered_or_active(lua_State *lua, int *index_val)
{
    int win_index   = *index_val;
    int v           = 0;
    if (win_index < 0 || !lua_isnumber(lua, -1))
        goto out;
    lui_window_t *win = _get_window_arg(-1);
    if (!win)
        return 0;
    v = win->index == (uint32)win_index;
    out:
        lua_pushboolean(lua, v);
        return 1;
}

static void
_remove_window_from_shown(lui_window_t *win)
{
    lui_window_t    **shown     = _wins.shown;
    uint32          num_shown   = darr_num(shown);
    for (uint32 i = 0; i < num_shown; ++i)
    {
        if (shown[i] != win)
            continue;
        darr_erase(shown, i);
        _wins.shown = shown;
        return;
    }
    muta_assert(0);
}

static inline void
_update_text_wh(lui_text_t *text)
{
    lui_window_t *win = _get_window_of_text(text);
    gui_compute_text_wh(&text->w, &text->h, text->string, text->font,
        text->scale, text->wrap, win->w, win->h);
}

#define GET_OBJ_WINDOW(obj_ptr, name_in_union) \
    lui_window_obj_data_t *od = (lui_window_obj_data_t*)((uint8*)(obj_ptr) - \
        offsetof(union lui_window_obj_type_data_t, name_in_union) - \
        offsetof(lui_window_obj_data_t, data)); \
    return _wins.all[od->obj->window];

static inline lui_window_t *
_get_window_of_text(lui_text_t *text)
    {GET_OBJ_WINDOW(text, text);}

static inline lui_window_t *
_get_window_of_texture(lui_texture_t *texture)
    {GET_OBJ_WINDOW(texture, texture);}

static inline lui_window_t *
_get_window_of_button(lui_button_t *button)
    {GET_OBJ_WINDOW(button, button);}

static inline lui_window_t *
_get_window_of_text_input(lui_text_input_t *ti)
    {GET_OBJ_WINDOW(ti, text_input);}
