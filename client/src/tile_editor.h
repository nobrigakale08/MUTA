#ifndef MUTA_TILE_EDITOR_H
#define MUTA_TILE_EDITOR_H

#include "../../shared/types.h"

void
tile_editor_update(double dt);

void
tile_editor_open();

void
tile_editor_close();

void
tile_editor_keydown(int key, bool32 is_repeat);

void
tile_editor_mousewheel(int x, int y);

void
tile_editor_text_input(const char *text);

void
tile_editor_file_drop(const char *path);

#endif /*MUTA_TILE_EDITOR_H*/