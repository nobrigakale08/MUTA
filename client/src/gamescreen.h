void
gamescreen_update(double dt);

void
gamescreen_open();

void
gamescreen_close();

void
gamescreen_os_quit();

void
gamescreen_keydown(int key, bool32 is_repeat);

void
gamescreen_keyup(int key);

void
gamescreen_text_input(const char *text);

void
gamescreen_mousebuttonup(uint8 button, int x, int y);

void
gamescreen_mousebuttondown(uint8 button, int x, int y);

entity_t *
gamescreen_get_player_target(); /* tmp */
