#include "sprite_context.h"
#include "bitmap.h"

#define SPRITE_ANIM_STEP 0.016667f

int
sprite_context_init(sprite_context_t *sc, int num_sprites)
{
    sprite_context_t tmp = {0};

    if (sprite_pool_init(&tmp.sprite_pool, 64, 4) != 0)
        return 1;

    *sc = tmp;

    return 0;
}

void
sprite_context_destroy(sprite_context_t *sc)
{
    sprite_pool_destroy(&sc->sprite_pool);
}

void
sprite_context_clear(sprite_context_t *sc)
{
    sprite_pool_clear(&sc->sprite_pool);
    sc->res_sprites = 0;
}

sprite_t *
create_sprite(sprite_context_t *sc)
{
    sprite_t *ret = sprite_pool_reserve(&sc->sprite_pool);
    if (!ret) return 0;

    ret->anim       = 0;
    ret->next       = sc->res_sprites;
    ret->speed      = 1.f;
    ret->flags      = 0;

    sc->res_sprites = ret;

    return ret;
}

void
free_sprite(sprite_t *s, sprite_context_t *sc)
{
    if (s != sc->res_sprites)
    {
        for (sprite_t *os = sc->res_sprites; os; os = os->next)
        {
            if (os->next == s)
                {os->next = s->next; break;}
        }
    }
    else
        sc->res_sprites = s->next;

    sprite_pool_free(&sc->sprite_pool, s);
}

void
sprite_context_update(sprite_context_t *sc, double dt)
{
    sc->anim_accum += dt;

    for (;sc->anim_accum >= SPRITE_ANIM_STEP;
          sc->anim_accum -= SPRITE_ANIM_STEP)
    {
        anim_frame_t *frame;

        for (sprite_t *s = sc->res_sprites; s; s = s->next)
        {
            if (GET_BITFLAG(s->flags, SPRITE_ANIM_PAUSED))  continue;
            if (!s->anim)                                   continue;
            if (!s->anim->frames)                           continue;
            if (s->anim->num_frames <= 0)                   continue;

            s->time_passed += (float)SPRITE_ANIM_STEP;

            frame   = &s->anim->frames[s->frame_num];

            while (frame->dur * s->speed < s->time_passed)
            {
                s->time_passed -= frame->dur;

                if (s->frame_num != s->anim->num_frames - 1)
                    s->frame_num++;
                else if (GET_BITFLAG(s->flags, SPRITE_ANIM_LOOP))
                    s->frame_num = 0;

                frame   = &s->anim->frames[s->frame_num];
            }
        }
    }
}

void
sprite_play_anim(sprite_t *s, anim_t *a)
{
    if (!s) return;
    s->anim         = a;
    s->frame_num    = 0;
    s->time_passed  = 0.f;
}

tex_t *
sprite_get_cur_tex(sprite_t *s)
{
    if (s && s->anim && s->anim->frames && s->anim->num_frames > 0)
        return s->anim->frames[s->frame_num].tex;
    return 0;
}

