#include <time.h>
#include <stdio.h>
#include <inttypes.h>
#include "../../shared/net.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"
#include "../../shared/rwbits.inl"
#include "../../shared/packets.h"
#include "../../shared/acc_utils.h"
#include "../../shared/emote.h"
#include "core.h"
#include "mainmenu.h"
#include "editor.h"
#include "tile_editor.h"
#include "sprite_editor.h"
#include "gamescreen.h"
#include "characterselectscreen.h"
#include "charactercreatescreen.h"
#include "animatorscreen.h"
#include "render.h"
#include "assets.h"
#include "audio.h"
#include "gui.h"
#include "utils.inl"
#include "gamestate.h"
#include "lui.h"
#include "sprite_context.h"

typedef struct click_event_t        click_event_t;
typedef struct sv_info_parse_res_t  sv_info_parse_res_t;
typedef struct network_event_t           network_event_t;

#define KEEP_ALIVE_FREQ                     2500
#define CORE_MAX_CLICK_EVENTS               32
#define CORE_MAX_SLASH_CMDS                 256
#define IS_CONNECTION_OPEN() \
    (c_conn_status != CONNSTATUS_DISCONNECTED && \
     c_conn_status != CONNSTATUS_CONNECTING)
#define MAX_NETWORK_EVENTS                  8

enum network_thread_state
{
    NETWORK_THREAD_RECV,
    NETWORK_THREAD_CONNECT_TO_SHARD
};

enum network_event
{
    NETWORK_EVENT_START_HANDSHAKE_WITH_SHARD
};

struct click_event_t
{
    int     x, y;
    uint8   button, down;
};

struct sv_info_parse_res_t
{
    bool32  found;
    addr_t  addr;
};

struct network_event_t
{
    union
    {
        int type;
    };
};

static gui_win_style_t      _menu_window_style;
static gui_button_style_t   _menu_button_style;
static sprite_context_t     _shared_sprite_ctx;
static cursor_t             *_current_cursor;

static struct
{
    click_event_t   events[CORE_MAX_CLICK_EVENTS];
    uint32          num;
} _click_events;

static struct
{
    slash_cmd_t     cmds[CORE_MAX_SLASH_CMDS];
    int             num;
} _slash_cmds;

window_t                main_window;
perf_clock_t            main_perf_clock;
mouse_state_t           mouse_state;
screen_t                main_menu_screen;
screen_t                game_screen;
screen_t                editor_screen;
screen_t                character_select_screen;
screen_t                character_create_screen;
screen_t                tile_editor_screen;
screen_t				sprite_editor_screen;
screen_t                animator_screen;
gui_win_style_t         *menu_window_style  = &_menu_window_style;
gui_button_style_t      *menu_button_style  = &_menu_button_style;
kth_pool_t              th_pool;
game_config_t           game_cfg;
spritesheet_t           *icons_ss;
sprite_context_t        *shared_sprite_ctx  = &_shared_sprite_ctx;
const uint8             *keyboard_state;
int                     num_keyboard_keys;
bool32                  player_sex;
uint8                   bcolor_white[4]     = {255, 255, 255, 255};
uint8                   bcolor_blue [4]     = { 10,  62, 179, 255};
uint8                   bcolor_black[4]     = {  0,   0,   0, 255};
int                     core_dc_reason;
const char *            core_dc_reason_str;
character_props_t       core_character_props[MAX_CHARACTERS_PER_ACC];
uint32                  core_num_character_props;
bool32                  core_character_creation_in_progress;
int                     core_character_creation_error;
shard_info_t            core_shards[CORE_MAX_SHARDS];
uint32                  core_num_shards;
uint32                  core_selected_shard;

#ifdef _MUTA_DEBUG
const char *core_auto_login_name;
const char *core_auto_login_pw;
#endif

static bool32           _app_running;
static screen_t         *_next_screen;
static screen_t         *_cur_screen;
static thread_t         _nw_thread;
static addr_t           _sv_addr;

/* Client vars */
enum conn_status_t      c_conn_status = CONNSTATUS_DISCONNECTED;
bool32                  c_authed;
static socket_t         _c_sock;
static socket_t         _c_shard_connect_sock;
static addr_t           _c_shard_addr;
static uint32           _c_conn_begin_time;
static uint32           _c_last_send_time;
static world_pos_t      _c_last_sv_pos;
static bool32           _c_have_sv_pos;
static double_msg_buf_t _c_msgs_in;
static byte_buf_t       _c_msg_buf_out;
static cryptchan_t      _c_cryptchan;
static bool32           _c_should_dc;
static uint8            _c_auth_token[AUTH_TOKEN_SZ];
static mutex_t          _c_sock_mutex;

static char             _acc_name[MAX_ACC_NAME_LEN + 1];
static char             _pw[MAX_PW_LEN + 1];
static char             _text_input[32];
static int32            _network_thread_state;
static blocking_queue_t _network_event_queue;

cryptchan_t             *c_cryptchan = &_c_cryptchan;

static int
_window_init(window_t *win, const char *title, int x, int y, int w, int h,
    bool32 fullscreen, bool32 maximized, bool32 windowed);

static void
_window_destroy(window_t *win);

static int
_window_maximize(window_t *win);

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val);

static void
_read_tile_cfg_callback(void *ctx, const char *opt, const char *val);

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val);

static thread_ret_t
_network_thread_callback(void *args);

static byte_buf_t *
_send_var_encrypted_msg_to_sv(int len);

static int
_read_packet(uint8 *pckt, int pckt_len);
/* Returns the amount of unread bytes (possible incomplete packet, or -1 if we
 * should disconnect. */

static slash_cmd_t *
_find_slash_cmd(const char *name);

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s);

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s);

static int
_handle_svmsg_char_login_fail(svmsg_char_login_fail_t *s);

static int
_handle_svmsg_login_result(svmsg_login_result_t *s);

static int
_handle_svmsg_character_list(svmsg_character_list_t *s);

static int
_handle_svmsg_create_character_fail(svmsg_create_character_fail_t *s);

static int
_handle_svmsg_new_character_created(svmsg_new_character_created_t *s);

static int
_handle_svmsg_player_init_data(svmsg_player_init_data_t *s);

static int
_handle_svmsg_shard_list_item(svmsg_shard_list_item_t *s);

static int
_handle_svmsg_shard_select_fail(svmsg_shard_select_fail_t *s);

static int
_handle_svmsg_shard_select_success(svmsg_shard_select_success_t *s);

static inline void
_store_sv_pos(int32 x, int32 y, int8 z);

static void
_init_menu_window_style();

static void
_init_menu_button_style();

static void
_update_gui_inputs(gui_input_state_t *is);

static int
_load_permanent_assets();

static void
_set_dc_reason(int reason);

static int
_post_click_event(click_event_t ev);

static void
_handle_window_event(SDL_Event *ev);

static int
_init_slash_cmds();

static int
_slash_cmd_exit(slash_cmd_t *cmd, union slash_cmd_arg_t arg, const char *args);

static int
_slash_cmd_emote(slash_cmd_t *cmd, union slash_cmd_arg_t arg, const char *args);

static void
_check_network_events(network_event_t *events, uint32 num_events);

int
core_maximize_window()
    {return _window_maximize(&main_window);}

int
core_read_config(const char *path)
{
    game_config_t *cfg = &game_cfg;

    /* Set default values */
    cfg->display.win_size[0]    = 800;
    cfg->display.win_size[1]    = 480;
    cfg->display.win_fullscreen = 1;
    cfg->display.win_windowed   = 1;
    cfg->display.win_maximized  = 1;
    cfg->vsync                  = 1;
    cfg->fps_limit              = 300;
    cfg->sprite_batch_size      = 65536;
    cfg->music_volume           = 0.25f;
    cfg->sound_volume           = 0.25f;
    cfg->tile_tex_path          = 0;
    cfg->tile_sprite_def_path   = 0;
    cfg->tile_px_w              = 128;
    cfg->tile_px_h              = 128;
    cfg->tile_top_px_h          = 64;

    cfg->game_data.map_path = set_dynamic_str(cfg->game_data.map_path,
        "muta-data/common/main_map/main");
    cfg->game_data.tile_path = set_dynamic_str(cfg->game_data.tile_path,
        "muta-data/common/tiles.dat");
    cfg->game_data.player_race_path = set_dynamic_str(
        cfg->game_data.player_race_path, "muta-data/common/player_races.dat");
    cfg->game_data.static_obj_path = set_dynamic_str(
        cfg->game_data.static_obj_path, "muta-data/common/static_objs.dat");
    cfg->game_data.dynamic_obj_path = set_dynamic_str(
        cfg->game_data.dynamic_obj_path, "muta-data/common/dynamic_objs.dat");
    cfg->game_data.creature_path = set_dynamic_str(cfg->game_data.creature_path,
        "muta-data/common/creatures.dat");
    cfg->game_data.map_db_path = set_dynamic_str(cfg->game_data.map_db_path,
        "muta-data/common/maps.mapdb");

    memset(cfg->account_name, 0, sizeof(cfg->account_name));
    parse_cfg_file(path, _read_cfg_callback, cfg);
	parse_cfg_file("muta-data/common/tiles.dat", _read_tile_cfg_callback, cfg);

    puts("Config:");
    printf("window fullscreen\t= %d\n", cfg->display.win_fullscreen);
    printf("window size\t\t= %dx%d\n", cfg->display.win_size[0],
        cfg->display.win_size[1]);
    printf("vsync\t\t\t= %d\n", cfg->vsync);
    printf("sprite bath size\t= %d\n", cfg->sprite_batch_size);
    printf("sound volume\t\t= %f\n", cfg->sound_volume);
    printf("music volume\t\t= %f\n", cfg->music_volume);
    return 0;
}

int
core_get_main_thread_fps() {return (int)main_perf_clock.fps;}

byte_buf_t *
core_send_msg_to_sv(int len)
{
    muta_assert(IS_CONNECTION_OPEN());
    int tot_sz = MSGTSZ + len;
    if (BBUF_FREE_SPACE(&_c_msg_buf_out) < tot_sz)
    {
        int r = net_send_all(_c_sock, _c_msg_buf_out.mem,
            _c_msg_buf_out.num_bytes);
        if (r < 0)
        {
            DEBUG_PRINTFF("failed.\n");
            core_disconnect_from_sv();
            return 0;
        }
        BBUF_CLEAR(&_c_msg_buf_out);
    }
    return &_c_msg_buf_out;
}

int
save_config(const char *path)
{
    game_config_t *cfg = &game_cfg;
    FILE *f = fopen(path, "w+");
    if (!f)
        return 1;
    fprintf(f, "window fullscreen       = %d\n", cfg->display.win_fullscreen);
    fprintf(f, "window size             = %dx%d\n", cfg->display.win_size[0],
        cfg->display.win_size[1]);
    fprintf(f, "window windowed         = %d\n", cfg->display.win_windowed);
    fprintf(f, "window maximized        = %d\n", cfg->display.win_maximized);
    fprintf(f, "vsync                   = %d\n", cfg->vsync);
    fprintf(f, "sound volume            = %d\n", (int)(cfg->sound_volume * 100.f));
    fprintf(f, "music volume            = %d\n", (int)(cfg->music_volume * 100.f));
    fprintf(f, "sprite batch size       = %d\n", cfg->sprite_batch_size);
    fprintf(f, "account                 = %s\n", cfg->account_name);
    fprintf(f, "tile texture path       = %s\n", cfg->tile_tex_path);
    fprintf(f, "tile pixel width        = %d\n", cfg->tile_px_w);
    fprintf(f, "tile pixel height       = %d\n", cfg->tile_px_h);
    fprintf(f, "tile top pixel height   = %d\n", cfg->tile_top_px_h);
    fprintf(f, "map path                = %s\n", cfg->game_data.map_path);
    fprintf(f, "tile path               = %s\n", cfg->game_data.tile_path);
    fprintf(f, "player race path        = %s\n", cfg->game_data.player_race_path);
    fprintf(f, "static obj path         = %s\n", cfg->game_data.static_obj_path);
    fprintf(f, "dynamic obj path        = %s\n", cfg->game_data.dynamic_obj_path);
    fprintf(f, "creature path           = %s\n", cfg->game_data.creature_path);
    fprintf(f, "map db path             = %s\n", cfg->game_data.map_db_path);
    fclose(f);
    return 0;
}

void
perf_clock_init(perf_clock_t *clock, uint32 fps_limit)
{
    clock->delta        = 0;
    clock->last_tick    = 0;
    clock->fps          = 0;
    clock->frame_num    = 0;
    clock->target_fps   = fps_limit;
}

void
perf_clock_tick(perf_clock_t *clock)
{
    uint64 tick_time = SDL_GetPerformanceCounter();
    uint64 perf_freq = SDL_GetPerformanceFrequency();

    if (clock->last_tick != 0.0f)
        clock->delta = (double)(tick_time - clock->last_tick) / (double)perf_freq;
    else
        clock->delta = 0.016f;

    clock->last_tick = tick_time;

    if (clock->delta > 0.0f)
        clock->fps = (uint32)(1.0f / clock->delta);

    if (clock->target_fps != 0)
    {
        double target_time = 1.0f / (double)clock->target_fps * 1000.0f;

        if ((double)(tick_time - clock->last_tick) < target_time)
        {
            uint64 substraction = tick_time - clock->last_tick;
            sleep_ms((uint)(target_time - substraction));
        }
    }
    clock->frame_num++;
}

void
core_set_screen(screen_t *screen)
    {_next_screen = screen;}

int
core_init(int argc, char **argv)
{
    #define PRINT_BEGIN(s) printf("%s: initializing " s "\n", __func__);
    #define PRINT_FINISH() printf("%s: done.\n", __func__);

    core_read_config("config.cfg");

    srand((uint)time(0));
    _set_dc_reason(DC_REASON_NONE);

    if (kth_pool_init(&th_pool, 2, 256) || kth_pool_run(&th_pool))
        return 1;
    if (SDL_Init(SDL_INIT_VIDEO))
        return 2;

    keyboard_state = SDL_GetKeyboardState(&num_keyboard_keys);

    int win_res = _window_init(&main_window, "MUTA v. " MUTA_VERSION_STR,
        WIN_POS_ANY, WIN_POS_ANY, game_cfg.display.win_size[0],
        game_cfg.display.win_size[1], game_cfg.display.win_fullscreen,
        game_cfg.display.win_maximized, game_cfg.display.win_windowed);

    if (win_res != 0)
    {
        DEBUG_PRINTF("%s: SDL error: %s.\n", __func__, SDL_GetError());
        return 4;
    }

    perf_clock_init(&main_perf_clock, game_cfg.fps_limit);

    if (r_init(&main_window) != 0)
        return 5;

    r_set_vsync(game_cfg.vsync);

    if (_init_slash_cmds())
        return 6;

    /* Initialize screens */
    main_menu_screen.name           = "Main menu screen";
    main_menu_screen.update         = main_menu_update;
    main_menu_screen.open           = main_menu_open;
    main_menu_screen.close          = main_menu_close;
    main_menu_screen.keydown        = main_menu_keydown;
    main_menu_screen.os_quit        = main_menu_os_quit;
    main_menu_screen.text_input     = main_menu_text_input;

    editor_screen.name              = "Editor screen";
    editor_screen.update            = editor_update;
    editor_screen.open              = editor_open;
    editor_screen.close             = editor_close;
    editor_screen.keydown           = editor_keydown;
    editor_screen.mousewheel        = editor_mousewheel;
    editor_screen.mousebuttondown   = editor_mousebuttondown;
    editor_screen.text_input        = editor_text_input;

    tile_editor_screen.name         = "Tile editor screen";
    tile_editor_screen.update       = tile_editor_update;
    tile_editor_screen.open         = tile_editor_open;
    tile_editor_screen.close        = tile_editor_close;
    tile_editor_screen.keydown      = tile_editor_keydown;
    tile_editor_screen.mousewheel   = tile_editor_mousewheel;
    //tile_editor_screen.mousebuttondown = tile_editor_mousebuttondown;
    tile_editor_screen.text_input   = tile_editor_text_input;
	tile_editor_screen.file_drop	= tile_editor_file_drop;

    sprite_editor_screen.name               = "Sprite editor screen";
	sprite_editor_screen.update             = sprite_editor_update;
	sprite_editor_screen.open               = sprite_editor_open;
	sprite_editor_screen.close              = sprite_editor_close;
	sprite_editor_screen.keydown            = sprite_editor_keydown;
	sprite_editor_screen.mousewheel         = sprite_editor_mousewheel;
	sprite_editor_screen.mousebuttonup      = sprite_editor_mousebuttonup;
	sprite_editor_screen.mousebuttondown    = sprite_editor_mousebuttondown;
	sprite_editor_screen.text_input         = sprite_editor_text_input;
	sprite_editor_screen.file_drop          = sprite_editor_file_drop;

    game_screen.name                = "Game screen";
    game_screen.update              = gamescreen_update;
    game_screen.open                = gamescreen_open;
    game_screen.close               = gamescreen_close;
    game_screen.os_quit             = gamescreen_os_quit;
    game_screen.keydown             = gamescreen_keydown;
    game_screen.keyup               = gamescreen_keyup;
    game_screen.text_input          = gamescreen_text_input;
    game_screen.mousebuttonup       = gamescreen_mousebuttonup;
    game_screen.mousebuttondown     = gamescreen_mousebuttondown;

    character_select_screen.name        = "Character screen";
    character_select_screen.open        = character_select_screen_open;
    character_select_screen.close       = character_select_screen_close;
    character_select_screen.update      = character_select_screen_update;
    character_select_screen.text_input  = character_select_screen_text_input;
    character_select_screen.keydown     = character_select_screen_keydown;

    character_create_screen.name        = "Character creation screen";
    character_create_screen.open        = character_create_screen_open;
    character_create_screen.close       = character_create_screen_close;
    character_create_screen.update      = character_create_screen_update;
    character_create_screen.text_input  = character_create_screen_text_input;
    character_create_screen.keydown     = character_create_screen_keydown;

    animator_screen.name            = "Animator screen";
    animator_screen.update          = animator_screen_update;
    animator_screen.open            = animator_screen_open;
    animator_screen.close           = animator_screen_close;
    animator_screen.keydown         = animator_screen_keydown;
    animator_screen.mousewheel      = animator_screen_mousewheel;
    animator_screen.mousebuttonup   = animator_screen_mousebuttonup;
    animator_screen.mousebuttondown = animator_screen_mousebuttondown;
    animator_screen.text_input      = animator_screen_text_input;
    animator_screen.file_drop       = animator_screen_file_drop;

    static uint8 msg_buf_mem[MUTA_MTU];
    BBUF_INIT(&_c_msg_buf_out, msg_buf_mem, MUTA_MTU);

    if (double_msg_buf_init(&_c_msgs_in, MUTA_MTU * 4))
        return 8;
    if (init_socket_api())
        return 9;

    _init_menu_window_style();
    _init_menu_button_style();

    PRINT_BEGIN("audio");

    if (initialize_audio() != 0)
        printf("Failed to initialize audio.\n");
    else
    {
        printf("Setting music volume to %f, sound volume to %f.\n",
            game_cfg.music_volume, game_cfg.sound_volume);
        set_master_volume(game_cfg.sound_volume, game_cfg.music_volume);
    }

    PRINT_FINISH();

    PRINT_BEGIN("assets");
    if (as_load()) return 10;
    if (_load_permanent_assets())
        printf("Failed to load permanent assets!\n");
    PRINT_FINISH();

    if (sprite_context_init(shared_sprite_ctx, 64) != 0)        return 11;
    if (gui_init(64, _update_gui_inputs, r_render_gui) != 0)    return 12;
    if (crypt_init())                                           return 13;

    PRINT_BEGIN("world data");
    init_world_data();
    PRINT_FINISH();

    PRINT_BEGIN("game state");
    gs_init();
    PRINT_FINISH();

    core_set_cursor("default");

    #undef PRINT_BEGIN
    #undef PRINT_FINISH

    mutex_init(&_c_sock_mutex);
    blocking_queue_init(&_network_event_queue, MAX_NETWORK_EVENTS,
        sizeof(network_event_t));
    return 0;
}

int
core_run(int argc, char **argv, screen_t *first_screen)
{
#ifdef _MUTA_DEBUG
    for (int i = 1; i < argc; ++i)
    {
        if (streq(argv[i], "-l") || streq(argv[i], "--login"))
        {
            if (argc < i + 3) break;
            if (core_auto_login_name || core_auto_login_pw)
            {
                core_auto_login_name   = 0;
                core_auto_login_pw     = 0;
                break;
            }
            if (!accut_is_name_legal(argv[i + 1]))
                break;
            core_auto_login_name    = argv[i + 1];
            core_auto_login_pw      = argv[i + 2];
        }
    }
#endif

    bind_tex_jobs();
    core_set_screen(first_screen);
    _app_running = 1;

    while (_app_running)
        core_update();

    /* Clean up */
    if (c_conn_status != CONNSTATUS_DISCONNECTED)
    {
        if (core_disconnect_from_sv() == 0)
            DEBUG_PUTS("Successfully disconnected from server.");
        else
            DEBUG_PUTS("Disconnected from server, but there were errors.");
    }

    if (save_config("config.cfg") != 0)
        LOGF("%s: failed to save configuration!\n", __func__);
    /* Gracefully shutdown threads first, because they may still have ongoing
     * asset load jobs on them. This way they don't get leaked. */
    kth_pool_shutdown(&th_pool, 1);
    kth_pool_destroy(&th_pool);
    bind_tex_jobs();
    gs_destroy();
    gui_destroy();
    as_destroy();
    r_destroy();
    free_audio();
    blocking_queue_destroy(&_network_event_queue);
    mutex_init(&_c_sock_mutex);
    _window_destroy(&main_window);
    SDL_Quit();
    return 0;
}

void
core_update()
{
    _text_input[0] = 0;
    perf_clock_tick(&main_perf_clock);
    uint32 cur_time = SDL_GetTicks();
    bind_tex_jobs();
    update_audio();
    as_update();

    if (_c_should_dc)
    {
        DEBUG_PRINTFF("disconnecting (1).\n");
        core_disconnect_from_sv();
        _set_dc_reason(DC_REASON_SERVER_CLOSED_CONNECTION);
    }

    if (IS_CONNECTION_OPEN())
    {
        network_event_t events[MAX_NETWORK_EVENTS];
        uint32 num_events = blocking_queue_pop_num(&_network_event_queue,
            MAX_NETWORK_EVENTS, events);
        _check_network_events(events, num_events);
        bbuf_t *bb = double_msg_buf_swap(&_c_msgs_in);
        if (bb->num_bytes > 0)
        {
            int left = _read_packet(bb->mem, bb->num_bytes);
            if (left >= 0)
                bbuf_cut_portion(bb, 0, bb->num_bytes - left);
            else
            {
                DEBUG_PRINTFF("disconnecting (2), left: %d.\n", left);
                core_disconnect_from_sv();
            }
        }
    }

    if (_next_screen != _cur_screen)
    {
        if (_cur_screen && _cur_screen->close)
        {
            DEBUG_PRINTF("Closing screen %s...\n", _cur_screen->name);
            _cur_screen->close();
            DEBUG_PRINTF("Successfully closed screen %s.\n",
                _cur_screen->name);
        }
        if (_next_screen && _next_screen->open)
        {
            gui_clear();
            DEBUG_PRINTF("Opening screen %s...\n", _next_screen->name);
            _next_screen->open();
            DEBUG_PRINTF("Successfully opened screen %s.\n",
                _next_screen->name);
        }
        _cur_screen = _next_screen;
    }

    SDL_Event e;
    while (SDL_PollEvent(&e))
    {
        switch (e.type)
        {
        case SDL_QUIT:
            if (_cur_screen->os_quit)
                _cur_screen->os_quit();
            else
                core_shutdown();
            break;
        case SDL_KEYDOWN:
            if (_cur_screen->keydown)
                _cur_screen->keydown(e.key.keysym.sym, 0);
            int len = (int)strlen(_text_input);
            switch (e.key.keysym.sym)
            {
            #define INSERT_GUI_CHAR(name) \
                if (len >= sizeof(_text_input) - 1) \
                    break; \
                _text_input[len]        = name; \
                _text_input[len + 1]    = 0;
            case SDLK_LEFT:
                if (keyboard_state[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_LEFT);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_LEFT);}
                break;
            case SDLK_RIGHT:
                if (keyboard_state[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_RIGHT);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_RIGHT);}
                break;
            case SDLK_BACKSPACE:
                if (keyboard_state[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_BACKSPACE);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_BACKSPACE);}
                break;
            case SDLK_DELETE:
                if (keyboard_state[SDL_SCANCODE_LCTRL])
                    {INSERT_GUI_CHAR(GUI_CHAR_LONG_DELETE);}
                else
                    {INSERT_GUI_CHAR(GUI_CHAR_DELETE);}
                break;
            case SDLK_RETURN:
                INSERT_GUI_CHAR('\n');
                break;
            #undef INSERT_GUI_CHAR
            }
            break;
        case SDL_KEYUP:
            if (_cur_screen->keyup)
                _cur_screen->keyup(e.key.keysym.sym);
            break;
        case SDL_MOUSEWHEEL:
            if (_cur_screen->mousewheel)
                _cur_screen->mousewheel(e.wheel.x, e.wheel.y);
            break;
        case SDL_TEXTINPUT:
            if (_cur_screen->text_input)
                _cur_screen->text_input(e.text.text);
            strncat(_text_input, e.text.text,
                sizeof(_text_input) - strlen(_text_input) - 1);
            break;
        case SDL_WINDOWEVENT:
            _handle_window_event(&e);
            break;
        case SDL_MOUSEBUTTONDOWN:
        {
            click_event_t ce;
            ce.button   = e.button.button;
            ce.x        = e.button.x;
            ce.y        = e.button.y;
            ce.down     = 1;
            _post_click_event(ce);
            mouse_state.buttons |= SDL_BUTTON(ce.button);
        }
            break;
        case SDL_MOUSEBUTTONUP:
        {
            click_event_t ce;
            ce.button   = e.button.button;
            ce.x        = e.button.x;
            ce.y        = e.button.y;
            ce.down     = 0;
            _post_click_event(ce);
            SET_BITFLAG_OFF(mouse_state.buttons, SDL_BUTTON(ce.button));
        }
            break;
        case SDL_DROPFILE:
            if (_cur_screen->file_drop)
                _cur_screen->file_drop(e.drop.file);
            break;
        }
    }

    /* Update mouse state */
    mouse_state.last_x  = mouse_state.x;
    mouse_state.last_y  = mouse_state.y;

    int mx, my;
    SDL_GetMouseState(&mx, &my);
    mouse_state.x = mx;
    mouse_state.y = my;

    /* Update the current screen */
    if (_cur_screen && _cur_screen->update)
        _cur_screen->update(main_perf_clock.delta);

    /* This may have been called by the screen before */
    core_execute_click_events();

    if (IS_CONNECTION_OPEN())
    {
        uint32 tp = cur_time - _c_last_send_time;
        /* Send a keep-alive message if its time for it and nothing else
         * was sent */
        if (tp >= KEEP_ALIVE_FREQ && _c_msg_buf_out.num_bytes == 0)
            clmsg_keep_alive_write(&_c_msg_buf_out);
        if (_c_msg_buf_out.num_bytes > 0)
        {
            net_send_all(_c_sock, _c_msg_buf_out.mem, _c_msg_buf_out.num_bytes);
            BBUF_CLEAR(&_c_msg_buf_out);
            _c_last_send_time = SDL_GetTicks();
        }
    }
}

int
core_shutdown()
    {_app_running = 0; return 0;}

void
core_execute_click_events()
{
    if (!_click_events.num) return;
    click_event_t *ev;
    for (uint i = 0; i < _click_events.num; ++i)
    {
        ev = &_click_events.events[i];
        if (ev->down)
        {
            if (_cur_screen->mousebuttondown)
                _cur_screen->mousebuttondown(ev->button, ev->x, ev->y);
        } else
        {
            if (_cur_screen->mousebuttonup)
                _cur_screen->mousebuttonup(ev->button, ev->x, ev->y);
        }
    }
    _click_events.num = 0;
}

float
core_compute_pos_scaled_by_target_viewport(int *x, int *y)
{
    int vp[4];
    float s = core_compute_target_viewport(vp);

    *x -= vp[0];
    *y -= vp[1];
    if (s != 1 && s != 0)
    {
        float t = 1 / s;
        *x = (int)(*x * t);
        *y = (int)(*y * t);
    }
    return s;
}

int
core_connect_to_sv(const char *acc_name, const char *pw)
{
    if (c_conn_status != CONNSTATUS_DISCONNECTED)
        return 1;
    if (!gs_initialized())
    {
        DEBUG_PUTS("Cannot connect to server: gamestate uninitialized.");
        return 2;
    }
    sv_info_parse_res_t addr_res = {0};
    if (parse_cfg_file("server_info.cfg", _read_server_info_callback, &addr_res)
    ||  !addr_res.found)
    {
        puts("server_info.cfg not found or invalid.");
        addr_init(&addr_res.addr, 127, 0, 0, 1, DEFAULT_LOGIN_PORT);
    }
    int acc_name_len    = (int)strlen(acc_name);
    int pw_len          = (int)strlen(pw);
    if (!accut_is_name_legaln(acc_name, acc_name_len))
        return 3;
    if (!accut_is_pw_legaln(pw, pw_len))
        return 4;
    strcpy(_acc_name, acc_name);
    strcpy(_pw, pw);
    c_authed        = 0;
    _c_should_dc    = 0;
    _c_sock         = KSYS_INVALID_SOCKET;
    int ret;
    _c_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (_c_sock == KSYS_INVALID_SOCKET)
        {ret = 5; goto failure;}
    if (net_make_sock_reusable(_c_sock))
        {ret = 6; goto failure;}
    BBUF_CLEAR(&_c_msg_buf_out);
    double_msg_buf_clear(&_c_msgs_in);
    /* Send the client's public key to the server at this point to begin
     * handshaking. */
    clmsg_pub_key_t s;
    if (cryptchan_init(&_c_cryptchan, s.key))
        {ret = 6; goto failure;}
    clmsg_pub_key_write(&_c_msg_buf_out, &s);
    c_conn_status                       = CONNSTATUS_CONNECTING;
    _sv_addr                            = addr_res.addr;
    core_character_creation_in_progress = 0;
    core_character_creation_error       = 0;
    core_num_shards                     = 0;
    DEBUG_PRINTF("Beginning connection attempt to: %i.%i.%i.%i...\n",
        ADDR_IP_TO_PRINTF_ARGS(&addr_res.addr));
    _network_thread_state = NETWORK_THREAD_RECV;
    blocking_queue_clear(&_network_event_queue);
    if (thread_create(&_nw_thread, _network_thread_callback, 0))
        {ret = 7; goto failure;}
    return 0;

    failure:
    {
        DEBUG_PRINTF("%s failed with code %d.\n", __func__, ret);
        close_socket(_c_sock);
        c_conn_status = CONNSTATUS_DISCONNECTED;
        return ret;
    }
}

int
core_disconnect_from_sv()
{
    DEBUG_PRINTF("Disconnecting from server.\n");
    if (c_conn_status != CONNSTATUS_DISCONNECTED)
    {
        mutex_lock(&_c_sock_mutex);
        net_close_blocking_sock(_c_shard_connect_sock);
        net_close_blocking_sock(_c_sock);
        mutex_unlock(&_c_sock_mutex);
        c_conn_status   = CONNSTATUS_DISCONNECTED;
        c_authed        = 0;
        thread_join(&_nw_thread);
        _c_should_dc    = 0;
    }
    gs_end_session();
    return 0;
}

float
core_compute_target_viewport_scale()
{
    float sx = (float)main_window.w / (float)RESOLUTION_W;
    float sy = (float)main_window.h / (float)RESOLUTION_H;
    float s = sx > sy ? sy : sx;
    return s;
}

float
core_compute_target_viewport(int *ret)
{
    float s = core_compute_target_viewport_scale();
    ret[2] = (int)(s * (float)(RESOLUTION_W));
    ret[3] = (int)(s * (float)(RESOLUTION_H));
    ret[0] = main_window.w / 2 - ret[2] / 2;
    ret[1] = main_window.h / 2 - ret[3] / 2;
    return s;
}

void
core_window_to_gui_coords(int *ret_in_x, int *ret_in_y)
{
    int     vp[4];
    float   sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.0000001f);
    *ret_in_x = (int)((float)(*ret_in_x - vp[0]) / sc);
    *ret_in_y = (int)((float)(*ret_in_y - vp[1]) / sc);
}

int
core_send_create_character_msg_to_sv(character_props_t *cp)
{
    clmsg_create_character_t s;
    s.name      = cp->name;
    s.name_len  = cp->name_len;
    s.race      = (uint8)cp->race;
    s.sex       = (uint8)cp->sex;
    byte_buf_t *bb = core_send_msg_to_sv(
        CLMSG_CREATE_CHARACTER_COMPUTE_SZ(s.name_len));
    if (!bb)
        return 1;
    clmsg_create_character_write(bb, &s);
    core_character_creation_in_progress = 1;
    core_character_creation_error       = 0;
    return 0;
}

int
core_send_log_in_character_msg(uint64 id)
{
    clmsg_log_in_character_t s;
    s.id = id;
    bbuf_t *bb = core_send_msg_to_sv(CLMSG_LOG_IN_CHARACTER_SZ);
    if (!bb)
        return 1;
    clmsg_log_in_character_write(bb, &s);
    return 0;
}

int
core_select_shard(uint32 index)
{
    if (c_conn_status != CONNSTATUS_SELECTING_SHARD)
        return 1;
    muta_assert(index < core_num_shards);
    muta_assert(c_conn_status == CONNSTATUS_SELECTING_SHARD);
    clmsg_select_shard_t s;
    s.name      = core_shards[index].name;
    s.name_len  = (uint8)strlen(s.name);
    bbuf_t *bb = core_send_msg_to_sv(CLMSG_SELECT_SHARD_COMPUTE_SZ(s.name_len));
    if (!bb)
        return 1;
    clmsg_select_shard_write(bb, &s);
    c_conn_status = CONNSTATUS_WAITING_FOR_CHARACTER_LIST;
    DEBUG_PRINTFF("Attempting to log in to shard shard %s...\n",
        core_shards[index].name);
    core_selected_shard = index;
    return 0;
}

int
core_cancel_select_shard()
{
    if (c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
        return 1;
    bbuf_t *bb = core_send_msg_to_sv(0);
    if (!bb)
        return 2;
    clmsg_cancel_select_shard_write(bb);
    c_conn_status = CONNSTATUS_SELECTING_SHARD;
    return 0;
}

int
core_cancel_character_creation()
{
    IMPLEMENTME();
    if (!core_character_creation_in_progress)
        return 1;
    core_character_creation_in_progress = 0;
    return 0;
}

int
core_register_slash_cmd(const char *name, const char *desc,
    bool32 dynamic_alloc_desc, union slash_cmd_arg_t *arg,
    int (*callback)(slash_cmd_t *cmd, union slash_cmd_arg_t arg,
        const char *args))
{
    if (!name)                                      return 1;
    if (name[0] == '/')                             return 2;
    if (strlen(name) > CORE_MAX_SLASH_CMD_NAME_LEN) return 3;
    slash_cmd_t *cmd = _find_slash_cmd(name);
    if (cmd) return 4;
    if (_slash_cmds.num == CORE_MAX_SLASH_CMDS) return 5;
    cmd = &_slash_cmds.cmds[_slash_cmds.num++];
    strcpy(cmd->name, name);
    if (!dynamic_alloc_desc)
        cmd->description = desc;
    else
        cmd->description = create_dynamic_str(desc);
    if (arg) cmd->arg = *arg;
    cmd->callback = callback;
    return 0;
}

int
core_set_cursor(const char *name)
{
    if (!name)
    {
        SDL_SetCursor(0);
        _current_cursor = 0;
        return 0;
    }
    cursor_t *cursor = as_get_cursor(name);
    if (!cursor)
        return 1;
    if (_current_cursor == cursor)
        return 0;
    SDL_SetCursor(cursor->data);
    _current_cursor = cursor;
    return 0;
}

int
core_execute_slash_cmd_from_chat(const char *msg)
{
#if 0
    muta_assert(msg && msg[0] == '/');
    char buf[CORE_MAX_SLASH_CMD_NAME_LEN + 1] = {0};
    for (const char *c = msg + 1;
         *c && *c != ' ' && *c != '\t' && c < msg + CORE_MAX_SLASH_CMD_NAME_LEN;
         ++c)
        buf[(int)(c - msg) - 1] = (char)tolower(*c);
    slash_cmd_t *cmd = _find_slash_cmd(buf);
    if (!cmd)
    {
        core_chat_printf("\"/%s\": no such command.", buf);
        return -1;
    }
    const char *args = msg + strlen(cmd->name) + 1;
    for (;*args == '\t' || *args == ' '; ++args);
    if (!cmd->callback)
        return 1;
    int r = cmd->callback(cmd, cmd->arg, args);
    if (r) DEBUG_PRINTF("Slash command %s returned code %d.\n", cmd->name, r);
#endif
    return 0;
}

static int
_window_init(window_t *win, const char *title, int x, int y, int w, int h,
    bool32 fullscreen, bool32 maximized, bool32 windowed)
{
    if (w < 1 || h < 1)
        return 1;

    int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

    if (fullscreen)
    {
        if (windowed) /* Windowed fullscreen */
        {
            flags |= SDL_WINDOW_BORDERLESS;
            flags |= SDL_WINDOW_MAXIMIZED;
        } else
            flags |= SDL_WINDOW_FULLSCREEN;
    } else
    {
        flags |= maximized ? SDL_WINDOW_MAXIMIZED : 0;
        flags |= SDL_WINDOW_RESIZABLE;
    }

    win->sdl_win = SDL_CreateWindow(title, x, y, w, h, flags);
    if (!win->sdl_win) return 1;
    SDL_GetWindowSize(win->sdl_win, &win->w, &win->h);
    return 0;
}

static void
_window_destroy(window_t *win)
{
    SDL_DestroyWindow(win->sdl_win);
    memset(win, 0, sizeof(window_t));
}

static int
_window_maximize(window_t *win)
{
    if (!win)           return 1;
    if (!win->sdl_win)  return 2;
    SDL_MaximizeWindow(win->sdl_win);
    SDL_GetWindowSize(win->sdl_win, &win->w, &win->h);
    return 0;
}

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val)
{
    game_config_t *cfg = ctx;

    if (streq(opt, "window fullscreen"))
    {
        bool32 v;
        if (!str_to_bool(val, &v)) cfg->display.win_fullscreen = v;
    } else
    if (streq(opt, "window size"))
    {
        int w, h;
        if (sscanf(val, "%dx%d", &w, &h) != 2) return;
        if (w < 1 || h < 1) return;
        cfg->display.win_size[0] = w;
        cfg->display.win_size[1] = h;
        printf("cfg: %s = %dx%d\n", opt, cfg->display.win_size[0],
            cfg->display.win_size[1]);
    } else
    if (streq(opt, "vsync"))
    {
        bool32 v;
        if (!str_to_bool(val, &v)) cfg->vsync = v;
        printf("cfg: %s = %s\n", opt, val);
    } else
    if (streq(opt, "fps limit"))
    {
        int v = atoi(val);
        if (v > 0) cfg->fps_limit = v;
    } else
    if (streq(opt, "sound volume"))
    {
        float v = (float)atoi(val) / 100.f;
        if (v >= 0.f && v <= 1.f)
            cfg->sound_volume = v;
    } else
    if (streq(opt, "music volume"))
    {
        float v = (float)atoi(val) /  100.f;
        if (v >= 0.f && v <= 1.f)
            cfg->music_volume = v;
    } else
    if (streq(opt, "sprite batch size"))
    {
        int v = atoi(val);
        if (v > 0) cfg->sprite_batch_size = v;
    } else
    if (streq(opt, "account"))
    {
        int len = (int)strlen(val);
        if (accut_is_name_legaln(val, len))
            memcpy(cfg->account_name, val, len + 1);
    } else
    if (streq(opt, "window maximized"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            cfg->display.win_maximized = v;
    } else
    if (streq(opt, "window windowed"))
    {
        bool32 v;
        if (!str_to_bool(val, &v))
            cfg->display.win_windowed = v;
    } else
    if (streq(opt, "tile texture path"))
    {
        if (str_is_valid_file_path(val))
            cfg->tile_tex_path = set_dynamic_str(cfg->tile_tex_path, val);
    } else
    if (streq(opt, "tile pixel width"))
    {
        int v = atoi(val);
        if (v > 0) cfg->tile_px_w = v;
    } else
    if (streq(opt, "tile pixel height"))
    {
        int v = atoi(val);
        if (v > 0) cfg->tile_px_h = v;
    } else
    if (streq(opt, "tile top pixel height"))
    {
        int v = atoi(val);
        if (v > 0) cfg->tile_top_px_h = v;
    } else
    if (streq(opt, "map path"))
        cfg->game_data.map_path = set_dynamic_str(cfg->game_data.map_path, val);
    if (streq(opt, "tile path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.tile_path = set_dynamic_str(
                cfg->game_data.tile_path, val);
    } else
    if (streq(opt, "player race path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.player_race_path = set_dynamic_str(
                cfg->game_data.player_race_path, val);
    } else
    if (streq(opt, "static obj path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.static_obj_path = set_dynamic_str(
                cfg->game_data.static_obj_path, val);
    } else
    if (streq(opt, "dynamic obj path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.dynamic_obj_path = set_dynamic_str(
                cfg->game_data.dynamic_obj_path, val);
    } else
    if (streq(opt, "creature path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.creature_path = set_dynamic_str(
                cfg->game_data.creature_path, val);
    } else
    if (streq(opt, "map db path"))
    {
        if (str_is_valid_file_path(val))
            cfg->game_data.map_db_path = set_dynamic_str(
                cfg->game_data.map_db_path, val);
    }
}

static void
_read_tile_cfg_callback(void *ctx, const char *opt, const char *val)
{
	game_config_t *cfg = ctx;
	if (streq(opt, "tile texture path"))
	{
		if (str_is_valid_file_path(val))
			cfg->tile_tex_path = set_dynamic_str(cfg->tile_tex_path, val);
	} else
	if (streq(opt, "tile pixel width"))
	{
		int v = atoi(val);
		if (v > 0) cfg->tile_px_w = v;
	} else
	if (streq(opt, "tile pixel height"))
	{
		int v = atoi(val);
		if (v > 0) cfg->tile_px_h = v;
	} else
	if (streq(opt, "tile top pixel height"))
	{
		int v = atoi(val);
		if (v > 0) cfg->tile_top_px_h = v;
	}
}

static void
_read_server_info_callback(void *ctx, const char *opt, const char *val)
{
    sv_info_parse_res_t *r = ctx;
    if (streq(opt, "hostname") && !addr_init_from_str(&r->addr, val,
        DEFAULT_LOGIN_PORT))
        r->found = 1;
}

static thread_ret_t
_network_thread_callback(void *args)
{
    DEBUG_PRINTF("Connecting to server on port %u...\n", _sv_addr.port);
    if (net_connect(_c_sock, _sv_addr) || net_disable_nagle(_c_sock))
    {
        char b[256];
        sock_err_str(b, 256);
        DEBUG_PRINTF("Failed to connect: %s\n", b);
        close_socket(_c_sock);
        c_conn_status = CONNSTATUS_DISCONNECTED;
        return 0;
    }
    DEBUG_PUTS("Handshaking with server...");
    c_conn_status       = CONNSTATUS_HANDSHAKING;
    _c_conn_begin_time  = SDL_GetTicks();
    socket_t sock = _c_sock;
    while (IS_CONNECTION_OPEN() && !_c_should_dc)
    {
        int32 state = interlocked_compare_exchange_int32(&_network_thread_state,
            -1, -1);
        switch (state)
        {
        case NETWORK_THREAD_RECV:
        {
            uint8   buf[MUTA_MTU];
            int     num_bytes = net_recv(sock, buf, sizeof(buf));
            if (num_bytes <= 0)
            {
                state = interlocked_compare_exchange_int32(
                    &_network_thread_state, -1, -1);
                if (state == NETWORK_THREAD_CONNECT_TO_SHARD)
                    break;
                _c_should_dc = 1;
                DEBUG_PRINTFF("disconnected, recv() returned %d\n", num_bytes);
                break;
            }
            bbuf_t *bb = double_msg_buf_begin_write(&_c_msgs_in, num_bytes);
            if (bb && !dynamic_bbuf_write_bytes(bb, buf, num_bytes))
                double_msg_buf_finalize_write(&_c_msgs_in);
        }
            break;
        case NETWORK_THREAD_CONNECT_TO_SHARD:
        {
            mutex_lock(&_c_sock_mutex);
            sock = _c_shard_connect_sock;
            mutex_unlock(&_c_sock_mutex);
            if (net_connect(sock, _c_shard_addr))
                goto fail;
            mutex_lock(&_c_sock_mutex);
            close_socket(_c_sock);
            _c_sock                 = sock;
            _c_shard_connect_sock   = KSYS_INVALID_SOCKET;
            mutex_unlock(&_c_sock_mutex);
            interlocked_compare_exchange_int32(&_network_thread_state,
                NETWORK_THREAD_RECV, NETWORK_THREAD_CONNECT_TO_SHARD);
            DEBUG_PRINTFF("Successfully connected to shard.\n");
            network_event_t network_event;
            network_event.type = NETWORK_EVENT_START_HANDSHAKE_WITH_SHARD;
            blocking_queue_push(&_network_event_queue, &network_event);
            break;
            fail:
                DEBUG_PRINTFF("Failed to connect to shard, retrying in 1 "
                    "second...\n");
                sleep_ms(1000);
        }
            break;
        }
    }
    return 0;
}

static byte_buf_t *
_send_var_encrypted_msg_to_sv(int len)
{
    int num_bytes = len + sizeof(msg_sz_t) + CRYPT_MSG_ADDITIONAL_BYTES;
    return core_send_msg_to_sv(num_bytes);
}

static int
_read_packet(uint8 *pckt, int pckt_len)
{
    byte_buf_t  bb          = BBUF_INITIALIZER(pckt, pckt_len);
    int         dc          = 0;
    bool32      incomplete  = 0;
    msg_type_t  type;

    #define HANDLE_MSG(upper_, lower_) \
        case upper_: \
        { \
            lower_##_t s; \
            incomplete = lower_##_read(&bb, &s); \
            if (!incomplete) dc = _handle_##lower_(&s); \
        } \
            break;

    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, msg_type_t, &type);
        switch (type)
        {
        HANDLE_MSG(SVMSG_PUB_KEY, svmsg_pub_key);
        HANDLE_MSG(SVMSG_STREAM_HEADER, svmsg_stream_header);
        HANDLE_MSG(SVMSG_CHARACTER_LIST, svmsg_character_list);
        HANDLE_MSG(SVMSG_CREATE_CHARACTER_FAIL, svmsg_create_character_fail);
        HANDLE_MSG(SVMSG_NEW_CHARACTER_CREATED, svmsg_new_character_created);
        case SVMSG_LOGIN_RESULT:
        {
            DEBUG_PUTS("SVMSG_LOGIN_RESULT");
            svmsg_login_result_t s;
            incomplete = svmsg_login_result_read_const_encrypted(&bb,
                &_c_cryptchan, &s);
            if (!incomplete) dc = _handle_svmsg_login_result(&s);
        }
            break;
        case SVMSG_CHAR_LOGIN_FAIL:
        {
            DEBUG_PUTS("SVMSG_CHAR_LOGIN_FAIL");
            svmsg_char_login_fail_t s;
            incomplete = svmsg_char_login_fail_read_const_encrypted(&bb,
                &_c_cryptchan, &s);
            if (!incomplete) dc = _handle_svmsg_char_login_fail(&s);
        }
            break;
        case SVMSG_PLAYER_INIT_DATA:
        {
            DEBUG_PUTS("SVMSG_PLAYER_INIT_DATA");
            svmsg_player_init_data_t s;
            incomplete = svmsg_player_init_data_read_var_encrypted(&bb,
                c_cryptchan, &s);
            if (!incomplete) dc = _handle_svmsg_player_init_data(&s);
        }
            break;
        case SVMSG_SHARD_LIST_ITEM:
        {
            DEBUG_PUTS("SVMSG_SHARD_LIST_ITEM");
            svmsg_shard_list_item_t s;
            incomplete = svmsg_shard_list_item_read(&bb, &s);
            if (!incomplete) dc = _handle_svmsg_shard_list_item(&s);
        }
            break;
        case SVMSG_SHARD_SELECT_FAIL:
        {
            DEBUG_PUTS("SVMSG_SHARD_SELECT_FAIL");
            svmsg_shard_select_fail_t s;
            incomplete = svmsg_shard_select_fail_read(&bb, &s);
            if (!incomplete) dc = _handle_svmsg_shard_select_fail(&s);
        }
            break;
        case SVMSG_SHARD_SELECT_SUCCESS:
        {
            DEBUG_PUTS("SVMSG_SHARD_SELECT_SUCCESS");
            svmsg_shard_select_success_t s;
            incomplete = svmsg_shard_select_success_read_var_encrypted(&bb,
                &_c_cryptchan, &s);
            if (!incomplete) dc = _handle_svmsg_shard_select_success(&s);
        }
            break;
        default:
        {
            int r = gs_read_packet(type, &bb);
            switch (r)
            {
            case GS_PACKET_OK:                              break;
            case GS_PACKET_INCOMPLETE:      incomplete = 1; break;
            case GS_PACKET_DISCONNECT:      dc = 1;         break;
            case GS_PACKET_UNDEFINED_MSG:
                DEBUG_PRINTFF("Unrecognized packet id %u.\n", type);
                dc = 1;
                break;
            }
        }
        }

        if (dc || incomplete < 0)
        {
            DEBUG_PRINTFF("bad packet, type %d, incomplete %d, "
                "dc %d\n", (int)type, incomplete, dc);
            return -1;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static slash_cmd_t *
_find_slash_cmd(const char *name)
{
    for (int i = 0; i < _slash_cmds.num; ++i)
        if (streq(_slash_cmds.cmds[i].name, name))
            return &_slash_cmds.cmds[i];
    return 0;
}

static int
_handle_svmsg_pub_key(svmsg_pub_key_t *s)
{
    clmsg_stream_header_t fwd;
    if (cryptchan_cl_store_pub_key(&_c_cryptchan, s->key, fwd.header))
        return 1;
    bbuf_t *bb = core_send_msg_to_sv(CLMSG_STREAM_HEADER_SZ);
    if (!bb)
        return 2;
    if (clmsg_stream_header_write(bb, &fwd))
        return 3;
    DEBUG_PRINTF("Received public key.\n");
    return 0;
}

static int
_handle_svmsg_stream_header(svmsg_stream_header_t *s)
{
    if (cryptchan_store_stream_header(&_c_cryptchan, s->header))
        return 1;
    if (!cryptchan_is_encrypted(&_c_cryptchan))
        return 2;
    if (c_conn_status == CONNSTATUS_HANDSHAKING)
    {
        /* Send login request*/
        clmsg_login_request_t fwd;
        fwd.acc_name        = _acc_name;
        fwd.pw              = _pw;
        fwd.acc_name_len    = (uint8)strlen(_acc_name);
        fwd.pw_len          = (uint8)strlen(_pw);
        DEBUG_PRINTF("Attempting to log in as %s...\n", fwd.acc_name);
        bbuf_t *bb = _send_var_encrypted_msg_to_sv(
            CLMSG_LOGIN_REQUEST_COMPUTE_SZ(fwd.acc_name_len, fwd.pw_len));
        if (!bb)
            return 3;
        if (clmsg_login_request_write_var_encrypted(bb, &_c_cryptchan, &fwd))
            return 4;
        DEBUG_PUTS("Sent login request to server.");
    } else
    {
        clmsg_auth_proof_t p_msg;
        p_msg.account_name      = _acc_name;
        p_msg.shard_name        = core_shards[core_selected_shard].name;
        p_msg.account_name_len  = (uint8)strlen(p_msg.account_name);
        p_msg.shard_name_len    = (uint8)strlen(p_msg.shard_name);
        memcpy(p_msg.token, _c_auth_token, AUTH_TOKEN_SZ);
        bbuf_t *bb = _send_var_encrypted_msg_to_sv(
            CLMSG_AUTH_PROOF_COMPUTE_SZ(p_msg.account_name_len,
                p_msg.shard_name_len));
        if (!bb)
            return 5;
        int r = clmsg_auth_proof_write_var_encrypted(bb, &_c_cryptchan, &p_msg);
        muta_assert(!r);
    }
    DEBUG_PUTS("Received stream header");
    return 0;
}

static int
_handle_svmsg_char_login_fail(svmsg_char_login_fail_t *s)
{
    DEBUG_PRINTF("Character login failed (server) - code: %d\n",
        (int)s->reason);
    _set_dc_reason(DC_REASON_CHAR_LOGIN_FAIL);
    return 1;
}

static int
_handle_svmsg_login_result(svmsg_login_result_t *s)
{
    if (c_conn_status != CONNSTATUS_HANDSHAKING)
        return 1;
    if (s->result != ACCUT_ACC_LOGIN_OK)
    {
        DEBUG_PRINTF("Server rejected login.");
        return 2;
    }
    c_authed        = 1;
    c_conn_status   = CONNSTATUS_WAITING_FOR_SHARD_LIST;
    DEBUG_PUTS("Successfully logged in to server.");
    return 0;
}

static int
_handle_svmsg_character_list(svmsg_character_list_t *s)
{
    if (c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
        return 1;
    uint32 num_chars = (uint32)s->ids_len;
    if (num_chars > MAX_CHARACTERS_PER_ACC)
        return 2;
    if (s->races_len != num_chars || s->sexes_len != num_chars ||
        s->name_indices_len != num_chars)
        return 3;
    uint32 num_names = 0;
    for (uint32 i = 0; i < s->names_len; ++i)
        if (!s->names[i])
            num_names++;
    if (num_names != num_chars)
        return 4;
    for (uint32 i = 0; i < num_chars; ++i)
        if (s->name_indices[i] >= s->names_len)
            return 5;
    core_num_character_props = 0;
    uint32 i = 0;
    for (; i < num_chars; ++i)
    {
        character_props_t   *cp     = &core_character_props[i];
        const char          *name   = s->names + s->name_indices[i];
        uint32              len     = (uint32)strlen(name);
        if (!accut_check_character_name(name, len))
            return 6;
        memcpy(cp->name, name, len + 1);
        cp->name_len    = len;
        cp->id          = s->ids[i];
        cp->race        = s->races[i];
        cp->sex         = s->sexes[i];
    }
    core_num_character_props    = i;
    c_conn_status               = CONNSTATUS_CONNECTED;
    return 0;
}

static int
_handle_svmsg_create_character_fail(svmsg_create_character_fail_t *s)
{
    if (!core_character_creation_in_progress)
    {
        DEBUG_PRINTFF("received without creation in progress!\n");
        return 0;
    }
    core_character_creation_in_progress = 0;
    core_character_creation_error       = s->reason;
    return 0;
}

static int
_handle_svmsg_new_character_created(svmsg_new_character_created_t *s)
{
    if (core_num_character_props == MAX_CHARACTERS_PER_ACC)
        return 1;
    muta_assert(s->name_len <= MAX_CHARACTER_NAME_LEN);
    character_props_t *cp = &core_character_props[core_num_character_props++];
    memcpy(cp->name, s->name, s->name_len);
    cp->name[s->name_len] = 0;
    cp->id          = s->id;
    cp->name_len    = s->name_len;
    cp->race        = s->race;
    cp->sex         = s->sex;
    core_character_creation_in_progress = 0;
    core_character_creation_error       = 0;
    return 0;
}

static int
_handle_svmsg_player_init_data(svmsg_player_init_data_t *s)
{
    char name[256];
    memcpy(name, s->name, s->name_len);
    name[s->name_len] = 0;
    return gs_begin_session(s->map_id, s->char_id, name, 0 /* Race */, s->x,
        s->y, s->z, s->dir);
}

static int
_handle_svmsg_shard_list_item(svmsg_shard_list_item_t *s)
{
    if (c_conn_status != CONNSTATUS_WAITING_FOR_SHARD_LIST
    &&  c_conn_status != CONNSTATUS_SELECTING_SHARD
    &&  c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
    {
        DEBUG_PRINTFF("Error: connection status was %d.\n", c_conn_status);
        return 1;
    }
    muta_assert(s->name_len <= MAX_SHARD_NAME_LEN);
    char name[MAX_SHARD_NAME_LEN + 1];
    memcpy(name, s->name, s->name_len);
    name[s->name_len] = 0;
    if (!str_is_ascii(name))
        return 2;
    for (uint32 i = 0; i < core_num_shards; ++i)
        if (streq(core_shards[i].name, name))
        {
            core_shards[i].online = s->is_online;
            if (c_conn_status == CONNSTATUS_WAITING_FOR_CHARACTER_LIST &&
                i == core_selected_shard)
                c_conn_status = CONNSTATUS_SELECTING_SHARD;
            return 0;
        }
    if (core_num_shards == CORE_MAX_SHARDS)
        return 3;
    shard_info_t *si = &core_shards[core_num_shards++];
    memcpy(si->name, name, s->name_len + 1);
    si->online = s->is_online;
    if (c_conn_status == CONNSTATUS_WAITING_FOR_SHARD_LIST)
        c_conn_status = CONNSTATUS_SELECTING_SHARD;
    return 0;
}

static int
_handle_svmsg_shard_select_fail(svmsg_shard_select_fail_t *s)
{
    if (c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST
    && c_conn_status != CONNSTATUS_SELECTING_SHARD)
        return 1;
    c_conn_status = CONNSTATUS_SELECTING_SHARD;
    return 0;
}

static int
_handle_svmsg_shard_select_success(svmsg_shard_select_success_t *s)
{
    if (c_conn_status != CONNSTATUS_WAITING_FOR_CHARACTER_LIST)
        return 1;
    DEBUG_PRINTFF("Successfully selected a shard. Disconnecting from login "
        "server and beginning to connect to shard.\n");
    muta_assert(_network_thread_state == NETWORK_THREAD_RECV);
    _c_shard_addr.ip    = s->ip;
    _c_shard_addr.port  = DEFAULT_CLIENT_PORT;


    socket_t socket = net_tcp_ipv4_sock();
    if (socket == KSYS_INVALID_SOCKET)
        return 2;
    if (net_disable_nagle(socket))
    {
        close_socket(socket);
        return 3;
    }
    mutex_lock(&_c_sock_mutex);
    _c_shard_connect_sock = socket;
    mutex_unlock(&_c_sock_mutex);
    interlocked_compare_exchange_int32(&_network_thread_state,
        NETWORK_THREAD_CONNECT_TO_SHARD, NETWORK_THREAD_RECV);
    net_close_blocking_sock(_c_sock);
    cryptchan_clear(&_c_cryptchan);
    _c_sock = KSYS_INVALID_SOCKET;
    memcpy(_c_auth_token, s->token, AUTH_TOKEN_SZ);
    return 0;
}

static inline void
_store_sv_pos(int32 x, int32 y, int8 z)
{
    _c_last_sv_pos.x    = x;
    _c_last_sv_pos.y    = y;
    _c_last_sv_pos.z    = z;
    _c_have_sv_pos      = 1;
}

static void
_init_menu_window_style()
{
    gui_win_style_t         *s  = &_menu_window_style;
    gui_win_state_style_t   *ss = &s->active;
    ss->bg_col[3]                       = 255;
    ss->border.color[0]                 = 25;
    ss->border.color[1]                 = 25;
    ss->border.color[2]                 = 25;
    ss->border.color[3]                 = 255;
    ss->border.widths[GUI_EDGE_TOP]     = 4;
    ss->border.widths[GUI_EDGE_BOTTOM]  = 4;
    ss->border.widths[GUI_EDGE_LEFT]    = 4;
    ss->border.widths[GUI_EDGE_RIGHT]   = 4;
    gui_win_state_style_t *ssb = &s->hovered;
    memcpy(ssb, ss, sizeof(gui_win_state_style_t));
    ssb = &s->inactive;
    memcpy(ssb, ss, sizeof(gui_win_state_style_t));
}

static void
_init_menu_button_style()
{
    gui_button_style_t *s = &_menu_button_style;
    *s = gui_create_button_style();
    SET_BYTE_COLOR(s->normal.bg_col,  13, 13, 13, 255);
    SET_BYTE_COLOR(s->hovered.bg_col, 13, 43, 43, 255);
    SET_BYTE_COLOR(s->pressed.bg_col, 13, 83, 13, 255);
    SET_BYTE_COLOR(s->normal.border.color,  13, 13, 13, 255);
    SET_BYTE_COLOR(s->hovered.border.color, 33, 33, 33, 255);
    SET_BYTE_COLOR(s->pressed.border.color, 43, 43, 43, 255);
}

static void
_update_gui_inputs(gui_input_state_t *is)
{
    int     vp[4];
    float   sc = core_compute_target_viewport(vp);
    sc = MAX(sc, 0.0000001f);
    uint32 s    = mouse_state.buttons;
    is->mx      = (int)((float)(mouse_state.x - vp[0]) / sc);
    is->my      = (int)((float)(mouse_state.y - vp[1]) / sc);
    is->buttons = 0;
    is->buttons |= ((s & BUTTON_BIT(BUTTON_LEFT))  ? GUI_MB_LEFT   : 0);
    is->buttons |= ((s & BUTTON_BIT(BUTTON_RIGHT)) ? GUI_MB_RIGHT  : 0);
    is->buttons |= ((s & BUTTON_BIT(BUTTON_MIDDLE))? GUI_MB_MIDDLE : 0);
    is->coord_space[0] = 0;
    is->coord_space[1] = 0;
    is->coord_space[2] = RESOLUTION_W;
    is->coord_space[3] = RESOLUTION_H;
    strncpy(is->text_input, _text_input, sizeof(is->text_input) - 1);
    is->delta_time = (float)main_perf_clock.delta;
}

static int
_load_permanent_assets()
{
    FIXME();
    icons_ss = as_claim_spritesheet_by_name(
        "assets/itemspritesheet_1.ss", 1);
    if (icons_ss == 0)
    {
        DEBUG_PRINTF("Failed to load icons_ss\n");
        return 1;
    }
    return 0;
}

static void
_set_dc_reason(int reason)
{
    switch (reason)
    {
    case DC_REASON_NONE:
        core_dc_reason_str = "No reason.";
        break;
    case DC_REASON_CHAR_LOGIN_FAIL:
        core_dc_reason_str = "Character login failed.";
        break;
    case DC_REASON_SERVER_CLOSED_CONNECTION:
        core_dc_reason_str = "Server closed connection.";
        break;
    default:
        muta_assert(0);
    }
    core_dc_reason = reason;
}

static int
_post_click_event(click_event_t ev)
{
    if (_click_events.num == CORE_MAX_CLICK_EVENTS)
        return 1;
    _click_events.events[_click_events.num++] = ev;
    return 0;
}

static void
_handle_window_event(SDL_Event *ev)
{
    switch (ev->window.event)
    {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
    {
        main_window.w = ev->window.data1;
        main_window.h = ev->window.data2;
        r_viewport(0, 0, main_window.w, main_window.h);
        r_scissor(0, 0, main_window.w, main_window.h);
        lui_event_t lui_event;
        lui_event.type = LUI_EVENT_APP_WINDOW_SIZE;
        lui_event.app_window.w = ev->window.data1;
        lui_event.app_window.w = ev->window.data2;
        lui_post_event(&lui_event);
    }
        break;
    case SDL_WINDOWEVENT_MINIMIZED:
        break;
    case SDL_WINDOWEVENT_MAXIMIZED:
        break;
    }
}

static int
_init_slash_cmds()
{
    core_register_slash_cmd("exit", "Exit the game.", 0, 0, _slash_cmd_exit);
    uint num_emotes = emote_num_emotes();
    for (uint i = 0; i < num_emotes; ++i)
    {
        union slash_cmd_arg_t arg;
        arg.arg_uint64 = (uint64)i;
        core_register_slash_cmd(emote_get_name_by_id(i), 0, 0, &arg,
            _slash_cmd_emote);
    }
    return 0;
}

static int
_slash_cmd_exit(slash_cmd_t *cmd, union slash_cmd_arg_t arg, const char *args)
{
    if (args && args[0]) return 1;
    core_shutdown();
    return 0;
}

static int
_slash_cmd_emote(slash_cmd_t *cmd, union slash_cmd_arg_t arg, const char *args)
{
    clmsg_player_emote_t s;
    s.emote_id = (emote_id_t)arg.arg_uint64;
    entity_t *t = 0;//gamescreen_get_player_target();
    DEBUG_PRINTFF("EMOTE CMD TARGETING NEEDS TO BE FIXED\n");
    if (t && t->type == ENT_TYPE_PLAYER)
    {
        s.target_id     = t->type_data.player.id;
        s.have_target   = 1;
    } else
        s.have_target = 0;
    bbuf_t *bb = core_send_msg_to_sv(CLMSG_PLAYER_EMOTE_SZ);
    if (!bb) return 2;
    clmsg_player_emote_write(bb, &s);
    return 0;
}

static void
_check_network_events(network_event_t *events, uint32 num_events)
{
    for (uint32 i = 0; i < num_events; ++i)
    {
        switch (events[i].type)
        {
        case NETWORK_EVENT_START_HANDSHAKE_WITH_SHARD:
        {
            muta_assert(!cryptchan_is_encrypted(&_c_cryptchan));
            clmsg_pub_key_t k_msg;
            cryptchan_init(&_c_cryptchan, k_msg.key);
            bbuf_t *bb = core_send_msg_to_sv(CLMSG_PUB_KEY_SZ);
            if (!bb)
                return;
            clmsg_pub_key_write(bb, &k_msg);
        }
            break;
        }
    }
}
