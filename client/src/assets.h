#ifndef MUTA_ASSETS_H
#define MUTA_ASSETS_H

#include "bitmap.h"

/* Forward declaration(s) */
typedef struct tex_t             tex_t;
typedef struct sfont_t           sfont_t;
typedef struct iso_anim_t        iso_anim_t;
typedef struct ae_layer_t        ae_layer_t;
typedef struct ae_frame_t        ae_frame_t;
typedef struct ae_animation_t    ae_animation_t;
typedef struct animated_entity_t animated_entity_t;

/* Types defined here */
typedef struct tex_job_t        tex_job_t;
typedef struct tex_asset_t      tex_asset_t;
typedef struct spritesheet_t    spritesheet_t;
typedef struct iso_anim_asset_t iso_anim_asset_t;
typedef struct anim_asset_t     anim_asset_t;
typedef struct ae_asset_t       ae_asset_t;
typedef struct ae_set_t         ae_set_t;
typedef struct sound_t          sound_t;
typedef struct sound_asset_t    sound_asset_t;
typedef struct cursor_t         cursor_t;

#define MATLOCK_PATH    0
#define MATLOCK_ID      0xFFFFFFFF

enum character_animations
{
    CHANIM_IDLE,
    CHANIM_WALK,
    CHANIM_MELEE_ATTACK,

    NUM_CHANIMS
};

#define INVALID_CHANIM NUM_CHANIMS

enum ae_layer_type
{
    LAYER_BODY = 0,
    LAYER_HEAD,
    LAYER_LHAND,
    LAYER_RHAND,
    LAYER_LEGS,
    LAYER_FEET,

    LAYER_BODY_ARMOUR,
    LAYER_HEAD_ARMOUR,
    LAYER_LHAND_ARMOUR,
    LAYER_RHAND_ARMOUR,
    LAYER_LEGS_ARMOUR,
    LAYER_FEET_ARMOUR,

    LAYER_LHAND_ITEM,
    LAYER_RHAND_ITEM,

    NUM_LAYER_TYPES
};

struct tex_job_t
{
    tex_asset_t *ta;
};

struct tex_asset_t
{
    tex_t       tex;
    img_t       bitmap;
    uint32      user_count;
    uint        keep_loaded:1;
    uint        keep_bitmap:1;
    int         min_filter:2;
    int         mag_filter:2;
    uint        loading:1;
    uint        loaded:1;
    int         wrapping:4;
    uint        in_unclaimed_queue:1;
};

struct iso_anim_asset_t
{
    iso_anim_t  anim;
    uint32      user_count;
};

struct spritesheet_t
{
    tex_asset_t         *ta;
    float               (*clips)[4];
    char                **clip_names;
    int                 num_clips;
    uint32              user_count;
    int8                keep_loaded;
};

struct anim_asset_t
{
    anim_t      anim;
    uint32      user_count;
    int8        keep_loaded;
};

struct ae_set_t
{
    dchar *str_id;
    dchar *idle;
    dchar *walk;
};

struct sound_t
{
    uint buffer;
};

struct sound_asset_t
{
    sound_t     sound;
    uint32      user_count;
    uint        keep_loaded:1;
    uint        loaded:1;
    uint        loading:1;
    uint        errors_loading:1;
    uint        in_unclaimed_queue:1;
};

struct cursor_t
{
    dchar   *name;
    void    *data;
};

struct ae_layer_t
{
    tex_asset_t *ta;
    uint8        color[4];
    float        clip[4];
    float        ox, oy;
    float        rot;
    int          flip;
    int          type;
};

struct ae_frame_t
{
    ae_layer_t *layers;
    int         num_layers;
    float       ox, oy;
};

struct ae_animation_t
{
    ae_frame_t  *frames;
    int         num_frames;
};

struct animated_entity_t
{
    ae_animation_t  anims[8];
    ae_animation_t  *cur_anim;
};

struct ae_asset_t
{
    animated_entity_t   ae;
    uint32              user_count;
    int8                keep_loaded;
};

extern sfont_t  sf_default;
extern sfont_t  sf_fancy;
extern sfont_t  sf_fancy_small;

int
as_load();

void
as_destroy();

void
as_update();

int
bind_tex_jobs();

char *
get_layer_type_string(int layer);

/* Claiming and unclaiming functions:
 * Each asset is reference-counted. If the asset is not marked as "keep loaded",
 * it may be unloaded when the reference count reaches zero. Because of this,
 * an unclaim function must be called when an asset is no longer needed */

tex_asset_t *
as_claim_tex_by_name(const char *name, bool32 async);

tex_asset_t *
as_claim_tex_by_id(uint32 id, bool32 async);

void
as_unclaim_tex_asset(tex_asset_t *t);

anim_asset_t *
as_claim_anim_asset_by_name(const char *name, bool32 async);

anim_asset_t *
as_claim_anim_asset_by_id(uint32 id, bool32 async);

void
as_unclaim_anim_asset(anim_asset_t *a);

ae_asset_t *
as_claim_ae_by_name(const char *name, bool32 async);

ae_asset_t *
as_claim_ae_by_id(uint32 id, bool32 async);

void
as_unclaim_ae(ae_asset_t *ae);

ae_set_t *
as_get_ae_set(const char *name);

sfont_t *
as_get_font(const char *name);

sfont_t *
as_get_default_font();

sfont_t *
as_get_default_pixel_font();

spritesheet_t *
as_claim_spritesheet_by_name(const char *name, bool32 async);

spritesheet_t *
as_claim_spritesheet_by_id(uint32 id, bool32 async);

void
as_unclaim_spritesheet(spritesheet_t *ss);

sound_asset_t *
as_claim_sound_by_name(const char *name, bool32 async);

sound_asset_t *
as_claim_sound_by_id(uint32 id, bool32 async);

void
as_unclaim_sound(sound_asset_t *sa);

cursor_t *
as_get_cursor(const char *name);

/* Helper functions for various asset types: */

const dchar *
tex_asset_get_path(tex_asset_t *ta);

const dchar *
anim_asset_get_path(anim_asset_t *aa);

const dchar *
ae_asset_get_path(ae_asset_t *aa);

const dchar *
spritesheet_get_path(spritesheet_t *ss);

const dchar *
sound_asset_get_path(sound_asset_t *sa);

static inline tex_t *
spritesheet_get_tex(spritesheet_t *ss);

static inline float *
spritesheet_get_clip(spritesheet_t *ss, int32 index);

float *
spritesheet_get_clip_by_name(spritesheet_t *ss, const char *name);

static inline float
spritesheet_get_clip_dim(spritesheet_t *ss, int clip_index, int dim);
/* Get a single dimension from a spritesheet, or 0 if something goes wrong */

static inline float
spritesheet_clip_w(spritesheet_t *ss, int clip_index);

static inline float
spritesheet_clip_h(spritesheet_t *ss, int clip_index);

#define tex_asset_tex(ta_)  ((ta_) ? &(ta_)->tex  : 0)
#define tex_asset_w(ta_)    ((ta_) ? (ta_)->tex.w : 0)
#define tex_asset_h(ta_)    ((ta_) ? (ta_)->tex.h : 0)
#define ta_tex(ta_) tex_asset_tex(ta_)

static inline tex_t *
get_spritesheet_tex(spritesheet_t *ss);

static inline tex_t *
spritesheet_get_tex(spritesheet_t *ss)
{
    if (!ss || !ss->ta) return 0;
    return &ss->ta->tex;
}

static inline float *
spritesheet_get_clip(spritesheet_t *ss, int32 index)
{
    if (!ss) return 0;
    if (index >= 0 && index < ss->num_clips)
        return ss->clips[index];
    return 0;
}

static inline float
spritesheet_get_clip_dim(spritesheet_t *ss, int clip_index, int dim)
{
    if (!ss)                                            return 0;
    if (clip_index < 0 || clip_index >= ss->num_clips)  return 0;
    if (dim < 0 || dim > 3)                             return 0;
    return ss->clips[clip_index][dim];
}

static inline float
spritesheet_clip_w(spritesheet_t *ss, int clip_index)
{
    if (ss && clip_index >= 0 && clip_index < ss->num_clips)
        return ss->clips[clip_index][2] - ss->clips[clip_index][0];
    return 0;
}

static inline float
spritesheet_clip_h(spritesheet_t *ss, int clip_index)
{
    if (ss && clip_index >= 0 && clip_index < ss->num_clips)
        return ss->clips[clip_index][3] - ss->clips[clip_index][1];
    return 0;
}

static inline tex_t *
get_spritesheet_tex(spritesheet_t *ss)
    {return ss ? tex_asset_tex(ss->ta) : 0;}

#endif /* MUTA_ASSETS_H */
