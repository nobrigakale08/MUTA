#include "hotkey.h"
#include "../../shared/containers.h"
#include "core.h"
#include "lui.h"

DYNAMIC_HASH_TABLE_DEFINITION(str_hk_action_table, hk_action_t*, const char*,
    uint64, fnv_hash32_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(uint64_hk_action_table, hk_action_t*, uint64,
    uint64, HASH_FROM_NUM, 2);
DYNAMIC_HASH_TABLE_DEFINITION(str_int_table, int, const char*,
    uint32, fnv_hash32_from_str, 2);
DYNAMIC_HASH_TABLE_DEFINITION(int_str_table, const char*, int,
    int, HASH_FROM_NUM, 2);

static obj_pool_t               _action_pool;
static str_hk_action_table_t    _action_table;
static uint64_hk_action_table_t _int_action_table;
static str_int_table_t          _key_str_enum_table;
static int_str_table_t          _key_enum_str_table;
static hk_action_t              **_all_actions;
static hk_action_t **_repeat_actions; /* A separate array is kept for polling */

static inline hk_action_t *
_get_action_by_name(const char *name)
{
    hk_action_t **ret = str_hk_action_table_get_ptr(&_action_table, name);
    return ret ? *ret : 0;
}

static int
_on_action_def(void *ctx, const char *def, const char *val);

static int
_on_action_val(void *ctx, const char *def, const char *val);

static uint64
_hash_from_key_combo(int key_enum, int mods);

static void
_create_key_enum(const char *name, int key_enum)
{
    str_int_table_einsert(&_key_str_enum_table, name, key_enum);
    int_str_table_einsert(&_key_enum_str_table, key_enum, name);
}

int
hk_init()
{
    str_hk_action_table_einit(&_action_table, 256);
    uint64_hk_action_table_einit(&_int_action_table, 256);
    obj_pool_init(&_action_pool, 256, sizeof(hk_action_t));
    str_int_table_einit(&_key_str_enum_table, 128);
    int_str_table_einit(&_key_enum_str_table, 128);
    darr_reserve(_all_actions, 128);
    darr_reserve(_repeat_actions, 32);

    _create_key_enum("Q", KEYCODE(q));
    _create_key_enum("W", KEYCODE(w));
    _create_key_enum("E", KEYCODE(e));
    _create_key_enum("R", KEYCODE(r));
    _create_key_enum("T", KEYCODE(t));
    _create_key_enum("Y", KEYCODE(y));
    _create_key_enum("U", KEYCODE(u));
    _create_key_enum("I", KEYCODE(i));
    _create_key_enum("O", KEYCODE(o));
    _create_key_enum("P", KEYCODE(p));
    _create_key_enum("A", KEYCODE(a));
    _create_key_enum("S", KEYCODE(s));
    _create_key_enum("D", KEYCODE(d));
    _create_key_enum("F", KEYCODE(f));
    _create_key_enum("G", KEYCODE(g));
    _create_key_enum("H", KEYCODE(h));
    _create_key_enum("J", KEYCODE(j));
    _create_key_enum("K", KEYCODE(k));
    _create_key_enum("L", KEYCODE(l));
    _create_key_enum("Z", KEYCODE(z));
    _create_key_enum("X", KEYCODE(x));
    _create_key_enum("C", KEYCODE(c));
    _create_key_enum("V", KEYCODE(v));
    _create_key_enum("B", KEYCODE(b));
    _create_key_enum("N", KEYCODE(n));
    _create_key_enum("M", KEYCODE(m));
    _create_key_enum("Escape", KEYCODE(ESCAPE));
    _create_key_enum("Enter", KEYCODE(RETURN));
    _create_key_enum("F1", KEYCODE(F1));
    _create_key_enum("F2", KEYCODE(F2));
    _create_key_enum("F3", KEYCODE(F3));
    _create_key_enum("F4", KEYCODE(F4));
    _create_key_enum("F5", KEYCODE(F5));
    _create_key_enum("F6", KEYCODE(F6));
    _create_key_enum("F7", KEYCODE(F7));
    _create_key_enum("F8", KEYCODE(F8));
    _create_key_enum("F9", KEYCODE(F9));
    _create_key_enum("F10", KEYCODE(F10));
    _create_key_enum("F11", KEYCODE(F11));
    _create_key_enum("F12", KEYCODE(F12));
    _create_key_enum("F13", KEYCODE(F13));

    hk_reload();
    return 0;
}

void
hk_destroy()
{
    obj_pool_destroy(&_action_pool);
    str_hk_action_table_destroy(&_action_table);
    uint64_hk_action_table_destroy(&_int_action_table);
    str_int_table_destroy(&_key_str_enum_table);
    darr_free(_all_actions);
    darr_free(_repeat_actions);
}

int
hk_reload()
{
    str_hk_action_table_clear(&_action_table);
    hk_action_t *action = 0;
    int r = parse_def_file("hotkeys.def", _on_action_def, _on_action_val,
        &action);
    if (r)
        printf("Errors parsing hotkeys.def (%d).\n", r);
    return r;
}

int
hk_new_action(const char *name, hk_action_callback_t callback,
    int press_event_type)
{
    muta_assert(press_event_type >= 0 &&
        press_event_type < NUM_HK_PRESS_EVENT_TYPES);
    if (!name)
        return 1;
    if (strlen(name) > HK_MAX_ACTION_NAME_LEN)
        return 2;
    if (_get_action_by_name(name))
        return 0;
    hk_action_t *action = obj_pool_reserve(&_action_pool);
    strcpy(action->name, name);
    action->keys[0].keycode     = -1;
    action->keys[1].keycode     = -1;
    action->callback            = callback;
    action->press_event_type    = press_event_type;
    str_hk_action_table_einsert(&_action_table, name, action);
    darr_push(_all_actions, action);

    if (press_event_type == HK_PRESS_REPEAT)
        darr_push(_repeat_actions, action);

    lui_event_t lui_event;
    lui_event.type                      = LUI_EVENT_NEW_HOTKEY_ACTION;
    lui_event.new_hotkey_action.action  = action;
    lui_post_event(&lui_event);

    return 0;
}

int
hk_str_to_keycode(const char *str)
{
    if (!str)
        return -1;
    int *ret = str_int_table_get_ptr(&_key_str_enum_table, str);
    return ret ? *ret : -1;
}

const char *
hk_key_enum_to_str(int key_enum)
{
    static const char *none = "None";
    if (key_enum == -1)
    {
        DEBUG_PRINTFF("%d, returning None (keycode was -1)\n", key_enum);
        return none;
    }
    const char **ret = int_str_table_get_ptr(&_key_enum_str_table, key_enum);
    if (!ret)
        DEBUG_PRINTFF("%d, returning None (undefined key)\n", key_enum);
    return ret ? *ret : none;
}

int
hk_bind_key(const char *action_name, int index, int keycode, int mods)
{
    DEBUG_PRINTFF("action name: %s, index: %d, keycode: %d, mods: %d\n",
        action_name, index, keycode, mods);
    if (!action_name || index < 0 || index > 1)
        return 1;
    hk_action_t *action = _get_action_by_name(action_name);
    if (!action)
        return 2;
    if ((action->keys[0].keycode == keycode && action->keys[0].mods == mods) ||
        (action->keys[1].keycode == keycode && action->keys[1].mods == mods))
        return 0;
    action->keys[index].keycode = keycode;
    action->keys[index].mods    = mods;
    uint64 hash = _hash_from_key_combo(keycode, mods);

    /* Was key-mod combo bound to something before? */
    hk_action_t **old_action_ptr = uint64_hk_action_table_get_ptr(
        &_int_action_table, hash);

    if (old_action_ptr)
    {
        hk_action_t *old_action = *old_action_ptr;
        muta_assert(old_action != action);
        DEBUG_PRINTFF("old action found.\n");
        hk_key_mod_pair_t *mp;
        if (old_action->keys[0].keycode == keycode &&
            old_action->keys[0].mods == mods)
            mp = &old_action->keys[0];
        else
            mp = &old_action->keys[1];
        mp->keycode = -1;
        lui_event_t lui_event;
        lui_event.type                          = LUI_EVENT_KEY_BIND_CHANGED;
        lui_event.key_bind_changed.key_index    = (int)(mp - old_action->keys);
        lui_event.key_bind_changed.action       = old_action;
        lui_post_event(&lui_event);

        uint64_hk_action_table_erase(&_int_action_table, hash);
    }
    uint64_hk_action_table_einsert(&_int_action_table, hash, action);
    lui_event_t lui_event;
    lui_event.type                          = LUI_EVENT_KEY_BIND_CHANGED;
    lui_event.key_bind_changed.key_index    = index;
    lui_event.key_bind_changed.action       = action;
    lui_post_event(&lui_event);
    DEBUG_PRINTFF("keys now: (%d, %d), (%d, %d)\n", action->keys[0].keycode,
        action->keys[0].mods, action->keys[1].keycode, action->keys[1].mods);

    return 0;
}

int
hk_bind_key_from_str(const char *action_name, int index, const char *key)
{
    int keycode, mods;
    int rr;
    if ((rr = hk_str_to_key_combo(key, &keycode, &mods)))
    {
        DEBUG_PRINTF("rr: %d\n", rr);
        return 1;
    }
    int r = hk_bind_key(action_name, index, keycode, mods);
    if (r)
    {
        DEBUG_PRINTFF("hk_bind_key() failed with code %d.\n", r);
        return 2;
    }
    return 0;
}

hk_action_t *
hk_get_action(int key_enum, int mods)
{
    uint64 hash = _hash_from_key_combo(key_enum, mods);
    hk_action_t **ret = uint64_hk_action_table_get_ptr(
        &_int_action_table, hash);
    if (ret)
        return *ret;
    hash    = _hash_from_key_combo(key_enum, 0);
    ret     = uint64_hk_action_table_get_ptr(&_int_action_table, hash);
    return ret ? *ret : 0;
}

hk_action_t **
hk_get_actions(uint32 *ret_num)
{
    if (ret_num)
        *ret_num = darr_num(_all_actions);
    return _all_actions;
}

hk_action_t *
hk_get_action_by_name(const char *name)
{
    hk_action_t **action = str_hk_action_table_get_ptr(&_action_table, name);
    return action ? *action : 0;
}

hk_action_t **
hk_get_repeat_actions(uint32 *ret_num)
{
    if (ret_num)
        *ret_num = darr_num(_repeat_actions);
    return _repeat_actions;
}

int
hk_str_to_key_combo(const char *str, int *ret_keycode, int *ret_mods)
{
    if (!str)
        return 1;
    int         mods        = 0;
    const char  *s          = str;
    for (;;)
    {
        if (!strncmp(s, "SHIFT-", 6))
        {
            if (mods & HK_MOD_SHIFT)
                return 2;
            mods |= HK_MOD_SHIFT;
            s += 6;
        } else
        if (!strncmp(s, "CTRL-", 5))
        {
            if (mods & HK_MOD_CTRL)
                return 3;
            mods |= HK_MOD_CTRL;
            s += 5;
        } else
        if (!strncmp(s, "ALT-", 4))
        {
            if (mods & HK_MOD_ALT)
                return 4;
            mods |= HK_MOD_ALT;
            s += 4;
        } else
            break;
    }
    int *keycode = str_int_table_get_ptr(&_key_str_enum_table, s);
    if (!keycode)
        return 5;
    if (ret_keycode)
        *ret_keycode = *keycode;
    if (ret_mods)
        *ret_mods = mods;
    return 0;
}

int
hk_key_combo_to_str(int keycode, int mods, char *ret_buf)
{
    if (keycode == -1)
    {
        strcpy(ret_buf, "None");
        return 0;
    }
    const char *key_name = hk_key_enum_to_str(keycode);
    if (!key_name)
        return 1;
    char *s = ret_buf;
    if (mods & HK_MOD_SHIFT)
    {
        memcpy(s, "SHIFT-", 6);
        s += 6;
    }
    if (mods & HK_MOD_CTRL)
    {
        memcpy(s, "CTRL-", 5);
        s += 5;
    }
    if (mods & HK_MOD_ALT)
    {
        memcpy(s, "ALT-", 4);
        s += 4;
    }
    muta_assert(15 + strlen(key_name) <= HK_MAX_KEY_COMBO_NAME_LEN);
    strcpy(s, key_name);
    DEBUG_PRINTFF("keycode %d, mods %d.\n", keycode, mods);
    return 0;
}

static int
_on_action_def(void *ctx, const char *def, const char *val)
{
    if (!streq(def, "action"))
        return 1;
    if (strlen(val) > 31)
        return 2;
    hk_action_t **ap       = ctx;
    hk_action_t *action    = _get_action_by_name(val);
    if (action)
        *ap = action;
    else
    {
        *ap = obj_pool_reserve(&_action_pool);
        str_hk_action_table_einsert(&_action_table, val, *ap);
        strcpy((*ap)->name, val);
        (*ap)->keys[0].keycode = -1;
        (*ap)->keys[1].keycode = -1;
    }
    return 0;
}

static int
_on_action_val(void *ctx, const char *key, const char *val)
{
    hk_action_t **ap = ctx;
    if (streq(key, "key1"))
        (*ap)->keys[0].keycode = hk_str_to_keycode(val);
    else if (streq(key, "key2"))
        (*ap)->keys[1].keycode = hk_str_to_keycode(val);
    else
        return 3;
    return 0;
}

static uint64
_hash_from_key_combo(int key_enum, int mods)
{
    char buf[2 * sizeof(int)];
    memcpy(buf + 0, &key_enum, sizeof(int));
    memcpy(buf + sizeof(int), &mods, sizeof(int));
    return fnv_hash64_from_data(buf, 2 * sizeof(int));
}
