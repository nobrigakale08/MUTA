#ifndef MUTA_WORLD_H
#define MUTA_WORLD_H

#include "../../shared/common_utils.h"
#include "../../shared/world_common.h"
#include "../../shared/containers.h"
#include "../../shared/muta_map_format.h"

#include "../../shared/common_defs.h" /* tmp, for char names */

#define MAP_CHUNK_W MUTA_CHUNK_W
#define MAP_CHUNK_T MUTA_CHUNK_T

/* Neither type or name must contain the _t postifx */
#define ENT_CMP_DECL(type, name, index) \
typedef struct name##_item_t    name##_item_t; \
struct name##_item_t {entity_t *e; type##_t cmp;}; \
typedef name##_item_t* name##_t; \
static inline int name##_init(name##_t *p, uint32 num) \
    {darr_reserve(*p, num); return 0;} \
static inline void name##_destroy(name##_t *p) {darr_free(*p); *p = 0;} \
static inline void name##_clear(name##_t *p) {darr_clear(*p);} \
name##_item_t           *name##_get_items(name##_t *p, uint32 *ret_num); \
int                     entity_attach_##type(entity_t *e, type##_t *cmp); \
type##_t                *entity_attach_empty_##type(entity_t *e); \
void                    entity_remove_##type(entity_t *e); \
type##_t                *entity_get_##type(entity_t *e); \
bool32                  entity_has_##type(entity_t *e); \
entity_t                *type##_get_entity(type##_t *cmp);

/* Declare a component type with a constant address handle capable of holding
 * data not updated frequently, such as pointers to assets. */
#define ENT_HANDLED_CMP_DECL(htype, utype, name, cmp_index) \
typedef struct htype##_cont_t   htype##_cont_t; \
typedef struct utype##_cont_t   utype##_cont_t; \
typedef struct name##_t         name##_t; \
 \
struct htype##_cont_t \
{ \
    entity_t        *e; \
    uint32          uindex; \
    htype##_t       h; \
    htype##_cont_t  *next; \
}; \
 \
struct utype##_cont_t \
{ \
    htype##_cont_t  *h; \
    utype##_t       u; \
}; \
\
DYNAMIC_OBJ_POOL_DEFINITION(htype##_cont_t, htype##_cont_pool, 16); \
struct name##_t \
{ \
    utype_t             *updatables; \
    htype##_cont_pool_t handles; \
}; \
htype##_t   *entity_attach_##utype(entity_t *e); \
utype##_t   *entity_get_##utype(entity_t *e); \
utype##_t   *htype##_get_##utype(htype##_t *h); \
void        entity_remove_##utype(entity_t *e); \
entity_t    *utype##_get_entity(utype##_t *u); \
entity_t    *htype##_get_entity(htype##_t *h);

/* Declare a component with no updatable procedures */
#define ENT_STATIC_CMP_DECL(type, name, cmp_index) \
typedef struct name##_item_t name##_item_t; \
struct name##_item_t \
{ \
    type##_t        cmp; \
    entity_t        *e; \
    name##_item_t   *next; \
}; \
DYNAMIC_OBJ_POOL_DEFINITION(name##_item_t, name, 16); \
type##_t    *entity_attach_empty_##type(entity_t *e); \
void        entity_remove_##type(entity_t *e); \
type##_t    *entity_get_##type(entity_t *e); \
entity_t    *type##_get_entity(type##_t *cmp);



/* Forward declarations */
typedef struct tex_t                tex_t;
typedef struct tex_asset_t          tex_asset_t;
typedef struct iso_anim_frame_t     iso_anim_frame_t;
typedef struct iso_anim_t           iso_anim_t;
typedef struct iso_anim_asset_t     iso_anim_asset_t;
typedef struct spritesheet_t        spritesheet_t;
typedef struct char_anim_set_t      char_anim_set_t;
typedef struct ae_asset_t           ae_asset_t;
typedef struct ae_frame_t           ae_frame_t;
typedef struct ae_set_t             ae_set_t;

/* Types defined */
typedef struct entity_handle_t          entity_handle_t;
typedef struct entity_t                 entity_t;
typedef entity_t*                       entity_ptr_darr_t;
typedef struct mobility_data_t          mobility_data_t;
typedef struct cmp_renderable_t         cmp_renderable_t;
typedef struct cmp_spritesheet_t        cmp_spritesheet_t;
typedef struct cmp_iso_animator_t       cmp_iso_animator_t;
typedef struct cmp_ae_animator_t        cmp_ae_animator_t;
typedef struct cmp_player_ae_set_t      cmp_player_ae_set_t;
typedef struct cmp_creature_ae_set_t    cmp_creature_ae_set_t;
typedef struct map_chunk_t              map_chunk_t;
typedef struct chunk_cache_t            chunk_cache_t;
typedef struct map_t                    map_t;
typedef struct world_render_props_t     world_render_props_t;
typedef struct world_event_t            world_event_t;
typedef world_event_t                   world_event_darr_t;
typedef struct world_event_listener_t   world_event_listener_t;
typedef struct world_t                  world_t;

enum entity_types
{
    ENT_TYPE_UNKNOWN = 0,
    ENT_TYPE_STATIC_OBJ,
    ENT_TYPE_CREATURE,
    ENT_TYPE_PLAYER
};

enum entity_flags
{
    ENT_SEX         = (1 << 0),
    ENT_RESERVED    = (1 << 1)
};

enum entity_component_indices
{
    ENTCMP_MOBILITY_DATA = 0,
    ENTCMP_RENDERABLE,
    ENTCMP_ISO_ANIMATOR,
    ENTCMP_AE_ANIMATOR,
    MAX_ENTITY_COMPONENTS
};

enum entity_handled_component_indices
{
    ENTHCMP_SPRITESHEET = 0,
    ENTHCMP_PLAYER_AE_SET,
    ENTHCMP_CREATURE_AE_SET,
    MAX_ENTITY_HANDLED_COMPONENTS
};

struct entity_handle_t
{
    entity_t        *ptr;
    player_guid_t   id;
};

struct entity_t
{
    union
    {
        struct
        {
            player_guid_t       id;
            player_race_id_t    race;
            player_race_def_t   *race_def;
            char                name[MAX_CHARACTER_NAME_LEN + 1];
        } player;
        struct
        {
            static_obj_type_id_t    type_id;
            uint32                  file_index; /* 0xFFFFFFFF if none */
        } static_obj;
        struct
        {
            dobj_guid_t     id;
            dobj_type_id_t  type_id;
        } dynamic_obj;
        struct
        {
            creature_guid_t     id;
            creature_type_id_t  type_id;
            uint8               flags;
        } creature;
    } type_data;
    world_t     *w;
    entity_t    *target;
    uint32      cmps[MAX_ENTITY_COMPONENTS];
    void        *hcmps[MAX_ENTITY_HANDLED_COMPONENTS];
    int32       x, y;
    int8        z;
    uint8       flags;
    uint8       type; /* One of the ENTTYPE_ values (creature, etc.) */
    uint8       dir;
};

DYNAMIC_HASH_TABLE_DEFINITION(pguid_entity_table, entity_t *, player_guid_t,
    player_guid_t, HASH_FROM_NUM, 3);
DYNAMIC_HASH_TABLE_DEFINITION(cguid_entity_table, entity_t *, creature_guid_t,
    creature_guid_t, HASH_FROM_NUM, 3);

struct mobility_data_t
{
    float   walk_timer;
    float   time_idle;
    int32   last_x, last_y;
    int8    last_z;
    uint8   walk_speed;
};

struct cmp_renderable_t
{
    tex_t   *tex;
    float   clip[4];
    int16   ox, oy;
    float   x, y;
    float   scale[2];
    float   rot;
    int32   tx, ty; /* Required here for depth sorting */
    int8    tz;
    uint8   flip;
    int8    travel_dir;
};

struct cmp_iso_animator_t
{
    iso_anim_asset_t    *asset;
    int32               frame_num;
    int32               num_frames;
    float               frame_dur, time_passed;
    uint                paused:1;
    uint                loop:1;
};

struct cmp_ae_animator_t
{
    ae_asset_t  *asset;
    uint16      frame_num;
    uint16      num_frames;
    ae_frame_t  *frames;
    uint        loop:1;
};

struct cmp_player_ae_set_t
{
    ae_asset_t  *idle_asset;
    const char  *idle_fp;
    ae_asset_t  *walk_asset;
    const char  *walk_fp;
};

struct cmp_creature_ae_set_t
{
    ae_set_t    *ae_set;
    const char  *ae_set_name;
    ae_asset_t  *idle_asset;
    ae_asset_t  *walk_asset;
};

struct cmp_spritesheet_t
{
    spritesheet_t   *asset;
    uint32          clip;
};

/* Entity component pools */
ENT_CMP_DECL(mobility_data, mobility_data_pool, ENTCMP_MOBILITY_DATA);
ENT_CMP_DECL(cmp_renderable, cmp_renderable_pool, ENTCMP_RENDERABLE);
ENT_CMP_DECL(cmp_iso_animator, cmp_iso_animator_pool, ENTCMP_ISO_ANIMATOR);
ENT_CMP_DECL(cmp_ae_animator, cmp_ae_animator_pool, ENTCMP_AE_ANIMATOR);
ENT_STATIC_CMP_DECL(cmp_spritesheet, cmp_spritesheet_pool, ENTCHMP_SPRITESHEET);
ENT_STATIC_CMP_DECL(cmp_player_ae_set, cmp_player_ae_set_pool,
    ENTHCMP_PLAYER_AE_SET);
ENT_STATIC_CMP_DECL(cmp_creature_ae_set, cmp_creature_ae_set_pool,
    ENTHCMP_CREATURE_AE_SET);

struct map_chunk_t
{
    char    *path;
    mutex_t mtx;
};

struct chunk_cache_t
{
    map_chunk_t         *chunk;
    muta_chunk_file_t   file;
    entity_ptr_darr_t   *static_objs;
    mutex_t             mtx;
};
/* A tile cache holds as many tiles as each chunk may contain and points to
 * a chunk the tiles of which it currently holds */

struct map_t
{
    muta_map_file_t file;
    map_chunk_t     *chunks;
    int             w, h;
    int             num_alloc_chunks;
    int             tw, th;
    int             cam_x, cam_y, cam_tx, cam_ty;
    chunk_cache_t   caches[9];
    chunk_cache_t   *cache_ptrs[9];
    /* The tile data of the 9 chunks surrounding the player is loaded in and
     * out of these caches as the player moves on the map, starting from cam_x,
     * cam_y. */
};

struct world_render_props_t
{
    int     tx, ty, tz, tw, th, tt, st; /* Tiled dimensions of the scene  */
    float   bx, by;                     /* Pixel offset of the first tile */
    int     coord_space[4], screen_area[4], scissor[4];
};


enum world_event_types
{
    WEV_MAP_CHANGED,
    WEV_WALK_FINISHED,
    WEV_DESPAWN_ENTITY,
    WEV_NUM
};

struct world_event_t
{
    int type;
    union world_event_data_t
    {
        struct {uint32 mobility_data_index;} walk_finished;
        entity_t *despawn_entity;
    } d;
};

struct world_event_listener_t
{
    int     event;
    uint32  index; /* In the table slot's array of listeners */
    void    *data;
    void    (*callback)(world_t*, void*, world_event_t*);
#ifdef _MUTA_DEBUG
    int     reserved;
#endif
};

DYNAMIC_HASH_TABLE_DEFINITION(world_event_listener_table,
    world_event_listener_t**, int, int, HASH_FROM_NUM, 2);

struct world_t
{
    bool32                      initialized;
    double                      tick_timer;
    map_t                       map;
    obj_pool_t                  entities;
    pguid_entity_table_t        players;
    cguid_entity_table_t        creatures;
    mobility_data_pool_t        mobility_data_pool;
    cmp_renderable_pool_t       cmp_renderable_pool;
    cmp_spritesheet_pool_t      cmp_spritesheet_pool;
    cmp_iso_animator_pool_t     cmp_iso_animator_pool;
    cmp_ae_animator_pool_t      cmp_ae_animator_pool;
    cmp_player_ae_set_pool_t    cmp_player_ae_set_pool;
    cmp_creature_ae_set_pool_t  cmp_creature_ae_set_pool;
    entity_ptr_darr_t           *removed_entities;
    fixed_stack_t               frame_stack;
    world_render_props_t        render_props;
    world_event_darr_t          *events;
    mutex_t                     event_mtx;
    struct
    {
        world_event_listener_table_t    table;
        obj_pool_t                      pool;
        world_event_listener_t          ****arrays;
    } event_listeners;
};

const char *
get_creature_def_anim_set(creature_def_t *def, int index);

iso_anim_asset_t *
get_creature_anim(int32 id);

int
init_world_data();

int
reload_world_data();

int
map_init(map_t *map, int w, int h);
/* The unit of w and h are map_chunks */

void
map_resize(map_t *map, int w, int h);

void
map_destroy(map_t *map);

void
entity_walk(entity_t *e, int dir);

void
entity_walk_to(entity_t *e, int32 x, int32 y, int8 z);

entity_handle_t
entity_get_handle(entity_t *e);

int
entity_get_sex(entity_t *e);

void
entity_set_dir(entity_t *e, int dir);

int
world_render_props_px_pos_from_window(world_render_props_t *rp, int *ret_in_x,
    int *ret_in_y);

int
world_init(world_t *w, int w_in_chunks, int h_in_chunks, int num_initial_ents);

int
world_init_from_file(world_t *w, const char *fp);

int
world_load_map_by_id(world_t *w, uint32 id);

int
world_init_by_id(world_t *w, map_id_t id);

#define world_initialized(w) ((w)->initialized)

void
world_destroy(world_t *w);

void
world_clear(world_t *w);

int
world_write_static_objs(world_t *w);

bool32
world_set_cam_pos(world_t *w, int tx, int ty);

void
world_update(world_t *w, world_render_props_t *props, double dt);

entity_t *
world_spawn_static_object(world_t *w, uint32 type_id, int32 x, int32 y, int8 z,
    int dir);

entity_t *
world_spawn_player(world_t *w, player_guid_t id, const char *name, int x,
    int y, int z, int dir, player_race_id_t race, bool32 sex);

void
world_despawn_player(world_t *w, entity_t *e);

void
world_despawn_player_by_id(world_t *w, player_guid_t id);

void
despawn_static_obj(entity_t *e);

entity_t *
world_spawn_creature(world_t *w, creature_guid_t id, uint32 type_id, int x,
    int y, int z, int dir, uint8 flags);

void
world_despawn_creature(entity_t *cr);

entity_t *
world_get_player_by_id(world_t *w, player_guid_t id);

entity_t *
world_get_creature_by_id(world_t *w, creature_guid_t id);

int
world_set_player_dir_by_id(world_t *w, player_guid_t id, int dir);

entity_t *
world_get_static_obj_by_window_coords(world_t *w, int x, int y);

entity_t *
world_get_entity_by_window_coords(world_t *w, int x, int y);

int
world_get_entities_by_window_coords(world_t *w, int x, int y,
    entity_t **ret_arr, int arr_sz);

int
world_get_visible_entities_inside_box(world_t *wld, int x, int y, int z, int w,
    int h, int t, entity_t **ret_arr, int arr_sz);

int
set_player_dir(entity_t *e, int dir);

#define entity_handle_get(h) \
    ((h.ptr && h.ptr->flags & ENT_RESERVED) ? h.ptr : 0)

int
static_obj_set_pos(entity_t *e, int32 x, int32 y, int8 z);

int
player_set_pos(entity_t *e, int32 x, int32 y, int8 z);

bool32
world_can_player_enter_tile(world_t *w, int x, int y, int z);

int
world_compute_chunk_cache_coords(world_t *w, chunk_cache_t *ch, int *ret_x,
    int *ret_y);

world_event_listener_t *
world_register_event_listener(world_t *w, int event, void *data,
    void (*callback)(world_t*, void*, world_event_t*));

void
world_unregister_event_listener(world_t *w, world_event_listener_t *el);

void
world_post_event(world_t *w, world_event_t *ev);

bool32
world_get_solid_tile_by_px_pos(world_t *w, int x, int y, int ret_pos[3]);
/* Returns non-zero if a tile was found */

#endif /* MUTA_WORLD_H */
