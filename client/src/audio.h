#ifndef AUDIO_SYSTEM_H
#define AUDIO_SYSTEM_H

#include "../../shared/types.h"

#define MAX_SOUND_CHANNELS 32

/* Forward declaration(s) */
typedef struct sound_asset_t    sound_asset_t;
typedef struct sound_t          sound_t;

int
play_sound_asset(sound_asset_t *sound, float volume);

int
play_sound_asset_on_channel(sound_asset_t *sound, int channel, float volume,
    float pitch, bool32 loop);

int
play_music_asset_on_channel(sound_asset_t *sound, int channel, float volume,
    float pitch, bool32 loop);

int
initialize_audio();

int
free_audio();

int
create_buffer(uint *buffer, const char *file);

int
au_destroy_buffer(uint *buffer);

int
load_sound_to_channel(sound_t sound, uint channel);

int
load_ogg(au_buf_t *dest_buffer, const char *file);

int
load_wav(au_buf_t *dest_buffer, const char *file);

int
play_sound(sound_t sound, float volume);

int
play_channel(uint channel);

int
play_sound_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop);

int
play_music_on_channel(sound_t sound, int channel, float volume, float pitch,
    bool32 loop);

int
start_sound(uint channel, float volume, float pitch, bool32 loop);

int
pause_channel(uint channel);

int
pause_all_channels();

int
resume_channel(uint channel);

int
resume_all_channels();

int
stop_sound(sound_t sound);

int
stop_sounds(sound_t sound);

int
stop_channel(uint channel);

int
stop_all_channels();

int
get_free_channel(int *channel);

int
get_channel_state(uint channel);

int
check_al_error();

int
set_channel_lock(uint channel, bool32 locked);

int
set_channel_volume(uint channel, float channel_vol);

void
set_master_volume(float sound_vol, float music_vol);

void
update_audio();

void
dequeue_sound_asset(sound_asset_t *sa);

#endif /* AUDIO_SYSTEM_H */
