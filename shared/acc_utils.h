#ifndef MUTA_COMMON_ACC_UTILS_H
#define MUTA_COMMON_ACC_UTILS_H

#include "types.h"

enum account_login_result_codes
{
    ACCUT_ACC_LOGIN_OK = 0,
    ACCUT_ACC_LOGIN_WRONG_INFO,
    ACCUT_ACC_LOGIN_ALREADY_IN,
    ACCUT_ACC_LOGIN_DB_BUSY,
    ACCUT_ACC_LOGIN_DB_DOWN
};

bool32
accut_is_name_legal(const char *name);
/* Assumes a zero-terminated string */

bool32
accut_is_name_legaln(const char *name, int suggested_len);

bool32
accut_is_pw_legaln(const char *pw, uint8 suggested_len);

void
accut_strip_illegal_name_symbols(char *buf);

void
accut_strip_illegal_pw_symbols(char *buf);

bool32
accut_check_character_name(const char *name, uint32 claimed_len);

bool32
accut_check_character_name_symbol(char c);

#endif /* MUTA_COMMON_ACC_UTILS_H */
