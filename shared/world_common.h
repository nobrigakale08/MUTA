/* Common world-related data structures and defines to both, the server and
 * the client. */

#ifndef MUTA_WORLD_COMMON_H
#define MUTA_WORLD_COMMON_H

#include "types.h"

/* Forward declaration(s) */
typedef uint64                      perm_str_t;
typedef struct muta_chunk_file_t    muta_chunk_file_t;

/* Types defined here */
typedef uint16                      entity_type_id_t;
typedef uint8                       entity_def_flags_t;
typedef struct tile_def_t           tile_def_t;
typedef struct tile_gfx_def_t       tile_gfx_def_t;
typedef struct static_obj_def_t     static_obj_def_t;
typedef struct dynamic_obj_def_t    dynamic_obj_def_t;
typedef struct player_race_def_t    player_race_def_t;
typedef struct creature_def_t       creature_def_t;
typedef struct wc_cfg_t             wc_cfg_t;

#define MAX_TILE_TYPES              0xFFFF
#define MAX_ENTITY_TYPES            0xFFFF
#define MAX_CREATURE_TYPE_NAME_LEN  256
#define INTEREST_AREA_W             32
#define INTEREST_AREA_H             INTEREST_AREA_W
#define INTEREST_AREA_T             8
#define IA_W                        INTEREST_AREA_W
#define IA_T                        INTEREST_AREA_T
#define INVALID_ISODIR              NUM_ISODIRS
#define INVISIBLE_COLLIDER          (tile_defs_num - 1)

#define WC_WALK_MAX_SECONDS 12.25f
/* The most amount of time traveling from a tile to the next may take. Speed,
 * represented as a value between 0 to 255, is a reverse percentage of this. */

#define WC_WALK_SPEED_TO_PERCENT(speed) ((float)(speed) / 255.f)
#define WC_WALK_SPEED_PERCENT_TO_SEC(prcnt) \
    ((1.f - (prcnt)) * WC_WALK_MAX_SECONDS)
#define WC_WALK_SPEED_TO_SEC(speed) \
    (WC_WALK_SPEED_PERCENT_TO_SEC(WC_WALK_SPEED_TO_PERCENT(speed)))
#define WC_PLAYER_BASE_WALK_SPEED 245
#define WC_CREATURE_BASE_WALK_SPEED 245

enum iso_dir_t
{
    ISODIR_NORTH = 0,
    ISODIR_NORTH_EAST,
    ISODIR_EAST,
    ISODIR_SOUTH_EAST,
    ISODIR_SOUTH,
    ISODIR_SOUTH_WEST,
    ISODIR_WEST,
    ISODIR_NORTH_WEST,
    NUM_ISODIRS
};

enum wc_gfx_types
{
    WC_GFX_NONE = 0,
    WC_GFX_TEXTURE,
    WC_GFX_SPRITESHEET,
    WC_GFX_ANIMATION,
    WC_GFX_AE_SET
};

enum ramp_t
{
    RAMP_NONE = 0,
    RAMP_NORTH,
    RAMP_EAST,
    RAMP_SOUTH,
    RAMP_WEST,
    RAMP_NORTH_EAST,
    RAMP_NORTH_WEST,
    RAMP_SOUTH_EAST,
    RAMP_SOUTH_WEST
};
/* ramp_t implies towards which direction does a ramp go UP */

enum creature_sex_t
{
    SEX_MALE,
    SEX_FEMALE
};

enum wc_creature_flags
{
    WC_CREATURE_SEX         = (1 << 0),
    WC_CREATURE_TALKABLE    = (1 << 1),
    WC_CREATURE_ATTACKABLE  = (1 << 2)
};

struct tile_def_t
{
    dchar   *name;
    bool32  passthrough;
    enum    ramp_t ramp;
};

struct tile_gfx_def_t
{
    float   clip[4];
    float   ox, oy;
};

/* Static objects: unchanging objects on the map that are not tiles. Contained
 * in client and server map files. */
struct static_obj_def_t
{
    static_obj_type_id_t    type_id;
    dchar                   *name;
    dchar                   *examine;
    union
    {
        struct
        {
            const dchar *path;
            int         clip;
            int16       ox, oy;
            float       sx, sy;
            float       rot;
            uint8       flip;
        } spritesheet;
        char *animation;
    } gfx_data;
    uint                    gfx_type:4;
    uint                    passthrough:4;
};

struct player_race_def_t
{
    player_race_id_t    id;
    dchar               *name;
    dchar               *description;
    dchar               *ae_set;
    dchar               *anim_idle;
    dchar               *anim_walk;
};

/* Dynamic objects: dynamically spawned objects that are not creatures. Objects
 * such as mines are of this kind. */
struct dynamic_obj_def_t
{
    dchar   *name;
    dchar   *description;
};

struct creature_def_t
{
    dchar   *id_str;
    uint32  id_num;
    dchar   *name;
    dchar   *description;
    float   sex; /* Male-to-female random ratio */
    int     gfx_type;
    dchar   *script_name;
    union
    {
        dchar *tex_name;
        dchar *ae_set_name;
    } gfx_data;
    struct
    {
        uint attackable:1;
        uint aggressive:1;
    }       flags; /* Default */
};

extern tile_def_t       *tile_defs;
extern tile_gfx_def_t   *tile_gfx_defs;
extern uint32           tile_defs_num;

extern enum ramp_t ramp_up_table[NUM_ISODIRS];
/* Required directions of movement per each ramp */

extern enum ramp_t ramp_down_table[NUM_ISODIRS];
/* Required directions of movement per each ramp */

extern player_race_id_darr_t *wc_player_race_ids;
/* For iterating over the different player races */

bool32 wc_tile_defs_initialized();
bool32 wc_creature_defs_initialized();
bool32 wc_player_race_defs_initialized();
bool32 wc_static_obj_defs_initialized();
bool32 wc_dynamic_obj_defs_initialized();
bool32 wc_all_defs_initialized();

int
wc_load_tile_defs(const char *fp, bool32 use_gfx);

int
wc_load_creature_defs(const char *fp);

int
wc_load_creature_defs_unfinished(const char *fp);

int
wc_load_dynamic_obj_defs(const char *fp);

int
wc_load_static_obj_defs(const char *fp);

int
wc_load_player_race_defs(const char *fp);

void
wc_destroy_tile_defs();

void
wc_destroy_creature_defs();

void
wc_destroy_static_obj_defs();

void
wc_destroy_player_race_defs();

void
wc_destroy_dynamic_obj_defs();

const char *
wc_iso_dir_to_str(int dir);

creature_def_t *
wc_get_creature_def(uint32 type_id);

creature_def_t *
wc_get_creature_def_by_str_id(const char *name);

static_obj_def_t *
wc_get_static_obj_def(static_obj_type_id_t id);

dynamic_obj_def_t *
wc_get_dynamic_obj_def(dynamic_obj_type_id_t id);

player_race_def_t *
wc_get_player_race_def(player_race_id_t id);

bool32
wc_chunk_file_tiles_ok(muta_chunk_file_t *ch);

bool32
wc_is_player_race_name_legal(const char *name);

#define wc_is_creature_name_legal(name) wc_is_player_race_name_legal(name)

static inline int
check_ramp_up(enum iso_dir_t dir, enum ramp_t ramp);

static inline int
check_ramp_down(enum iso_dir_t dir, enum ramp_t ramp);

static inline int
ramp_is_diagonal(enum ramp_t ramp);

static inline int
dir_is_diagonal(enum iso_dir_t dir);

static inline int
check_ramp_up(enum iso_dir_t dir, enum ramp_t ramp)
{
    switch (ramp)
    {
        case RAMP_NORTH:
            return (dir == ISODIR_NORTH) ? 0 : 1;
        case RAMP_EAST:
            return (dir == ISODIR_EAST)  ? 0 : 1;
        case RAMP_SOUTH:
            return (dir == ISODIR_SOUTH) ? 0 : 1;
        case RAMP_WEST:
            return (dir == ISODIR_WEST)  ? 0 : 1;
        case RAMP_NORTH_EAST:
            return (dir == ISODIR_NORTH || dir == ISODIR_EAST ||
                    dir == ISODIR_NORTH_EAST) ? 0 : 1;
        case RAMP_NORTH_WEST:
            return (dir == ISODIR_NORTH || dir == ISODIR_WEST ||
                    dir == ISODIR_NORTH_WEST) ? 0 : 1;
        case RAMP_SOUTH_EAST:
            return (dir == ISODIR_SOUTH || dir == ISODIR_EAST ||
                    dir == ISODIR_SOUTH_EAST) ? 0 : 1;
        case RAMP_SOUTH_WEST:
            return (dir == ISODIR_SOUTH || dir == ISODIR_WEST ||
                    dir == ISODIR_SOUTH_WEST) ? 0 : 1;
        default:
            return 1;
    };
}

static inline int
check_ramp_down(enum iso_dir_t dir, enum ramp_t ramp)
{
    switch (ramp)
    {
        case RAMP_NORTH:
            return (dir == ISODIR_SOUTH) ? 0 : 1;
        case RAMP_EAST:
            return (dir == ISODIR_WEST)  ? 0 : 1;
        case RAMP_SOUTH:
            return (dir == ISODIR_NORTH) ? 0 : 1;
        case RAMP_WEST:
            return (dir == ISODIR_EAST)  ? 0 : 1;
        case RAMP_NORTH_EAST:
            return (dir == ISODIR_SOUTH || dir == ISODIR_WEST ||
                    dir == ISODIR_SOUTH_WEST) ? 0 : 1;
        case RAMP_NORTH_WEST:
            return (dir == ISODIR_SOUTH || dir == ISODIR_EAST ||
                    dir == ISODIR_SOUTH_EAST) ? 0 : 1;
        case RAMP_SOUTH_EAST:
            return (dir == ISODIR_NORTH || dir == ISODIR_WEST ||
                    dir == ISODIR_NORTH_WEST) ? 0 : 1;
        case RAMP_SOUTH_WEST:
            return (dir == ISODIR_NORTH || dir == ISODIR_EAST ||
                    dir == ISODIR_NORTH_EAST) ? 0 : 1;
        default:
            return 1;
    };
}

static inline int
ramp_is_diagonal(enum ramp_t ramp)
{
    switch (ramp)
    {
        case RAMP_NORTH_EAST:
        case RAMP_NORTH_WEST:
        case RAMP_SOUTH_EAST:
        case RAMP_SOUTH_WEST:
            return 1;
        default:
            return 0;
    }
}

static inline int
dir_is_diagonal(enum iso_dir_t dir)
{
    switch (dir)
    {
        case ISODIR_NORTH_EAST:
        case ISODIR_NORTH_WEST:
        case ISODIR_SOUTH_EAST:
        case ISODIR_SOUTH_WEST:
            return 1;
        default:
            return 0;
    }
}

#endif /* MUTA_WORLD_COMMON_H */
