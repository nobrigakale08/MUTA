typedef uint8 msg_type_t;
#define MSGTSZ sizeof(msg_type_t)
#include "rwbits.inl"
#include "common_defs.h"
#if MUTA_ENDIANNESS == MUTA_LIL_ENDIAN
#   define WRITE_MSG_TYPE(mem, val) \
        *(msg_type_t*)(mem) = (msg_type_t)(val); \
        (mem) = (uint8*)(mem) + sizeof(msg_type_t);
#elif MUTA_ENDIANNESS == MUTA_BIG_ENDIAN
#   error "Big endian unsupported"
#else
#   error "Endianness undefined"
#endif /* MUTA_LIL_ENDIAN */
#define MUTA_PROTOCOL_VERSION 3
enum muta_svmsg_type
{
    SVMSG_PUB_KEY = 1,
    SVMSG_STREAM_HEADER,
    SVMSG_LOGIN_RESULT,
    SVMSG_CHARACTER_LIST,
    SVMSG_PLAYER_INIT_DATA,
    SVMSG_CHAR_LOGIN_FAIL,
    SVMSG_GLOBAL_CHAT_BROADCAST,
    SVMSG_WALK_CREATURE,
    SVMSG_WALK_PLAYER_CHARACTER,
    SVMSG_NEW_CREATURE,
    SVMSG_NEW_PLAYER_CHARACTER,
    SVMSG_PLAYER_CHARACTER_DIR,
    SVMSG_PAWN_DIR,
    SVMSG_REMOVE_CREATURE,
    SVMSG_REMOVE_PLAYER_CHARACTER,
    SVMSG_NEW_DYNAMIC_OBJ,
    SVMSG_REMOVE_DYNAMIC_OBJ,
    SVMSG_PLAYER_PLAYER_EMOTE,
    SVMSG_UPDATE_PLAYER_POSITION,
    SVMSG_CREATE_CHARACTER_FAIL,
    SVMSG_NEW_CHARACTER_CREATED,
    SVMSG_CREATURE_DIR,
    SVMSG_SHARD_LIST_ITEM,
    SVMSG_SHARD_SELECT_FAIL,
    SVMSG_SHARD_SELECT_SUCCESS,

    NUM_MUTA_SVMSG_TYPE
};

#if NUM_MUTA_SVMSG_TYPE > 255
#   error too many MUTA_SVMSG_TYPE types
#endif

MSG_WRITE_PREP_DEFINITION(svmsg, MSGTSZ, WRITE_MSG_TYPE);

enum muta_clmsg_type
{
    CLMSG_PUB_KEY = 1,
    CLMSG_STREAM_HEADER,
    CLMSG_REGISTER_ACCOUNT,
    CLMSG_LOGIN_REQUEST,
    CLMSG_KEEP_ALIVE,
    CLMSG_CHAT_MSG,
    CLMSG_PLAYER_MOVE,
    CLMSG_FIND_PATH,
    CLMSG_LAST_RECEIVED_POS,
    CLMSG_PLAYER_EMOTE,
    CLMSG_CREATE_CHARACTER,
    CLMSG_LOG_IN_CHARACTER,
    CLMSG_AUTH_PROOF,
    CLMSG_SELECT_SHARD,
    CLMSG_CANCEL_SELECT_SHARD,

    NUM_MUTA_CLMSG_TYPE
};

#if NUM_MUTA_CLMSG_TYPE > 255
#   error too many MUTA_CLMSG_TYPE types
#endif

MSG_WRITE_PREP_DEFINITION(clmsg, MSGTSZ, WRITE_MSG_TYPE);

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} svmsg_pub_key_t;

#define SVMSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
svmsg_pub_key_write(byte_buf_t *buf, svmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
svmsg_pub_key_read(byte_buf_t *buf, svmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} svmsg_stream_header_t;

#define SVMSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
svmsg_stream_header_write(byte_buf_t *buf, svmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
svmsg_stream_header_read(byte_buf_t *buf, svmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint8 result;
} svmsg_login_result_t;

#define SVMSG_LOGIN_RESULT_SZ (sizeof(uint8))

static inline int
svmsg_login_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + SVMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_LOGIN_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, SVMSG_LOGIN_RESULT_SZ);
}

static inline int
svmsg_login_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVMSG_LOGIN_RESULT_SZ))
        return -1;
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint64 ids_len;
    uint8 names_len;
    uint8 races_len;
    uint8 sexes_len;
    uint8 name_indices_len;
    const char *names;
    uint64 *ids;
    uint8 *races;
    uint8 *sexes;
    uint8 *name_indices;
} svmsg_character_list_t;

#define SVMSG_CHARACTER_LIST_SZ (sizeof(uint64) + sizeof(uint8) + sizeof(uint8) + sizeof(uint8) + sizeof(uint8))

#define SVMSG_CHARACTER_LIST_COMPUTE_SZ(names_len, ids_len, races_len, sexes_len, name_indices_len) \
    (SVMSG_CHARACTER_LIST_SZ + (names_len) + (ids_len * sizeof(uint64)) + (races_len * sizeof(uint8)) + (sexes_len * sizeof(uint8)) + (name_indices_len * sizeof(uint8)))

#define SVMSG_CHARACTER_LIST_MAX_SZ \
    (SVMSG_CHARACTER_LIST_COMPUTE_SZ(8, 8, 8, 8, 8))

static inline int
svmsg_character_list_write(byte_buf_t *buf, svmsg_character_list_t *s)
{
    int sz = SVMSG_CHARACTER_LIST_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->name_indices_len);
    if (sz > SVMSG_CHARACTER_LIST_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        SVMSG_CHARACTER_LIST_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->name_indices_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_CHARACTER_LIST);
    WRITE_UINT64(mem, s->ids_len);
    WRITE_UINT8(mem, s->names_len);
    WRITE_UINT8(mem, s->races_len);
    WRITE_UINT8(mem, s->sexes_len);
    WRITE_UINT8(mem, s->name_indices_len);
    WRITE_STR(mem, s->names, s->names_len);
    WRITE_UINT64_VARARR(mem, s->ids, s->ids_len);
    WRITE_UINT8_VARARR(mem, s->races, s->races_len);
    WRITE_UINT8_VARARR(mem, s->sexes, s->sexes_len);
    WRITE_UINT8_VARARR(mem, s->name_indices, s->name_indices_len);

    return 0;
}

static inline int
svmsg_character_list_read(byte_buf_t *buf, svmsg_character_list_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SVMSG_CHARACTER_LIST_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT64(mem, &s->ids_len);
    READ_UINT8(mem, &s->names_len);
    READ_UINT8(mem, &s->races_len);
    READ_UINT8(mem, &s->sexes_len);
    READ_UINT8(mem, &s->name_indices_len);

    if (SVMSG_CHARACTER_LIST_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->name_indices_len) >
        SVMSG_CHARACTER_LIST_MAX_SZ)
        return -1;

    int req_sz = SVMSG_CHARACTER_LIST_COMPUTE_SZ(s->names_len, s->ids_len, s->races_len, s->sexes_len, s->name_indices_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_VARCHAR(mem, s->names, s->names_len);
    READ_UINT64_VARARR(mem, s->ids, s->ids_len);
    READ_UINT8_VARARR(mem, s->races, s->races_len);
    READ_UINT8_VARARR(mem, s->sexes, s->sexes_len);
    READ_UINT8_VARARR(mem, s->name_indices, s->name_indices_len);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint64 char_id;
    uint32 map_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
    uint8 sex;
    const char *name;
} svmsg_player_init_data_t;

#define SVMSG_PLAYER_INIT_DATA_SZ (sizeof(uint8) + sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8) + sizeof(uint8))

#define SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(name_len) \
    (SVMSG_PLAYER_INIT_DATA_SZ + (name_len))

#define SVMSG_PLAYER_INIT_DATA_MAX_SZ \
    (SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
svmsg_player_init_data_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_player_init_data_t *s)
{
    int sz = SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(s->name_len);
    if (sz > SVMSG_PLAYER_INIT_DATA_MAX_SZ)
        return -1;

    uint8 *mem = prep_svmsg_write_var_encrypted(buf, SVMSG_PLAYER_INIT_DATA, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT64(mem, s->char_id);
    WRITE_UINT32(mem, s->map_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);
    WRITE_UINT8(mem, s->sex);
    WRITE_STR(mem, s->name, s->name_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
svmsg_player_init_data_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_player_init_data_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, SVMSG_PLAYER_INIT_DATA_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->name_len);

    if (SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(s->name_len) >
        SVMSG_PLAYER_INIT_DATA_MAX_SZ)
        return -1;

    if (sz < SVMSG_PLAYER_INIT_DATA_COMPUTE_SZ(s->name_len)) return -1;

    READ_UINT64(mem, &s->char_id);
    READ_UINT32(mem, &s->map_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);
    READ_UINT8(mem, &s->sex);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint64 char_id;
    uint8 reason;
} svmsg_char_login_fail_t;

#define SVMSG_CHAR_LOGIN_FAIL_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
svmsg_char_login_fail_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_char_login_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + SVMSG_CHAR_LOGIN_FAIL_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_CHAR_LOGIN_FAIL);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT64(mem, s->char_id);
    WRITE_UINT8(mem, s->reason);

    return cryptchan_encrypt(cc, dst, src, SVMSG_CHAR_LOGIN_FAIL_SZ);
}

static inline int
svmsg_char_login_fail_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_char_login_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVMSG_CHAR_LOGIN_FAIL_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        SVMSG_CHAR_LOGIN_FAIL_SZ))
        return -1;
    READ_UINT64(mem, &s->char_id);
    READ_UINT8(mem, &s->reason);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint8 msg_len;
    const char *name;
    const char *msg;
} svmsg_global_chat_broadcast_t;

#define SVMSG_GLOBAL_CHAT_BROADCAST_SZ (sizeof(uint8) + sizeof(uint8))

#define SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(name_len, msg_len) \
    (SVMSG_GLOBAL_CHAT_BROADCAST_SZ + (name_len) + (msg_len))

#define SVMSG_GLOBAL_CHAT_BROADCAST_MAX_SZ \
    (SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(64, 256))

static inline int
svmsg_global_chat_broadcast_write(byte_buf_t *buf, svmsg_global_chat_broadcast_t *s)
{
    int sz = SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(s->name_len, s->msg_len);
    if (sz > SVMSG_GLOBAL_CHAT_BROADCAST_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(s->name_len, s->msg_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_GLOBAL_CHAT_BROADCAST);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT8(mem, s->msg_len);
    WRITE_STR(mem, s->name, s->name_len);
    WRITE_STR(mem, s->msg, s->msg_len);

    return 0;
}

static inline int
svmsg_global_chat_broadcast_read(byte_buf_t *buf, svmsg_global_chat_broadcast_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SVMSG_GLOBAL_CHAT_BROADCAST_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);
    READ_UINT8(mem, &s->msg_len);

    if (SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(s->name_len, s->msg_len) >
        SVMSG_GLOBAL_CHAT_BROADCAST_MAX_SZ)
        return -1;

    int req_sz = SVMSG_GLOBAL_CHAT_BROADCAST_COMPUTE_SZ(s->name_len, s->msg_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_VARCHAR(mem, s->name, s->name_len);
    READ_VARCHAR(mem, s->msg, s->msg_len);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} svmsg_walk_creature_t;

#define SVMSG_WALK_CREATURE_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
svmsg_walk_creature_write(byte_buf_t *buf, svmsg_walk_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_WALK_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_WALK_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
svmsg_walk_creature_read(byte_buf_t *buf, svmsg_walk_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_WALK_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} svmsg_walk_player_character_t;

#define SVMSG_WALK_PLAYER_CHARACTER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
svmsg_walk_player_character_write(byte_buf_t *buf, svmsg_walk_player_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_WALK_PLAYER_CHARACTER_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_WALK_PLAYER_CHARACTER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
svmsg_walk_player_character_read(byte_buf_t *buf, svmsg_walk_player_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_WALK_PLAYER_CHARACTER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 type_id;
    int32 x;
    int32 y;
    int8 z;
    int8 dir;
    uint8 flags;
} svmsg_new_creature_t;

#define SVMSG_NEW_CREATURE_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(int8) + sizeof(uint8))

static inline int
svmsg_new_creature_write(byte_buf_t *buf, svmsg_new_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_NEW_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_NEW_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->type_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_INT8(mem, s->dir);
    WRITE_UINT8(mem, s->flags);

    return 0;
}

static inline int
svmsg_new_creature_read(byte_buf_t *buf, svmsg_new_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_NEW_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->type_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_INT8(mem, &s->dir);
    READ_UINT8(mem, &s->flags);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint64 id;
    int32 type_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
    uint8 sex;
    const char *name;
} svmsg_new_player_character_t;

#define SVMSG_NEW_PLAYER_CHARACTER_SZ (sizeof(uint8) + sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8) + sizeof(uint8))

#define SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(name_len) \
    (SVMSG_NEW_PLAYER_CHARACTER_SZ + (name_len))

#define SVMSG_NEW_PLAYER_CHARACTER_MAX_SZ \
    (SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
svmsg_new_player_character_write(byte_buf_t *buf, svmsg_new_player_character_t *s)
{
    int sz = SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(s->name_len);
    if (sz > SVMSG_NEW_PLAYER_CHARACTER_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_NEW_PLAYER_CHARACTER);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->type_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);
    WRITE_UINT8(mem, s->sex);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
svmsg_new_player_character_read(byte_buf_t *buf, svmsg_new_player_character_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SVMSG_NEW_PLAYER_CHARACTER_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(s->name_len) >
        SVMSG_NEW_PLAYER_CHARACTER_MAX_SZ)
        return -1;

    int req_sz = SVMSG_NEW_PLAYER_CHARACTER_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->type_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);
    READ_UINT8(mem, &s->sex);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint64 id;
    uint8 dir;
} svmsg_player_character_dir_t;

#define SVMSG_PLAYER_CHARACTER_DIR_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
svmsg_player_character_dir_write(byte_buf_t *buf, svmsg_player_character_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_PLAYER_CHARACTER_DIR_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_PLAYER_CHARACTER_DIR);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
svmsg_player_character_dir_read(byte_buf_t *buf, svmsg_player_character_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_PLAYER_CHARACTER_DIR_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint8 dir;
} svmsg_pawn_dir_t;

#define SVMSG_PAWN_DIR_SZ (sizeof(uint8))

static inline int
svmsg_pawn_dir_write(byte_buf_t *buf, svmsg_pawn_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_PAWN_DIR_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_PAWN_DIR);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
svmsg_pawn_dir_read(byte_buf_t *buf, svmsg_pawn_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_PAWN_DIR_SZ);
    if (!mem) return 1;
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
} svmsg_remove_creature_t;

#define SVMSG_REMOVE_CREATURE_SZ (sizeof(uint64))

static inline int
svmsg_remove_creature_write(byte_buf_t *buf, svmsg_remove_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_REMOVE_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_REMOVE_CREATURE);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
svmsg_remove_creature_read(byte_buf_t *buf, svmsg_remove_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_REMOVE_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint64 id;
} svmsg_remove_player_character_t;

#define SVMSG_REMOVE_PLAYER_CHARACTER_SZ (sizeof(uint64))

static inline int
svmsg_remove_player_character_write(byte_buf_t *buf, svmsg_remove_player_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_REMOVE_PLAYER_CHARACTER_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_REMOVE_PLAYER_CHARACTER);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
svmsg_remove_player_character_read(byte_buf_t *buf, svmsg_remove_player_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_REMOVE_PLAYER_CHARACTER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 type_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} svmsg_new_dynamic_obj_t;

#define SVMSG_NEW_DYNAMIC_OBJ_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
svmsg_new_dynamic_obj_write(byte_buf_t *buf, svmsg_new_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_NEW_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_NEW_DYNAMIC_OBJ);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->type_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
svmsg_new_dynamic_obj_read(byte_buf_t *buf, svmsg_new_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_NEW_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->type_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
} svmsg_remove_dynamic_obj_t;

#define SVMSG_REMOVE_DYNAMIC_OBJ_SZ (sizeof(uint64))

static inline int
svmsg_remove_dynamic_obj_write(byte_buf_t *buf, svmsg_remove_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_REMOVE_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_REMOVE_DYNAMIC_OBJ);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
svmsg_remove_dynamic_obj_read(byte_buf_t *buf, svmsg_remove_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_REMOVE_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint16 emote_id;
    int8 have_target;
    uint64 player_id;
    uint64 target_id;
} svmsg_player_player_emote_t;

#define SVMSG_PLAYER_PLAYER_EMOTE_SZ (sizeof(uint16) + sizeof(int8) + sizeof(uint64) + sizeof(uint64))

static inline int
svmsg_player_player_emote_write(byte_buf_t *buf, svmsg_player_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_PLAYER_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_PLAYER_PLAYER_EMOTE);
    WRITE_UINT16(mem, s->emote_id);
    WRITE_INT8(mem, s->have_target);
    WRITE_UINT64(mem, s->player_id);
    WRITE_UINT64(mem, s->target_id);

    return 0;
}

static inline int
svmsg_player_player_emote_read(byte_buf_t *buf, svmsg_player_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_PLAYER_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    READ_UINT16(mem, &s->emote_id);
    READ_INT8(mem, &s->have_target);
    READ_UINT64(mem, &s->player_id);
    READ_UINT64(mem, &s->target_id);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} svmsg_update_player_position_t;

#define SVMSG_UPDATE_PLAYER_POSITION_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
svmsg_update_player_position_write(byte_buf_t *buf, svmsg_update_player_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_UPDATE_PLAYER_POSITION_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_UPDATE_PLAYER_POSITION);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
svmsg_update_player_position_read(byte_buf_t *buf, svmsg_update_player_position_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_UPDATE_PLAYER_POSITION_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint8 reason;
} svmsg_create_character_fail_t;

#define SVMSG_CREATE_CHARACTER_FAIL_SZ (sizeof(uint8))

static inline int
svmsg_create_character_fail_write(byte_buf_t *buf, svmsg_create_character_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_CREATE_CHARACTER_FAIL_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_CREATE_CHARACTER_FAIL);
    WRITE_UINT8(mem, s->reason);

    return 0;
}

static inline int
svmsg_create_character_fail_read(byte_buf_t *buf, svmsg_create_character_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_CREATE_CHARACTER_FAIL_SZ);
    if (!mem) return 1;
    READ_UINT8(mem, &s->reason);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint64 id;
    uint8 race;
    uint8 sex;
    uint32 map_id;
    uint32 instance_id;
    int32 x;
    int32 y;
    int8 z;
    const char *name;
} svmsg_new_character_created_t;

#define SVMSG_NEW_CHARACTER_CREATED_SZ (sizeof(uint8) + sizeof(uint64) + sizeof(uint8) + sizeof(uint8) + sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8))

#define SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(name_len) \
    (SVMSG_NEW_CHARACTER_CREATED_SZ + (name_len))

#define SVMSG_NEW_CHARACTER_CREATED_MAX_SZ \
    (SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
svmsg_new_character_created_write(byte_buf_t *buf, svmsg_new_character_created_t *s)
{
    int sz = SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(s->name_len);
    if (sz > SVMSG_NEW_CHARACTER_CREATED_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_NEW_CHARACTER_CREATED);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->race);
    WRITE_UINT8(mem, s->sex);
    WRITE_UINT32(mem, s->map_id);
    WRITE_UINT32(mem, s->instance_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
svmsg_new_character_created_read(byte_buf_t *buf, svmsg_new_character_created_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SVMSG_NEW_CHARACTER_CREATED_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(s->name_len) >
        SVMSG_NEW_CHARACTER_CREATED_MAX_SZ)
        return -1;

    int req_sz = SVMSG_NEW_CHARACTER_CREATED_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->race);
    READ_UINT8(mem, &s->sex);
    READ_UINT32(mem, &s->map_id);
    READ_UINT32(mem, &s->instance_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint64 id;
    uint8 dir;
} svmsg_creature_dir_t;

#define SVMSG_CREATURE_DIR_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
svmsg_creature_dir_write(byte_buf_t *buf, svmsg_creature_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_CREATURE_DIR_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_CREATURE_DIR);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
svmsg_creature_dir_read(byte_buf_t *buf, svmsg_creature_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_CREATURE_DIR_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint8 is_online;
    const char *name;
} svmsg_shard_list_item_t;

#define SVMSG_SHARD_LIST_ITEM_SZ (sizeof(uint8) + sizeof(uint8))

#define SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(name_len) \
    (SVMSG_SHARD_LIST_ITEM_SZ + (name_len))

#define SVMSG_SHARD_LIST_ITEM_MAX_SZ \
    (SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(MAX_SHARD_NAME_LEN))

static inline int
svmsg_shard_list_item_write(byte_buf_t *buf, svmsg_shard_list_item_t *s)
{
    int sz = SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(s->name_len);
    if (sz > SVMSG_SHARD_LIST_ITEM_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_SHARD_LIST_ITEM);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT8(mem, s->is_online);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
svmsg_shard_list_item_read(byte_buf_t *buf, svmsg_shard_list_item_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SVMSG_SHARD_LIST_ITEM_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(s->name_len) >
        SVMSG_SHARD_LIST_ITEM_MAX_SZ)
        return -1;

    int req_sz = SVMSG_SHARD_LIST_ITEM_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT8(mem, &s->is_online);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint8 reason;
} svmsg_shard_select_fail_t;

#define SVMSG_SHARD_SELECT_FAIL_SZ (sizeof(uint8))

static inline int
svmsg_shard_select_fail_write(byte_buf_t *buf, svmsg_shard_select_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + SVMSG_SHARD_SELECT_FAIL_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, SVMSG_SHARD_SELECT_FAIL);
    WRITE_UINT8(mem, s->reason);

    return 0;
}

static inline int
svmsg_shard_select_fail_read(byte_buf_t *buf, svmsg_shard_select_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SVMSG_SHARD_SELECT_FAIL_SZ);
    if (!mem) return 1;
    READ_UINT8(mem, &s->reason);

    return 0;
}

typedef struct
{
    uint8 shard_name_len;
    uint32 ip;
    uint16 port;
    const char *shard_name;
    uint8 token[AUTH_TOKEN_SZ];
} svmsg_shard_select_success_t;

#define SVMSG_SHARD_SELECT_SUCCESS_SZ (sizeof(uint8) + sizeof(uint32) + sizeof(uint16) + AUTH_TOKEN_SZ)

#define SVMSG_SHARD_SELECT_SUCCESS_COMPUTE_SZ(shard_name_len) \
    (SVMSG_SHARD_SELECT_SUCCESS_SZ + (shard_name_len))

#define SVMSG_SHARD_SELECT_SUCCESS_MAX_SZ \
    (SVMSG_SHARD_SELECT_SUCCESS_COMPUTE_SZ(MAX_SHARD_NAME_LEN))

static inline int
svmsg_shard_select_success_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_shard_select_success_t *s)
{
    int sz = SVMSG_SHARD_SELECT_SUCCESS_COMPUTE_SZ(s->shard_name_len);
    if (sz > SVMSG_SHARD_SELECT_SUCCESS_MAX_SZ)
        return -1;

    uint8 *mem = prep_svmsg_write_var_encrypted(buf, SVMSG_SHARD_SELECT_SUCCESS, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->shard_name_len);
    WRITE_UINT32(mem, s->ip);
    WRITE_UINT16(mem, s->port);
    WRITE_STR(mem, s->shard_name, s->shard_name_len);
    WRITE_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
svmsg_shard_select_success_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    svmsg_shard_select_success_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, SVMSG_SHARD_SELECT_SUCCESS_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->shard_name_len);

    if (SVMSG_SHARD_SELECT_SUCCESS_COMPUTE_SZ(s->shard_name_len) >
        SVMSG_SHARD_SELECT_SUCCESS_MAX_SZ)
        return -1;

    if (sz < SVMSG_SHARD_SELECT_SUCCESS_COMPUTE_SZ(s->shard_name_len)) return -1;

    READ_UINT32(mem, &s->ip);
    READ_UINT16(mem, &s->port);
    READ_VARCHAR(mem, s->shard_name, s->shard_name_len);
    READ_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return 0;
}

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} clmsg_pub_key_t;

#define CLMSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
clmsg_pub_key_write(byte_buf_t *buf, clmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
clmsg_pub_key_read(byte_buf_t *buf, clmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} clmsg_stream_header_t;

#define CLMSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
clmsg_stream_header_write(byte_buf_t *buf, clmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
clmsg_stream_header_read(byte_buf_t *buf, clmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
clmsg_register_account_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_REGISTER_ACCOUNT);

    return 0;
}

typedef struct
{
    uint8 acc_name_len;
    uint8 pw_len;
    const char *acc_name;
    const char *pw;
} clmsg_login_request_t;

#define CLMSG_LOGIN_REQUEST_SZ (sizeof(uint8) + sizeof(uint8))

#define CLMSG_LOGIN_REQUEST_COMPUTE_SZ(acc_name_len, pw_len) \
    (CLMSG_LOGIN_REQUEST_SZ + (acc_name_len) + (pw_len))

#define CLMSG_LOGIN_REQUEST_MAX_SZ \
    (CLMSG_LOGIN_REQUEST_COMPUTE_SZ(64, 16))

static inline int
clmsg_login_request_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    clmsg_login_request_t *s)
{
    int sz = CLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->acc_name_len, s->pw_len);
    if (sz > CLMSG_LOGIN_REQUEST_MAX_SZ)
        return -1;

    uint8 *mem = prep_clmsg_write_var_encrypted(buf, CLMSG_LOGIN_REQUEST, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->acc_name_len);
    WRITE_UINT8(mem, s->pw_len);
    WRITE_STR(mem, s->acc_name, s->acc_name_len);
    WRITE_STR(mem, s->pw, s->pw_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
clmsg_login_request_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    clmsg_login_request_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, CLMSG_LOGIN_REQUEST_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->acc_name_len);
    READ_UINT8(mem, &s->pw_len);

    if (CLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->acc_name_len, s->pw_len) >
        CLMSG_LOGIN_REQUEST_MAX_SZ)
        return -1;

    if (sz < CLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->acc_name_len, s->pw_len)) return -1;

    READ_VARCHAR(mem, s->acc_name, s->acc_name_len);
    READ_VARCHAR(mem, s->pw, s->pw_len);

    return 0;
}

static inline int
clmsg_keep_alive_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_KEEP_ALIVE);

    return 0;
}

typedef struct
{
    uint8 msg_len;
    const char *msg;
} clmsg_chat_msg_t;

#define CLMSG_CHAT_MSG_SZ (sizeof(uint8))

#define CLMSG_CHAT_MSG_COMPUTE_SZ(msg_len) \
    (CLMSG_CHAT_MSG_SZ + (msg_len))

#define CLMSG_CHAT_MSG_MAX_SZ \
    (CLMSG_CHAT_MSG_COMPUTE_SZ(256))

static inline int
clmsg_chat_msg_write(byte_buf_t *buf, clmsg_chat_msg_t *s)
{
    int sz = CLMSG_CHAT_MSG_COMPUTE_SZ(s->msg_len);
    if (sz > CLMSG_CHAT_MSG_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        CLMSG_CHAT_MSG_COMPUTE_SZ(s->msg_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_CHAT_MSG);
    WRITE_UINT8(mem, s->msg_len);
    WRITE_STR(mem, s->msg, s->msg_len);

    return 0;
}

static inline int
clmsg_chat_msg_read(byte_buf_t *buf, clmsg_chat_msg_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < CLMSG_CHAT_MSG_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->msg_len);

    if (CLMSG_CHAT_MSG_COMPUTE_SZ(s->msg_len) >
        CLMSG_CHAT_MSG_MAX_SZ)
        return -1;

    int req_sz = CLMSG_CHAT_MSG_COMPUTE_SZ(s->msg_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_VARCHAR(mem, s->msg, s->msg_len);

    return 0;
}

typedef struct
{
    uint8 dir;
} clmsg_player_move_t;

#define CLMSG_PLAYER_MOVE_SZ (sizeof(uint8))

static inline int
clmsg_player_move_write(byte_buf_t *buf, clmsg_player_move_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_PLAYER_MOVE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_PLAYER_MOVE);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
clmsg_player_move_read(byte_buf_t *buf, clmsg_player_move_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_PLAYER_MOVE_SZ);
    if (!mem) return 1;
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    int32 x;
    int32 y;
    int8 z;
} clmsg_find_path_t;

#define CLMSG_FIND_PATH_SZ (sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
clmsg_find_path_write(byte_buf_t *buf, clmsg_find_path_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_FIND_PATH_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_FIND_PATH);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
clmsg_find_path_read(byte_buf_t *buf, clmsg_find_path_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_FIND_PATH_SZ);
    if (!mem) return 1;
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint16 sequence;
} clmsg_last_received_pos_t;

#define CLMSG_LAST_RECEIVED_POS_SZ (sizeof(uint16))

static inline int
clmsg_last_received_pos_write(byte_buf_t *buf, clmsg_last_received_pos_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_LAST_RECEIVED_POS_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_LAST_RECEIVED_POS);
    WRITE_UINT16(mem, s->sequence);

    return 0;
}

static inline int
clmsg_last_received_pos_read(byte_buf_t *buf, clmsg_last_received_pos_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_LAST_RECEIVED_POS_SZ);
    if (!mem) return 1;
    READ_UINT16(mem, &s->sequence);

    return 0;
}

typedef struct
{
    uint16 emote_id;
    int8 have_target;
    uint64 target_id;
} clmsg_player_emote_t;

#define CLMSG_PLAYER_EMOTE_SZ (sizeof(uint16) + sizeof(int8) + sizeof(uint64))

static inline int
clmsg_player_emote_write(byte_buf_t *buf, clmsg_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_PLAYER_EMOTE);
    WRITE_UINT16(mem, s->emote_id);
    WRITE_INT8(mem, s->have_target);
    WRITE_UINT64(mem, s->target_id);

    return 0;
}

static inline int
clmsg_player_emote_read(byte_buf_t *buf, clmsg_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    READ_UINT16(mem, &s->emote_id);
    READ_INT8(mem, &s->have_target);
    READ_UINT64(mem, &s->target_id);

    return 0;
}

typedef struct
{
    uint8 name_len;
    uint8 race;
    uint8 sex;
    const char *name;
} clmsg_create_character_t;

#define CLMSG_CREATE_CHARACTER_SZ (sizeof(uint8) + sizeof(uint8) + sizeof(uint8))

#define CLMSG_CREATE_CHARACTER_COMPUTE_SZ(name_len) \
    (CLMSG_CREATE_CHARACTER_SZ + (name_len))

#define CLMSG_CREATE_CHARACTER_MAX_SZ \
    (CLMSG_CREATE_CHARACTER_COMPUTE_SZ(MAX_CHARACTER_NAME_LEN))

static inline int
clmsg_create_character_write(byte_buf_t *buf, clmsg_create_character_t *s)
{
    int sz = CLMSG_CREATE_CHARACTER_COMPUTE_SZ(s->name_len);
    if (sz > CLMSG_CREATE_CHARACTER_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        CLMSG_CREATE_CHARACTER_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_CREATE_CHARACTER);
    WRITE_UINT8(mem, s->name_len);
    WRITE_UINT8(mem, s->race);
    WRITE_UINT8(mem, s->sex);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
clmsg_create_character_read(byte_buf_t *buf, clmsg_create_character_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < CLMSG_CREATE_CHARACTER_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (CLMSG_CREATE_CHARACTER_COMPUTE_SZ(s->name_len) >
        CLMSG_CREATE_CHARACTER_MAX_SZ)
        return -1;

    int req_sz = CLMSG_CREATE_CHARACTER_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT8(mem, &s->race);
    READ_UINT8(mem, &s->sex);
    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

typedef struct
{
    uint64 id;
} clmsg_log_in_character_t;

#define CLMSG_LOG_IN_CHARACTER_SZ (sizeof(uint64))

static inline int
clmsg_log_in_character_write(byte_buf_t *buf, clmsg_log_in_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ + CLMSG_LOG_IN_CHARACTER_SZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_LOG_IN_CHARACTER);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
clmsg_log_in_character_read(byte_buf_t *buf, clmsg_log_in_character_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CLMSG_LOG_IN_CHARACTER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint8 account_name_len;
    uint8 shard_name_len;
    const char *account_name;
    const char *shard_name;
    uint8 token[AUTH_TOKEN_SZ];
} clmsg_auth_proof_t;

#define CLMSG_AUTH_PROOF_SZ (sizeof(uint8) + sizeof(uint8) + AUTH_TOKEN_SZ)

#define CLMSG_AUTH_PROOF_COMPUTE_SZ(account_name_len, shard_name_len) \
    (CLMSG_AUTH_PROOF_SZ + (account_name_len) + (shard_name_len))

#define CLMSG_AUTH_PROOF_MAX_SZ \
    (CLMSG_AUTH_PROOF_COMPUTE_SZ(MAX_ACC_NAME_LEN, MAX_SHARD_NAME_LEN))

static inline int
clmsg_auth_proof_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    clmsg_auth_proof_t *s)
{
    int sz = CLMSG_AUTH_PROOF_COMPUTE_SZ(s->account_name_len, s->shard_name_len);
    if (sz > CLMSG_AUTH_PROOF_MAX_SZ)
        return -1;

    uint8 *mem = prep_clmsg_write_var_encrypted(buf, CLMSG_AUTH_PROOF, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->account_name_len);
    WRITE_UINT8(mem, s->shard_name_len);
    WRITE_STR(mem, s->account_name, s->account_name_len);
    WRITE_STR(mem, s->shard_name, s->shard_name_len);
    WRITE_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
clmsg_auth_proof_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    clmsg_auth_proof_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, CLMSG_AUTH_PROOF_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->account_name_len);
    READ_UINT8(mem, &s->shard_name_len);

    if (CLMSG_AUTH_PROOF_COMPUTE_SZ(s->account_name_len, s->shard_name_len) >
        CLMSG_AUTH_PROOF_MAX_SZ)
        return -1;

    if (sz < CLMSG_AUTH_PROOF_COMPUTE_SZ(s->account_name_len, s->shard_name_len)) return -1;

    READ_VARCHAR(mem, s->account_name, s->account_name_len);
    READ_VARCHAR(mem, s->shard_name, s->shard_name_len);
    READ_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return 0;
}

typedef struct
{
    uint8 name_len;
    const char *name;
} clmsg_select_shard_t;

#define CLMSG_SELECT_SHARD_SZ (sizeof(uint8))

#define CLMSG_SELECT_SHARD_COMPUTE_SZ(name_len) \
    (CLMSG_SELECT_SHARD_SZ + (name_len))

#define CLMSG_SELECT_SHARD_MAX_SZ \
    (CLMSG_SELECT_SHARD_COMPUTE_SZ(MAX_SHARD_NAME_LEN))

static inline int
clmsg_select_shard_write(byte_buf_t *buf, clmsg_select_shard_t *s)
{
    int sz = CLMSG_SELECT_SHARD_COMPUTE_SZ(s->name_len);
    if (sz > CLMSG_SELECT_SHARD_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, MSGTSZ + \
        CLMSG_SELECT_SHARD_COMPUTE_SZ(s->name_len));
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_SELECT_SHARD);
    WRITE_UINT8(mem, s->name_len);
    WRITE_STR(mem, s->name, s->name_len);

    return 0;
}

static inline int
clmsg_select_shard_read(byte_buf_t *buf, clmsg_select_shard_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < CLMSG_SELECT_SHARD_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->name_len);

    if (CLMSG_SELECT_SHARD_COMPUTE_SZ(s->name_len) >
        CLMSG_SELECT_SHARD_MAX_SZ)
        return -1;

    int req_sz = CLMSG_SELECT_SHARD_COMPUTE_SZ(s->name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_VARCHAR(mem, s->name, s->name_len);

    return 0;
}

static inline int
clmsg_cancel_select_shard_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, MSGTSZ);
    if (!mem) return 1;
    WRITE_MSG_TYPE(mem, CLMSG_CANCEL_SELECT_SHARD);

    return 0;
}

