#include "sv_time.h"

sys_time_t program_start_time = {0};

int
init_time()
    {return get_monotonic_time(&program_start_time);}

void
perf_clock_init(perf_clock_t *pc, uint32 target_fps)
{
    pc->last_tick   = get_program_ticks_ms();
    pc->delta_ms    = 0;
    pc->delta_s     = 0.f;
    pc->target_fps  = target_fps;
}

void
perf_clock_tick(perf_clock_t *pc)
{
    uint64 time_now     = (uint64)get_program_ticks_ms();
    uint64 frame_dur    = time_now - pc->last_tick;
    uint32 target_fps   = (uint32)pc->target_fps;

    if (target_fps > 0)
    {
        uint32 tar_dur = (uint32)(1.f / (float)target_fps * 1000.f);

        if (frame_dur < tar_dur)
        {
            uint32 tar_time = (uint32)(tar_dur - frame_dur);
            sleep_ms(tar_time);

            time_now    = (uint64)get_program_ticks_ms();
            frame_dur   = time_now - pc->last_tick;
        }
    }

    pc->last_tick   = time_now;
    pc->delta_ms    = (uint32)frame_dur;
    pc->delta_s     = (double)frame_dur / 1000.f;
    pc->time        = time_now;
}
