typedef uint8 wmsg_type_t;
#define WMSGTSZ sizeof(wmsg_type_t)

#if MUTA_ENDIANNESS == MUTA_LIL_ENDIAN
#define WRITE_WMSG_TYPE(mem, val) \
    *(wmsg_type_t*)(mem) = (wmsg_type_t)(val); \
    (mem) = (uint8*)(mem) + sizeof(wmsg_type_t);
#elif MUTA_ENDIANNESS == MUTA_BIG_ENDIAN
#   error "Big endian unsupported"
#else
#   error "Endianness undefined"
#endif /* MUTA_LIL_ENDIAN */

enum muta_wmsg_types
{
    WMSG_KEEP_ALIVE = 0,
    WMSG_PUB_KEY,
    WMSG_STREAM_HEADER,

    NUM_MUTA_WMSG_TYPES
};

#if NUM_MUTA_WMSG_TYPES > 255
#   error too many MUTA_WMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(wmsg, WMSGTSZ, WRITE_WMSG_TYPE);

enum muta_swmsg_types
{
    SWMSG_LOAD_INSTANCE_PART = NUM_MUTA_WMSG_TYPES,
    SWMSG_REMOVE_INSTANCE_PART,
    SWMSG_SPAWN_PLAYER,
    SWMSG_DESPAWN_PLAYER,
    SWMSG_MOVE_PLAYER,
    SWMSG_FIND_PATH_PLAYER,
    SWMSG_SPAWN_DYNAMIC_OBJ,
    SWMSG_DESPAWN_DYNAMIC_OBJ,
    SWMSG_TELEPORT_PLAYER,
    SWMSG_SPAWN_CREATURE,
    SWMSG_DESPAWN_CREATURE,
    SWMSG_RELOAD_ENTITY_SCRIPT,

    NUM_MUTA_SWMSG_TYPES
};

#if NUM_MUTA_SWMSG_TYPES > 255
#   error too many MUTA_SWMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(swmsg, WMSGTSZ, WRITE_WMSG_TYPE);

enum muta_wsmsg_types
{
    WSMSG_LOAD_INSTANCE_PART_RESULT = NUM_MUTA_WMSG_TYPES,
    WSMSG_DESPAWN_PLAYER,
    WSMSG_REMOVE_CREATURE,
    WSMSG_WALK_CREATURE,
    WSMSG_WALK_PLAYER,
    WSMSG_REQUEST_SPAWN_CREATURE,
    WSMSG_SPAWN_PLAYER_FAIL,
    WSMSG_CONFIRM_SPAWN_PLAYER,
    WSMSG_PLAYER_DIR,
    WSMSG_REQUEST_SPAWN_DYNAMIC_OBJ,
    WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ,
    WSMSG_TELEPORT_PLAYER,
    WSMSG_CONFIRM_SPAWN_CREATURE,
    WSMSG_CREATURE_DIR,
    WSMSG_CREATURE_PLAYER_EMOTE,

    NUM_MUTA_WSMSG_TYPES
};

#if NUM_MUTA_WSMSG_TYPES > 255
#   error too many MUTA_WSMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(wsmsg, WMSGTSZ, WRITE_WMSG_TYPE);

static inline int
wmsg_keep_alive_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WMSG_KEEP_ALIVE);

    return 0;
}

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} wmsg_pub_key_t;

#define WMSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
wmsg_pub_key_write(byte_buf_t *buf, wmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WMSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
wmsg_pub_key_read(byte_buf_t *buf, wmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} wmsg_stream_header_t;

#define WMSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
wmsg_stream_header_write(byte_buf_t *buf, wmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WMSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
wmsg_stream_header_read(byte_buf_t *buf, wmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint32 map_id;
    uint32 part_id;
    uint32 cx;
    uint32 cy;
    uint32 cw;
    uint32 ch;
    uint32 num_entities;
} swmsg_load_instance_part_t;

#define SWMSG_LOAD_INSTANCE_PART_SZ (sizeof(uint32) + sizeof(uint32) + sizeof(uint32) + sizeof(uint32) + sizeof(uint32) + sizeof(uint32) + sizeof(uint32))

static inline int
swmsg_load_instance_part_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    swmsg_load_instance_part_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + SWMSG_LOAD_INSTANCE_PART_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_LOAD_INSTANCE_PART);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->map_id);
    WRITE_UINT32(mem, s->part_id);
    WRITE_UINT32(mem, s->cx);
    WRITE_UINT32(mem, s->cy);
    WRITE_UINT32(mem, s->cw);
    WRITE_UINT32(mem, s->ch);
    WRITE_UINT32(mem, s->num_entities);

    return cryptchan_encrypt(cc, dst, src, SWMSG_LOAD_INSTANCE_PART_SZ);
}

static inline int
swmsg_load_instance_part_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    swmsg_load_instance_part_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        SWMSG_LOAD_INSTANCE_PART_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        SWMSG_LOAD_INSTANCE_PART_SZ))
        return -1;
    READ_UINT32(mem, &s->map_id);
    READ_UINT32(mem, &s->part_id);
    READ_UINT32(mem, &s->cx);
    READ_UINT32(mem, &s->cy);
    READ_UINT32(mem, &s->cw);
    READ_UINT32(mem, &s->ch);
    READ_UINT32(mem, &s->num_entities);

    return 0;
}

typedef struct
{
    uint32 part_id;
} swmsg_remove_instance_part_t;

#define SWMSG_REMOVE_INSTANCE_PART_SZ (sizeof(uint32))

static inline int
swmsg_remove_instance_part_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    swmsg_remove_instance_part_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + SWMSG_REMOVE_INSTANCE_PART_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_REMOVE_INSTANCE_PART);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->part_id);

    return cryptchan_encrypt(cc, dst, src, SWMSG_REMOVE_INSTANCE_PART_SZ);
}

static inline int
swmsg_remove_instance_part_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    swmsg_remove_instance_part_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        SWMSG_REMOVE_INSTANCE_PART_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        SWMSG_REMOVE_INSTANCE_PART_SZ))
        return -1;
    READ_UINT32(mem, &s->part_id);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 race;
    uint8 dir;
    uint8 sex;
} swmsg_spawn_player_t;

#define SWMSG_SPAWN_PLAYER_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8) + sizeof(uint8) + sizeof(uint8))

static inline int
swmsg_spawn_player_write(byte_buf_t *buf, swmsg_spawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_SPAWN_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_SPAWN_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->race);
    WRITE_UINT8(mem, s->dir);
    WRITE_UINT8(mem, s->sex);

    return 0;
}

static inline int
swmsg_spawn_player_read(byte_buf_t *buf, swmsg_spawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_SPAWN_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->race);
    READ_UINT8(mem, &s->dir);
    READ_UINT8(mem, &s->sex);

    return 0;
}

typedef struct
{
    uint64 id;
} swmsg_despawn_player_t;

#define SWMSG_DESPAWN_PLAYER_SZ (sizeof(uint64))

static inline int
swmsg_despawn_player_write(byte_buf_t *buf, swmsg_despawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_DESPAWN_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_DESPAWN_PLAYER);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
swmsg_despawn_player_read(byte_buf_t *buf, swmsg_despawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_DESPAWN_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint64 id;
    uint8 dir;
} swmsg_move_player_t;

#define SWMSG_MOVE_PLAYER_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
swmsg_move_player_write(byte_buf_t *buf, swmsg_move_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_MOVE_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_MOVE_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
swmsg_move_player_read(byte_buf_t *buf, swmsg_move_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_MOVE_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} swmsg_find_path_player_t;

#define SWMSG_FIND_PATH_PLAYER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
swmsg_find_path_player_write(byte_buf_t *buf, swmsg_find_path_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_FIND_PATH_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_FIND_PATH_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
swmsg_find_path_player_read(byte_buf_t *buf, swmsg_find_path_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_FIND_PATH_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 type_id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} swmsg_spawn_dynamic_obj_t;

#define SWMSG_SPAWN_DYNAMIC_OBJ_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
swmsg_spawn_dynamic_obj_write(byte_buf_t *buf, swmsg_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_SPAWN_DYNAMIC_OBJ);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->type_id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
swmsg_spawn_dynamic_obj_read(byte_buf_t *buf, swmsg_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->type_id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
} swmsg_despawn_dynamic_obj_t;

#define SWMSG_DESPAWN_DYNAMIC_OBJ_SZ (sizeof(uint64))

static inline int
swmsg_despawn_dynamic_obj_write(byte_buf_t *buf, swmsg_despawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_DESPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_DESPAWN_DYNAMIC_OBJ);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
swmsg_despawn_dynamic_obj_read(byte_buf_t *buf, swmsg_despawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_DESPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} swmsg_teleport_player_t;

#define SWMSG_TELEPORT_PLAYER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
swmsg_teleport_player_write(byte_buf_t *buf, swmsg_teleport_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_TELEPORT_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_TELEPORT_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
swmsg_teleport_player_read(byte_buf_t *buf, swmsg_teleport_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_TELEPORT_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 type_id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
    uint8 flags;
} swmsg_spawn_creature_t;

#define SWMSG_SPAWN_CREATURE_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8) + sizeof(uint8))

static inline int
swmsg_spawn_creature_write(byte_buf_t *buf, swmsg_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_SPAWN_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->type_id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);
    WRITE_UINT8(mem, s->flags);

    return 0;
}

static inline int
swmsg_spawn_creature_read(byte_buf_t *buf, swmsg_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->type_id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);
    READ_UINT8(mem, &s->flags);

    return 0;
}

typedef struct
{
    uint64 id;
} swmsg_despawn_creature_t;

#define SWMSG_DESPAWN_CREATURE_SZ (sizeof(uint64))

static inline int
swmsg_despawn_creature_write(byte_buf_t *buf, swmsg_despawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + SWMSG_DESPAWN_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_DESPAWN_CREATURE);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
swmsg_despawn_creature_read(byte_buf_t *buf, swmsg_despawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, SWMSG_DESPAWN_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint8 script_name_len;
    const char *script_name;
} swmsg_reload_entity_script_t;

#define SWMSG_RELOAD_ENTITY_SCRIPT_SZ (sizeof(uint8))

#define SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(script_name_len) \
    (SWMSG_RELOAD_ENTITY_SCRIPT_SZ + (script_name_len))

#define SWMSG_RELOAD_ENTITY_SCRIPT_MAX_SZ \
    (SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(SWMSG_RELOAD_ENTITY_SCRIPT))

static inline int
swmsg_reload_entity_script_write(byte_buf_t *buf, swmsg_reload_entity_script_t *s)
{
    int sz = SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(s->script_name_len);
    if (sz > SWMSG_RELOAD_ENTITY_SCRIPT_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + \
        SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(s->script_name_len));
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, SWMSG_RELOAD_ENTITY_SCRIPT);
    WRITE_UINT8(mem, s->script_name_len);
    WRITE_STR(mem, s->script_name, s->script_name_len);

    return 0;
}

static inline int
swmsg_reload_entity_script_read(byte_buf_t *buf, swmsg_reload_entity_script_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < SWMSG_RELOAD_ENTITY_SCRIPT_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT8(mem, &s->script_name_len);

    if (SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(s->script_name_len) >
        SWMSG_RELOAD_ENTITY_SCRIPT_MAX_SZ)
        return -1;

    int req_sz = SWMSG_RELOAD_ENTITY_SCRIPT_COMPUTE_SZ(s->script_name_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_VARCHAR(mem, s->script_name, s->script_name_len);

    return 0;
}

typedef struct
{
    uint32 part_id;
    uint8 result;
} wsmsg_load_instance_part_result_t;

#define WSMSG_LOAD_INSTANCE_PART_RESULT_SZ (sizeof(uint32) + sizeof(uint8))

static inline int
wsmsg_load_instance_part_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    wsmsg_load_instance_part_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + WSMSG_LOAD_INSTANCE_PART_RESULT_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_LOAD_INSTANCE_PART_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->part_id);
    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, WSMSG_LOAD_INSTANCE_PART_RESULT_SZ);
}

static inline int
wsmsg_load_instance_part_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    wsmsg_load_instance_part_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        WSMSG_LOAD_INSTANCE_PART_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        WSMSG_LOAD_INSTANCE_PART_RESULT_SZ))
        return -1;
    READ_UINT32(mem, &s->part_id);
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} wsmsg_despawn_player_t;

#define WSMSG_DESPAWN_PLAYER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
wsmsg_despawn_player_write(byte_buf_t *buf, wsmsg_despawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_DESPAWN_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_DESPAWN_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
wsmsg_despawn_player_read(byte_buf_t *buf, wsmsg_despawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_DESPAWN_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} wsmsg_remove_creature_t;

#define WSMSG_REMOVE_CREATURE_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
wsmsg_remove_creature_write(byte_buf_t *buf, wsmsg_remove_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_REMOVE_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_REMOVE_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
wsmsg_remove_creature_read(byte_buf_t *buf, wsmsg_remove_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_REMOVE_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} wsmsg_walk_creature_t;

#define WSMSG_WALK_CREATURE_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
wsmsg_walk_creature_write(byte_buf_t *buf, wsmsg_walk_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_WALK_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_WALK_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
wsmsg_walk_creature_read(byte_buf_t *buf, wsmsg_walk_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_WALK_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} wsmsg_walk_player_t;

#define WSMSG_WALK_PLAYER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
wsmsg_walk_player_write(byte_buf_t *buf, wsmsg_walk_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_WALK_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_WALK_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
wsmsg_walk_player_read(byte_buf_t *buf, wsmsg_walk_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_WALK_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 inst_part_id;
    int32 type_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} wsmsg_request_spawn_creature_t;

#define WSMSG_REQUEST_SPAWN_CREATURE_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
wsmsg_request_spawn_creature_write(byte_buf_t *buf, wsmsg_request_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_REQUEST_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_REQUEST_SPAWN_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->type_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_request_spawn_creature_read(byte_buf_t *buf, wsmsg_request_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_REQUEST_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->type_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
} wsmsg_spawn_player_fail_t;

#define WSMSG_SPAWN_PLAYER_FAIL_SZ (sizeof(uint64))

static inline int
wsmsg_spawn_player_fail_write(byte_buf_t *buf, wsmsg_spawn_player_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_SPAWN_PLAYER_FAIL_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_SPAWN_PLAYER_FAIL);
    WRITE_UINT64(mem, s->id);

    return 0;
}

static inline int
wsmsg_spawn_player_fail_read(byte_buf_t *buf, wsmsg_spawn_player_fail_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_SPAWN_PLAYER_FAIL_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 map_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} wsmsg_confirm_spawn_player_t;

#define WSMSG_CONFIRM_SPAWN_PLAYER_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
wsmsg_confirm_spawn_player_write(byte_buf_t *buf, wsmsg_confirm_spawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_CONFIRM_SPAWN_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_CONFIRM_SPAWN_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->map_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_confirm_spawn_player_read(byte_buf_t *buf, wsmsg_confirm_spawn_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_CONFIRM_SPAWN_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->map_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
    uint8 dir;
} wsmsg_player_dir_t;

#define WSMSG_PLAYER_DIR_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
wsmsg_player_dir_write(byte_buf_t *buf, wsmsg_player_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_PLAYER_DIR_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_PLAYER_DIR);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_player_dir_read(byte_buf_t *buf, wsmsg_player_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_PLAYER_DIR_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint32 type_id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} wsmsg_request_spawn_dynamic_obj_t;

#define WSMSG_REQUEST_SPAWN_DYNAMIC_OBJ_SZ (sizeof(uint32) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
wsmsg_request_spawn_dynamic_obj_write(byte_buf_t *buf, wsmsg_request_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_REQUEST_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_REQUEST_SPAWN_DYNAMIC_OBJ);
    WRITE_UINT32(mem, s->type_id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_request_spawn_dynamic_obj_read(byte_buf_t *buf, wsmsg_request_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_REQUEST_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT32(mem, &s->type_id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} wsmsg_confirm_spawn_dynamic_obj_t;

#define WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
wsmsg_confirm_spawn_dynamic_obj_write(byte_buf_t *buf, wsmsg_confirm_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_confirm_spawn_dynamic_obj_read(byte_buf_t *buf, wsmsg_confirm_spawn_dynamic_obj_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_CONFIRM_SPAWN_DYNAMIC_OBJ_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
    int32 x;
    int32 y;
    int8 z;
} wsmsg_teleport_player_t;

#define WSMSG_TELEPORT_PLAYER_SZ (sizeof(uint64) + sizeof(int32) + sizeof(int32) + sizeof(int8))

static inline int
wsmsg_teleport_player_write(byte_buf_t *buf, wsmsg_teleport_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_TELEPORT_PLAYER_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_TELEPORT_PLAYER);
    WRITE_UINT64(mem, s->id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);

    return 0;
}

static inline int
wsmsg_teleport_player_read(byte_buf_t *buf, wsmsg_teleport_player_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_TELEPORT_PLAYER_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);

    return 0;
}

typedef struct
{
    uint64 id;
    uint32 inst_part_id;
    int32 x;
    int32 y;
    int8 z;
    uint8 dir;
} wsmsg_confirm_spawn_creature_t;

#define WSMSG_CONFIRM_SPAWN_CREATURE_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(int32) + sizeof(int32) + sizeof(int8) + sizeof(uint8))

static inline int
wsmsg_confirm_spawn_creature_write(byte_buf_t *buf, wsmsg_confirm_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_CONFIRM_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_CONFIRM_SPAWN_CREATURE);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT32(mem, s->inst_part_id);
    WRITE_INT32(mem, s->x);
    WRITE_INT32(mem, s->y);
    WRITE_INT8(mem, s->z);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_confirm_spawn_creature_read(byte_buf_t *buf, wsmsg_confirm_spawn_creature_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_CONFIRM_SPAWN_CREATURE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT32(mem, &s->inst_part_id);
    READ_INT32(mem, &s->x);
    READ_INT32(mem, &s->y);
    READ_INT8(mem, &s->z);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 id;
    uint8 dir;
} wsmsg_creature_dir_t;

#define WSMSG_CREATURE_DIR_SZ (sizeof(uint64) + sizeof(uint8))

static inline int
wsmsg_creature_dir_write(byte_buf_t *buf, wsmsg_creature_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_CREATURE_DIR_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_CREATURE_DIR);
    WRITE_UINT64(mem, s->id);
    WRITE_UINT8(mem, s->dir);

    return 0;
}

static inline int
wsmsg_creature_dir_read(byte_buf_t *buf, wsmsg_creature_dir_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_CREATURE_DIR_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->id);
    READ_UINT8(mem, &s->dir);

    return 0;
}

typedef struct
{
    uint64 creature_id;
    uint64 player_id;
    uint16 emote_id;
} wsmsg_creature_player_emote_t;

#define WSMSG_CREATURE_PLAYER_EMOTE_SZ (sizeof(uint64) + sizeof(uint64) + sizeof(uint16))

static inline int
wsmsg_creature_player_emote_write(byte_buf_t *buf, wsmsg_creature_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WMSGTSZ + WSMSG_CREATURE_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    WRITE_WMSG_TYPE(mem, WSMSG_CREATURE_PLAYER_EMOTE);
    WRITE_UINT64(mem, s->creature_id);
    WRITE_UINT64(mem, s->player_id);
    WRITE_UINT16(mem, s->emote_id);

    return 0;
}

static inline int
wsmsg_creature_player_emote_read(byte_buf_t *buf, wsmsg_creature_player_emote_t *s)
{
    uint8 *mem = bbuf_reserve(buf, WSMSG_CREATURE_PLAYER_EMOTE_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->creature_id);
    READ_UINT64(mem, &s->player_id);
    READ_UINT16(mem, &s->emote_id);

    return 0;
}

