typedef uint8 lmsg_type_t;
#define LMSGTSZ sizeof(lmsg_type_t)
#include "rwbits.inl"
#include "sv_common_defs.h"
#define WRITE_LMSG_TYPE(mem, val) \
    *(lmsg_type_t*)(mem) = (lmsg_type_t)(val); \
    (mem) = (uint8*)(mem) + sizeof(lmsg_type_t);
enum muta_lmsg_types
{
    LMSG_PUB_KEY = 0,
    LMSG_STREAM_HEADER,

    NUM_MUTA_LMSG_TYPES
};

#if NUM_MUTA_LMSG_TYPES > 255
#   error too many MUTA_LMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(lmsg, LMSGTSZ, WRITE_LMSG_TYPE);

enum muta_tlmsg_types
{
    TLMSG_LOGIN_REQUEST = NUM_MUTA_LMSG_TYPES,
    TLMSG_PLAYER_SELECT_SHARD_RESULT,
    TLMSG_ONLINE_FOR_PLAYERS,
    TLMSG_OFFLINE_FOR_PLAYERS,

    NUM_MUTA_TLMSG_TYPES
};

#if NUM_MUTA_TLMSG_TYPES > 255
#   error too many MUTA_TLMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(tlmsg, LMSGTSZ, WRITE_LMSG_TYPE);

enum muta_flmsg_types
{
    FLMSG_LOGIN_RESULT = NUM_MUTA_LMSG_TYPES,
    FLMSG_PLAYER_SELECT_SHARD,
    FLMSG_PLAYER_CANCEL_SELECT_SHARD,

    NUM_MUTA_FLMSG_TYPES
};

#if NUM_MUTA_FLMSG_TYPES > 255
#   error too many MUTA_FLMSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(flmsg, LMSGTSZ, WRITE_LMSG_TYPE);

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} lmsg_pub_key_t;

#define LMSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
lmsg_pub_key_write(byte_buf_t *buf, lmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ + LMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, LMSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
lmsg_pub_key_read(byte_buf_t *buf, lmsg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} lmsg_stream_header_t;

#define LMSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
lmsg_stream_header_write(byte_buf_t *buf, lmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ + LMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, LMSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
lmsg_stream_header_read(byte_buf_t *buf, lmsg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint8 shard_name_len;
    uint8 password_len;
    const char *shard_name;
    const char *password;
} tlmsg_login_request_t;

#define TLMSG_LOGIN_REQUEST_SZ (sizeof(uint8) + sizeof(uint8))

#define TLMSG_LOGIN_REQUEST_COMPUTE_SZ(shard_name_len, password_len) \
    (TLMSG_LOGIN_REQUEST_SZ + (shard_name_len) + (password_len))

#define TLMSG_LOGIN_REQUEST_MAX_SZ \
    (TLMSG_LOGIN_REQUEST_COMPUTE_SZ(MAX_SHARD_NAME_LEN, MAX_LOGIN_SERVER_PW_LEN))

static inline int
tlmsg_login_request_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tlmsg_login_request_t *s)
{
    int sz = TLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->shard_name_len, s->password_len);
    if (sz > TLMSG_LOGIN_REQUEST_MAX_SZ)
        return -1;

    uint8 *mem = prep_tlmsg_write_var_encrypted(buf, TLMSG_LOGIN_REQUEST, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->shard_name_len);
    WRITE_UINT8(mem, s->password_len);
    WRITE_STR(mem, s->shard_name, s->shard_name_len);
    WRITE_STR(mem, s->password, s->password_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
tlmsg_login_request_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tlmsg_login_request_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, TLMSG_LOGIN_REQUEST_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->shard_name_len);
    READ_UINT8(mem, &s->password_len);

    if (TLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->shard_name_len, s->password_len) >
        TLMSG_LOGIN_REQUEST_MAX_SZ)
        return -1;

    if (sz < TLMSG_LOGIN_REQUEST_COMPUTE_SZ(s->shard_name_len, s->password_len)) return -1;

    READ_VARCHAR(mem, s->shard_name, s->shard_name_len);
    READ_VARCHAR(mem, s->password, s->password_len);

    return 0;
}

typedef struct
{
    uint64 account_id;
    uint32 login_session_id;
    uint8 result;
} tlmsg_player_select_shard_result_t;

#define TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ (sizeof(uint64) + sizeof(uint32) + sizeof(uint8))

static inline int
tlmsg_player_select_shard_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tlmsg_player_select_shard_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, TLMSG_PLAYER_SELECT_SHARD_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT32(mem, s->login_session_id);
    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
}

static inline int
tlmsg_player_select_shard_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tlmsg_player_select_shard_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TLMSG_PLAYER_SELECT_SHARD_RESULT_SZ))
        return -1;
    READ_UINT64(mem, &s->account_id);
    READ_UINT32(mem, &s->login_session_id);
    READ_UINT8(mem, &s->result);

    return 0;
}

static inline int
tlmsg_online_for_players_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, TLMSG_ONLINE_FOR_PLAYERS);

    return 0;
}

static inline int
tlmsg_offline_for_players_write(byte_buf_t *buf)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, TLMSG_OFFLINE_FOR_PLAYERS);

    return 0;
}

typedef struct
{
    uint8 result;
} flmsg_login_result_t;

#define FLMSG_LOGIN_RESULT_SZ (sizeof(uint8))

static inline int
flmsg_login_result_write(byte_buf_t *buf, flmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ + FLMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, FLMSG_LOGIN_RESULT);
    WRITE_UINT8(mem, s->result);

    return 0;
}

static inline int
flmsg_login_result_read(byte_buf_t *buf, flmsg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, FLMSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint8 account_name_len;
    uint64 account_id;
    uint32 login_session_id;
    uint32 ip;
    const char *account_name;
    uint8 token[AUTH_TOKEN_SZ];
} flmsg_player_select_shard_t;

#define FLMSG_PLAYER_SELECT_SHARD_SZ (sizeof(uint8) + sizeof(uint64) + sizeof(uint32) + sizeof(uint32) + AUTH_TOKEN_SZ)

#define FLMSG_PLAYER_SELECT_SHARD_COMPUTE_SZ(account_name_len) \
    (FLMSG_PLAYER_SELECT_SHARD_SZ + (account_name_len))

#define FLMSG_PLAYER_SELECT_SHARD_MAX_SZ \
    (FLMSG_PLAYER_SELECT_SHARD_COMPUTE_SZ(MAX_ACC_NAME_LEN))

static inline int
flmsg_player_select_shard_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    flmsg_player_select_shard_t *s)
{
    int sz = FLMSG_PLAYER_SELECT_SHARD_COMPUTE_SZ(s->account_name_len);
    if (sz > FLMSG_PLAYER_SELECT_SHARD_MAX_SZ)
        return -1;

    uint8 *mem = prep_flmsg_write_var_encrypted(buf, FLMSG_PLAYER_SELECT_SHARD, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->account_name_len);
    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT32(mem, s->login_session_id);
    WRITE_UINT32(mem, s->ip);
    WRITE_STR(mem, s->account_name, s->account_name_len);
    WRITE_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
flmsg_player_select_shard_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    flmsg_player_select_shard_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, FLMSG_PLAYER_SELECT_SHARD_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->account_name_len);

    if (FLMSG_PLAYER_SELECT_SHARD_COMPUTE_SZ(s->account_name_len) >
        FLMSG_PLAYER_SELECT_SHARD_MAX_SZ)
        return -1;

    if (sz < FLMSG_PLAYER_SELECT_SHARD_COMPUTE_SZ(s->account_name_len)) return -1;

    READ_UINT64(mem, &s->account_id);
    READ_UINT32(mem, &s->login_session_id);
    READ_UINT32(mem, &s->ip);
    READ_VARCHAR(mem, s->account_name, s->account_name_len);
    READ_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return 0;
}

typedef struct
{
    uint64 account_id;
    uint32 login_session_id;
} flmsg_player_cancel_select_shard_t;

#define FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ (sizeof(uint64) + sizeof(uint32))

static inline int
flmsg_player_cancel_select_shard_write(byte_buf_t *buf, flmsg_player_cancel_select_shard_t *s)
{
    uint8 *mem = bbuf_reserve(buf, LMSGTSZ + FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ);
    if (!mem) return 1;
    WRITE_LMSG_TYPE(mem, FLMSG_PLAYER_CANCEL_SELECT_SHARD);
    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT32(mem, s->login_session_id);

    return 0;
}

static inline int
flmsg_player_cancel_select_shard_read(byte_buf_t *buf, flmsg_player_cancel_select_shard_t *s)
{
    uint8 *mem = bbuf_reserve(buf, FLMSG_PLAYER_CANCEL_SELECT_SHARD_SZ);
    if (!mem) return 1;
    READ_UINT64(mem, &s->account_id);
    READ_UINT32(mem, &s->login_session_id);

    return 0;
}

