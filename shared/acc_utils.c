#include <ctype.h>
#include "acc_utils.h"
#include "common_defs.h"
#include "common_utils.h"

static char _legal_pw_symbols[] =
{
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'o', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '0', ',', ';', '.', ':', '-', '_', '!', '#',
    '@', '&', '(', ')', '{', '}', '[', ']', '?', '+'
};

static char _legal_acc_name_symbols[] =
{
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'o', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '0'
};

bool32
accut_is_name_legaln(const char *name, int len)
{
    if (len >= MIN_ACC_NAME_LEN && len <= MAX_ACC_NAME_LEN)
        return str_contains_only_symbolsn(name, _legal_acc_name_symbols,
            sizeof(_legal_acc_name_symbols), len);
    return 0;

}

bool32
accut_is_name_legal(const char *name)
{
    int len = (int)strlen(name);
    if (len < MIN_ACC_NAME_LEN || len > MAX_ACC_NAME_LEN)
        return 0;
    return str_contains_only_symbols(name, _legal_acc_name_symbols,
        sizeof(_legal_acc_name_symbols));
}

bool32
accut_is_pw_legaln(const char *pw, uint8 len)
{
    if (len < MIN_PW_LEN || len > MAX_PW_LEN)
        return 0;
    for (int i = 0; i < (int)len; ++i)
        if (pw[i] == 0)
            return 0;
    return str_contains_only_symbolsn(pw, _legal_pw_symbols,
        sizeof(_legal_pw_symbols), len);
}

void
accut_strip_illegal_name_symbols(char *buf)
{
    str_strip_undef_symbols(buf, _legal_acc_name_symbols,
        sizeof(_legal_acc_name_symbols));
}

void
accut_strip_illegal_pw_symbols(char *buf)
{
    str_strip_undef_symbols(buf, _legal_pw_symbols, sizeof(_legal_pw_symbols));
}

bool32
accut_check_character_name(const char *name, uint32 claimed_len)
{
    if (!name)
        return 0;
    if (claimed_len < MIN_CHARACTER_NAME_LEN
    ||  claimed_len > MAX_CHARACTER_NAME_LEN)
        return 0;
    for (uint32 i = 0; i < claimed_len; ++i)
        if (!accut_check_character_name_symbol(name[i]))
            return 0;
    return 1;
}

bool32
accut_check_character_name_symbol(char c)
    {return isalpha(c) || isdigit(c);}
