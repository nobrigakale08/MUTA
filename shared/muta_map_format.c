#include "muta_map_format.h"
#include "ksys.h"
#include "types.h"
#include "stb/stb_sprintf.h"
#include "common_utils.h"
#include "common_defs.h"
#include "world_common.h"

#define NUM_TILES_PER_CHUNK \
    (MUTA_CHUNK_W * MUTA_CHUNK_W * MUTA_CHUNK_T)
#define MAP_PADDING         64
#define CHUNK_FILE_PADDING  64
#define MAP_DB_PADDING      32

static int
_muta_map_db_check_validity(muta_map_db_t *db);

int
muta_map_file_init_for_edit(muta_map_file_t *mf, const char *name,
    uint32 version, uint32 w, uint32 h)
{
    memset(mf, 0, sizeof(muta_map_file_t));

    int ret = 0;

    if (muta_map_file_set_name(mf, name)) return 1;
    mf->header.version = version;

    mf->chunk_paths = calloc(w * h, sizeof(char*));
    if (!mf->chunk_paths) {ret = 2; goto out;}

    mf->header.w = w;
    mf->header.h = h;

    uint32 gv[3] = MUTA_VERSION_ARRAY;
    for (int i = 0; i < 3; ++i) mf->header.game_version[i] = gv[i];

    out:
        if (ret)
            muta_map_file_destroy(mf);
        return ret;

}

int
muta_map_file_load(muta_map_file_t *mf, const char *path)
{
    memset(mf, 0, sizeof(muta_map_file_t));

    /* Create a temporary spot for file paths */
    uint dir_path_len = (uint)strlen(path);

    for (int i = dir_path_len - 1; i >= 0; --i)
    {
        if (path[i] != '/' && i != 0) continue;
        dir_path_len = i + 1;
        break;
    }

    DEBUG_PRINTFF("opening file %s.\n", path);

    /* Read file */
    FILE *f = fopen(path, "rb");
    if (!f) return 1;

    int ret = 0;

    if (fread_uint8_arr(f, (uint8*)mf->header.name, MUTA_MAP_FILE_NAME_LEN))
        {ret = 2; goto out;}
    if (fread_uint32(f, &mf->header.id))
        {ret = 3; goto out;}
    if (fread_uint32(f, &mf->header.version))
        {ret = 4; goto out;}
    if (fread_uint32_arr(f, mf->header.game_version, 3))
        {ret = 5; goto out;}
    uint32 dims[2];
    if (fread_uint32_arr(f, dims, 2))
        {ret = 6; goto out;}

    mf->chunk_paths = calloc(dims[0] * dims[1], sizeof(char*));
    if (!mf->chunk_paths) {ret = 7; goto out;}

    mf->header.w = dims[0];
    mf->header.h = dims[1];

    if (fseek(f, MAP_PADDING, SEEK_CUR))
        {ret = 8; goto out;}

    uint32 i, j;
    uint16 name_len;

    for (i = 0 ; i < mf->header.w; ++i)
        for (j = 0; j < mf->header.h; ++j)
        {
            if (fread_uint16(f, &name_len))
                {ret = 9; goto out;}

            uint tot_len = dir_path_len + name_len;
            dchar **s = &mf->chunk_paths[j * mf->header.w + i];
            *s = create_empty_dynamic_str(tot_len);
            if (!*s)
                {ret = 10; goto out;}

            memcpy(*s, path, dir_path_len);
            set_dynamic_str_len(*s, dir_path_len);
            (*s)[dir_path_len] = 0;

            if (fread_uint8_arr(f, (uint8*)*s + dir_path_len, name_len))
                {ret = 11; goto out;}
            (*s)[tot_len] = 0;
            set_dynamic_str_len(*s, tot_len);
        }

    out:
        if (ret)
            muta_map_file_destroy(mf);
        fclose(f);
        return ret;
}

int
muta_map_file_save(muta_map_file_t *mf, const char *path)
{
    if (!str_is_valid_file_name(mf->header.name))   return 1;
    if (mf->header.w < 1 || mf->header.h < 1)       return 2;
    if (!mf->chunk_paths)                           return 3;

    uint32 game_version[3] = MUTA_VERSION_ARRAY;
    for (int i = 0; i < 3; ++i) mf->header.game_version[i] = game_version[i];

    FILE *f = fopen(path, "wb+");
    if (!f) return 4;

    int ret = 0;

    if (fwrite_uint8_arr(f, (uint8*)mf->header.name, MUTA_MAP_FILE_NAME_LEN))
        {ret = 4; goto out;}
    if (fwrite_uint32(f, mf->header.id))
        {ret = 5; goto out;}
    if (fwrite_uint32(f, mf->header.version))
        {ret = 6; goto out;}
    if (fwrite_uint32_arr(f, mf->header.game_version, 3))
        {ret = 7; goto out;}
    if (fwrite_uint32(f, mf->header.w))
        {ret = 8; goto out;}
    if (fwrite_uint32(f, mf->header.h))
        {ret = 9; goto out;}

    /* Padding */
    if (fseek(f, MAP_PADDING, SEEK_CUR))
        {ret = 10; goto out;}

    uint32  i, j;
    uint16  path_len, save_len;
    char    *save_name;
    for (i = 0; i < mf->header.w; ++i)
        for (j = 0; j < mf->header.h; ++j)
        {
            dchar *s = mf->chunk_paths[j * mf->header.w + i];
            path_len = get_dynamic_str_len(s);
            if (path_len < 1) {ret = 11; goto out;}

            for (int k = path_len - 1; k >= 0; --k)
            {
                if (s[k] != '/' && k != 0) continue;
                uint16 tmp  = (int)k;
                if (tmp != 0) tmp++;
                save_name   = s + tmp;
                save_len    = path_len - tmp + 1;
                break;
            }

            if (fwrite_uint16(f, save_len))
                {ret = 12; goto out;}
            if (fwrite_uint8_arr(f, (uint8*)save_name, save_len))
                {ret = 13; goto out;}
        }

    out: fclose(f);
    return ret;
}

void
muta_map_file_destroy(muta_map_file_t *mf)
{
    uint32 num = mf->header.w * mf->header.h;
    for (uint32 i = 0; i < num; ++i)
        free_dynamic_str(mf->chunk_paths[i]);
    free(mf->chunk_paths);
    memset(mf, 0, sizeof(muta_map_file_t));
}

int
muta_map_file_set_name(muta_map_file_t *mf, const char *name)
{
    if (!str_is_valid_file_name(name))
        return 1;
    uint len = (uint)strlen(name);
    if (len > MUTA_MAP_FILE_NAME_LEN)
        return 2;
    memcpy(mf->header.name, name, len + 1);
    return 0;
}

int
muta_map_file_set_chunk_path(muta_map_file_t *mf, uint32 x, uint32 y,
    const char *path)
{
    if (mf->header.w <= x || mf->header.h <= y) return 1;
    if (!mf->chunk_paths) return 2;
    if (!str_is_valid_file_path(path)) return 3;
    dchar **s = &mf->chunk_paths[y * mf->header.w + x];
    dchar *ns = set_dynamic_str(*s, path);
    if (!ns) return 4;
    *s = ns;
    return 0;
}

char *
muta_map_file_get_chunk_path(muta_map_file_t *mf, uint32 x, uint32 y)
{
    if (x >= mf->header.w || y >= mf->header.h) return 0;
    if (!mf->chunk_paths) return 0;
    return mf->chunk_paths[y * mf->header.w + x];
}

char *
muta_map_file_get_chunk_name(muta_map_file_t *mf, uint32 x, uint32 y)
{
    char *n = muta_map_file_get_chunk_path(mf, x, y);
    if (!n) return 0;
    uint path_len = (uint)strlen(n);
    char *ret;
    for (int i = path_len - 1; i >= 0; --i)
    {
        if (n[i] != '/' && i != 0) continue;
        ret = n + i;
        if (i != 0) ret++;
        break;
    }
    return ret;
}

int
muta_map_file_set_chunk_path_pattern(muta_map_file_t *mf, const char *pat)
{
    if (!str_is_valid_file_path(pat)) return 1;

    uint32 x, y;

    char buf[256];
    char *wbuf = buf;

    uint len = (uint)strlen(pat);
    if (len + 18 > 256)
    {
        if (mf->header.w >= 1000000 || mf->header.h >= 1000000)
            return 2;
        wbuf = malloc(len + 18);
        if (!wbuf) return 3;
    }

    int ret = 0;

    for (x = 0; x < mf->header.w; ++x)
        for (y = 0; y < mf->header.h; ++y)
        {
            stbsp_snprintf(wbuf, len + 18, "%s%u_%u.dat", pat, x, y);
            dchar **s = &mf->chunk_paths[y * mf->header.w + x];
            char *ns = set_dynamic_str(*s, wbuf);
            if (!ns) {ret = 4; goto out;}
            *s = ns;
        }

    out:
    if (wbuf != buf) free(wbuf);
    return ret;
}

int
muta_chunk_file_init(muta_chunk_file_t *mch)
{
    memset(mch, 0, sizeof(muta_chunk_file_t));
    mch->tiles = calloc(NUM_TILES_PER_CHUNK, sizeof(tile_t));
    if (!mch->tiles) return 1;
    mch->static_objs = calloc(256, sizeof(stored_static_obj_t));
    if (!mch->static_objs) {muta_chunk_file_destroy(mch); return 2;}
    mch->max_static_objs = 256;
    return 0;
}

int
muta_chunk_file_load(muta_chunk_file_t *mch, const char *path)
{
    if (!path)
    {
        memset(mch->tiles, 0, NUM_TILES_PER_CHUNK * sizeof(tile_t));
        mch->header.num_static_objs = 0;
        return 0;
    }

    if (!mch->tiles) return 1;
    int err;

    FILE *f = fopen(path, "rb");
    if (!f) {err = 1; goto fail;}

    /* Name */
    if (fread_uint8_arr(f, (uint8*)mch->header.name, MUTA_CHUNK_FILE_NAME_LEN))
        {err = 2; goto fail;}

    mch->header.name[MUTA_CHUNK_FILE_NAME_LEN] = 0;

    /* Location (in chunks) */
    if (fread_uint32_arr(f, mch->header.location, 2))
        {err = 3; goto fail;}

    /* Number of static objects */
    if (fread_uint32(f, &mch->header.num_static_objs))
        {err = 4; goto fail;}

    /* Padding */
    if (fseek(f, CHUNK_FILE_PADDING, SEEK_CUR))
        {err = 5; goto fail;}

    /* Allocate more static objs if required */
    uint num_objs = mch->header.num_static_objs;
    if (num_objs > mch->max_static_objs)
    {
        stored_static_obj_t *objs = realloc(mch->static_objs,
            num_objs * sizeof(stored_static_obj_t));
        if (!objs) {err = 6; goto fail;}
        mch->static_objs        = objs;
        mch->max_static_objs    = mch->header.num_static_objs;
    }

    /* Tiles */
    if (fread_uint16_arr(f, mch->tiles, NUM_TILES_PER_CHUNK))
        {err = 7; goto fail;}

    /* Static objects */
    stored_static_obj_t *objs = mch->static_objs;
    for (uint32 i = 0; i < num_objs; ++i)
    {
        if (fread_uint32(f, &objs[i].type_id))
            {err = 8; goto fail;}
        if (fread_uint8(f, &objs[i].dir)
        ||  fread_uint8(f, &objs[i].x)
        ||  fread_uint8(f, &objs[i].y)
        ||  fread_int8(f,  &objs[i].z))
            {err = 9; goto fail;}
        if (objs[i].dir >= NUM_ISODIRS)
            {err = 10; goto fail;}
    }

    fclose(f);
    return 0;

    fail:
        safe_fclose(f);
        memset(mch->tiles, 0, NUM_TILES_PER_CHUNK * sizeof(tile_t));
        mch->header.num_static_objs = 0;
        return err;
}

void
muta_chunk_file_destroy(muta_chunk_file_t *mf)
{
    free(mf->tiles);
    free(mf->static_objs);
    memset(mf, 0, sizeof(muta_chunk_file_t));
}

int
muta_chunk_file_save(muta_chunk_file_t *mch, const char *dir_path)
{
    if (!str_is_valid_file_name(mch->header.name))
        return 1;
    if (!mch->tiles)
        return 2;
    /* +1 slash */
    uint len = (uint)(strlen(dir_path) + strlen(mch->header.name) + 1);

    dchar *fp = create_empty_dynamic_str(len);
    if (!fp) return 3;

    fp = append_to_dynamic_str(fp, dir_path);
    fp = append_to_dynamic_str(fp, "/");
    fp = append_to_dynamic_str(fp, mch->header.name);

    FILE *f = fopen(fp, "wb+");
    if (!f) return 4;

    int ret = 0;
    if (fwrite_uint8_arr(f, (uint8*)mch->header.name, MUTA_CHUNK_FILE_NAME_LEN))
        {ret = 5; goto out;}
    if (fwrite_uint32_arr(f, mch->header.location, 2))
        {ret = 6; goto out;}
    if (fwrite_uint32(f, mch->header.num_static_objs))
        {ret = 7; goto out;}
    if (fseek(f, CHUNK_FILE_PADDING, SEEK_CUR))
        {ret = 8; goto out;}
    if (fwrite_uint16_arr(f, mch->tiles, NUM_TILES_PER_CHUNK))
        {ret = 9; goto out;}
    uint32 num_objs             = mch->header.num_static_objs;
    stored_static_obj_t *objs   = mch->static_objs;
    for (uint32 i = 0; i < num_objs; ++i)
    {
        if (objs[i].dir > NUM_ISODIRS)
            {ret = 10; goto out;}
        if (fwrite_uint32(f, objs[i].type_id)
        ||  fwrite_uint8(f, objs[i].dir)
        ||  fwrite_uint8(f, objs[i].x)
        ||  fwrite_uint8(f, objs[i].y)
        ||  fwrite_int8(f, objs[i].z))
            {ret = 11; goto out;}
    }
    out:
        free_dynamic_str(fp);
        fclose(f);
        return ret;
}

int
muta_chunk_file_set_name(muta_chunk_file_t *mch, const char *name)
{
    if (!str_is_valid_file_name(name))
        return 1;
    int len = (int)strlen(name);
    if (len > MUTA_CHUNK_FILE_NAME_LEN)
        return 2;
    memcpy(mch->header.name, name, len + 1);
    return 0;
}

int
muta_chunk_file_push_static_obj(muta_chunk_file_t *mch,
    static_obj_type_id_t type_id, uint8 x, uint8 y, int8 z)
{
    if (x >= MUTA_CHUNK_W || y >= MUTA_CHUNK_W || z >= MUTA_CHUNK_T || z < 0)
        return 1;

    if (mch->header.num_static_objs == mch->max_static_objs)
    {
        uint32 new_max = mch->max_static_objs * 105 / 100;
        stored_static_obj_t *new_objs = realloc(mch->static_objs,
            new_max * sizeof(stored_static_obj_t));
        if (!new_objs) return 2;
        mch->max_static_objs    = new_max;
        mch->static_objs        = new_objs;
    }

    stored_static_obj_t *obj = &mch->static_objs[mch->header.num_static_objs++];
    obj->type_id    = type_id;
    obj->x          = x;
    obj->y          = y;
    obj->z          = z;
    return 0;
}

void
muta_chunk_file_erase_static_obj(muta_chunk_file_t * mch, uint32 index)
{
    uint32 num = mch->header.num_static_objs;
    if (num <= index) return;
    if (index != mch->header.num_static_objs - 1)
    {
        uint32 nxt = index + 1;
        memmove(mch->static_objs + index, mch->static_objs + nxt,
            (num - nxt) * sizeof(stored_static_obj_t));
    }
    mch->header.num_static_objs--;
}

int
muta_map_db_load(muta_map_db_t *db, const char *fp)
{
    memset(db, 0, sizeof(muta_map_db_t));
    FILE *f = fopen(fp, "rb");
    if (!f) return 1;

    int ret = 0;

    if (fread_uint32(f, &db->header.version))       {ret = 2; goto out;}
    if (fread_uint32(f, &db->header.running_id))    {ret = 3; goto out;}
    if (fread_uint32(f, &db->header.num_entries))   {ret = 4; goto out;}

    if (fseek(f, MAP_DB_PADDING, SEEK_CUR))
        {ret = 5; goto out;}

    db->entries = calloc(db->header.num_entries, sizeof(muta_map_db_entry_t));
    if (!db->entries)
        {ret = 6; goto out;}

    uint32 num = db->header.num_entries;
    uint16 len;
    muta_map_db_entry_t *entry;

    for (uint32 i = 0; i < num; ++i)
    {
        entry = &db->entries[i];

        /* Read id */
        if (fread_uint32(f, &entry->id))
            {ret = 7; goto out;}

        /* Read name */
        if (fread_uint16(f, &len))
            {ret = 8; goto out;}

        entry->name = create_empty_dynamic_str(len);
        if (!entry->name)
            {ret = 9; goto out;}

        if (fread_uint8_arr(f, (uint8*)entry->name, len))
            {ret = 10; goto out;}

        set_dynamic_str_len(entry->name, len);

        /* Read path */
        if (fread_uint16(f, &len))
            {ret = 11; goto out;}

        entry->path = create_empty_dynamic_str(len);
        if (!entry->path)
            {ret = 12; goto out;}

        if (fread_uint8_arr(f, (uint8*)entry->path, len))
            {ret = 13; goto out;}

        set_dynamic_str_len(entry->path, len);
    }

    if (_muta_map_db_check_validity(db))
        {ret = 14; goto out;}

    out:
        fclose(f);
        if (ret)
            muta_map_db_destroy(db);
        return ret;
}

int
muta_map_db_save(muta_map_db_t *db, const char *fp)
{
    if (_muta_map_db_check_validity(db))
        return 1;

    FILE *f = fopen(fp, "wb+");
    if (!f) return 2;

    int ret = 0;

    if (fwrite_uint32(f, db->header.version))       {ret = 2; goto out;}
    if (fwrite_uint32(f, db->header.running_id))    {ret = 3; goto out;}
    if (fwrite_uint32(f, db->header.num_entries))   {ret = 4; goto out;}
    if (fseek(f, MAP_DB_PADDING, SEEK_CUR))         {ret = 5; goto out;}

    for (uint32 i = 0; i < db->header.num_entries; ++i)
    {
        /* Write id */
        if (fwrite_uint32(f, db->entries[i].id))
            {ret = 6; goto out;}

        /* Write name */
        if (fwrite_uint16(f, get_dynamic_str_len(db->entries[i].name)))
            {ret = 7; goto out;}

        if (fwrite_uint8_arr(f, (uint8*)db->entries[i].name,
            get_dynamic_str_len(db->entries[i].name)))
            {ret = 8; goto out;}

        /* Write path */
        if (fwrite_uint16(f, get_dynamic_str_len(db->entries[i].path)))
            {ret = 9; goto out;}

        if (fwrite_uint8_arr(f, (uint8*)db->entries[i].path,
            get_dynamic_str_len(db->entries[i].path)))
            {ret = 10; goto out;}
    }
    out: {return ret;}
}

void
muta_map_db_destroy(muta_map_db_t *db)
{
    if (db->entries)
        for (uint32 i = 0; i < db->header.num_entries; ++i)
        {
            free_dynamic_str(db->entries[i].name);
            free_dynamic_str(db->entries[i].path);
        }
    memset(db, 0, sizeof(muta_map_db_t));
}

muta_map_db_entry_t *
muta_map_db_get_entry_by_name(muta_map_db_t *db, const char *name)
{
    if (!db->entries) return 0;
    for (uint32 i = 0; i < db->header.num_entries; ++i)
        if (streq(db->entries[i].name, name)) return &db->entries[i];
    return 0;
}

muta_map_db_entry_t *
muta_map_db_get_entry_by_id(muta_map_db_t *db, uint32 id)
{
    if (!db->entries) return 0;
    for (uint32 i = 0; i < db->header.num_entries; ++i)
        if (db->entries[i].id == id) return &db->entries[i];
    return 0;
}

static int
_muta_map_db_check_validity(muta_map_db_t *db)
{
    if (db->header.num_entries > 0 && !db->entries) return 1;

    /* Check for duplicate entries and name/path validity */
    for (uint32 i = 0; i < db->header.num_entries; ++i)
    {
        if (!db->entries[i].name) return 2;
        if (!db->entries[i].path) return 3;
        if (!str_is_valid_file_path(db->entries[i].path))
            return 4;

        for (uint32 j = 0; j < db->header.num_entries; ++j)
        {
            if (i == j) continue;
            if (db->entries[i].id == db->entries[j].id)             return 5;
            if (streq(db->entries[i].name, db->entries[j].name))    return 6;
        }
    }
    return 0;
}
