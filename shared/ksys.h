/* System utility wrappers for GNU/Linux (mostly GCC) and Win32 (cl) */

#ifndef KSYS_H
#define KSYS_H

/* Compile settings */

/* Note: on Win32, you have to link with ws2_32.lib for sockets to work (you
 * can try the option KSYS_LINK_WINSOCK2 below) */
#ifndef KSYS_SOCKETS
#   define KSYS_SOCKETS 1
#endif /* KSYS_SOCKETS */

/* Attempt to link winsock2 with a pragma comment so that it doesn't need to be
 * included in linker instructions. Does nothing if not building on Windows
 * or if sockets are not enabled above. */
#define KSYS_LINK_WINSOCK2 1

/* End of compile settings */

#define KSYS_OS_GNU_LINUX 1
#define KSYS_OS_WIN32 2

#if defined(__linux__)
#   define KSYS_OS KSYS_OS_GNU_LINUX
#elif defined(_WIN32)
#   define KSYS_OS KSYS_OS_WIN32
#else
#   error "ksys.h: unsupported platform."
#endif

/* The inline keyword isn't defined in some older Microsoft compiler versions */
#if KSYS_OS == KSYS_OS_WIN32
#   if defined(_MSC_VER) && _MSC_VER <= 1800
#       define inline __inline
#   endif
#endif

#define KSYS_INL static inline

/* Types */
#if KSYS_OS == KSYS_OS_GNU_LINUX

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <errno.h>
#include <time.h>   /* For nanosleep() */
#include <alloca.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h> /* For file system ops */
#include <sys/eventfd.h>
#include <poll.h>
#include <sys/time.h>

#if KSYS_SOCKETS
#include <sys/socket.h>
#include <netinet/in.h>     /* For sockaddr_in, etc. */
#include <fcntl.h>
#include <netinet/tcp.h>    /* For TCP flags (TCP_NODELAY...) */
#include <string.h>         /* For strlen */
#include <arpa/inet.h>      /* For inet_pton() and similar */
#include <sys/ioctl.h>      /* For ioctl() */
typedef int socket_t;
#define KSYS_INVALID_SOCKET (-1)
#endif /* KSYS_SOCKETS */

typedef void *(*thread_func_t)(void*);
typedef void * thread_ret_t;

typedef pthread_mutex_t mutex_t;
typedef pthread_t       thread_t;
typedef pthread_cond_t  cond_var_t;
typedef sem_t           semaphore_t;
typedef struct waiter_t waiter_t;

typedef DIR *           dir_handle_t;
typedef struct dirent * dir_entry_t;

struct waiter_t
{
    int fd; /* eventfd */
};

#elif (KSYS_OS == KSYS_OS_WIN32)

#if KSYS_SOCKETS
#include <winsock2.h>
#include <ws2tcpip.h>
typedef SOCKET socket_t;
#define KSYS_INVALID_SOCKET INVALID_SOCKET
#if KSYS_LINK_WINSOCK2
#   pragma comment(lib, "ws2_32.lib")
#endif
#else
#endif /* KSYS_SOCKETS */

/* windows.h needs to be included after the sock libs */
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <sys/stat.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

#define thread_func_t LPTHREAD_START_ROUTINE
#define thread_ret_t DWORD WINAPI

typedef HANDLE              thread_t;
typedef CRITICAL_SECTION    mutex_t;
typedef CONDITION_VARIABLE  cond_var_t;
typedef HANDLE              semaphore_t;
typedef struct waiter_t     waiter_t;
typedef HANDLE              dir_handle_t;
typedef WIN32_FIND_DATA     dir_entry_t;

struct waiter_t
{
    HANDLE h;
};

#endif /* KSYS_OS_WIN32 */

/* Included and defined on all systems: */

#include <stdint.h> /* For atomic operations on 32 bit values */

typedef struct
{
    uint64_t    sec;
    uint64_t    msec;
} sys_time_t;

/* Thread callbacks
 * The function passed to thread_create needs to have the following signature:
 * thread_ret_t function(void *arguments)
 * Return values:
 * The function should return 0 (or nothing, but you'll get compiler warnings).
 * This is due to the fact the signature for a thread callback differs for the win32
 * and pthread APIs, but NULL is a valid return value for both. */
KSYS_INL int thread_init(thread_t *thread);
KSYS_INL int thread_create(thread_t *thread,
    thread_func_t func, void *args);
KSYS_INL int thread_join(thread_t *thread);
KSYS_INL int thread_detach(thread_t *thread);
KSYS_INL int thread_kill(thread_t *thread);
KSYS_INL int thread_destroy(thread_t *thread);

KSYS_INL void mutex_init(mutex_t *mutex);
KSYS_INL int  mutex_destroy(mutex_t *mutex);
KSYS_INL int  mutex_lock(mutex_t *mutex);
KSYS_INL int  mutex_unlock(mutex_t *mutex);

KSYS_INL void cond_var_init(cond_var_t *cond_var);
KSYS_INL int  cond_var_destroy(cond_var_t *cv);
KSYS_INL int  cond_var_wait(cond_var_t *cond_var, mutex_t *mutex);
KSYS_INL int  cond_var_timed_wait(cond_var_t *cond_var, mutex_t *mutex,
    int timeout_ms);
KSYS_INL int  cond_var_signal_one(cond_var_t *cvar);
KSYS_INL int  cond_var_signal_all(cond_var_t *cvar);

KSYS_INL int semaphore_init(semaphore_t *sem, int initial_count);
KSYS_INL int semaphore_wait(semaphore_t *sem);
KSYS_INL int semaphore_release(semaphore_t *sem);

KSYS_INL int waiter_init(waiter_t *w);
KSYS_INL void waiter_destroy(waiter_t *w);
/* If timeout_ms is -1, will sleep indefinitely */
KSYS_INL int waiter_wait(waiter_t *w, int timeout_ms);
KSYS_INL int waiter_wake(waiter_t *w);

KSYS_INL int get_num_physical_cpus();

/* Atomics
 * Note: these are based on compiler intrinsics, so not every compiler is
 * supported (most likely only GCC and MSVC) */
KSYS_INL int32_t interlocked_exchange_int32(int32_t volatile *target,
    int32_t value);
KSYS_INL int32_t interlocked_compare_exchange_int32(int32_t volatile *target,
    int32_t exchange, int32_t comparand);
KSYS_INL int32_t interlocked_increment_int32(int32_t volatile *target);
KSYS_INL int32_t interlocked_decrement_int32(int32_t volatile *target);

/* Time manipulation functions */
KSYS_INL int sleep_ms(unsigned int msec);
KSYS_INL int get_monotonic_time(sys_time_t *ret_time);

/* Memory */
/* stack_alloc() defined as a macrois */

/* File system */
KSYS_INL int create_directory(const char *path);
KSYS_INL int file_exists(const char *path);
KSYS_INL dir_handle_t open_directory(const char *path,
    dir_entry_t *ret_first_de);
KSYS_INL void close_directory(dir_handle_t dir);
KSYS_INL int get_next_file_in_directory(dir_handle_t h, dir_entry_t *ret_de);
KSYS_INL char *get_dir_entry_name(dir_entry_t *de);

#if KSYS_SOCKETS
/* Sockets
 * Most socket functions act in a similar enough manner on all platforms -
 * they return 0 upon success and take in the same parameters. Included here are
 * some functions that do not act the same on different platforms.
 *
 * socket() function return value
 * An exception to the above is the socket() function, because the value
 * indicating an invalid socket is different depending on the platform. A
 * socket file descriptor's validity can be checked by comparing it to the
 * value of the macro KSYS_INVALID_SOCKET. */

/* On windows, initializes winsock - on *nix, does nothing */
KSYS_INL int init_socket_api();
/* Socket closing functions differ slightly depending on platform */
KSYS_INL int close_socket(socket_t s);
KSYS_INL int make_socket_non_block(socket_t sock);
KSYS_INL int socket_num_readable_bytes(socket_t s, long int *ret);
/* Get the last socket error as a string to the appointed buffer. Return value
 * is the length of the string */
KSYS_INL int sock_err_str(char *buf, int buf_len);

#endif /* KSYS_SOCKETS */

#if KSYS_OS == KSYS_OS_GNU_LINUX

static inline struct timespec
_milliseconds_to_timespec(unsigned int msec)
{
    struct timespec tm = {0};
    tm.tv_sec  = (time_t)msec / (time_t)1000;
    tm.tv_nsec = ((long)msec  - (long)tm.tv_sec * (long)1000) * 1000000;
    return tm;
}

/* Note: GCC specific */
#define MEM_BARRIER() __sync_synchronize()

KSYS_INL int
thread_init(thread_t *thread)
{
    char *s;
    char *e = (char*)thread + sizeof(thread_t);
    for (s = (char*)thread; s < e; ++s) *s = 0;
    return 0;
}

KSYS_INL int
thread_create(thread_t *thread, thread_func_t func, void *args)
    {return pthread_create(thread, 0, func, args);}

KSYS_INL int
thread_join(thread_t *thread) {return pthread_join(*thread, 0);}

KSYS_INL int
thread_detach(thread_t *thread) {return pthread_detach(*thread);}

KSYS_INL int
thread_kill(thread_t *thread) {return pthread_kill(*thread, SIGKILL);}

KSYS_INL int
thread_destroy(thread_t *thread)
{
    pthread_kill(*thread, SIGKILL);
    char *s;
    char *e = (char*)thread + sizeof(thread_t);
    for (s = (char*)thread; s < e; ++s) *s = 0;
    return 0;
}

KSYS_INL void
mutex_init(mutex_t *mutex) {pthread_mutex_init(mutex, 0);}

KSYS_INL int
mutex_destroy(mutex_t *mutex) {return pthread_mutex_destroy(mutex);}

KSYS_INL int
mutex_lock(mutex_t *mutex) {return pthread_mutex_lock(mutex);}

KSYS_INL int
mutex_unlock(mutex_t *mutex) {return pthread_mutex_unlock(mutex);}

KSYS_INL void
cond_var_init(cond_var_t *cv) {pthread_cond_init(cv, 0);}

KSYS_INL int
cond_var_destroy(cond_var_t *cv) {return pthread_cond_destroy(cv);}

KSYS_INL int
cond_var_wait(cond_var_t *cv, mutex_t *m) {return pthread_cond_wait(cv, m);}

KSYS_INL int
cond_var_timed_wait(cond_var_t *cv, mutex_t *m, int timeout_ms)
{
    if (timeout_ms == -1)
        return pthread_cond_wait(cv, m);
    struct timeval time_now;
    gettimeofday(&time_now, 0);
    struct timespec now_tm;
    now_tm.tv_sec   = time_now.tv_sec;
    now_tm.tv_nsec  = time_now.tv_usec * 1000;
    struct timespec add_tm;
    add_tm = _milliseconds_to_timespec((unsigned int)timeout_ms);
    struct timespec then_tm;
    then_tm.tv_sec  = now_tm.tv_sec + add_tm.tv_sec;
    then_tm.tv_nsec = now_tm.tv_nsec + add_tm.tv_nsec;
    then_tm.tv_sec += (then_tm.tv_nsec / 1000000000);
    then_tm.tv_nsec -= (then_tm.tv_nsec / 1000000000) * 1000000000;
    return pthread_cond_timedwait(cv, m, &then_tm) ? -1 : 0;
}

KSYS_INL int
cond_var_signal_one(cond_var_t *cv) {return pthread_cond_signal(cv);}

KSYS_INL int
cond_var_signal_all(cond_var_t *cv) {return pthread_cond_broadcast(cv);}

KSYS_INL int
semaphore_init(semaphore_t *sem, int initial_count)
{
    return sem_init(sem, 0, (unsigned int)initial_count);
}

KSYS_INL int
semaphore_wait(semaphore_t *sem) {return sem_wait(sem);}

KSYS_INL int
semaphore_release(semaphore_t *sem) {return sem_post(sem);}

KSYS_INL int waiter_init(waiter_t *w)
{
    int fd = eventfd(0, EFD_SEMAPHORE);
    if (fd < 0)
        return 1;
    w->fd = fd;
    return 0;
}

KSYS_INL void waiter_destroy(waiter_t *w)
    {close(w->fd);}

KSYS_INL int waiter_wait(waiter_t *w, int timeout_ms)
{
    struct pollfd pfd;
    pfd.fd      = w->fd;
    pfd.events  = POLLIN | POLLHUP;
    int num = poll(&pfd, 1, timeout_ms);
    if (num < 1)
        return 1;
    return 0;
}

KSYS_INL int waiter_wake(waiter_t *w)
{
    uint64_t data = 1;
    return (int)!(write(w->fd, &data, sizeof(data)) == sizeof(data));
}

KSYS_INL int
get_num_physical_cpus()
{
    return (int)sysconf(_SC_NPROCESSORS_ONLN);
}

KSYS_INL int32_t
interlocked_exchange_int32(int32_t volatile *target, int32_t value)
{
    int32_t ret = __sync_lock_test_and_set(target, value);
    __sync_synchronize();
    return ret;
}

KSYS_INL int32_t
interlocked_compare_exchange_int32(int32_t volatile *target, int32_t exchange,
    int32_t comparand)
{
    return __sync_val_compare_and_swap(target, comparand, exchange);
}

KSYS_INL int32_t
interlocked_increment_int32(int32_t volatile *target)
{
    return __sync_add_and_fetch(target, 1);
}

KSYS_INL int32_t
interlocked_decrement_int32(int32_t volatile *target)
{
    return __sync_sub_and_fetch(target, 1);
}

/* Time */
KSYS_INL int
sleep_ms(unsigned int msec)
{
    struct timespec req = _milliseconds_to_timespec(msec);
    return nanosleep(&req, &req);
}

KSYS_INL int
get_monotonic_time(sys_time_t *ret_time)
{
    if (!ret_time) return 1;
    struct timespec t;
    if (clock_gettime(CLOCK_MONOTONIC, &t) != 0)
        return 2;
    ret_time->sec   = t.tv_sec;
    ret_time->msec  = t.tv_nsec / 1000000;
    return 0;
}

#define stack_alloc(size) alloca(size)

KSYS_INL int
create_directory(const char *path)
{
    struct stat s;
    if (!stat(path, &s)) return 0;
    return mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0;
}

KSYS_INL int
file_exists(const char *path)
    {struct stat s; return (!stat(path, &s));}

KSYS_INL dir_handle_t
open_directory(const char *path, dir_entry_t *ret_first_de)
{
    DIR *ret = opendir(path);
    if (!ret) return 0;
    if (ret_first_de) *ret_first_de = readdir(ret);
    return ret;
}

KSYS_INL void close_directory(dir_handle_t dir) {closedir(dir);}

KSYS_INL int
get_next_file_in_directory(dir_handle_t h, dir_entry_t *ret_de)
{
    dir_entry_t de = readdir(h);
    if (!de) return 1;
    if (ret_de) *ret_de = de;
    return 0;
}

KSYS_INL char
*get_dir_entry_name(dir_entry_t *de)
    {return de && *de ? (*de)->d_name : 0;}

#if KSYS_SOCKETS
KSYS_INL int init_socket_api() {return 0;}
KSYS_INL int close_socket(socket_t s) {return close(s);}

KSYS_INL int
make_socket_non_block(socket_t sock)
{
    int flags = fcntl(sock, F_GETFL, 0);
    return fcntl(sock, F_SETFL, flags | O_NONBLOCK) != -1 ? 0 : 1;
}

KSYS_INL int
socket_num_readable_bytes(socket_t s, long int *ret)
{
    int num_bytes;
    int res = ioctl(s, FIONREAD, &num_bytes);
    if (!res)
    {
        *ret = (long int)num_bytes;
        return 0;
    }
    return 1;
}

KSYS_INL int
sock_err_str(char *buf, int buf_len)
{
    if (!buf || buf_len <= 0) return 0;
    const char *s = strerror(errno);
    if (s)
        strncpy(buf, s, buf_len);
    else
        buf[0] = '\0';
    return strlen(buf);
}

#endif /* KSYS_SOCKETS */

#elif KSYS_OS == KSYS_OS_WIN32

/* System utilities for Windows
 *
 * For functions that return a status as an integer, unless otherwise stated,
 * 0 indicates success. Otherwise, where supported, the return value will be
 * the value returned by the win32 API function GetLastError(). */


#define MEM_BARRIER() MemoryBarrier()

KSYS_INL int
thread_init(thread_t *thread)
    {*thread = NULL; return 0;}

KSYS_INL int
thread_create(thread_t *thread, thread_func_t func, void *args)
{
    if (*thread != NULL)
        CloseHandle(*thread);
    *thread = CreateThread(0, 0, func, args, 0, 0);
    return *thread == NULL;
}

KSYS_INL int
thread_join(thread_t *thread)
{
    DWORD ra = WaitForSingleObject(*thread, INFINITE);
    DWORD rb = CloseHandle(*thread);
    *thread = NULL;
    return 0 | (ra == WAIT_OBJECT_0 ? 0 : (1 << 0)) | \
        (rb == TRUE ? 0 : (1 << 1));
}

KSYS_INL
int thread_detach(thread_t *thread)
{
    int cr;
    if (*thread != NULL)
    {
        cr = CloseHandle(*thread);
        *thread = NULL;
    } else
        cr = TRUE;
    return cr != TRUE;
}

KSYS_INL
int thread_kill(thread_t *thread)
{
    int tr = TerminateThread(*thread, THREAD_TERMINATE) != 0 ? 0 : GetLastError();
    int cr = CloseHandle(*thread);
    *thread = NULL;
    return 0 | (tr == FALSE ? (1 << 0) : 0) | (cr == FALSE ? (1 << 1) : 0);
}

KSYS_INL
int thread_destroy(thread_t *thread)
{
    if (*thread != NULL)
    {
        int tr = TerminateThread(*thread, THREAD_TERMINATE);
        int cr = CloseHandle(*thread);
        *thread = NULL;
        return 0 | (tr == FALSE ? (1 << 0) : 0) | (cr == FALSE ? (1 << 1) : 0);
    }
    return 0;
}

KSYS_INL void
mutex_init(mutex_t *mutex) {InitializeCriticalSectionAndSpinCount(mutex, 2000);}

KSYS_INL int
mutex_destroy(mutex_t *mutex) {DeleteCriticalSection(mutex); return 0;}

KSYS_INL int
mutex_lock(mutex_t *mutex) {EnterCriticalSection(mutex); return 0;}

KSYS_INL int
mutex_unlock(mutex_t *mutex) {LeaveCriticalSection(mutex); return 0;}

KSYS_INL void
cond_var_init(cond_var_t *cv) {InitializeConditionVariable(cv);}

KSYS_INL int
cond_var_destroy(cond_var_t *cv) { /* Win32 does not need this */ return 0; }

KSYS_INL int
cond_var_wait(cond_var_t *cv, mutex_t *m)
{
    return SleepConditionVariableCS(cv, m, INFINITE) ? 0 : GetLastError();
}

KSYS_INL int
cond_var_timed_wait(cond_var_t *cv, mutex_t *m, int timeout_ms)
{
    return SleepConditionVariableCS(cv, m,
        timeout_ms == -1 ? INFINITE : (DWORD)timeout_ms) ? 0 : -1;
}

KSYS_INL int
cond_var_signal_one(cond_var_t *cv) {WakeConditionVariable(cv); return 0;}

KSYS_INL int
cond_var_signal_all(cond_var_t *cv) {WakeAllConditionVariable(cv); return 0;}

KSYS_INL int
semaphore_init(semaphore_t *sem, int initial_count)
{
    *sem = CreateSemaphoreEx(0,
        (LONG)initial_count,
        (LONG)INT_MAX, 0, 0,
        SEMAPHORE_ALL_ACCESS);

    return *sem ? 0 : GetLastError();
}

KSYS_INL int
semaphore_wait(semaphore_t *sem)
{
    return WaitForSingleObject(*sem, INFINITE) != WAIT_FAILED \
        ? 0 : GetLastError();
}

KSYS_INL int
semaphore_release(semaphore_t *sem)
{
    return ReleaseSemaphore(*sem, 1, 0) ? 0 : GetLastError();
}

KSYS_INL int waiter_init(waiter_t *w)
    {return !(w->h = CreateEventA(0, 0, FALSE, 0));}

KSYS_INL void waiter_destroy(waiter_t *w)
    {CloseHandle(w->h);}

KSYS_INL int waiter_wait(waiter_t *w, int timeout_ms)
{
    DWORD to = timeout_ms == -1 ? INFINITE : (DWORD)timeout_ms;
    WaitForSingleObject(w->h, to);
    return 0;
}

KSYS_INL int waiter_wake(waiter_t *w)
    {return !SetEvent(w->h);}

KSYS_INL int
get_num_physical_cpus()
{
    SYSTEM_INFO info;
    GetSystemInfo(&info);
    return info.dwNumberOfProcessors;
}

KSYS_INL int32_t
interlocked_exchange_int32(int32_t volatile *target, int32_t value)
{
    return InterlockedExchange(target, value);
}

KSYS_INL int32_t
interlocked_compare_exchange_int32(int32_t volatile *target, int32_t exchange,
    int32_t comparand)
{
    return InterlockedCompareExchange(target, exchange, comparand);
}

KSYS_INL int32_t
interlocked_increment_int32(int32_t volatile *target)
{
    return InterlockedIncrement(target);
}

KSYS_INL int32_t
interlocked_decrement_int32(int32_t volatile *target)
{
    return InterlockedDecrement(target);
}

KSYS_INL int
sleep_ms(unsigned int msec) {Sleep(msec); return 0;}

KSYS_INL int
get_monotonic_time(sys_time_t *ret_time)
{
    static LARGE_INTEGER freq = {0};

    if (freq.QuadPart == 0 && QueryPerformanceFrequency(&freq) == 0)
        return 1;

    LARGE_INTEGER num_ticks;
    if (QueryPerformanceCounter(&num_ticks) == 0)
        return 2;

    num_ticks.QuadPart = num_ticks.QuadPart / (freq.QuadPart / 1000);

    ret_time->sec   = (uint64_t)(num_ticks.QuadPart / 1000);
    ret_time->msec  = (uint64_t)(num_ticks.QuadPart % 1000);

    return 0;
}

#define stack_alloc(size) _alloca(size)

KSYS_INL int
create_directory(const char *path)
{
    DWORD a = GetFileAttributes(path);
    if (a != INVALID_FILE_ATTRIBUTES && a & FILE_ATTRIBUTE_DIRECTORY)
        return 0;
    BOOL r = CreateDirectory(path, 0);
    return !(r || (r && GetLastError() == ERROR_ALREADY_EXISTS));
}

KSYS_INL int
file_exists(const char *path)
    {struct _stat s; return (!_stat(path, &s));}

KSYS_INL dir_handle_t
open_directory(const char *path, dir_entry_t *ret_first_de)
{
    if (!path) return 0;
    char stack_buf[512];
    char *buf = stack_buf;
    for (;;)
    {
        int num_req = snprintf(buf, 512, "%s/*", path);
        if (num_req > 511)
        {
            buf = malloc(num_req + 1);
            if (!buf) return 0;
        } else
            break;
    }
    dir_handle_t ret = FindFirstFile(buf, ret_first_de);
    if (buf != stack_buf) free(buf);
    return ret;
}

KSYS_INL void
close_directory(dir_handle_t dir)
    {FindClose(dir);}

KSYS_INL int
get_next_file_in_directory(dir_handle_t h, dir_entry_t *ret_de)
    {return FindNextFile(h, ret_de) ? 0 : 1;}

KSYS_INL char *
get_dir_entry_name(dir_entry_t *de) {return de ? de->cFileName : 0;}

#if KSYS_SOCKETS

KSYS_INL int init_socket_api()
{
    WSADATA wsa_data;
    return WSAStartup(MAKEWORD(2, 2), &wsa_data);
};

KSYS_INL int
close_socket(socket_t s)
    {return closesocket(s) != SOCKET_ERROR ? 0 : -1;}

KSYS_INL int
make_socket_non_block(socket_t sock)
{
    u_long mode = 1;
    return ioctlsocket(sock, FIONBIO, &mode);
}

KSYS_INL int
socket_num_readable_bytes(socket_t s, long int *ret)
{
    u_long num_bytes;
    int res = ioctlsocket(s, FIONREAD, &num_bytes);
    if (!res)
    {
        *ret = (long int)num_bytes;
        return 0;
    }
    return 1;
}

KSYS_INL int
sock_err_str(char *buf, int buf_len)
{
    if (!buf || buf_len <= 0) return 0;
    wchar_t *str = 0;
    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        0,
        WSAGetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPWSTR)&str, 0, 0);
    size_t len = wcstombs(buf, str, buf_len);
    if (len < 1) buf[0] = 0;
    LocalFree(str);
    return (int)len;
}

#endif /* KSYS_SOCKETS */

#endif /* End of system switch */

#undef KSYS_INL

#endif /* KSYS_H */
