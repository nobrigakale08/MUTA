typedef uint8 proxy_msg_type_t;
#define PROXY_MSGTSZ sizeof(proxy_msg_type_t)
#include "rwbits.inl"
#include "sv_common_defs.h"
#include "crypt.h"
#define WRITE_PROXY_MSG_TYPE(mem, val) \
    *(proxy_msg_type_t*)(mem) = (proxy_msg_type_t)(val); \
    (mem) = (uint8*)(mem) + sizeof(proxy_msg_type_t);
enum muta_proxy_msg_types
{
    PROXY_MSG_PUB_KEY = 0,
    PROXY_MSG_STREAM_HEADER,

    NUM_MUTA_PROXY_MSG_TYPES
};

#if NUM_MUTA_PROXY_MSG_TYPES > 255
#   error too many MUTA_PROXY_MSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(proxy_msg, PROXY_MSGTSZ, WRITE_PROXY_MSG_TYPE);

enum muta_tproxy_msg_types
{
    TPROXY_MSG_LOGIN_RESULT = NUM_MUTA_PROXY_MSG_TYPES,
    TPROXY_MSG_OPENED_SOCKET,
    TPROXY_MSG_CLOSED_SOCKET,
    TPROXY_MSG_PLAYER_PACKET,

    NUM_MUTA_TPROXY_MSG_TYPES
};

#if NUM_MUTA_TPROXY_MSG_TYPES > 255
#   error too many MUTA_TPROXY_MSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(tproxy_msg, PROXY_MSGTSZ, WRITE_PROXY_MSG_TYPE);

enum muta_fproxy_msg_types
{
    FPROXY_MSG_LOGIN = NUM_MUTA_PROXY_MSG_TYPES,
    FPROXY_MSG_OPENED_SOCKET_RESULT,
    FPROXY_MSG_PLAYER_CRYPTCHAN,
    FPROXY_MSG_PLAYER_PACKET,
    FPROXY_MSG_CLOSE_SOCKET,

    NUM_MUTA_FPROXY_MSG_TYPES
};

#if NUM_MUTA_FPROXY_MSG_TYPES > 255
#   error too many MUTA_FPROXY_MSG_TYPES types
#endif

MSG_WRITE_PREP_DEFINITION(fproxy_msg, PROXY_MSGTSZ, WRITE_PROXY_MSG_TYPE);

typedef struct
{
    uint8 key[CRYPTCHAN_PUB_KEY_SZ];
} proxy_msg_pub_key_t;

#define PROXY_MSG_PUB_KEY_SZ (CRYPTCHAN_PUB_KEY_SZ)

static inline int
proxy_msg_pub_key_write(byte_buf_t *buf, proxy_msg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + PROXY_MSG_PUB_KEY_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, PROXY_MSG_PUB_KEY);
    WRITE_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

static inline int
proxy_msg_pub_key_read(byte_buf_t *buf, proxy_msg_pub_key_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSG_PUB_KEY_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->key, CRYPTCHAN_PUB_KEY_SZ);

    return 0;
}

typedef struct
{
    uint8 header[CRYPTCHAN_STREAM_HEADER_SZ];
} proxy_msg_stream_header_t;

#define PROXY_MSG_STREAM_HEADER_SZ (CRYPTCHAN_STREAM_HEADER_SZ)

static inline int
proxy_msg_stream_header_write(byte_buf_t *buf, proxy_msg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + PROXY_MSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, PROXY_MSG_STREAM_HEADER);
    WRITE_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

static inline int
proxy_msg_stream_header_read(byte_buf_t *buf, proxy_msg_stream_header_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSG_STREAM_HEADER_SZ);
    if (!mem) return 1;
    READ_UINT8_FIXARR(mem, s->header, CRYPTCHAN_STREAM_HEADER_SZ);

    return 0;
}

typedef struct
{
    uint8 result;
    uint8 shard_name_len;
    uint8 shard_name[MAX_SHARD_NAME_LEN];
} tproxy_msg_login_result_t;

#define TPROXY_MSG_LOGIN_RESULT_SZ (sizeof(uint8) + sizeof(uint8) + MAX_SHARD_NAME_LEN)

static inline int
tproxy_msg_login_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TPROXY_MSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, TPROXY_MSG_LOGIN_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->result);
    WRITE_UINT8(mem, s->shard_name_len);
    WRITE_UINT8_FIXARR(mem, s->shard_name, MAX_SHARD_NAME_LEN);

    return cryptchan_encrypt(cc, dst, src, TPROXY_MSG_LOGIN_RESULT_SZ);
}

static inline int
tproxy_msg_login_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_login_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TPROXY_MSG_LOGIN_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TPROXY_MSG_LOGIN_RESULT_SZ))
        return -1;
    READ_UINT8(mem, &s->result);
    READ_UINT8(mem, &s->shard_name_len);
    READ_UINT8_FIXARR(mem, s->shard_name, MAX_SHARD_NAME_LEN);

    return 0;
}

typedef struct
{
    uint8 account_name_len;
    uint32 socket_id;
    uint64 account_id;
    uint32 ip;
    const char *account_name;
    uint8 token[AUTH_TOKEN_SZ];
} tproxy_msg_opened_socket_t;

#define TPROXY_MSG_OPENED_SOCKET_SZ (sizeof(uint8) + sizeof(uint32) + sizeof(uint64) + sizeof(uint32) + AUTH_TOKEN_SZ)

#define TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(account_name_len) \
    (TPROXY_MSG_OPENED_SOCKET_SZ + (account_name_len))

#define TPROXY_MSG_OPENED_SOCKET_MAX_SZ \
    (TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(MAX_ACC_NAME_LEN))

static inline int
tproxy_msg_opened_socket_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_opened_socket_t *s)
{
    int sz = TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(s->account_name_len);
    if (sz > TPROXY_MSG_OPENED_SOCKET_MAX_SZ)
        return -1;

    uint8 *mem = prep_tproxy_msg_write_var_encrypted(buf, TPROXY_MSG_OPENED_SOCKET, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->account_name_len);
    WRITE_UINT32(mem, s->socket_id);
    WRITE_UINT64(mem, s->account_id);
    WRITE_UINT32(mem, s->ip);
    WRITE_STR(mem, s->account_name, s->account_name_len);
    WRITE_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
tproxy_msg_opened_socket_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_opened_socket_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, TPROXY_MSG_OPENED_SOCKET_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->account_name_len);

    if (TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(s->account_name_len) >
        TPROXY_MSG_OPENED_SOCKET_MAX_SZ)
        return -1;

    if (sz < TPROXY_MSG_OPENED_SOCKET_COMPUTE_SZ(s->account_name_len)) return -1;

    READ_UINT32(mem, &s->socket_id);
    READ_UINT64(mem, &s->account_id);
    READ_UINT32(mem, &s->ip);
    READ_VARCHAR(mem, s->account_name, s->account_name_len);
    READ_UINT8_FIXARR(mem, s->token, AUTH_TOKEN_SZ);

    return 0;
}

typedef struct
{
    uint32 socket_id;
} tproxy_msg_closed_socket_t;

#define TPROXY_MSG_CLOSED_SOCKET_SZ (sizeof(uint32))

static inline int
tproxy_msg_closed_socket_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_closed_socket_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + TPROXY_MSG_CLOSED_SOCKET_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, TPROXY_MSG_CLOSED_SOCKET);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->socket_id);

    return cryptchan_encrypt(cc, dst, src, TPROXY_MSG_CLOSED_SOCKET_SZ);
}

static inline int
tproxy_msg_closed_socket_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    tproxy_msg_closed_socket_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        TPROXY_MSG_CLOSED_SOCKET_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        TPROXY_MSG_CLOSED_SOCKET_SZ))
        return -1;
    READ_UINT32(mem, &s->socket_id);

    return 0;
}

typedef struct
{
    uint16 data_len;
    uint32 socket_id;
    const char *data;
} tproxy_msg_player_packet_t;

#define TPROXY_MSG_PLAYER_PACKET_SZ (sizeof(uint16) + sizeof(uint32))

#define TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(data_len) \
    (TPROXY_MSG_PLAYER_PACKET_SZ + (data_len))

#define TPROXY_MSG_PLAYER_PACKET_MAX_SZ \
    (TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(MUTA_MTU))

static inline int
tproxy_msg_player_packet_write(byte_buf_t *buf, tproxy_msg_player_packet_t *s)
{
    int sz = TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len);
    if (sz > TPROXY_MSG_PLAYER_PACKET_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len));
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, TPROXY_MSG_PLAYER_PACKET);
    WRITE_UINT16(mem, s->data_len);
    WRITE_UINT32(mem, s->socket_id);
    WRITE_STR(mem, s->data, s->data_len);

    return 0;
}

static inline int
tproxy_msg_player_packet_read(byte_buf_t *buf, tproxy_msg_player_packet_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < TPROXY_MSG_PLAYER_PACKET_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT16(mem, &s->data_len);

    if (TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len) >
        TPROXY_MSG_PLAYER_PACKET_MAX_SZ)
        return -1;

    int req_sz = TPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT32(mem, &s->socket_id);
    READ_VARCHAR(mem, s->data, s->data_len);

    return 0;
}

typedef struct
{
    uint8 password_len;
    const char *password;
} fproxy_msg_login_t;

#define FPROXY_MSG_LOGIN_SZ (sizeof(uint8))

#define FPROXY_MSG_LOGIN_COMPUTE_SZ(password_len) \
    (FPROXY_MSG_LOGIN_SZ + (password_len))

#define FPROXY_MSG_LOGIN_MAX_SZ \
    (FPROXY_MSG_LOGIN_COMPUTE_SZ(MAX_PROXY_PW_LEN))

static inline int
fproxy_msg_login_write_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_login_t *s)
{
    int sz = FPROXY_MSG_LOGIN_COMPUTE_SZ(s->password_len);
    if (sz > FPROXY_MSG_LOGIN_MAX_SZ)
        return -1;

    uint8 *mem = prep_fproxy_msg_write_var_encrypted(buf, FPROXY_MSG_LOGIN, sz);
    if (!mem) return 1;

    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT8(mem, s->password_len);
    WRITE_STR(mem, s->password, s->password_len);

    return cryptchan_encrypt(cc, dst, src, sz);
}

static inline int
fproxy_msg_login_read_var_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_login_t *s)
{
    msg_sz_t sz;
    int r = prep_msg_read_var_encrypted(buf, FPROXY_MSG_LOGIN_MAX_SZ, &sz);
    if (r) return r;

    uint8 *mem = bbuf_reserve(buf, sz);

    if (cryptchan_decrypt(cc, mem, mem, sz) < 0)
        return -1;

    READ_UINT8(mem, &s->password_len);

    if (FPROXY_MSG_LOGIN_COMPUTE_SZ(s->password_len) >
        FPROXY_MSG_LOGIN_MAX_SZ)
        return -1;

    if (sz < FPROXY_MSG_LOGIN_COMPUTE_SZ(s->password_len)) return -1;

    READ_VARCHAR(mem, s->password, s->password_len);

    return 0;
}

typedef struct
{
    uint32 socket_id;
    uint8 result;
} fproxy_msg_opened_socket_result_t;

#define FPROXY_MSG_OPENED_SOCKET_RESULT_SZ (sizeof(uint32) + sizeof(uint8))

static inline int
fproxy_msg_opened_socket_result_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_opened_socket_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + FPROXY_MSG_OPENED_SOCKET_RESULT_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, FPROXY_MSG_OPENED_SOCKET_RESULT);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->socket_id);
    WRITE_UINT8(mem, s->result);

    return cryptchan_encrypt(cc, dst, src, FPROXY_MSG_OPENED_SOCKET_RESULT_SZ);
}

static inline int
fproxy_msg_opened_socket_result_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_opened_socket_result_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        FPROXY_MSG_OPENED_SOCKET_RESULT_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        FPROXY_MSG_OPENED_SOCKET_RESULT_SZ))
        return -1;
    READ_UINT32(mem, &s->socket_id);
    READ_UINT8(mem, &s->result);

    return 0;
}

typedef struct
{
    uint32 socket_id;
    uint8 pk[CRYPTCHAN_PUB_KEY_SZ];
    uint8 sk[CRYPTCHAN_SEC_KEY_SZ];
    uint8 wx[CRYPTCHAN_SESSION_KEY_SZ];
    uint8 rx[CRYPTCHAN_SESSION_KEY_SZ];
    uint8 read_key[CRYPTSTREAM_KEY_SZ];
    uint8 read_nonce[CRYPTSTREAM_NONCE_SZ];
    uint8 write_key[CRYPTSTREAM_KEY_SZ];
    uint8 write_nonce[CRYPTSTREAM_NONCE_SZ];
} fproxy_msg_player_cryptchan_t;

#define FPROXY_MSG_PLAYER_CRYPTCHAN_SZ (sizeof(uint32) + CRYPTCHAN_PUB_KEY_SZ + CRYPTCHAN_SEC_KEY_SZ + CRYPTCHAN_SESSION_KEY_SZ + CRYPTCHAN_SESSION_KEY_SZ + CRYPTSTREAM_KEY_SZ + CRYPTSTREAM_NONCE_SZ + CRYPTSTREAM_KEY_SZ + CRYPTSTREAM_NONCE_SZ)

static inline int
fproxy_msg_player_cryptchan_write_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_player_cryptchan_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        CRYPT_MSG_ADDITIONAL_BYTES + FPROXY_MSG_PLAYER_CRYPTCHAN_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, FPROXY_MSG_PLAYER_CRYPTCHAN);
    uint8 *dst = mem;
    mem += CRYPT_MSG_ADDITIONAL_BYTES;
    uint8 *src = mem;

    WRITE_UINT32(mem, s->socket_id);
    WRITE_UINT8_FIXARR(mem, s->pk, CRYPTCHAN_PUB_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->sk, CRYPTCHAN_SEC_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->wx, CRYPTCHAN_SESSION_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->rx, CRYPTCHAN_SESSION_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->read_key, CRYPTSTREAM_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->read_nonce, CRYPTSTREAM_NONCE_SZ);
    WRITE_UINT8_FIXARR(mem, s->write_key, CRYPTSTREAM_KEY_SZ);
    WRITE_UINT8_FIXARR(mem, s->write_nonce, CRYPTSTREAM_NONCE_SZ);

    return cryptchan_encrypt(cc, dst, src, FPROXY_MSG_PLAYER_CRYPTCHAN_SZ);
}

static inline int
fproxy_msg_player_cryptchan_read_const_encrypted(byte_buf_t *buf, cryptchan_t *cc,
    fproxy_msg_player_cryptchan_t *s)
{
    uint8 *mem = bbuf_reserve(buf, CRYPT_MSG_ADDITIONAL_BYTES + \
        FPROXY_MSG_PLAYER_CRYPTCHAN_SZ);
    if (!mem) return 1;
    if (cryptchan_decrypt(cc, mem, mem, CRYPT_MSG_ADDITIONAL_BYTES + \
        FPROXY_MSG_PLAYER_CRYPTCHAN_SZ))
        return -1;
    READ_UINT32(mem, &s->socket_id);
    READ_UINT8_FIXARR(mem, s->pk, CRYPTCHAN_PUB_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->sk, CRYPTCHAN_SEC_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->wx, CRYPTCHAN_SESSION_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->rx, CRYPTCHAN_SESSION_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->read_key, CRYPTSTREAM_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->read_nonce, CRYPTSTREAM_NONCE_SZ);
    READ_UINT8_FIXARR(mem, s->write_key, CRYPTSTREAM_KEY_SZ);
    READ_UINT8_FIXARR(mem, s->write_nonce, CRYPTSTREAM_NONCE_SZ);

    return 0;
}

typedef struct
{
    uint16 data_len;
    uint32 socket_id;
    const char *data;
} fproxy_msg_player_packet_t;

#define FPROXY_MSG_PLAYER_PACKET_SZ (sizeof(uint16) + sizeof(uint32))

#define FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(data_len) \
    (FPROXY_MSG_PLAYER_PACKET_SZ + (data_len))

#define FPROXY_MSG_PLAYER_PACKET_MAX_SZ \
    (FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(MUTA_MTU))

static inline int
fproxy_msg_player_packet_write(byte_buf_t *buf, fproxy_msg_player_packet_t *s)
{
    int sz = FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len);
    if (sz > FPROXY_MSG_PLAYER_PACKET_MAX_SZ)
        return -1;

    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + \
        FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len));
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, FPROXY_MSG_PLAYER_PACKET);
    WRITE_UINT16(mem, s->data_len);
    WRITE_UINT32(mem, s->socket_id);
    WRITE_STR(mem, s->data, s->data_len);

    return 0;
}

static inline int
fproxy_msg_player_packet_read(byte_buf_t *buf, fproxy_msg_player_packet_t *s)
{
    int free_space = (int)BBUF_FREE_SPACE(buf);

    if (free_space < FPROXY_MSG_PLAYER_PACKET_SZ)
        return 1;

    uint8 *mem = BBUF_CUR_PTR(buf);
    READ_UINT16(mem, &s->data_len);

    if (FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len) >
        FPROXY_MSG_PLAYER_PACKET_MAX_SZ)
        return -1;

    int req_sz = FPROXY_MSG_PLAYER_PACKET_COMPUTE_SZ(s->data_len);
    if (free_space < req_sz) return 2;

    bbuf_reserve(buf, req_sz);

    READ_UINT32(mem, &s->socket_id);
    READ_VARCHAR(mem, s->data, s->data_len);

    return 0;
}

typedef struct
{
    uint32 socket_id;
} fproxy_msg_close_socket_t;

#define FPROXY_MSG_CLOSE_SOCKET_SZ (sizeof(uint32))

static inline int
fproxy_msg_close_socket_write(byte_buf_t *buf, fproxy_msg_close_socket_t *s)
{
    uint8 *mem = bbuf_reserve(buf, PROXY_MSGTSZ + FPROXY_MSG_CLOSE_SOCKET_SZ);
    if (!mem) return 1;
    WRITE_PROXY_MSG_TYPE(mem, FPROXY_MSG_CLOSE_SOCKET);
    WRITE_UINT32(mem, s->socket_id);

    return 0;
}

static inline int
fproxy_msg_close_socket_read(byte_buf_t *buf, fproxy_msg_close_socket_t *s)
{
    uint8 *mem = bbuf_reserve(buf, FPROXY_MSG_CLOSE_SOCKET_SZ);
    if (!mem) return 1;
    READ_UINT32(mem, &s->socket_id);

    return 0;
}

