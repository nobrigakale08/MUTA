#include "net.h"
#include <string.h>
#include <stdio.h>

void
addr_init(addr_t *addr, uint8 a, uint8 b, uint8 c, uint8 d, uint16 port)
{
    uint32 ip;
    ((uint8*)&ip)[3] = a;
    ((uint8*)&ip)[2] = b;
    ((uint8*)&ip)[1] = c;
    ((uint8*)&ip)[0] = d;
    addr->ip    = ip;
    addr->port  = port;
}

int
addr_init_from_str(addr_t *addr, const char *ip, uint16 port)
{
    int        ret = 0;
    uint       a, b, c, d;
    const char *fmt = "%d.%d.%d.%d";
    if (sscanf(ip, fmt, &a, &b, &c, &d) == 4)
    {
        if (a < 256 && b < 256 && c < 256 && d < 256)
            addr_init(addr, a, b, c, d, port);
        else
            ret = 1;
    } else
    if (strcmp(ip, "localhost"))
        addr_init(addr, 127, 0, 0, 1, port);
    else
        ret = 2;
    return ret;
}

addr_t
create_addr(uint8 a, uint8 b, uint8 c, uint8 d, uint16 port)
{
    addr_t ret;
    addr_init(&ret, a, b, c, d, port);
    return ret;
}

socket_t
net_async_tcp_socket()
{
    socket_t s;
#if defined(_WIN32)
    s = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED);
#else
    s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
#endif
    return s;
}

int
net_send_all(socket_t sock, const void *msg, int len)
{
    int sent    = 0;
    int res     = 0;
    while (sent < len && res >= 0)
    {
        res = send(sock, msg, len, 0);
        sent += res;
    }
    return res < 0 ? res : sent;
}

int
net_send_all_to(socket_t sock, const void *msg, int len, addr_t *addr)
{
    struct sockaddr_in in_addr;
    addr_to_sockaddr(addr, &in_addr);
    int sent    = 0;
    int err     = 0;
    while (sent < len && err >= 0)
    {
        err = sendto(sock, msg, len, 0, (struct sockaddr *)&in_addr,
            sizeof(in_addr));
        if (err >= 0)
            sent += err;
        else
            return err;
    }
    return sent;
}

int
net_recv_from(socket_t sock, void *buffer, int buf_len, addr_t *sender_addr)
{
    struct sockaddr_in  sa;
    socklen_t           addr_len = sizeof(sa);

    int received = recvfrom(sock, buffer, buf_len, 0, (struct sockaddr*)&sa,
        &addr_len);
    addr_from_sockaddr(sender_addr, (struct sockaddr *)&sa);

    return received;
}

int
net_bind(socket_t sock, uint16 port)
{
    struct sockaddr_in in_addr;
    in_addr.sin_family      = AF_INET;
    in_addr.sin_addr.s_addr = INADDR_ANY;
    in_addr.sin_port        = htons(port);
    return bind(sock, (struct sockaddr*)&in_addr, sizeof(in_addr));
}

int
net_make_sock_reusable(socket_t sock)
{
    int yes = 1;
    return setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes));
}

socket_t
net_tcp_ipv4_listen_sock(uint16 port, int backlog)
{
    socket_t s = net_tcp_ipv4_sock();
    if (s == KSYS_INVALID_SOCKET)
        return s;
    if (net_make_sock_reusable(s))
        goto fail;
    if (net_bind(s, port))
        goto fail;
    if (net_listen(s, backlog))
        goto fail;
    return s;
    fail:
        close_socket(s);
        return KSYS_INVALID_SOCKET;
}

socket_t
net_accept(socket_t sock, addr_t *addr)
{
    struct sockaddr sa;
    socklen_t addr_len = sizeof(sa);
    socket_t ret = accept(sock, &sa, &addr_len);
    addr_from_sockaddr(addr, &sa);
    return ret;
}

int
net_connect(socket_t sock, addr_t addr)
{
    struct sockaddr_in sa;
    addr_to_sockaddr(&addr, &sa);
    return connect(sock, (struct sockaddr *)&sa, sizeof(sa));
}

int
net_close_blocking_sock(socket_t sock)
{
#if defined(__linux__)
    return net_shutdown_sock(sock, SOCKSD_BOTH);
#elif defined(_WIN32)
    int r = shutdown(sock, SD_BOTH);
    if (r == 0)
        return close_socket(sock);
    return -1;
#else
#   error "Unsupported platform."
#endif
}

int
net_shutdown_sock(socket_t sock, enum sock_shutdown_t type)
{
    int ra = shutdown(sock, type);
    int rb = close_socket(sock);
    return (ra ? (1 << 0) : 0) | (rb ? (1 << 1) : 0);
}

socket_t
net_tcp_ipv4_sock()
{
    socket_t s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (s == KSYS_INVALID_SOCKET)
        return s;
    if (!net_make_sock_reusable(s))
        return s;
    close_socket(s);
    return KSYS_INVALID_SOCKET;
}

int
net_addr_of_sock(socket_t sock, addr_t *ret_addr)
{
    struct sockaddr sa;
#ifdef _WIN32
    int len = sizeof(sa);
#else
    socklen_t len = sizeof(sa);
#endif
    if (getpeername(sock, &sa, &len))
        return 1;
    addr_from_sockaddr(ret_addr, &sa);
    return 0;
}

void
addr_to_sockaddr(const addr_t *addr, struct sockaddr_in *sa)
{
    memset(sa, 0, sizeof(struct sockaddr_in));
    sa->sin_family      = AF_INET;
    sa->sin_addr.s_addr = htonl(addr->ip);
    sa->sin_port        = htons(addr->port);
}

void
addr_from_sockaddr(addr_t *addr, const struct sockaddr *sa)
{
    const struct sockaddr_in *in_sa = (const struct sockaddr_in *)sa;
    addr->ip    = ntohl(in_sa->sin_addr.s_addr);
    addr->port  = ntohs(in_sa->sin_port);
}

uint32
uint32_ip_from_uint8s(uint8 a, uint8 b, uint8 c, uint8 d)
{
    uint32 ret;
    uint8 *arr = (uint8*)&ret;
    arr[3] = a;
    arr[2] = b;
    arr[1] = c;
    arr[0] = d;
    return ret;
}

