::Replace architecture tags in a Makefile

@echo off

set ERRORLEVEL=0

set file=%1
set arch=%2

echo %0 called with %arch% for file %file%...

if [%arch%]==[x86] goto x86
if [%arch%]==[x64] goto x64
goto fail:

:x86
    call replace.bat x64 x86 %file% >> .Makefile.x86
    del %file%
    move .Makefile.x86 %file%
    goto out

:x64
    call replace.bat x86 x64 %file% >> .Makefile.x64
    del %file%
    move .Makefile.x64 %file%
    goto out

:fail
    echo %0: invalid architecture, must be x86 or x64
    set ERRORLEVEL=1

:out
