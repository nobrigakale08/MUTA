#ifndef MUTA_DB_CORE_H
#define MUTA_DB_CORE_H

#include "../../shared/types.h"

int
core_init(const char *cfg_fp);

bool32
core_update();
/* Returns true if program should exit */

int
core_store_sys_user(const char *name, const char *pw);
/* Attempt to store up a new user account for the database server */

#endif
