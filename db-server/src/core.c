#include <stdio.h>
#include "core.h"
#include "../../shared/netqueue.h"
#include "../../shared/sv_time.c"
#include "../../shared/sv_common_defs.h"
#include "../../shared/common_utils.h"
#include "../../shared/containers.h"
#include "../../shared/common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/rwbits.inl"
#include "../../shared/db_packets.h"
#include "../../shared/acc_utils.h"
#include "db.h"

typedef struct sys_acc_t        sys_acc_t;
typedef struct client_t         client_t;
typedef struct out_msg_note_t   out_msg_note_t;
typedef struct mt_cmd_t         mt_cmd_t;

#define MAX_SYS_IPS             32
#define MAX_LISTEN_PORTS        32
#define MAX_LISTENERS           8
#define CL_MSGS_IN_SZ           MUTA_MTU
#define CL_MSGS_OUT_SZ          MUTA_MTU
#define CL_CLEANUP_TIME         2000
#define MAX_SYS_ACC_NAME_LEN    64
#define MAX_SYS_ACCS            8

enum client_type_t
{
    CL_UNKNOWN,
    CL_MASTER,
    CL_WORLDD
};

enum client_flag_t
{
    CL_FLAG_RESERVED            = (1 << 0),
    CL_FLAG_ADDED_TO_SEND_QUEUE = (1 << 1)
};

struct sys_acc_t
{
    char name[MAX_SYS_ACC_NAME_LEN + 1];
    char pw[CRYPT_PW_HASH_SZ + 1];
};

struct client_t
{
    net_handle_t    net_handle;
    uint8           flags;
    cryptchan_t     cryptchan;
    uint32          id;
    client_t        *next;
    uint8           type;
    uint16          num_unread_msgs;
    uint64          last_recvd_from;
    mutex_t         out_mtx;
    byte_buf_t      msgs_in;
    uint8           msgs_in_mem[CL_MSGS_IN_SZ];
    mutex_t         in_mtx;
    byte_buf_t      msgs_out;
    uint8           msgs_out_mem[CL_MSGS_OUT_SZ];
};

struct out_msg_note_t
{
    client_t    *c;
    uint32      id;
};

struct mt_cmd_t
{
    int type;

    union
    {
        struct
        {
            client_t    *c;
            uint32      id;
        } dc_client;
    } d;
};

STATIC_OBJ_POOL_DEFINITION(client_t, client_pool);

static net_queue_t      _net_queue;
static perf_clock_t     _mt_clock;
static double_cmd_buf_t _mt_cmd_buf;

static struct
{
    uint32  ips[MAX_SYS_IPS];
    int     num;
} _sys_ips;

static struct
{
    uint16  ports[MAX_LISTEN_PORTS];
    int     num;
} _listen_ports;

static struct
{
    net_handle_t    handles[MAX_LISTENERS];
    int             num;
} _listeners;

static struct
{
    sys_acc_t   accs[MAX_SYS_ACCS];
    int         num;
    bool32      read_file;
} _sys_accs;

static struct
{
    uint32              running_id;
    client_t            *mem;
    client_pool_t       pool;
    client_t            *res;
    mutex_t             mtx;
    double_cmd_buf_t    out_msg_notes;
} _clients;

static struct
{
    int     tickrate;
    char    *mysql_sock_path;
} _config;

static int
_push_sys_ip(uint32 ip);

static bool32
_is_ip_sys_ip(uint32 ip);

static int
_push_listen_port(uint16 port);

static int
_push_sys_acc(const char *name, const char *pw);
/* Will update pw if account exists */

static int
_read_sys_accs_file();

static int
_save_sys_accs_file();

static sys_acc_t *
_get_sys_acc(const char *name);

static client_t *
_create_new_client(socket_t fd);

static void
_disconnect_and_free_client(client_t *c);

static byte_buf_t *
_begin_send_msg_to_client(client_t *c, int sz);

static byte_buf_t *
_begin_send_const_encrypted_msg_to_client(client_t *c, int sz);

static int
_finalize_send_msg_to_client(client_t *c);
/* Returns 0 on success */

static int
_read_unknown_client_packet(client_t *c);
/* Returns amount of bytes left in the buffer after read, or -1 if client
 * should be disconnected due to erroneus messages. */

static int
_read_master_packet(client_t *c);

static void
_send_pending_client_packets();

static void
_nq_accept_callback(net_handle_t *h, socket_t fd, addr_t addr, int err);

static void
_nq_read_callback(net_handle_t *h, int num_bytes);

static int
_init_clients(int max);

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val);

static void
_read_config(const char *fp);

static int
_begin_listening();

static void
_update_client_timers();

static inline int
_handle_unknown_dbmsg_pub_key(client_t *c, dbmsg_pub_key_t *s);

static inline int
_handle_unknown_dbmsg_stream_header(client_t *c, dbmsg_stream_header_t *s);

static inline int
_handle_unknown_dbmsg_login(client_t *c, dbmsg_login_t *s);

static inline int
_handle_master_tdbmsg_query_account_exists(client_t *c,
    tdbmsg_query_account_exists_t *s);

static inline int
_handle_master_tdbmsg_query_login_account(client_t *c,
    tdbmsg_query_login_account_t *s);

static int
_handle_master_tdbmsg_query_account_first_id(client_t *c,
    tdbmsg_query_account_first_character_id_t *s);

static int
_handle_master_tdbmsg_insert_create_character(client_t *c,
    tdbmsg_insert_create_character_t *s);

static inline int
_handle_tdbmsg_update_character_position(client_t *c,
    tdbmsg_update_character_position_t *s);

static inline int
_handle_tdbmsg_query_account_characters(client_t *c,
    tdbmsg_query_account_characters_t *s);

int
core_init(const char *cfg_fp)
{
    _read_config(cfg_fp);
    _read_sys_accs_file();
    perf_clock_init(&_mt_clock, _config.tickrate);
    if (double_cmd_buf_init(&_mt_cmd_buf, sizeof(mt_cmd_t), 1024))
        return 1;
    if (init_time())                        return 2;
    if (init_socket_api())                  return 3;
    int nq_res = net_queue_init(&_net_queue, 1, _nq_read_callback,
        _nq_accept_callback);
    if (crypt_init())                       return 4;
    if (nq_res)                             return 5;
    if (_init_clients(32))                  return 6;
    if (db_init(_config.mysql_sock_path))   return 7;
    if (net_queue_run(&_net_queue))         return 8;
    if (_begin_listening())                 return 9;
    return 0;
}

bool32
core_update()
{
    static uint64 last_printed = 0;
    perf_clock_tick(&_mt_clock);

    /* Attempt to reconnect to the database on every tick if dc'd */
    if (db_conn_state() == DBCONN_DISCONNECTED)
    {
        uint64 t = get_program_ticks_ms();
        if (!last_printed || t - last_printed > 2500)
        {
            puts("Attempting to connect to MariaDB server...");
            last_printed = t;
        }
        db_connect("localhost", "muta", "muta");
    }
    _update_client_timers();
    _send_pending_client_packets();
    return 0;
}

int
core_store_sys_user(const char *name, const char *pw)
{
    if (!accut_is_name_legaln(name, (int)strlen(name))) return 1;
    char pw_hash[CRYPT_PW_HASH_SZ];
    if (crypt_storable_pw_hash(pw, (int)strlen(pw), pw_hash,
        CRYPT_PW_STRENGTH_MEDIUM))
        return 2;
    if (_push_sys_acc(name, pw_hash))   return 2;
    if (_save_sys_accs_file())          return 3;
    return 0;
}

static int
_push_sys_ip(uint32 ip)
{
    if (_is_ip_sys_ip(ip)) return 0;
    if (_sys_ips.num == MAX_SYS_IPS) return 1;
    _sys_ips.ips[_sys_ips.num++] = ip;
    uint8 *a = (uint8*)&ip;
    printf("Pushed system ip %u.%u.%u.%u\n",
        (uint)a[3], (uint)a[2], (uint)a[1], (uint)a[0]);
    return 0;
}

static bool32
_is_ip_sys_ip(uint32 ip)
{
    int num = _sys_ips.num;
    for (int i = 0; i < num; ++i)
        if (_sys_ips.ips[i] == ip)
            return 1;
    return 0;
}

static int
_push_listen_port(uint16 port)
{
    int num = _listen_ports.num;
    for (int i = 0; i < num; ++i)
        if (_listen_ports.ports[i] == port)
            return 0;
    if (num == MAX_LISTEN_PORTS)
        return 1;
    _listen_ports.ports[num]   = port;
    _listen_ports.num          = num + 1;
    printf("Pushed new listen port %u\n", (uint)port);
    return 0;
}

static int
_push_sys_acc(const char *name, const char *pw)
{
    sys_acc_t *a = _get_sys_acc(name);
    if (!a)
    {
        if (_sys_accs.num == MAX_SYS_ACCS) return 1;
        sys_acc_t *a = &_sys_accs.accs[_sys_accs.num++];
        strncpy(a->name, name, MAX_SYS_ACC_NAME_LEN);
        a->name[MAX_SYS_ACC_NAME_LEN] = 0;
        strncpy(a->pw,   pw,   CRYPT_PW_HASH_SZ);
    } else
        strncpy(a->pw, pw, CRYPT_PW_HASH_SZ);
    return 0;
}

static sys_acc_t *
_get_sys_acc(const char *name)
{
    int num = _sys_accs.num;
    for (int i = 0; i < num; ++i)
        if (streq(_sys_accs.accs[i].name, name))
            return &_sys_accs.accs[i];
    return 0;
}

static int
_read_sys_accs_file()
{
    FILE *f = fopen("sys_accs.txt", "r");
    if (!f)
    {
        puts("No sys_accs.txt found.");
        return 1;
    }

    char    name[128];
    char    pw[386];
    bool32  name_or_pw  = 0;
    int     ret         = 0;
    int     r;
    char    *buf;

    for (;;)
    {
        buf = !name_or_pw ? name : pw;

        r = fscanf(f, "%s", buf);
        if (!r || r == EOF) break;
        if (name_or_pw)
        {
            if (_push_sys_acc(name, pw))
                ret = 2;
            name_or_pw = 0;
        } else
            name_or_pw = 1;
    }

    _sys_accs.read_file = 1;
    safe_fclose(f);
    return ret;
}

static int
_save_sys_accs_file()
{
    if (!_sys_accs.read_file)
        _read_sys_accs_file();

    FILE *f = fopen("sys_accs.txt", "a+");
    if (!f) return 1;

    for (int i = 0; i < _sys_accs.num; ++i)
    {
        fputs(_sys_accs.accs[i].name, f);   fputs("\n", f);
        fputs(_sys_accs.accs[i].pw, f);     fputs("\n", f);
    }

    safe_fclose(f);
    puts("Saved sys_accs.txt.");
    return 0;
}

static client_t *
_create_new_client(socket_t fd)
{
    mutex_lock(&_clients.mtx);
    client_t *c = client_pool_reserve(&_clients.pool);
    if (!c) {mutex_unlock(&_clients.mtx); return 0;}
    uint32 id = _clients.running_id++;
    mutex_unlock(&_clients.mtx);

    c->type     = CL_UNKNOWN;
    c->flags    = CL_FLAG_RESERVED;
    c->id       = id;

    int err;

    if (net_handle_init(&c->net_handle, fd, NETQOP_READ, CL_UNKNOWN, c))
        {err = 1; goto err;}

    cryptchan_clear(&c->cryptchan);

    if (net_queue_add(&_net_queue, &c->net_handle, BBUF_CUR_PTR(&c->msgs_in),
            BBUF_FREE_SPACE(&c->msgs_in)))
        {err = 3; goto err;}

    return c;

    err:
        SET_BITFLAG_OFF(c->flags, CL_FLAG_RESERVED);
        mutex_lock(&_clients.mtx);
        client_pool_free(&_clients.pool, c);
        mutex_unlock(&_clients.mtx);
        printf("Client creation failed, err: %d.", err);
        return 0;
}

static void
_disconnect_and_free_client(client_t *c)
{
    mutex_lock(&_clients.mtx);

    mutex_lock(&c->out_mtx);
    mutex_lock(&c->in_mtx);
    net_handle_close(&c->net_handle);
    client_pool_free(&_clients.pool, c);
    SET_BITFLAG_OFF(c->flags, CL_FLAG_RESERVED);
    mutex_unlock(&c->in_mtx);
    mutex_unlock(&c->out_mtx);

    mutex_unlock(&_clients.mtx);
}

static byte_buf_t *
_begin_send_msg_to_client(client_t *c, int sz)
{
    mutex_lock(&c->out_mtx);
    int tot_sz = DBMSGTSZ + sz;

    if (BBUF_FREE_SPACE(&c->msgs_out) < tot_sz)
    {
        DEBUG_PRINTFF("bbuf full\n");
        if (send_all_from_bbuf(c->net_handle.fd, &c->msgs_out, MUTA_MTU))
            {mutex_unlock(&c->out_mtx); return 0;}
        if (_dynamic_bbuf_ensure_capacity_for_n_more(&c->msgs_out, tot_sz))
            {mutex_unlock(&c->out_mtx); return 0;}
    }

    return &c->msgs_out;
}

static byte_buf_t *
_begin_send_const_encrypted_msg_to_client(client_t *c, int sz)
{
    int tot_sz = CRYPT_MSG_ADDITIONAL_BYTES + sz;
    return _begin_send_msg_to_client(c, tot_sz);
}

static int
_finalize_send_msg_to_client(client_t *c)
{
    int ret = 0;

    if (!(c->flags & CL_FLAG_ADDED_TO_SEND_QUEUE))
    {
        out_msg_note_t note;
        note.c  = c;
        note.id = c->id;

        if (!double_cmd_buf_push(&_clients.out_msg_notes, &note))
            c->flags |= CL_FLAG_ADDED_TO_SEND_QUEUE;
        else
            ret = 1;
    }

    mutex_unlock(&c->out_mtx);
    return ret;
}

static int
_read_unknown_client_packet(client_t *c)
{
    byte_buf_t bb = BBUF_INITIALIZER(c->msgs_in.mem, c->msgs_in.num_bytes);

    int dc          = 0;
    int incomplete  = 0;

    dbmsg_type_t type;

    while (BBUF_FREE_SPACE(&bb) >= DBMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, dbmsg_type_t, &type);

        switch (type)
        {
        case DBMSG_PUB_KEY:
        {
            puts("DBMSG_PUB_KEY");
            dbmsg_pub_key_t s;
            incomplete = dbmsg_pub_key_read(&bb, &s);
            if (!incomplete) dc = _handle_unknown_dbmsg_pub_key(c, &s);
        }
            break;
        case DBMSG_STREAM_HEADER:
        {
            puts("DBMSG_STREAM_HEADER");
            dbmsg_stream_header_t s;
            incomplete = dbmsg_stream_header_read(&bb, &s);
            if (!incomplete)
                dc = _handle_unknown_dbmsg_stream_header(c, &s);
        }
            break;
        case DBMSG_LOGIN:
        {
            puts("DBMSG_LOGIN");
            dbmsg_login_t s;
            incomplete = dbmsg_login_read_var_encrypted(&bb,
                &c->cryptchan, &s);
            if (!incomplete) dc = _handle_unknown_dbmsg_login(c, &s);
        }
            break;
        default:
            dc = 1;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: packet type was %d, dc was %d, incomplete was %d\n",
                __func__, (int)type, dc, incomplete);
            return -1;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static int
_read_master_packet(client_t *c)
{
    byte_buf_t bb = BBUF_INITIALIZER(c->msgs_in.mem, c->msgs_in.num_bytes);

    int dc          = 0;
    int incomplete  = 0;

    dbmsg_type_t type;

    while (BBUF_FREE_SPACE(&bb) >= DBMSGTSZ && !incomplete)
    {
        BBUF_READ(&bb, dbmsg_type_t, &type);

        printf("MASTER PACKET: %d\n", (int)type);

        switch (type)
        {
        case TDBMSG_QUERY_ACCOUNT_EXISTS:
        {
            puts("TDBMSG_QUERY_ACCOUNT_EXISTS");
            tdbmsg_query_account_exists_t s;
            incomplete = tdbmsg_query_account_exists_read_var_encrypted(
                &bb, &c->cryptchan, &s);
            if (!incomplete) dc =
                _handle_master_tdbmsg_query_account_exists(c, &s);
        }
            break;
        case TDBMSG_QUERY_LOGIN_ACCOUNT:
        {
            puts("TDBMSG_QUERY_LOGIN_ACCOUNT");
            tdbmsg_query_login_account_t s;
            incomplete = tdbmsg_query_login_account_read_var_encrypted(&bb,
                &c->cryptchan, &s);
            if (!incomplete) dc =
                _handle_master_tdbmsg_query_login_account(c, &s);
        }
            break;
        case TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID:
        {
            puts("TDBMSG_QUERY_ACCOUNT_FIRST_CHARACTER_ID");
            tdbmsg_query_account_first_character_id_t s;
            incomplete =
                tdbmsg_query_account_first_character_id_read_const_encrypted
                (&bb, &c->cryptchan, &s);
            if (!incomplete) dc =
                _handle_master_tdbmsg_query_account_first_id(c, &s);
        }
            break;
        case TDBMSG_INSERT_CREATE_CHARACTER:
        {
            puts("TDBMSG_INSERT_CREATE_CHARACTER");
            tdbmsg_insert_create_character_t s;
            incomplete = tdbmsg_insert_create_character_read_var_encrypted(
                &bb, &c->cryptchan, &s);
            if (!incomplete) dc =
                _handle_master_tdbmsg_insert_create_character(c, &s);
        }
            break;
        case TDBMSG_UPDATE_CHARACTER_POSITION:
        {
            puts("TDBMSG_UPDATE_CHARACTER_POSITION");
            tdbmsg_update_character_position_t s;
            incomplete =
                tdbmsg_update_character_position_read_const_encrypted(&bb,
                    &c->cryptchan, &s);
            if (!incomplete) dc = _handle_tdbmsg_update_character_position(
                c, &s);
        }
            break;
        case TDBMSG_QUERY_ACCOUNT_CHARACTERS:
        {
            puts("TDBMSG_QUERY_ACCOUNT_CHARACTERS");
            tdbmsg_query_account_characters_t s;
            incomplete =
                tdbmsg_query_account_characters_read_const_encrypted(&bb,
                    &c->cryptchan, &s);
            if (!incomplete) dc = _handle_tdbmsg_query_account_characters(c,
                &s);
        }
            break;
        default:
            dc = 1;
        }

        if (dc || incomplete < 0)
        {
            printf("%s: packet type was %d, dc was %d, incomplete was %d\n",
                __func__, (int)type, dc, incomplete);
            return -1;
        }
    }
    return BBUF_FREE_SPACE(&bb);
}

static void
_send_pending_client_packets()
{
    double_cmd_buf_arr_t *dca = double_cmd_buf_swap(&_clients.out_msg_notes);

    int             num     = dca->num;
    out_msg_note_t  *notes  = dca->mem;
    out_msg_note_t  *n;
    client_t        *c;
    int             res;

    for (int i = 0; i < num; ++i)
    {
        n = &notes[i];
        c = n->c;
        mutex_lock(&c->out_mtx);

        if (c->flags & CL_FLAG_RESERVED && c->id == n->id)
        {
            res = send_all_from_bbuf(c->net_handle.fd, &c->msgs_out, MUTA_MTU);
            if (res) printf("%s: error sending some data.\n", __func__);
            SET_BITFLAG_OFF(c->flags, CL_FLAG_ADDED_TO_SEND_QUEUE);
        }
        mutex_unlock(&c->out_mtx);
    }

    double_cmd_buf_arr_clear(dca);
}

static void
_nq_accept_callback(net_handle_t *h, socket_t fd, addr_t addr, int err)
{
    muta_assert(!err);

    int code;
    printf("ip: %d.%d.%d.%d\n", ADDR_IP_TO_PRINTF_ARGS(&addr));

    if (!_is_ip_sys_ip(addr.ip))
        {code = 1; goto err;}

    client_t *c = _create_new_client(fd);
    if (!c) {code = 2; goto err;}

    printf("Accepted new client from addr %d.%d.%d.%d\n",
        ADDR_IP_TO_PRINTF_ARGS(&addr));

    int r = net_queue_add(&_net_queue, h, 0, 0);
    muta_assert(!r);

    return;

    err:
        net_shutdown_sock(fd, SOCKSD_BOTH);
        printf("%s: refused client, code: %d\n", __func__, code);
}

static void
_nq_read_callback(net_handle_t *h, int num_bytes)
{
    client_t *c = h->user_data.ptr;

    mutex_lock(&c->in_mtx);

    if (!(c->flags & CL_FLAG_RESERVED))
        {mutex_unlock(&c->in_mtx); return;}

    if (num_bytes > 0)
    {
        c->msgs_in.num_bytes += num_bytes;
        int left;

        switch (c->type)
        {
            case CL_UNKNOWN: left = _read_unknown_client_packet(c); break;
            case CL_MASTER:  left = _read_master_packet(c);         break;
        }

        if (left < 0)
        {
            printf("%s: disconnecting client (read_packet() returned < 0).\n",
                __func__);
            _disconnect_and_free_client(c);
        } else
        {
            if (left > 0)
                printf("%s: left: %d\n", __func__, left);
            bbuf_cut_portion(&c->msgs_in, 0, c->msgs_in.num_bytes - left);
            if (net_queue_add(&_net_queue, &c->net_handle,
                BBUF_CUR_PTR(&c->msgs_in), BBUF_FREE_SPACE(&c->msgs_in)))
                _disconnect_and_free_client(c);
        }
    } else
    {
        DEBUG_PRINTFF("client disconnected.\n");
        mutex_unlock(&c->in_mtx);
        _disconnect_and_free_client(c);
        return;
    }

    mutex_unlock(&c->in_mtx);
}

static int
_init_clients(int max)
{
    if (max <= 0)
        return 1;
    _clients.mem = malloc(max * sizeof(client_t));
    if (!_clients.mem)
        return 2;
    if (client_pool_init(&_clients.pool, _clients.mem, max))
        return 2;
    if (double_cmd_buf_init(&_clients.out_msg_notes, 512,
        sizeof(out_msg_note_t)))
        return 3;

    client_t *c;

    for (int i = 0; i < max; ++i)
    {
        c = &_clients.pool.items[i];
        mutex_init(&c->out_mtx);
        mutex_init(&c->in_mtx);
        BBUF_INIT(&c->msgs_in,  c->msgs_in_mem,  CL_MSGS_IN_SZ);
        BBUF_INIT(&c->msgs_out, c->msgs_out_mem, CL_MSGS_OUT_SZ);
    }

    mutex_init(&_clients.mtx);
    return 0;
}

static void
_read_cfg_callback(void *ctx, const char *opt, const char *val)
{
    (void)ctx;
    if (streq(opt, "listen port"))
    {
        int port = atoi(val);
        if (port >= 0 && port <= (uint16)0xFFFF)
            _push_listen_port((uint16)port);
    } else
    if (streq(opt, "system ip"))
    {
        int     ip[4];
        bool32  valid_ip = 0;
        if (sscanf(val, "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]) == 4)
            valid_ip = 1; else
        if (streq(val, "localhost"))
            {ip[0] = 127; ip[1] = 0; ip[2] = 0; ip[3] = 1; valid_ip = 1;}
        if (valid_ip)
            _push_sys_ip(uint32_ip_from_uint8s(ip[0], ip[1], ip[2], ip[3]));
    } else
    if (streq(opt, "tickrate"))
    {
        int tickrate = atoi(val);
        if (tickrate > 0) _config.tickrate = tickrate;
    } else
    if (streq(opt, "mysql socket path"))
        _config.mysql_sock_path = set_dynamic_str(_config.mysql_sock_path, val);
}

static void
_read_config(const char *fp)
{
    _push_sys_ip(uint32_ip_from_uint8s(127, 0, 0, 1));
    _push_listen_port(DEFAULT_DB_PORT);
    _config.tickrate = 30;
    _config.mysql_sock_path = set_dynamic_str(_config.mysql_sock_path, 0);
    if (parse_cfg_file(fp ? fp : "config.cfg", _read_cfg_callback, 0))
        puts("No config file found or file was invalid.");
}

static int
_begin_listening()
{
    _listeners.num = 0;

    socket_t    s;
    int         err = 0;

    for (int i = 0; i < _listen_ports.num; ++i)
    {
        s = net_async_tcp_socket();
        if (s == KSYS_INVALID_SOCKET)               {err = 2; break;}
        if (net_make_sock_reusable(s))              {err = 3; break;}
        if (net_disable_nagle(s))                   {err = 3; break;}
        if (make_socket_non_block(s))               {err = 4; break;}
        if (net_bind(s, _listen_ports.ports[i]))    {err = 5; break;}
        if (net_listen(s, 5))                       {err = 6; break;}
        if (net_handle_init(&_listeners.handles[i], s, NETQOP_LISTEN,
            0, &_listeners.handles[i]))
            {err = 7; break;}
        if (net_queue_add(&_net_queue, &_listeners.handles[i], 0, 0))
            {err = 8; break;}
        _listeners.num++;
    }

    if (err)
    {
        char b[512];
        sock_err_str(b, 512);
        net_shutdown_sock(s, SOCKSD_BOTH);
        for (int i = 0; i < _listeners.num; ++i)
            net_handle_close(&_listeners.handles[i]);
        DEBUG_PRINTF("%s: returning error %d.\n", __func__, err);
    }

    return err;
}

static void
_update_client_timers()
{
    static uint64 last_update = 0;

    uint64 cur_time = get_program_ticks_ms(&cur_time);

    if (cur_time - last_update < CL_CLEANUP_TIME)
        return;

    uint64 kickout_time = CL_CLEANUP_TIME;
    uint64 time_since;

    mutex_lock(&_clients.mtx);

    for (client_t *c = _clients.res; c; c = c->next)
    {
        top:

        time_since = cur_time - c->last_recvd_from;

        if (time_since < kickout_time)
            continue;

        client_t *kc    = c;
        c               = c->next;

        _disconnect_and_free_client(kc);

        goto top; /* Go back to the loop top so that we don't move on to next */
    }

    mutex_unlock(&_clients.mtx);
    last_update = cur_time;
}

static inline int
_handle_unknown_dbmsg_pub_key(client_t *c, dbmsg_pub_key_t *s)
{
    cryptchan_stream_header_t header;

    if (cryptchan_init(&c->cryptchan, 0))
        return 1;

    int r = cryptchan_sv_store_pub_key(&c->cryptchan, s->key, header);

    if (!r)
    {
        puts("Received client's public key!");

        /* Send our own key */
        dbmsg_pub_key_t fwd_a;
        memcpy(fwd_a.key, c->cryptchan.pk, CRYPTCHAN_PUB_KEY_SZ);

        byte_buf_t *bb;

        /* Send public key to client */
        bb = _begin_send_msg_to_client(c, DBMSG_PUB_KEY_SZ);
        if (!bb) return 2;
        dbmsg_pub_key_write(bb, &fwd_a);
        _finalize_send_msg_to_client(c);

        dbmsg_stream_header_t fwd_b;
        memcpy(fwd_b.header, header, CRYPTCHAN_STREAM_HEADER_SZ);

        /* Send stream header to client */
        bb = _begin_send_msg_to_client(c, DBMSG_STREAM_HEADER_SZ);
        if (!bb) return 2;
        dbmsg_stream_header_write(bb, &fwd_b);
        _finalize_send_msg_to_client(c);

        return 0;
    }

    printf("cryptchan_sv_store_pub_key() returned %d\n", r);
    return 3;
}

static inline int
_handle_unknown_dbmsg_stream_header(client_t *c, dbmsg_stream_header_t *s)
{
    puts("Received client's stream header!");
    if (cryptchan_store_stream_header(&c->cryptchan, s->header))    return 1;
    if (!cryptchan_is_encrypted(&c->cryptchan))                     return 2;
    puts("Connection with client is now encrypted. Waiting for login msg...");
    return 0;
}

static inline int
_handle_unknown_dbmsg_login(client_t *c, dbmsg_login_t *s)
{
    int name_len    = s->name_len;
    int pw_len      = s->pw_len;

    /*if (s->client_type >= NUM_SYS_LOGIN_RESULTS)    return 1;*/
    if (name_len > MAX_SYS_ACC_NAME_LEN)            return 2;
    if (pw_len > 255)                               return 3;

    char name[MAX_SYS_ACC_NAME_LEN + 1];
    char pw[256];

    memcpy(name, s->name, MAX(name_len, MAX_SYS_ACC_NAME_LEN));
    memcpy(pw, s->pw, MAX(pw_len, 255));
    name[MIN(name_len, MAX_SYS_ACC_NAME_LEN)] = 0;
    pw[MIN(pw_len, 255)] = 0;

    sys_acc_t *a = _get_sys_acc(name);
    if (!a) {printf("Login: system account %s not found.\n", name); return 4;}

    bool32 pw_res = crypt_test_stored_pw_hash(pw, pw_len, a->pw);
    if (!pw_res) {puts("Client provided incorrect password"); return 5;}

    printf("System user %s successfully logged in!\n", name);
    c->type = !s->client_type ? CL_MASTER : CL_WORLDD;

    byte_buf_t *bb = _begin_send_const_encrypted_msg_to_client(c,
        DBMSG_LOGIN_RESULT_SZ);

    if (bb)
    {
        dbmsg_login_result_t fwd;
        fwd.code = SYS_LOGIN_OK;
        dbmsg_login_result_write_const_encrypted(bb, &c->cryptchan, &fwd);
        _finalize_send_msg_to_client(c);
    } else
        return 5;

    printf("Successfully logged in system user %s.\n", name);
    return 0;
}

static inline int
_handle_master_tdbmsg_query_account_exists(client_t *c,
    tdbmsg_query_account_exists_t *s)
{
    if (s->name_len > MAX_ACC_NAME_LEN) return 1;

    char name[MAX_ACC_NAME_LEN + 1];
    memcpy(name, s->name, s->name_len);
    name[s->name_len] = 0;

    printf("Received TDBMSG_QUERY_ACCOUNT_EXISTS for account %s.\n", name);

    if (db_account_exists(name))
        puts("Account exists");
    else
        puts("Account does not exist!");

    return 0;
}

static inline int
_handle_master_tdbmsg_query_login_account(client_t *c,
    tdbmsg_query_login_account_t *s)
{
    if (s->name_len > MAX_ACC_NAME_LEN) return 1;
    if (s->pw_len > MAX_PW_LEN)         return 2;

    char name[MAX_ACC_NAME_LEN + 1];
    char pw[MAX_PW_LEN + 1];
    memcpy(name, s->name, s->name_len);
    name[s->name_len] = 0;
    memcpy(pw, s->pw, s->pw_len);
    pw[s->pw_len] = 0;

    int r = db_login_account(name, pw, s->pw_len);

    fdbmsg_reply_login_account_t fwd;
    fwd.query_id = s->query_id;

    switch (r)
    {
        case DB_ERR_OK:
            fwd.result = ACCUT_ACC_LOGIN_OK;
            break;
        case DB_ERR_DISCONNECTED:
            fwd.result = ACCUT_ACC_LOGIN_DB_DOWN;
            break;
        case DB_ERR_ACC_NO_EXISTS:
        {
            if (s->create_if_not_exists)
            {
                int res = db_create_account(name, pw, s->pw_len);
                if (!res && !db_login_account(name, pw, s->pw_len))
                    fwd.result = 0;
                else
                    fwd.result = ACCUT_ACC_LOGIN_WRONG_INFO;
            } else
                fwd.result = ACCUT_ACC_LOGIN_WRONG_INFO;
        }
            break;
        case DB_ERR_ACC_LOGIN_WRONG_PW:
            puts("Wrong pw!");
            fwd.result = ACCUT_ACC_LOGIN_WRONG_INFO;
            break;
        case DB_ERR_ACC_LOGIN_ALREADY_IN:
            puts("Already in!");
            fwd.result = ACCUT_ACC_LOGIN_ALREADY_IN;
            break;
        default:
            muta_assert(0);
    }

    printf("Login result for %s: %d (r: %d)\n", name, fwd.result, r);

    uint64 acc_id;
    int id_res = db_get_account_id(name, &acc_id);

    if (!id_res)
        fwd.account_id = acc_id;
    else
        fwd.result = ACCUT_ACC_LOGIN_DB_BUSY;

    bbuf_t *bb = _begin_send_const_encrypted_msg_to_client(c,
        FDBMSG_REPLY_LOGIN_ACCOUNT_SZ);
    if (!bb) return 3;
    int wr = fdbmsg_reply_login_account_write_const_encrypted(bb,
        &c->cryptchan, &fwd);
    muta_assert(!wr);
    _finalize_send_msg_to_client(c);
    return 0;
}

static int
_handle_master_tdbmsg_query_account_first_id(client_t *c,
    tdbmsg_query_account_first_character_id_t *s)
{
    uint64 ids[MAX_CHARACTERS_PER_ACC];
    int num = db_get_character_ids_of_account(s->account_id, ids);

    fdbmsg_reply_account_first_character_id_t fwd;
    fwd.query_id = s->query_id;

    if (num > 0)
    {
        fwd.character_id    = ids[0];
        fwd.error           = 0;
    } else
        fwd.error           = DBSV_ERR_NOT_FOUND;

    bbuf_t *bb = _begin_send_const_encrypted_msg_to_client(c,
        FDBMSG_REPLY_ACCOUNT_FIRST_CHARACTER_ID_SZ);
    if (!bb) return 1;
    fdbmsg_reply_account_first_character_id_write_const_encrypted(bb,
        &c->cryptchan, &fwd);
    _finalize_send_msg_to_client(c);
    return 0;
}

static int
_handle_master_tdbmsg_insert_create_character(client_t *c,
    tdbmsg_insert_create_character_t *s)
{
    if (!accut_check_character_name(s->name, s->name_len))
        return 1;

    db_character_props_t cp;
    cp.account_id   = s->acc_id;
    memcpy(cp.name, s->name, s->name_len);
    cp.name[s->name_len] = 0;
    cp.race         = s->race;
    cp.sex          = s->sex;
    cp.map_id       = s->map_id;
    cp.instance_id  = s->instance_id;
    cp.position.x   = s->x;
    cp.position.y   = s->y;
    cp.position.z   = s->z;

    fdbmsg_reply_create_character_t fwd = {0};
    fwd.query_id = s->query_id;

    int r = db_create_character(&cp);

    if (r)
    {
        if (r == DB_ERR_ENTRY_NOT_UNIQUE)
            fwd.error =  CREATE_CHARACTER_FAIL_EXISTS;
        else
            fwd.error = CREATE_CHARACTER_FAIL_UNKNOWN;
    } else
    {
        fwd.error       = 0;
        fwd.name        = cp.name;
        fwd.name_len    = s->name_len;
        fwd.race        = cp.race;
        fwd.sex         = cp.sex;
        fwd.map_id      = cp.map_id;
        fwd.instance_id = cp.instance_id;
        fwd.x           = cp.position.x;
        fwd.y           = cp.position.y;
        fwd.z           = cp.position.z;
    }

    bbuf_t *bb = _begin_send_msg_to_client(c, FDBMSG_REPLY_CREATE_CHARACTER_SZ);
    if (!bb)
        return 2;
    r = fdbmsg_reply_create_character_write(bb, &fwd);
    muta_assert(!r);
    _finalize_send_msg_to_client(c);
    return 0;
}

static inline int
_handle_tdbmsg_update_character_position(client_t *c,
    tdbmsg_update_character_position_t *s)
{
    if (db_update_char_pos(s->character_id, 0, s->x, s->y, s->z))
        return 0;
    fdbmsg_confirm_set_character_position_t fwd;
    fwd.query_id = s->query_id;
    bbuf_t *bb = _begin_send_msg_to_client(c,
        FDBMSG_CONFIRM_SET_CHARACTER_POSITION_SZ);
    if (!bb) return 1;
    fdbmsg_confirm_set_character_position_write(bb, &fwd);
    _finalize_send_msg_to_client(c);
    return 0;
}

static inline int
_handle_tdbmsg_query_account_characters(client_t *c,
    tdbmsg_query_account_characters_t *s)
{
    if (s->max > MAX_CHARACTERS_PER_ACC)
        return 1;

    MYSQL_RES *res;

    int r;
    if ((r = db_query_account_characters(s->account_id, s->max, &res)))
    {
        DEBUG_PRINTFF("db_query_account_characters() returned non-zero (%d).\n", r);
        return 0;
    }

    fdbmsg_reply_account_characters_t f;
    f.query_id = s->query_id;

    char    names[MAX_CHARACTERS_PER_ACC * (MAX_CHARACTER_NAME_LEN + 1)];
    uint64  ids[MAX_CHARACTERS_PER_ACC];
    uint8   races[MAX_CHARACTERS_PER_ACC];
    uint8   sexes[MAX_CHARACTERS_PER_ACC];
    uint32  map_ids[MAX_CHARACTERS_PER_ACC];
    uint32  instance_ids[MAX_CHARACTERS_PER_ACC];
    uint8   name_indices[MAX_CHARACTERS_PER_ACC];
    int32   xs[MAX_CHARACTERS_PER_ACC];
    int32   ys[MAX_CHARACTERS_PER_ACC];
    int8    zs[MAX_CHARACTERS_PER_ACC];

    f.names         = names;
    f.ids           = ids;
    f.races         = races;
    f.sexes         = sexes;
    f.map_ids       = map_ids;
    f.instance_ids  = instance_ids;
    f.name_indices  = name_indices;
    f.xs            = xs;
    f.ys            = ys;
    f.zs            = zs;
    f.names_len     = 0;

    char *name = names;

    uint32 i = 0;
    for (MYSQL_ROW row = mysql_fetch_row(res); row; row = mysql_fetch_row(res))
    {
        if (i == MAX_CHARACTERS_PER_ACC)
        {
            mysql_free_result(res);
            return 2;
        }

        uint32 tot_len = (uint32)strlen(row[0]) + 1;
        memcpy(name, row[0], tot_len);
        name_indices[i] = f.names_len;
        name        += tot_len;
        f.names_len += tot_len;
        ids[i]          = str_to_uint64(row[1]);
        races[i]        = atoi(row[2]);
        sexes[i]        = atoi(row[3]);
        map_ids[i]      = str_to_uint32(row[4]);
        instance_ids[i] = str_to_uint32(row[5]);
        xs[i]           = atoi(row[6]);
        ys[i]           = atoi(row[7]);
        zs[i]           = atoi(row[8]);

        i++;
    }

    f.ids_len           = i;
    f.races_len         = i;
    f.sexes_len         = i;
    f.map_ids_len       = i;
    f.instance_ids_len  = i;
    f.xs_len            = i;
    f.ys_len            = i;
    f.zs_len            = i;
    f.name_indices_len  = i;

    mysql_free_result(res);

    int msg_sz = (int)(FDBMSG_REPLY_ACCOUNT_CHARACTERS_COMPUTE_SZ(f.names_len, f.ids_len,
            f.races_len, f.sexes_len, f.map_ids_len, f.instance_ids_len,
            f.name_indices_len, f.xs_len, f.ys_len, f.zs_len));
    DEBUG_PRINTFF("msg_sz: %d\n", msg_sz);
    bbuf_t *bb = _begin_send_msg_to_client(c, msg_sz);
    DEBUG_PRINTF("free space: %d\n", BBUF_FREE_SPACE(bb));
    if (!bb)
        return 2;
    fdbmsg_reply_account_characters_write(bb, &f);
    _finalize_send_msg_to_client(c);
    return 0;
}
