#include "event.h"
#include "common.h"
#include "clients.h"
#include "db.h"
#include "shards.h"
#include "../../shared/common_utils.h"
#include "../../shared/sv_time.h"

#define MAX_EVENTS 256

static bool32   _running = 1;
static event_t  _events[MAX_EVENTS];


int main(int argc, char **argv)
{
    LOG("MUTA Login Server");
    int ret = 0;
    LOG("Initializing...");
    if (com_init())
        {ret = 1; goto fail;}
    if (cl_init())
        {ret = 2; goto fail;}
    if (db_init())
        {ret = 3; goto fail;}
    if (shards_init())
        {ret = 4; goto fail;}
    if (db_start())
        {ret = 5; goto fail;}
    if (shards_start())
        {ret = 6; goto fail;}
    if (cl_start())
        {ret = 7; goto fail;}
    LOG("Done initializing.");
    while (_running)
    {
        int num_events = event_wait(com_event_buf, _events, MAX_EVENTS, 500);
        com_current_tick = get_program_ticks_ms();
        for (int i = 0; i < num_events; ++i)
        {
            event_t *e = &_events[i];
            switch (e->type)
            {
            case EVENT_ACCEPT_CLIENT:
                cl_accept(&e->accept_client);
                break;
            case EVENT_READ_CLIENT:
                cl_read(&e->read_client);
                break;
            case EVENT_ACCOUNT_LOGIN_QUERY_FINISHED:
                cl_finish_login_attempt(&e->account_login_query_finished);
                break;
            case EVENT_ACCEPT_SHARD:
                shards_accept(&e->accept_shard);
                break;
            case EVENT_READ_SHARD:
                shards_read(&e->read_shard);
                break;
            default:
                muta_assert(0);
            }
        }
        cl_check_timeouts();
        shards_flush();
    }
    cl_stop();
    db_stop();
    return ret;
    fail:
        LOGF("Error %d!", ret);
        sleep_ms(2000);
        return ret;
}
