#ifndef MUTA_LOGIN_CLIENTS_H
#define MUTA_LOGIN_CLIENTS_H

/* Client polling and accepting runs on it's own thread. Events will be pushed
 * to the main event queue, and they are handled on the main thread using the
 * event handlers declared here. */

/* TODO: add client timeout timers */

#include "../../shared/types.h"

/* Forward declaration(s) */
typedef struct accept_client_event_t        accept_client_event_t;
typedef struct read_client_event_t          read_client_event_t;
typedef struct account_login_query_finished_event_t
    account_login_query_finished_event_t;

int
cl_init();

void
cl_destroy();

int
cl_start();

void
cl_stop();

void
cl_on_shard_status_changed(uint32 shard, bool32 online);

int
cl_finish_select_shard_attempt(uint64 account_id, uint32 login_session_id,
    int result);
/* The return value is 0 on success (if the client didn't disconnect meanwile).
 * If it's non-zero, the shard should be told to disconnect the client. */

void
cl_check_timeouts();

/*-- Event handlers --*/

void
cl_accept(accept_client_event_t *event);

void
cl_read(read_client_event_t *event);

void
cl_finish_login_attempt(account_login_query_finished_event_t *event);

#endif
