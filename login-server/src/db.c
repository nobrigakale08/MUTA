#ifdef _WIN32
    #include <my_global.h>
    #include <mysql.h>
    #include <errmsg.h>
#else
    #include <mariadb/mysql.h>
    #include <mariadb/errmsg.h>
#endif
#include <inttypes.h>
#include "db.h"
#include "common.h"
#include "event.h"
#include "../../shared/common_utils.h"
#include "../../shared/crypt.h"

typedef struct db_event_t               db_event_t;
typedef struct db_account_login_query_event_t db_account_login_query_event_t;

enum db_event
{
    DB_EVENT_ACCOUNT_LOGIN_QUERY
};

struct db_account_login_query_event_t
{
    int     type;
    uint32  login_session_id;
    uint32  name_hash;
    char    name[MAX_ACC_NAME_LEN + 1];
    char    password[MAX_PW_LEN];
    int     password_len;
};

struct db_event_t
{
    union
    {
        int                             type;
        db_account_login_query_event_t  account_login_query;
    };
};

static MYSQL        _mysql;
static thread_t     _thread;
static int          _running;
static event_buf_t  _event_buf;

static thread_ret_t
_main(void *args);

static void
_handle_events(db_event_t *events, int num_events);

static void
_handle_account_login_query(db_account_login_query_event_t *event);

static int
_mysql_query(MYSQL *db, const char *str, ...);

static int
_account_exists(const char *name);
/* Return values: 1 if exists, 0 if does not exist, and < 0 on error. */

static int
_create_account(const char *name, const char *password, int password_len);
/* This function does not check if the account already exists */

int
db_init()
{
    int ret = 0;
    if (!mysql_init(&_mysql))
        {ret = 1; goto fail;}
    my_bool yes = 1;
    mysql_options(&_mysql, MYSQL_OPT_RECONNECT, &yes);
    thread_init(&_thread);
    event_init(&_event_buf, sizeof(db_event_t), 128);
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        db_destroy();
        return ret;
}

void
db_destroy()
{
}

int
db_start()
{
    if (!mysql_real_connect(&_mysql, "127.0.0.1", "muta", "muta",
            "muta_accounts", 0, 0, CLIENT_MULTI_STATEMENTS))
    {
        LOGF("mysql_real_connect() failed: %s", mysql_error(&_mysql));
        return 1;
    }
    LOG("Successfully connected to database.");
    _running = 1;
    if (thread_create(&_thread, _main, 0))
        return 2;
    return 0;
}

void
db_stop()
{
    _running = 0;
    thread_join(&_thread);
}

int
db_query_account_login(uint32 login_session_id, const char *name, int name_len,
    uint32 name_hash, const char *password, int password_len)
{
    muta_assert(name_len <= MAX_ACC_NAME_LEN);
    muta_assert(password_len <= MAX_PW_LEN);
    db_event_t event;
    event.type = DB_EVENT_ACCOUNT_LOGIN_QUERY;
    memcpy(event.account_login_query.name, name, name_len);
    event.account_login_query.name[name_len] = 0;
    memcpy(event.account_login_query.password, password, password_len);
    event.account_login_query.password_len      = password_len;
    event.account_login_query.login_session_id  = login_session_id;
    event.account_login_query.name_hash         = name_hash;
    event_push(&_event_buf, &event, 1);
    return 0;
}

static thread_ret_t
_main(void *args)
{
    db_event_t events[16];
    while (_running)
    {
        int num_events = event_wait(&_event_buf, events, 16, -1);
        _handle_events(events, num_events);
    }
    return 0;
}

static void
_handle_events(db_event_t *events, int num_events)
{
    db_event_t *e;
    for (int i = 0; i < num_events; ++i)
    {
        e = &events[i];
        switch (e->type)
        {
        case DB_EVENT_ACCOUNT_LOGIN_QUERY:
            _handle_account_login_query(&e->account_login_query);
            break;
        }
    }
}

static void
_handle_account_login_query(db_account_login_query_event_t *event)
{
    MYSQL_RES   *res = 0;
    event_t     new_event;
    account_login_query_finished_event_t *new_event_data =
        &new_event.account_login_query_finished;
    new_event.type                      = EVENT_ACCOUNT_LOGIN_QUERY_FINISHED;
    new_event_data->login_session_id    = event->login_session_id;
    new_event_data->name_hash           = event->name_hash;
    DEBUG_LOGF("NEW EVENT %u, OLD EVENT ID: %u",
        new_event_data->login_session_id, event->login_session_id);
    int account_exists = _account_exists(event->name);
    if (account_exists < 0)
        {new_event_data->result = 1; goto out;}
    if (account_exists == 0 && _create_account(event->name, event->password,
        event->password_len))
        {new_event_data->result = 2; goto out;}
    const char *query = "SELECT id, name, pw FROM accounts WHERE name = '%s'";
    if (_mysql_query(&_mysql, query, event->name))
        {new_event_data->result = 3; goto out;}
    res = mysql_store_result(&_mysql);
    if (!res)
        {new_event_data->result = 4; goto out;}
    MYSQL_ROW row = mysql_fetch_row(res);
    for (; row; row = mysql_fetch_row(res))
    {
        if (!streq(row[1], event->name))
            continue;
        if (!crypt_test_stored_pw_hash(event->password, event->password_len,
            row[2]))
            {new_event_data->result = 5; goto out;}
        break;
    }
    if (!row)
        {new_event_data->result = 6; goto out;}
    uint64 account_id = str_to_uint64(row[0]);
    query = "UPDATE accounts SET last_login = CURRENT_TIMESTAMP WHERE id = "
        "'%" PRIu64 "'";
    if (_mysql_query(&_mysql, query, account_id))
        LOGF("Warning: failed to store account login time.");
    new_event_data->account_id  = account_id;
    new_event_data->result      = 0;
    out:
        if (res)
            mysql_free_result(res),
        event_push(com_event_buf, &new_event, 1);
}

static int
_mysql_query(MYSQL *db, const char *str, ...)
{
    int         len;
    char        buf[512];
    const int   buf_sz      = 512;
    char        *ustr       = buf;
    char        *heap_alloc = 0;
    va_list     args;
    va_start(args, str);
    len = vsnprintf(ustr, buf_sz - 1, str, args);
    va_end(args);
    if (len > buf_sz)
    {
        int     alloc_sz = len + 4;
        va_list nargs;
        heap_alloc  = emalloc(alloc_sz);
        ustr        = heap_alloc;
        va_start(nargs, str);
        vsnprintf(ustr, alloc_sz, str, nargs);
        va_end(nargs);
    }
    int ret = mysql_query(db, ustr);
    if (ret)
        DEBUG_LOGF("MySQL error: %s (%d).", mysql_error(&_mysql), ret);
    if (heap_alloc)
        free(heap_alloc);
    return ret;
}

static int
_account_exists(const char *name)
{
    const char  *q = "SELECT EXISTS(SELECT * FROM accounts WHERE name = '%s')";
    MYSQL_RES   *res    = 0;
    int         ret     = 0;
    if (_mysql_query(&_mysql, q, name))
        {ret = -1; goto out;}
    res = mysql_store_result(&_mysql);
    if (!res)
        {ret = -2; goto out;}
    MYSQL_ROW row = mysql_fetch_row(res);
    if (!row)
        {ret = -3; goto out;}
    ret = streq(row[0], "1");
    out:
        if (res)
            mysql_free_result(res);
        const char *rs = ret < 0 ? "error" : (ret > 0 ? "true" : "false");
        LOG("Account %s exist query result: %s.", name, rs);
        return ret;
}

static int
_create_account(const char *name, const char *password, int password_len)
{
    char password_hash[CRYPT_PW_HASH_SZ];
    if (crypt_storable_pw_hash(password, password_len, password_hash,
        CRYPT_PW_STRENGTH_MEDIUM))
    {
        LOGF("Failed to hash password for account %s.", name);
        return 1;
    }
    const char *q = "INSERT INTO accounts (name, pw) VALUES('%s', '%s')";
    if (_mysql_query(&_mysql, q, name, password_hash))
    {
        LOGF("Query failed.");
        return 2;
    }
    return 0;
}
