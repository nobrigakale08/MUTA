#ifndef MUTA_LOGIN_EVENT_H
#define MUTA_LOGIN_EVENT_H

#include "../../shared/net.h"

typedef struct accept_client_event_t        accept_client_event_t;
typedef struct read_client_event_t          read_client_event_t;
typedef struct account_login_query_finished_event_t
    account_login_query_finished_event_t;
typedef struct accept_shard_event_t         accept_shard_event_t;
typedef struct read_shard_event_t           read_shard_event_t;
typedef struct event_t                      event_t;

enum event_type
{
    EVENT_ACCEPT_CLIENT,
    EVENT_READ_CLIENT,
    EVENT_ACCOUNT_LOGIN_QUERY_FINISHED,
    EVENT_ACCEPT_SHARD,
    EVENT_READ_SHARD
};

struct accept_client_event_t
{
    int         type;
    socket_t    socket;
    addr_t      address;
};

struct read_client_event_t
{
    int     type;
    uint32  client_index;
    int     num_bytes;
    void    *memory;
};

struct account_login_query_finished_event_t
{
    int     type;
    int     result; /* 0 if ok */
    uint32  login_session_id;
    uint32  name_hash;
    uint64  account_id;
};

struct accept_shard_event_t
{
    int         type;
    uint32      shard;
    socket_t    socket;
};

struct read_shard_event_t
{
    int     type;
    uint32  shard;
    int     num_bytes;
    void    *memory;
};

struct event_t
{
    union
    {
        int                                     type;
        accept_client_event_t                   accept_client;
        read_client_event_t                     read_client;
        account_login_query_finished_event_t    account_login_query_finished;
        accept_shard_event_t                    accept_shard;
        read_shard_event_t                      read_shard;
    };
};

#endif
