#!/bin/sh
debug=$1
if [ "$debug" = "" ]; then
    cd proxy/rundir && urxvt -e sh -c ./muta_proxy &
    cd server/rundir && urxvt -e sh -c ./muta_server &
    cd login-server/rundir && urxvt -e sh -c ./muta_login_server &
    cd db-server/build && urxvt -e sh -c ./muta_db &
    cd worldd/rundir && urxvt -e sh -c ./muta_worldd &
elif [ "$debug" = "-g" ] || [ "$debug" = "--gdb" ]; then
    cd proxy/rundir && urxvt -e sh -c "gdb -ex run ./muta_proxy" &
    cd server/rundir && urxvt -e sh -c "gdb -ex run ./muta_server" &
    cd login-server/rundir && urxvt -e sh -c "gdb -ex run ./muta_login_server" &
    cd db-server/build && urxvt -e sh -c "gdb -ex run ./muta_db" &
    cd worldd/rundir && urxvt -e sh -c "gdb -ex run ./muta_worldd" &
fi
