git lfs install --skip-smudge
git clone https://gitlab.com/Partanen/MUTA-Assets
git clone https://gitlab.com/Partanen/MUTA-Data
git lfs install --force
cd MUTA-Assets  && git config credential.helper manager && git lfs pull
cd ..\MUTA-Data && git config credential.helper manager && git lfs pull
cd ..
