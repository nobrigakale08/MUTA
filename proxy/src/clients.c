#include "clients.h"
#include "common.h"
#include "event.h"
#include "shard.h"
#include "../../shared/common_utils.h"
#include "../../shared/netpoll.h"
#include "../../shared/net.h"
#include "../../shared/common_defs.h"
#include "../../shared/crypt.h"
#include "../../shared/packets.h"
#include "../../shared/acc_utils.h"

#define CL_IN_BUF_SZ    MUTA_MTU
#define CL_OUT_BUF_SZ   MUTA_MTU

typedef struct client_t         client_t;
typedef struct client_timer_t   client_timer_t;

enum client_flag
{
    CL_FLAG_IN_USE      = (1 << 0),
    CL_FLAG_WILL_FLUSH  = (1 << 1),
    CL_FLAG_AUTHED      = (1 << 2)
};

struct client_t
{
    socket_t    socket;
    uint32      id;
    uint32      flags;
    uint32      shard;
    uint32      flush_index;
    uint32      client_info;
    struct
    {
        int     num_bytes;
        uint8   memory[CL_IN_BUF_SZ];
    } in_buf;
    struct
    {
        int     num_bytes;
        uint8   memory[CL_OUT_BUF_SZ];
    } out_buf;
    cryptchan_t cryptchan;
};

struct client_timer_t
{
    uint32 client;
    uint64 timeout;
};

static fixed_pool(client_t) _clients;
static uint32               *_flush_clients;
static uint32               _num_flush_clients;
static uint32               _running_client_id;
static thread_t             _thread;
static netpoll_t            _netpoll;
static socket_t             _listen_socket;
static bool32               _running;
static segpool_t            _segpool;
static mutex_t              _segpool_mutex;

static struct
{
    client_timer_t  *all;
    uint32          first;
    uint32          num;
} _timers;

static thread_ret_t
_main(void *args);

static void
_allocate_event_messages(event_t *events, int num_events);

static void
_check_netpoll_events(netpoll_event_t *events, int num_events);

static void
_disconnect_client_internal(client_t *cl, bool32 del_from_netpoll);

static void
_disconnect_client(client_t *cl, bool32 del_from_netpoll);

static int
_read_unencrypted_client(client_t *cl);

static int
_read_crypted_client_packet(client_t *cl);

static int
_read_authed_client_packet(client_t *cl);

static int
_check_client_packet(client_t *cl); /* 0 on success */
/* Returns > 0 if packet is incomplete, 0 if packet was ok and < 0 if client
 * should be disconnected. */

static int
_handle_clmsg_auth_proof(client_t *cl, clmsg_auth_proof_t *s);

static bbuf_t
_send_msg_to_client(client_t *cl, int num_bytes);

int
cl_init()
{
    int ret = 0;
    fixed_pool_init(&_clients, com_config.max_clients);
    if (thread_init(&_thread))
        {ret = 2; goto fail;}
    if (netpoll_init(&_netpoll))
        {ret = 3; goto fail;}
    segpool_init(&_segpool);
    mutex_init(&_segpool_mutex);
    _flush_clients  = emalloc(com_config.max_clients * sizeof(uint32));
    _timers.all     = emalloc(com_config.max_clients * sizeof(client_timer_t));
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
cl_destroy()
{
}

int
cl_start()
{
    int ret = 0;
    _running = 1;
    netpoll_event_t netpoll_event;
    netpoll_event.events    = NETPOLL_READ;
    netpoll_event.data.u64  = 0xFFFFFFFFFFFFFFFF;
    _listen_socket = net_tcp_ipv4_listen_sock(com_config.client_port, 5);
    if (_listen_socket == KSYS_INVALID_SOCKET)
        {ret = 1; goto fail;}
    if (netpoll_add(&_netpoll, _listen_socket, &netpoll_event))
        {ret = 2; goto fail;}
    if (thread_create(&_thread, _main, 0))
        {ret = 3; goto fail;}
    return ret;
    fail:
        LOGF("Failed with code %d.", ret);
        return ret;
}

void
cl_flush()
{
    int         num_to_flush = _num_flush_clients;
    client_t    *cl;
    for (int i = 0; i < num_to_flush; ++i)
    {
        cl = &_clients.all[_flush_clients[i]];
        muta_assert(cl->out_buf.num_bytes);
        if (net_send_all(cl->socket, cl->out_buf.memory, cl->out_buf.num_bytes)
            <= 0)
        {
            _disconnect_client(cl, 1);
            i--;
            continue;
        }
        cl->out_buf.num_bytes = 0;
        cl->flags &= ~CL_FLAG_WILL_FLUSH;
    }
    _num_flush_clients = 0;
}

void
cl_accept(accept_client_event_t *event)
{
    if (!shard_check_client_ip(event->address.ip))
    {
        LOG("Client accepted IP check failed (ip %u), disconnecting.",
            event->address.ip);
        goto fail;
    }
    client_t *cl = fixed_pool_new(&_clients);
    if (!cl)
    {
        LOG("Cannot accept client: no client slots free.");
        goto fail;
    }
    muta_assert(!(cl->flags & CL_FLAG_IN_USE));
    cl->socket              = event->socket;
    cl->id                  = _running_client_id++;
    cl->shard               = 0xFFFFFFFF;
    cl->in_buf.num_bytes    = 0;
    cl->out_buf.num_bytes   = 0;
    cl->flags               = 0;
    uint32          data[2] = {fixed_pool_index(&_clients, cl), cl->id};
    netpoll_event_t netpoll_event;
    netpoll_event.events = NETPOLL_READ;
    cryptchan_clear(&cl->cryptchan);
    memcpy(&netpoll_event.data.u64, data, 8);
    if (netpoll_add(&_netpoll, event->socket, &netpoll_event))
    {
        LOG("Failed to add client socket to netpoll.");
        fixed_pool_free(&_clients, cl);
        goto fail;
    }
    cl->flags |= CL_FLAG_IN_USE;
    DEBUG_LOGF("Created new client.");
    return;
    fail:
        LOG("Failed to accept client.");
        close_socket(event->socket);
}

void
cl_read(read_client_event_t *event)
{
    client_t *cl = &_clients.all[event->client_index];
    if (!(cl->flags & CL_FLAG_IN_USE) || event->client_id != cl->id)
    {
        DEBUG_LOG("Attempted to read client no longer in use!");
        if (event->num_bytes > 0)
            goto free_memory;
        return;
    }
    if (event->num_bytes <= 0)
    {
        _disconnect_client(cl, 0);
        return;
    }
    int num_copied = 0;
    while (num_copied < event->num_bytes)
    {
        int num_to_copy = event->num_bytes - num_copied;
        muta_assert(num_to_copy > 0);
        memcpy(cl->in_buf.memory + cl->in_buf.num_bytes,
            event->memory + num_copied, num_to_copy);
        cl->in_buf.num_bytes += num_to_copy;
        if (_check_client_packet(cl) < 0)
        {
            _disconnect_client(cl, 1);
            break;
        }
        num_copied += num_to_copy;
    }
    free_memory:
        mutex_lock(&_segpool_mutex);
        segpool_free(&_segpool, event->memory);
        mutex_unlock(&_segpool_mutex);
}

void
cl_disconnect(uint32 client_index)
{
    client_t *cl = &_clients.all[client_index];
    muta_assert(cl->flags & CL_FLAG_IN_USE);
    if (cl->flags & CL_FLAG_AUTHED)
        shard_delete_client_info(cl->shard, cl->client_info, 0);
    _disconnect_client_internal(cl, 1);
}

void
cl_forward_shard_packet(uint32 client_index, const void *memory, int num_bytes)
{
    DEBUG_LOG("Forwarding %d bytes.", num_bytes);
    client_t *cl = &_clients.all[client_index];
    muta_assert(cl->flags & CL_FLAG_IN_USE);
    muta_assert(cl->flags & CL_FLAG_AUTHED);
    bbuf_t bb = _send_msg_to_client(cl, num_bytes - MSGTSZ);
    if (!bb.max_bytes)
    {
        LOG("Failed to forward proxy packet to client.");
        _disconnect_client(cl, 1);
        return;
    }
    BBUF_WRITE_BYTES(&bb, memory, num_bytes);
}

static thread_ret_t
_main(void *args)
{
    netpoll_event_t events[64];
    while (_running)
    {
        int num_events = netpoll_wait(&_netpoll, events, 64, 5000);
        _check_netpoll_events(events, num_events);
    }
    return 0;
}

static void
_allocate_event_messages(event_t *events, int num_events)
{
    mutex_lock(&_segpool_mutex);
    for (int i = 0; i < num_events; ++i)
    {
        if (events[i].type != EVENT_READ_CLIENT ||
            events[i].read_client.num_bytes <= 0)
            continue;
        void *memory = segpool_malloc(&_segpool,
            events[i].read_client.num_bytes);
        memcpy(memory, events[i].read_client.memory,
            events[i].read_client.num_bytes);
        events[i].read_client.memory = memory;
    }
    mutex_unlock(&_segpool_mutex);
}

static void
_check_netpoll_events(netpoll_event_t *events, int num_events)
{
    netpoll_event_t *e;
    event_t         new_events[8];
    int             num_new_events  = 0;
    int             buf_offset      = 0;
    uint8           buf[8 * MUTA_MTU];
    for (int i = 0; i < num_events; ++i)
    {
        e = &events[i];
        if (!(e->events & (NETPOLL_READ | NETPOLL_HUP)))
            continue;
        if (e->data.u64 == 0xFFFFFFFFFFFFFFFF)
        {
            addr_t      a;
            socket_t    s = net_accept(_listen_socket, &a);
            if (s == KSYS_INVALID_SOCKET)
            {
                DEBUG_LOGF("Accepted a client, but socket was invalid.");
                continue;
            }
            accept_client_event_t *ne =
                &new_events[num_new_events++].accept_client;
            ne->type    = EVENT_ACCEPT_CLIENT;
            ne->socket  = s;
            ne->address = a;
            DEBUG_LOGF("Accepted a client.");
        } else
        {
            uint32 data[2];
            memcpy(data, &e->data.u64, 8);
            client_t *cl = &_clients.all[data[0]];
            read_client_event_t *ne = &new_events[num_new_events++].read_client;
            ne->type            = EVENT_READ_CLIENT;
            ne->client_index    = data[0];
            ne->client_id       = data[1];
            ne->num_bytes = net_recv(cl->socket, buf + buf_offset, MUTA_MTU);
            ne->memory          = buf + buf_offset;
            if (ne->num_bytes > 0)
                buf_offset += ne->num_bytes;
            else
                netpoll_del(&_netpoll, cl->socket);
            DEBUG_LOG("Received %u bytes from client", ne->num_bytes);
        }
        if (num_new_events < 8)
            continue;
        _allocate_event_messages(new_events, num_new_events);
        event_push(com_event_buf, new_events, num_new_events);
        num_new_events  = 0;
        buf_offset      = 0;
    }
    if (!num_new_events)
        return;
    _allocate_event_messages(new_events, num_new_events);
    event_push(com_event_buf, new_events, num_new_events);
}

static void
_disconnect_client_internal(client_t *cl, bool32 del_from_netpoll)
{
    if (del_from_netpoll)
        netpoll_del(&_netpoll, cl->socket);
    net_shutdown_sock(cl->socket, SOCKSD_BOTH);
    cl->flags &= ~CL_FLAG_IN_USE;
    muta_assert(_clients.num_free < _clients.max);
    if (cl->flags & CL_FLAG_WILL_FLUSH)
        _flush_clients[cl->flush_index] = _flush_clients[--_num_flush_clients];
    fixed_pool_free(&_clients, cl);
    LOG("Disconnected a client.");
}

static void
_disconnect_client(client_t *cl, bool32 del_from_netpoll)
{
    muta_assert(cl->flags & CL_FLAG_IN_USE);
    if (cl->flags & CL_FLAG_AUTHED)
        shard_delete_client_info(cl->shard, cl->client_info, 1);
    _disconnect_client_internal(cl, del_from_netpoll);
}

static int
_read_unencrypted_client(client_t *cl)
{
    DEBUG_LOGF("Checking %d bytes.", cl->in_buf.num_bytes);
    bbuf_t bb = BBUF_INITIALIZER(cl->in_buf.memory, cl->in_buf.num_bytes);
    int         incomplete  = 0;
    int         dc          = 0;
    msg_type_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !dc && !incomplete &&
        !cryptchan_is_encrypted(&cl->cryptchan))
    {
        BBUF_READ(&bb, msg_type_t, &msg_type);
        switch (msg_type)
        {
        case CLMSG_PUB_KEY:
        {
            DEBUG_LOG("CLMSG_PUB_KEY");
            clmsg_pub_key_t s;
            incomplete = clmsg_pub_key_read(&bb, &s);
            if (incomplete)
                break;
            if (cryptchan_is_initialized(&cl->cryptchan))
                {dc = 1; break;}
            svmsg_pub_key_t         k_msg;
            svmsg_stream_header_t   h_msg;
            if (cryptchan_init(&cl->cryptchan, k_msg.key))
                {dc = 2; break;}
            if (cryptchan_sv_store_pub_key(&cl->cryptchan, s.key, h_msg.header))
                {dc = 3; break;}
            bbuf_t wbb = _send_msg_to_client(cl, SVMSG_PUB_KEY_SZ);
            if (!wbb.max_bytes)
                {dc = 4; break;}
            int r = svmsg_pub_key_write(&wbb, &k_msg);
            muta_assert(!r);
            wbb = _send_msg_to_client(cl, SVMSG_STREAM_HEADER_SZ);
            if (!wbb.max_bytes)
                {dc = 5; break;}
            r = svmsg_stream_header_write(&wbb, &h_msg);
            muta_assert(!r);
        }
            break;
        case CLMSG_STREAM_HEADER:
        {
            DEBUG_LOG("CLMSG_STREAM_HEADER");
            clmsg_stream_header_t s;
            incomplete = clmsg_stream_header_read(&bb, &s);
            if (incomplete)
                break;
            if (cryptchan_store_stream_header(&cl->cryptchan, s.header))
                {dc = 1; break;}
        }
            break;
        case CLMSG_KEEP_ALIVE:
            break;
        default:
            dc = 1;
        }
    }
    if (dc || incomplete < 0)
    {
        DEBUG_LOGF("msg_type: %u, dc: %d, incomplete: %d.", msg_type, dc,
            incomplete);
        return -1;
    }
    if (incomplete)
        bb.num_bytes -= MSGTSZ;
    int free_space = BBUF_FREE_SPACE(&bb);
    memmove(cl->in_buf.memory,
        cl->in_buf.memory + cl->in_buf.num_bytes - free_space, free_space);
    cl->in_buf.num_bytes = free_space;
    return 0;
}

static int
_read_crypted_client_packet(client_t *cl)
{
    uint8 buf[CL_IN_BUF_SZ];
    memcpy(buf, cl->in_buf.memory, cl->in_buf.num_bytes);
    bbuf_t bb = BBUF_INITIALIZER(buf, cl->in_buf.num_bytes);
    int         incomplete  = 0;
    int         dc          = 0;
    bool32      authed      = 0;
    msg_type_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !dc && !incomplete && !authed)
    {
        BBUF_READ(&bb, msg_type_t, &msg_type);
        switch (msg_type)
        {
        case CLMSG_AUTH_PROOF:
        {
            DEBUG_PUTS("CLMSG_AUTH_PROOF");
            clmsg_auth_proof_t s;
            incomplete = clmsg_auth_proof_read_var_encrypted(&bb,
                &cl->cryptchan, &s);
            if (incomplete)
                break;
            dc = _handle_clmsg_auth_proof(cl, &s);
            if (!dc)
                authed = 1;
        }
            break;
        default:
            dc = 1;
        }
    }
    if (dc || incomplete < 0)
    {
        DEBUG_LOGF("msg_type: %u, dc: %d, incomplete: %d.", msg_type, dc,
            incomplete);
        return -1;
    }
    int free_space = BBUF_FREE_SPACE(&bb);
    memmove(cl->in_buf.memory,
        cl->in_buf.memory + cl->in_buf.num_bytes - free_space, free_space);
    cl->in_buf.num_bytes = free_space;
    return 0;
}

static int
_read_authed_client_packet(client_t *cl)
{
    uint8 buf[CL_IN_BUF_SZ];
    memcpy(buf, cl->in_buf.memory, cl->in_buf.num_bytes);
    bbuf_t      bb          = BBUF_INITIALIZER(buf, cl->in_buf.num_bytes);
    int         incomplete  = 0;
    int         dc          = 0;
    msg_type_t  msg_type;
    while (BBUF_FREE_SPACE(&bb) >= MSGTSZ && !dc && !incomplete)
    {
        BBUF_READ(&bb, msg_type_t, &msg_type);
        switch (msg_type)
        {
        case CLMSG_CHAT_MSG:
        {
            clmsg_chat_msg_t s;
            incomplete = clmsg_chat_msg_read(&bb, &s);
        }
            break;
        case CLMSG_PLAYER_MOVE:
        {
            clmsg_player_move_t s;
            incomplete = clmsg_player_move_read(&bb, &s);
        }
            break;
        case CLMSG_FIND_PATH:
        {
            clmsg_find_path_t s;
            incomplete = clmsg_find_path_read(&bb, &s);
        }
            break;
        case CLMSG_LAST_RECEIVED_POS:
        {
            clmsg_last_received_pos_t s;
            incomplete = clmsg_last_received_pos_read(&bb, &s);
        }
            break;
        case CLMSG_PLAYER_EMOTE:
        {
            clmsg_player_emote_t s;
            incomplete = clmsg_player_emote_read(&bb, &s);
        }
            break;
        case CLMSG_CREATE_CHARACTER:
        {
            clmsg_create_character_t s;
            incomplete = clmsg_create_character_read(&bb, &s);
        }
            break;
        case CLMSG_LOG_IN_CHARACTER:
        {
            clmsg_log_in_character_t s;
            incomplete = clmsg_log_in_character_read(&bb, &s);
        }
            break;
        case CLMSG_KEEP_ALIVE:
            break;
        default:
            dc = 1;
        }
    }
    if (dc || incomplete < 0)
    {
        DEBUG_LOGF("msg_type: %u, dc: %d, incomplete: %d.", msg_type, dc,
            incomplete);
        return -1;
    }
    shard_forward_client_packet(cl->shard, cl->id, cl->in_buf.memory,
        cl->in_buf.num_bytes);
    int free_space = BBUF_FREE_SPACE(&bb);
    memmove(cl->in_buf.memory,
        cl->in_buf.memory + cl->in_buf.num_bytes - free_space, free_space);
    cl->in_buf.num_bytes = free_space;
    return 0;
}

static int
_check_client_packet(client_t *cl)
{
    if (!(cl->flags & CL_FLAG_AUTHED))
    {
        if (!cryptchan_is_encrypted(&cl->cryptchan))
            return _read_unencrypted_client(cl);
        int r;
        if (!(r = _read_crypted_client_packet(cl)) && cl->in_buf.num_bytes)
            return _read_authed_client_packet(cl);
        else
            return r;
    } else
        return _read_authed_client_packet(cl);
}

static int
_handle_clmsg_auth_proof(client_t *cl, clmsg_auth_proof_t *s)
{
    muta_assert(s->account_name_len <= MAX_ACC_NAME_LEN);
    muta_assert(s->shard_name_len <= MAX_SHARD_NAME_LEN);
    if (cl->flags & CL_FLAG_AUTHED)
        return 1;
    char account_name[MAX_ACC_NAME_LEN + 1];
    memcpy(account_name, s->account_name, s->account_name_len);
    account_name[s->account_name_len] = 0;
    if (!accut_is_name_legaln(account_name, s->account_name_len))
        return 2;
    char shard_name[MAX_SHARD_NAME_LEN + 1];
    memcpy(shard_name, s->shard_name, s->shard_name_len);
    shard_name[s->shard_name_len] = 0;
    uint32 client_info;
    uint32 shard;
    int r;
    if ((r = shard_check_client_auth_proof(shard_name, account_name, s->token,
        &cl->cryptchan, fixed_pool_index(&_clients, cl), &shard, &client_info)))
    {
        LOG("Client attempted to connect with invalid client info (error %d).",
            r);
        return 4;
    }
    DEBUG_LOG("SHARD SET TO %u", shard);
    cl->client_info = client_info;
    cl->shard       = shard;
    cl->flags |= CL_FLAG_AUTHED;
    LOG("Authed client based on apparently valid auth proof.");
    return 0;
}

static bbuf_t
_send_msg_to_client(client_t *cl, int num_bytes)
{
    bbuf_t ret = {0};
    int req_bytes = MSGTSZ + num_bytes;
    if (cl->out_buf.num_bytes + req_bytes > CL_OUT_BUF_SZ)
    {
        if (net_send_all(cl->socket, cl->out_buf.memory, cl->out_buf.num_bytes)
            <= 0)
            return ret;
        cl->out_buf.num_bytes = 0;
    }
    BBUF_INIT(&ret, cl->out_buf.memory + cl->out_buf.num_bytes, req_bytes);
    cl->out_buf.num_bytes += req_bytes;
    if (cl->flags & CL_FLAG_WILL_FLUSH)
        return ret;
    muta_assert(_num_flush_clients < com_config.max_clients);
    uint32 flush_index = _num_flush_clients;
    cl->flush_index = flush_index;
    cl->flags |= CL_FLAG_WILL_FLUSH;
    _flush_clients[flush_index] = fixed_pool_index(&_clients, cl);
    _num_flush_clients = flush_index + 1;
    return ret;
}
